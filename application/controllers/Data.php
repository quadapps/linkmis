<?php defined('BASEPATH') or die('Direct script access denied!');

class Data extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function get_chart_data()
	{
	   $year = date('Y');
		$data = get_data('tbl_agenttxns', "WHERE at_currency = 'SSP' AND YEAR(at_date) = $year GROUP BY MONTH(at_date)", "SUM(at_amt) AS total, MONTHNAME(at_date) AS month");
		foreach($data as $d) {
			$cats[] = $d['month'];
			$series[] = $d['total'];
		}
		$ssp = ['cats' => $cats, 'series' => $series];

		$data = get_data('tbl_agenttxns', "WHERE at_currency = 'USD' AND YEAR(at_date) = $year GROUP BY MONTH(at_date)", "SUM(at_amt) AS total, MONTHNAME(at_date) AS month");
		foreach($data as $d) {
			$cats1[] = $d['month'];
			$series1[] = $d['total'];
		}
		$usd = ['cats' => $cats1, 'series' => $series1];


		header("content-type: application/json"); 
		$jsonString=json_encode(['ssp' => $ssp, 'usd' => $usd], JSON_NUMERIC_CHECK);
		
		echo $jsonString;
	}

	public function get_txns($uid=false)
	{
		$ret = [];
		foreach(get_data('tbl_agenttxns', "WHERE at_creator = $uid AND at_active = 1") as $d) :
			$ret[] = [
				$d['at_txn'],
				$d['at_date'],
				$d['at_code'],
				$d['at_line'],
				$d['at_currency'].$d['at_amt'],
				$d['at_type'],
				'<button class="btn btn-danger btn-sm delete-this" data-id="'.$d['at_id'].'" data-tbl="agenttxns!" onclick="reverse_txn('.$d['at_id'].')"><i class="fa fa-trash-o"></i> </button>'
			];
		endforeach;

		echo json_encode(['data' => $ret]);
	}

	public function get_cashouts($type, $uid)
	{
		$ret = [];
		foreach(get_data('tbl_cashouts', "JOIN tbl_agents ON agent_empid = co_agentname WHERE co_type = $type AND co_active = 1 AND co_agentname = $uid") as $d) :

			if($d['co_status'] == 0) $status = '<span class="status-icon bg-warning"></span> Pending';

			if($d['co_status'] == 1) $status = '<span class="status-icon bg-primary"></span> In Transit';
													 
			if($d['co_status'] == 2) $status = '<span class="status-icon bg-success"></span> Processed';
			
			if($d['co_status'] == -1) $status = '<span class="status-icon bg-danger"></span> Declined';

			$ret[] = [
				$d['co_createdate'],
				$d['agent_code'],
				$d['agent_name'],
				$d['co_currency'].$d['co_amt'],
				get_fullname($d['co_creator']),
				$status,
				$d['co_status'] == 1 && $d['co_collector'] == 0 ? '<a href="javascript:process_co('.$d['co_id'].', 2)" class="btn btn-green btn-sm accept" data-id="'.$d['co_id'].'" data-status="2"><i class="fa fa-thumbs-up"></i> Accept</a>
															<a href="javascript:process_co('.$d['co_id'].', -1)" class="btn btn-danger btn-sm reject" data-id="'.$d['co_id'].'" data-status="-1"><i class="fa fa-thumbs-down"></i> Decline</a>' : ''
			];
		endforeach;

		echo json_encode(['data' => $ret]);
	}

	public function get_collections($uid, $des)
	{
		$ret = [];
		$data = get_data('tbl_collections', "JOIN tbl_agents ON collection_agent = agent_empid WHERE collection_creator = $uid AND collection_active = 1");
		foreach($data as $d) :
			$status = '';
			if($d['collection_status'] == 0) $status = '<span class="status-icon bg-primary"></span> Pending';
			if($d['collection_status'] == 1) $status = '<span class="status-icon bg-success"></span> Collected';
			if($d['collection_status'] == 2) $status = '<span class="status-icon bg-success"></span> Received';
			if($d['collection_status'] == -1) $status = '<span class="status-icon bg-danger"></span> Declined';

			$html = $d['collection_status'] == 0 && strtolower($des) == 'cash collector' ? '<a href="javascript:void(0)" class="btn btn-green btn-sm accept" data-currency="'.$d['collection_currency'].'" data-amt="'.$d['collection_amt'].'" data-id="'.$d['collection_id'].'" data-status="1"><i class="fa fa-thumbs-up"></i> Accept</a>
															<a href="javascript:void(0)" class="btn btn-danger btn-sm reject" data-id="'.$d['colection_id'].'" data-status="-1"><i class="fa fa-thumbs-down"></i> Decline</a>' : '';

			$ret[] = [
				$d['collection_createdate'],
				$d['agent_code'],
				$d['agent_name'],
				$d['collection_currency'].$d['collection_amt'],
				$d['collection_type'],
				$status,
				$html
			];
		endforeach;

		echo json_encode(['data' => $ret]);
	}

	public function get_recons($currency, $uid=false)
	{
		
		$and = '';
		if($uid) $and = " AND ar_empid = $uid ";
		$data = get_data('tbl_agentrecons', "JOIN tbl_employees ON emp_id = ar_empid WHERE ar_currency = '$currency' $and ", 'tbl_agentrecons.*, emp_fullname');

		$ret = [];
		foreach($data as $d) {
			$uid = $d['ar_empid'];
			$r = $d['ar_date'];
			$cb = $op = get_bal($r, $uid, $currency);
			$bal = $d;
			$ob = get_data('tbl_agentrecons', "WHERE ar_date < '$r' AND ar_currency = '$currency' AND ar_empid = $uid ORDER BY ar_date DESC LIMIT 1", '*', true);

			$ret[] = [
				$r,
				$d['emp_fullname'],
				isset($ob['ar_cb']) ? $ob['ar_cb'] : 0,
				$f = $cb['float']-$cb['fo'],
				$cb['deposits'],
				$cb['withdrawals'],
				isset($bal['ar_fb']) ? $bal['ar_fb'] : 0,
				$cb['co'],
				$cb['collections'],
				isset($bal['ar_cb']) ? $bal['ar_cb'] : 0
			];
		}

		echo json_encode(['data' => $ret]);
	}
}