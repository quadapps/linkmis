<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
       
    public function send_payslip($emp_id, $range) {

    	if (!empty($emp_id)) {

    		$this->load->model('hr_model', 'hrm');	

    		try {

    			$_date = explode('ziz', urldecode($range));
            	$_m = trim($_date[0]);
            	$_y = trim($_date[1]);
            	$_date_string = $_y.'-'.$_m.'-20';

	    		$payslip = $this->hrm->get_payslip($emp_id, $range);
	    		$message = "Find your payslip below, <br><br> $payslip";
	    		$to = 'francmut@gmail.com';//get_that_data('tbl_employees', 'emp_workemail', 'emp_id', $emp_id);
	    		$subject = 'PaySlip for '. date('M',strtotime($_date_string)) . ' ' . date('Y',strtotime($_date_string));

	    		//echo $payslip; exit;

	    		send_test_mail($to, $subject, $message, $file = '');

	    		$res = array(
	    			'status' => 1,
	    			'message' => 'PaySlip sent successfully'
	    		);

	    	} catch(Exception $e) {

	    		$res = array(
	    			'status' => 0,
	    			'message' => $e->getMessage()
	    		);

	    	}

    	}

    	echo json_encode($res);

    }

	public function daily_activity() {

		// filter date
		$date = '2015-10-14'; //date('Y-m-d');

		// graphic department

		// graphic designers
		$qgd = $this->db->query("select emp_id, user_id, emp_fullname from tbl_employees JOIN tbl_users ON user_empid = emp_id WHERE emp_deptid = 6 and emp_designation like '%GRAPHIC DESIGNER%' and emp_active = 1");
		$rgd = $qgd->result_array();

		$data = array();

		foreach ($rgd as $rg) {

			// create job
			$cr = $this->db->query("select * from tbl_jobs where job_creator = $rg[user_id] and date(job_createdate) = '$date' ");
			$crj = $cr->result_array();
			var_dump($crj);

			// upload artwork

			// update remarks

		}

		var_dump($data);

		// finance and it department

		// senior accountant -
		$saqy = $this->db->query("select emp_id, emp_fullname, emp_designation, user_id from tbl_employees join tbl_users on user_empid = emp_id where emp_designation like '%senior accountant%' and emp_active = 1");
		$sars = $saqy->result_array();
		$sauid = $sars[0]['user_id'];
		$saeid = $sars[0]['emp_id'];
		$safqn = $sars[0]['emp_fullname'];
		$sadsg = $sars[0]['emp_designation'];

		//

		// accountant -
		$aqy = $this->db->query("select emp_id, emp_fullname, emp_designation, user_id from tbl_employees join tbl_users on user_empid = emp_id where emp_designation like '%accountant%' and emp_active = 1");
		$ars = $aqy->result_array();
		$auid = $ars[0]['user_id'];
		$aeid = $ars[0]['emp_id'];
		$afqn = $ars[0]['emp_fullname'];
		$adsg = $ars[0]['emp_designation'];

		// cashier - monica
		$cqry = $this->db->query("select emp_id, emp_fullname, emp_designation, user_id from tbl_employees join tbl_users on user_empid = emp_id where emp_designation like '%cashier%' and emp_active = 1");
		$cemp = $cqry->result_array();
		$cemuid = $cemp[0]['user_id'];
		$cempid = $cemp[0]['emp_id'];
		$cempnm = $cemp[0]['emp_fullname'];
		$cedisig = $cemp[0]['emp_designation'];

		// requisition payments made
		$crpm = $this->db->query("SELECT requestpmt_amt, requestpmt_type, requestpmt_createdate, cashrequest_currency, cashrequest_amt, cashrequest_purpose FROM tbl_requestpmts JOIN tbl_cashrequests ON cashrequest_id = requestpmt_requestid WHERE DATE(requestpmt_createdate) = '$date' AND requestpmt_creator = $cemuid");
		$cres = $crpm->result_array();

		$out = '<p>Requisition Payments</p>'.$date.'<hr>';
		$out .= '<table><thead><tr><th>Requisition</th><th>Requisition Amount</th><th>Amount Paid</th><th>Payment Type</th><th>Payment Date</th></tr></thead>';
		$out .= '<tbody>';
		foreach($cres as $req) {
			$out .= "<tr><td>$req[cashrequest_purpose]</td><td>$req[cashrequest_amt] $req[cashrequest_currency]</td><td>$req[requestpmt_amt]  $req[cashrequest_currency]</td><td>$req[requestpmt_type]</td><td>$req[requestpmt_createdate]</td></tr>";
		}
		$out .= '</tbody></table>';

		echo $out;

		// withdrawals
		$crpm2 = $this->db->query("SELECT * FROM tbl_withdrawals WHERE DATE(withdrawal_createdate) = $date AND withdrawal_creator = $cemuid");
		$cres2 = $crpm2->result_array();

		$out = '<p>Withdrawals</p>'.$date.'<hr>';
		$out .= '<table><thead><tr><th>Id</th><th>Amount</th><th>Mode</th><th>Date</th></tr></thead>';
		$out .= '<tbody>';
		foreach($cres2 as $wit) {
			$out .= "<tr><td>$wit[withdrawal_id]</td><td>$wit[withdrawal_amt] $wit[withdrawal_currency]</td><td>$wit[withdrawal_mode]</td><td>$wit[withdrawal_date]</td></tr>";
		}
		$out .= '</tbody></table>';

		echo $out;

		// deposits
		$crpm3 = $this->db->query("SELECT * FROM tbl_deposits WHERE DATE(deposit_createdate) = '$date' AND deposit_creator = $cemuid");
		$cres3 = $crpm3->result_array();

		$out = '<p>Deposits</p>'.$date.'<hr>';
		$out = '<table><thead><tr><th>Id</th><th>Amount</th><th>Mode</th><th>Date</th></tr></thead>';
		$out .= '<tbody>';
		foreach($cres3 as $dep) {
			$out .= "<tr><td>$dep[deposit_id]</td><td>$dep[deposit_amt] $dep[deposit_currency]</td><td>$dep[deposit_mode]</td><td>$dep[deposit_date]</td></tr>";
		}
		$out .= '</tbody></table>';

		echo $out;

	}

	public function send_emails()
	{
		$data = get_data('tbl_mailqueue', "WHERE sent = 0");
		foreach ($data as $d) {
			$to = json_decode($d['emails']);
			if (json_last_error() === JSON_ERROR_NONE) {
			    // JSON is valid
			} else {
				$to = $d['emails'];
			}
			if(send_email($to, $d['subject'], $d['msg'], $d['file'], $d['name'], $d['url'], $d['url_title']))
				$this->db->update('tbl_mailqueue', ['sent' => 1], ['id' => $d['id']]);
			
		}
	}

}
?>