<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
/**
 * System
 * 
 * @package CRMS
 * @author Alfred Korir
 * @copyright 2014
 * @version $Id$
 * @access public
 */

require APPPATH.'third_party/epicollect/vendor/autoload.php';
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class Epicollect extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$form = 'ea34e84e51494a5d83b107a5accd2235_5fc10a87a83c6';
        $data = $this->epic->get_form_data($form);
        echo json_encode(['data' => $data]);
	}

	public function sync_agents()
	{
		$this->load->library('epic', ['id' => 2113, 'secret' => 'vysax9JA04nkqLsCNzbZTRt7c2TBpcKMblTHb8gn']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = 'ea34e84e51494a5d83b107a5accd2235_5fc10a87a83c6';
        $branch = 'ea34e84e51494a5d83b107a5accd2235_5fc10a87a83c6_5fc10b0763589';
        // $data = $this->epic->get_form_with_branch($form, $branch, $start, $end);
        $data = $this->epic->get_form_data('linkmis-agent', $form, $start, $end);
        echo $json = json_encode($data); exit;

        if(!empty($data)) {
        	$ret = [];
        	foreach($data as $d) {
        		$rows = $this->db->get_where('tbl_recons', ['ec5_uuid' => $d->ec5_uuid])->num_rows();
        		if($rows == 0) {
        			// if($d->COD > 0) {
        				// $branch = $this->epic->get_branch_data('linkmis-agent', $branch, $d->ec5_uuid, $start, $end);

	        			$ret[] = [
	        				'Agent_Code' => $d->Agent_Code,
	        				'ec5_uuid'=> $d->ec5_uuid,
	        				'uploaded_at' => $d->uploaded_at,
	        				'created_at' => $d->created_at,
	        				'SOD_Location' => $d->SOD_Location->latitude.','.$d->SOD_Location->longitude,
	        				'Opening_Float__USD' => $d->Opening_Float__USD,
	        				'Opening_Float__SSP' => $d->Opening_Float__SSP,
	        				'Opening_Cash__USD' => $d->Opening_Cash__USD,
	        				'Opening_Cash__SSP' => $d->Opening_Cash__SSP,
	        				'Float_In_SSP' => $d->Float_In_SSP,
	        				'Float_In_USD' => $d->Float_In_USD,
	        				'Float_Out_SSP' => $d->Float_Out_SSP,
	        				'Float_Out_USD' => $d->Float_Out_USD,
	        				'created_by' => $d->created_by
	        			];
	        		// }
        		}
        	}

        	$this->db->insert_batch('tbl_recons', $ret);
        	$this->get_agent_branches();
        }
	}

	public function get_agent_branches()
	{
		$this->load->library('epic', ['id' => 2113, 'secret' => 'vysax9JA04nkqLsCNzbZTRt7c2TBpcKMblTHb8gn']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = 'ea34e84e51494a5d83b107a5accd2235_5fc10a87a83c6';
        $branch = 'ea34e84e51494a5d83b107a5accd2235_5fc10a87a83c6_5fc10b0763589';

		$data = $this->epic->get_branch_data('linkmis-agent', $branch);
		echo(json_encode($data));

		if(!empty($data)) {
        	$ret = [];
        	foreach($data as $d) {
        		$rows = $this->db->get_where('tbl_reconbranches', ['recon_ec5_uuid' => $d->ec5_branch_owner_uuid])->num_rows();
        		if($rows == 0) {
	        			$ret[] = [
	        				'Float_Balance_SSP' => $d->Float_Balance_SSP,
	        				'Float_Balance_USD' => $d->Float_Balance_USD,
	        				'recon_ec5_uuid'=> $d->ec5_branch_owner_uuid,
	        				'uploaded_at' => $d->uploaded_at,
	        				'created_at' => $d->created_at,
	        				'COD_Location' => $d->COD_Location->latitude.','.$d->COD_Location->longitude,
	        				'Cash_In_SSP' => $d->Cash_In_SSP,
	        				'Cash_In_USD' => $d->Cash_In_USD,
	        				'Debit_SSP' => $d->Debit_SSP,
	        				'Debit_USD' => $d->Debit_USD,
	        				'Credit_SSP' => $d->Credit_SSP,
	        				'Credit_USD' => $d->Credit_USD,
	        				'Cash_Given_SSP' => $d->Cash_Given_SSP,
	        				'Cash_given_USD' => $d->Cash_given_USD,
	        				'created_by' => $d->created_by,
	        				'Cash_Received_SSP' => $d->Cash_Received_SSP,
	        				'Cash_Received_USD' => $d->Cash_Received_USD,
	        				'Cash_in_hand_SSP' => $d->Cash_in_hand_SSP,
	        				'Cash_in_hand_USD' => $d->Cash_in_hand_USD
	        			];
	        		
        		}
        	}

        	$this->db->insert_batch('tbl_reconbranches', $ret);
        }
	}

	public function sync_collections()
	{
		$this->load->library('epic', ['id' => 2250, 'secret' => 'UoqObYNIYpgtN49SdUNWWBvKeqwAwROf8jCvCD1u']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = '254cdce0c5284ca89d3c6fca4a8d4958_6022415b13984';
        $branch = 'ea34e84e51494a5d83b107a5accd2235_5fc10a87a83c6_5fc10b0763589';
        // $data = $this->epic->get_form_with_branch($form, $branch, $start, $end);
        $data = $this->epic->get_form_data('linkmis-cash-collector
',$form, $start, $end);
        $json = json_encode($data);
        // echo $json; exit;
        if(!empty($data)) {
        	$data = json_decode($json, true);
        	$ret = [];
        	foreach($data as $d) {
        		$rows = $this->db->get_where('tbl_collections', ['collection_epi' => $d['ec5_uuid']])->num_rows();
        		if($rows == 0) {
        			$ret = [
	        			'collection_epi' => $d['ec5_uuid'],
	        			'collection_email' => $d['created_by'],
	        			'collection_agent' => $d['Agent_Code'],
	        			'collection_agentname' => $d['Agent_Name'][0],
	        			'collection_agentline' => $d['Agent_Line'],
	        			'collection_date' => $d['created_at'],
	        			'collection_sspamt' => $d['SSP_Collected'],
	        			'collection_sspamt' => $d['SSP_Collected'],
	        			'collection_sspgiven' => $d['SSP_Issued'],
	        			'collection_usdamt' => $d['USD_Collected'],
	        			'collection_usdgiven' => $d['USD_Issued'],
	        			'collection_location' => $d['Location'],
	        			'collection_latlon' => $d['GPS']['latitude'].','.$d['GPS']['longitude']
	        		];

	        		$this->db->insert('tbl_collections', $ret);
        		}
        	}

        	
        }

        echo count($data);
	}

	public function get_form($type)
	{
		switch ($type) {
			case 'tdr':
				return 'ea34e84e51494a5d83b107a5accd2235_5fc10a87a83c6';
				break;
			
			default:
				# code...
				break;
		}
	}

	public function sync_cm()
	{
		$this->load->library('epic', ['id' => 2259, 'secret' => 'auRd1nTHoHeGudmwr1Dvyex931vstKWqc3HEXOzo']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = '679e61b3133744f3a2c3e52621ec5c51_602240c5f067d';
        $branch = '679e61b3133744f3a2c3e52621ec5c51_602240c5f067d_60267b3bb6012';
        // $data = $this->epic->get_form_with_branch($form, $branch, $start, $end);
        $data = $this->epic->get_form_data('linkmis-cluster-manager',$form, $start, $end);
        $json = json_encode($data);
        //echo $json; exit();

        	$ret = [];
        if(!empty($data)) {
        	$data = json_decode($json, true);
        	foreach($data as $d) {
        		$ret = [
	        			
	        			'tdr_email' => $d['created_by'],
	        			'tdr_bas' => $d['No_of_BAs'],
	        			'tdr_regs' => $d['Registrations_Targ'],
	        			'tdr_acts' => $d['Activations_Target'],
	        			'tdr_recs' => $d['Recruitments_Target'],
	        			'tdr_sodtime' => $d['created_at'],
	        			'tdr_sodloc' => $d['SOD_Location']['latitude'].','.$d['SOD_Location']['longitude']
	        		];

        		$rows = $this->db->get_where('tbl_tdractivities', ['tdr_epi' => $d['ec5_uuid']])->num_rows();

        		if($rows == 0) {
        			$ret['tdr_epi'] = $d['ec5_uuid'];
	        		$this->db->insert('tbl_tdractivities', $ret);
	        		
        		} else {
        			$this->db->update('tbl_tdractivities', $ret, ['tdr_epi' => $d['ec5_uuid']]);
        			
        		}

        		if($d['Close_of_Day'] == 1) 
	        			$this->update_cm($this->epic, $d['ec5_uuid'], $d['created_at']);
        	}

        	
        }

        return count($ret);
	}

	public function update_cm($epic, $id, $sod)
	{
		// $this->load->library('epic', ['id' => 2259, 'secret' => 'auRd1nTHoHeGudmwr1Dvyex931vstKWqc3HEXOzo']);
		// $start = date('Y-01-01'); $end = date('Y-m-d');
		// if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		// if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		// $form = '679e61b3133744f3a2c3e52621ec5c51_602240c5f067d';
        $branch = '679e61b3133744f3a2c3e52621ec5c51_602240c5f067d_60267b3bb6012';

		$data = $epic->get_branch_data('linkmis-cluster-manager', $branch, $id);
		$json = json_encode($data);
		//echo $json; exit;

		if(!empty($data)) {
        	$data = json_decode($json, true);
        	$att = [];
        	foreach($data as $d) {
        		$ret = [
	        			'tdr_regsach' => $d['Achieved_Registratio'],
	        			'tdr_recsach' => $d['Achieved_Recruitment'],
	        			'tdr_actsach' => $d['Achieved_Activations'],
	        			'tdr_basach' => $d['No_of_BAs_active'],
	        			'tdr_activations' => $d['Activated_Agents'],
	        			'tdr_codtime' => $d['created_at'],
	        			'tdr_codloc' => $d['COD_Location']['latitude'].','.$d['COD_Location']['longitude']
	        		];

	        	$this->db->update('tbl_tdractivities', $ret, ['tdr_epi' => $id]);

	        	$att[] = [
	        			'empcheckin_empid' => get_that_data('tbl_employees', 'emp_id', 'emp_gmail', $d['created_by']),
	        			'empcheckin_date' => date('Y-m-d', strtotime($d['created_at'])),
	        			'empcheckin_checkouttime' => $d['created_at'],
	        			'empcheckin_checkintime' => $sod
	        		];

        	}

        	$this->load->model('hr_model');
	        $this->hr_model->save_attendance($att);
        }
	}

	public function sync_tdr()
	{
		$this->load->library('epic', ['id' => 2321, 'secret' => '6zRwb9CugebwkNM6UiY7cGDDdztp614NuS8FjXEq']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = '1e1ab0ea17d44d8fae751a76bb2607f4_602240c5f067d';
        // $branch = '679e61b3133744f3a2c3e52621ec5c51_602240c5f067d_60267b3bb6012';
        // $data = $this->epic->get_form_with_branch($form, $branch, $start, $end);
        $data = $this->epic->get_form_data('linkmistdr-activities',$form, $start, $end);
        $json = json_encode($data);
        // echo $json; exit();
        $ret = [];
        if(!empty($data)) {
        	$data = json_decode($json, true);
        	
        	foreach($data as $d) {
        		$ret = [
	        			'tdr_email' => $d['created_by'],
	        			'tdr_bas' => $d['No_of_BAs'],
	        			'tdr_regs' => $d['Registrations_Targ'],
	        			'tdr_acts' => $d['Activations_Target'],
	        			'tdr_recs' => $d['Recruitments_Target'],
	        			'tdr_sodtime' => $d['created_at'],
	        			'tdr_sodloc' => $d['SOD_Location']['latitude'].','.$d['SOD_Location']['longitude']
	        		];

        		$rows = $this->db->get_where('tbl_tdractivities', ['tdr_epi' => $d['ec5_uuid']])->num_rows();

        		if($rows == 0) {
        			$ret['tdr_epi'] = $d['ec5_uuid'];
	        		$this->db->insert('tbl_tdractivities', $ret);
	        		
        		} else {
        			$this->db->update('tbl_tdractivities', $ret, ['tdr_epi' => $d['ec5_uuid']]);
        			
        		}

        		if($d['Close_of_Day'] == 1) 
	        			$this->update_tdr($this->epic, $d['ec5_uuid'], $d['created_at']);
        	}

        	
        }
        
        $cm = $this->sync_cm();
        echo count($ret) + $cm;
	}

	public function update_tdr($epic, $id, $sod)
	{
		// $this->load->library('epic', ['id' => 2259, 'secret' => 'auRd1nTHoHeGudmwr1Dvyex931vstKWqc3HEXOzo']);
		// $start = date('Y-01-01'); $end = date('Y-m-d');
		// if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		// if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		// $form = '679e61b3133744f3a2c3e52621ec5c51_602240c5f067d';
        $branch = '1e1ab0ea17d44d8fae751a76bb2607f4_602240c5f067d_60267b3bb6012';

		$data = $epic->get_branch_data('linkmistdr-activities', $branch, $id);
		$json = json_encode($data);
		//echo $json; exit;

		if(!empty($data)) {
        	$data = json_decode($json, true);
        	$att = [];
        	foreach($data as $d) {
        		$ret = [
	        			'tdr_regsach' => $d['Achieved_Registratio'],
	        			'tdr_recsach' => $d['Achieved_Recruitment'],
	        			'tdr_actsach' => $d['Achieved_Activations'],
	        			'tdr_basach' => $d['No_of_BAs_active'],
	        			'tdr_activations' => $d['Activated_Agents'],
	        			'tdr_codtime' => $d['created_at'],
	        			'tdr_codloc' => $d['COD_Location']['latitude'].','.$d['COD_Location']['longitude']
	        		];

	        	$this->db->update('tbl_tdractivities', $ret, ['tdr_epi' => $id]);

	        	$att[] = [
	        			'empcheckin_empid' => get_that_data('tbl_employees', 'emp_id', 'emp_gmail', $d['created_by']),
	        			'empcheckin_date' => date('Y-m-d', strtotime($d['created_at'])),
	        			'empcheckin_checkouttime' => $d['created_at'],
	        			'empcheckin_checkintime' => $sod
	        		];

        	}

        	$this->load->model('hr_model');
	        @$this->hr_model->save_attendance($att);
        }
	}

	public function sync_mdr()
	{
		$this->load->library('epic', ['id' => 2332, 'secret' => 'KtG5meFOJemdUv8Fr0ge4ezpT54833TiWrISlEw9']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = '70ce7a8d13e24d1eb5c9a383bde100c5_6034c80486171';
        
        $data = $this->epic->get_form_data('linkmis-mdr-visits', $form, $start, $end);
        // echo $json = json_encode($data); exit;

        if(!empty($data)) {
        	$ret = [];
        	foreach($data as $d) {
        		$rows = $this->db->get_where('tbl_mdractivities', ['mdr_epi' => $d->ec5_uuid])->num_rows();
        		if($rows == 0) {

	        			$ret[] = [
	        				'mdr_code' => $d->Merchant_Code,
	        				'mdr_name' => $d->Merchant_Name,
	        				'mdr_epi'=> $d->ec5_uuid,
	        				'mdr_location'=> $d->Location,
	        				// 'uploaded_at' => $d->uploaded_at,
	        				'mdr_createdate' => $d->created_at,
	        				'mdr_latlon' => $d->GPS->latitude.','.$d->GPS->longitude,
	        				'mdr_altno' => $d->Alternative_No,
	        				'mdr_reason' => $d->Reason,
	        				'mdr_comments' => $d->Remarks,
	        				'mdr_email' => $d->created_by
	        			];
	        		
        		}
        	}

        	$this->db->insert_batch('tbl_mdractivities', $ret);
        	echo count($ret);
        }
	}

	public function sync_rm()
	{
		$this->load->library('epic', ['id' => 2282, 'secret' => 'ItpZ3ZxOwLMzGBwxs3xK1FTHxdnxYqe7rxY84uKs']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = 'bbc5aa63ba3e4f1497769c8de769d214_6034d453abe92';
        
        $data = $this->epic->get_form_data('linkmis-rm', $form, $start, $end);
        // echo $json = json_encode($data); exit;

        if(!empty($data)) {
        	$ret = [];
        	foreach($data as $d) {
        		$rows = $this->db->get_where('tbl_rmactivities', ['rm_epi' => $d->ec5_uuid])->num_rows();
        		if($rows == 0) {

	        			$ret[] = [
	        				'rm_code' => $d->Merchant_Code,
	        				'rm_name' => $d->Merchant_Name,
	        				'rm_epi'=> $d->ec5_uuid,
	        				// 'uploaded_at' => $d->uploaded_at,
	        				'rm_createdate' => $d->created_at,
	        				'rm_latlon' => $d->GPS->latitude.','.$d->GPS->longitude,
	        				'rm_challenges' => $d->Current_Challenges,
	        				'rm_strategy' => $d->Strategy,
	        				'rm_followup' => $d->Follow_Up,
	        				'rm_contact' => $d->Contact_Person,
	        				'rm_pir' => $d->Previous_Issues,
	        				'rm_isfollowup' => $d->Is_follow_up,
	        				'rm_email' => $d->created_by
	        			];
	        		
        		}
        	}

        	$this->db->insert_batch('tbl_rmactivities', $ret);
        	echo count($ret);
        }
	}

	public function sync_merchant()
	{
		$this->load->library('epic', ['id' => 2284, 'secret' => 'c1RCHhDHVHm8uBvBZPiK8s23bO2dqsOvtz7NsCKp']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = '9c91c8c63df343df855a93858239288f_5df86da228024';
        
        $data = $this->epic->get_form_data('merchant-coordinates', $form, $start, $end);
        // echo $json = json_encode($data); exit;

        if(!empty($data)) {
        	$ret = [];
        	foreach($data as $d) {
        		$rows = $this->db->get_where('tbl_mdrvisits', ['mdr_epi' => $d->ec5_uuid])->num_rows();
        		if($rows == 0) {

	        			$ret[] = [
	        				'mdr_code' => $d->Merchant_Code,
	        				'mdr_merchantname' => $d->Merchant_Name,
	        				'mdr_epi'=> $d->ec5_uuid,
	        				'mdr_uploadedat' => $d->uploaded_at,
	        				'mdr_createdat' => $d->created_at,
	        				'mdr_location' => $d->Merchant_Location->latitude.','.$d->Merchant_Location->longitude,
	        				'mdr_name' => $d->MDR_Name,
	        				'mdr_visitdate' => $d->Date_of_Visit,
	        				'mdr_cpname' => $d->Contact_Person_Nam,
	        				'mdr_cpphone' => $d->Contact_Person_Pho,
	        				'mdr_merchanttype' => $d->Merchant_Type,
	        				'mdr_biztype' => $d->Business_Type,
	        				'mdr_code' => $d->Merchant_Code,
	        				'mdr_trained' => $d->Merchant_Received[0],
	        				'mdr_internalbranding' => $d->Internal_Branding,
	        				'mdr_externalbranding' => $d->External_Branding,
	        				'mdr_comment' => $d->MDR_Comment,
	        				// 'mdr_email' => $d->created_by
	        			];
	        		
	        		if(!empty($d->Internal_Branding)) $this->epic->get_photos('merchant-coordinates', $d->Internal_Branding);
	        		if(!empty($d->External_Branding)) $this->epic->get_photos('merchant-coordinates', $d->External_Branding);
        		}
        	}

        	$this->db->insert_batch('tbl_mdrvisits', $ret);
        	echo count($ret);
        }
	}

	public function get_mdr_media($name)
	{
		$this->load->library('epic', ['id' => 2284, 'secret' => 'c1RCHhDHVHm8uBvBZPiK8s23bO2dqsOvtz7NsCKp']);
		$data = $this->epic->get_photos('merchant-coordinates', $name);
		print_r ($data);
	}

	public function sync_agent_visits()
	{
		$this->load->library('epic', ['id' => 2301, 'secret' => 'WLPdL96hm70LRB0dRhhcW9eQoLuYQtRYNjbs4wH7']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = 'e1f671bdb59245408a7e08c386c8084b_60268b29aa0da';
        
        $data = $this->epic->get_form_data('linkmis-agent-visits', $form, $start, $end);
        // echo $json = json_encode($data); exit;

        if(!empty($data)) {
        	$i = 0;
        	foreach($data as $d) {
        		$rows = $this->db->get_where('tbl_agentvisits', ['av_epi' => $d->ec5_uuid])->num_rows();
        		if($rows == 0) {

	        			$ret = [
	        				'av_place' => $d->Location,
	        				'av_code' => $d->Agent_Code,
	        				'av_name' => $d->Agent_Name,
	        				'av_epi'=> $d->ec5_uuid,
	        				'av_updatedate' => $d->uploaded_at,
	        				'av_createdat' => $d->created_at,
	        				'av_location' => $d->GPS->latitude.','.$d->GPS->longitude,
	        				'SSP_Float_Balance' => $d->SSP_Float_Balance,
	        				'USD_Float_Balance' => $d->USD_Float_Balance,
	        				'SSP_Cash_at_hand' => $d->SSP_Cash_at_hand,
	        				'USD_Cash_at_hand' => $d->USD_Cash_at_hand,
	        				'Tarriff_Sheet_Available' => $d->Tarriff_Sheet_Ava,
	        				'Target_Sheet_Available' => $d->Target_Sheet,
	        				'Has_Books' => $d->Has_Books,
	        				'Assistant_Trained' => $d->Assistant_Trained,
	        				'KYC_Updated' => $d->KYC_Updated,
	        				'Does_Recordings' => $d->Does_Recordings,
	        				'Generic_Posters_Available' => $d->Generic_Posters_A,
	        				'Agent_Code_Visible' => $d->Agent_Code_Visibl,
	        				'Opportunity_Actions' => $d->Opportunity_Actions,
	        				'Action_Execution' => $d->Action_Execution,
	        				'Previous_Action_Pts' => $d->Previous_Action_Pts,
	        				'Staff_Name' => $d->Staff_Name,
	        				'av_createdby' => $d->created_by
	        			];
	        		$this->db->insert('tbl_agentvisits', $ret);
	        		$i++;
        		}
        	}
        	echo $i;
        }
	}

	public function sync_agent_report()
	{
		$this->load->library('epic', ['id' => 2314, 'secret' => 'OUvo3WO3kWZ9yWdzvwytjZa4m3Ny6UPTTff1iOc4']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = '77d1ccec26ff41bf92151deafd9f662a_604890c65e40d';
        $data = $this->epic->get_form_data('linkmis-agent-reporting', $form, $start, $end);
        // echo $json = json_encode($data); exit;

        if(!empty($data)) {
        	$i = 0;
        	foreach($data as $d) {
        		$row = $this->db->get_where('tbl_agentreports', ['ec5_uuid' => $d->ec5_uuid])->row();
        		if(empty($row)) {

	        			$ret = [
	        				'Agent_Code' => $d->Agent_Code,
	        				'ec5_uuid'=> $d->ec5_uuid,
	        				'uploaded_at' => $d->uploaded_at,
	        				'created_at' => $d->created_at,
	        				'SOD_Location' => $d->SOD_GPS->latitude.','.$d->SOD_GPS->longitude,
	        				// 'Opening_Float__USD' => $d->Opening_Float_USD,
	        				// 'Opening_Float__SSP' => $d->Opening_Float_SSP,
	        				// 'Opening_Cash__USD' => $d->Opening_Cash_USD,
	        				// 'Opening_Cash__SSP' => $d->Opening_Cash_SSP,
	        				'created_by' => $d->created_by
	        			];

	        			$this->db->insert('tbl_agentreports', $ret);
	        			
	        			if($d->COD == 1) {
	        				$this->get_ar_branches($this->epic, $d->ec5_uuid, $d->created_at);
	        			}

	        			$i++;

        		} else {
        			if(is_null($row->COD_Time))
        				$i = $this->get_ar_branches($this->epic, $row->ec5_uuid, $row->created_at);
        		}

        	}

        	
        	echo $i;
        }
	}

	public function get_ar_branches($epic, $id, $sod)
	{
		
        $branch = '77d1ccec26ff41bf92151deafd9f662a_604890c65e40d_604893abba881';

		$data = $epic->get_branch_data('linkmis-agent-reporting', $branch, $id);
		// echo(json_encode($data)); //exit();
		$i = 0;
		$att = [];
		if(!empty($data)) {
        	
        	foreach($data as $d) {
        		$ret = [
	        					// 'Closing_Float_SSP' => $d->Closing_Float_SSP,
	        					// 'Closing_Cash_SSP' => $d->Closing_Cash_SSP,
	        					// 'Closing_Float_USD' => $d->Closing_Float_USD,
	        					// 'Closing_Cash_USD' => $d->Closing_Cash_USD,
	        					'No_of_transaction' => $d->No_of_transaction,
	        					'COD_GPS' => $d->COD_GPS->latitude.','.$d->COD_GPS->longitude,
	        					'COD_Time' => $d->created_at,
	        				];

	        	$this->db->update('tbl_agentreports', $ret, ['ec5_uuid' => $id]);

	        	$i++;

	        	$att[] = [
	        			'empcheckin_empid' => get_that_data('tbl_employees', 'emp_id', 'emp_gmail', $d->created_by),
	        			'empcheckin_date' => date('Y-m-d', strtotime($d->created_at)),
	        			'empcheckin_checkouttime' => $d->created_at,
	        			'empcheckin_checkintime' => $sod
	        		];
        	}

        	$this->load->model('hr_model');
	        $this->hr_model->save_attendance($att);

        	
        }

        return $i;
	}

	public function sync_mdr_report()
	{
		$this->load->library('epic', ['id' => 2281, 'secret' => 'EHz0VADqkmxNmt960AFG86BVguJMUmmzBFRTBe7U']);
		$start = date('Y-01-01'); $end = date('Y-m-d');
		if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		$form = 'f0d255a36d5d4445baeade4a6b8078c2_6034c80486171';
        $data = $this->epic->get_form_data('linkmis-mdr', $form, $start, $end);
        // echo $json = json_encode($data); exit;

        if(!empty($data)) {
        	$i = 0;
        	foreach($data as $d) {
        		$row = $this->db->get_where('tbl_mdrreport', ['mr_epi' => $d->ec5_uuid])->row();
        		if(empty($row)) {

	        			$ret = [
	        				'mr_epi'=> $d->ec5_uuid,
	        				'mr_uploadedat' => $d->uploaded_at,
	        				'mr_createdat' => $d->created_at,
	        				'SOD_GPS' => $d->SOD_GPS->latitude.','.$d->SOD_GPS->longitude,
	        				'No_of_Recruitments' => $d->No_of_Recruitments,
	        				'No_of_Activations' => $d->No_of_Activations,
	        				'mr_email' => $d->created_by
	        			];

	        			$this->db->insert('tbl_mdrreport', $ret);
	        			
	        			if($d->COD == 1) {
	        				$this->get_mdr_branches($this->epic, $d->ec5_uuid, $d->created_at);
	        			}

	        			$i++;

        		} else {
        			if(is_null($row->COD_Time))
        				$i = $this->get_mdr_branches($this->epic, $row->mr_epi, $row->mr_createdat);
        		}

        	}

        	
        	echo $i;
        }
	}

	public function get_mdr_branches($epic, $id, $sod)
	{
		// $this->load->library('epic', ['id' => 2314, 'secret' => 'OUvo3WO3kWZ9yWdzvwytjZa4m3Ny6UPTTff1iOc4']);
		// $start = date('Y-01-01'); $end = date('Y-m-d');
		// if(isset($_GET['start'])) $start = date('Y-m-d', strtotime($_GET['start']));
		// if(isset($_GET['end'])) $end = date('Y-m-d', strtotime($_GET['end']));

		// $form = 'f0d255a36d5d4445baeade4a6b8078c2_6034c80486171';
        $branch = 'f0d255a36d5d4445baeade4a6b8078c2_6034c80486171_6048b61d4b133';

		$data = $epic->get_branch_data('linkmis-mdr', $branch, $id);
		// echo(json_encode($data)); exit();
		$i = 0;
		if(!empty($data)) {
        	$att = [];
        	foreach($data as $d) {
        		$ret = [
	        				'Merchant_Codes' => $d->Merchant_Codes,
	        				'Achieved_Recruitment' => $d->Achieved_Recruitment,
	        				'Achieved_Activations' => $d->Achieved_Activations,
	        				'Remarks' => $d->Remarks,
	        				'COD_GPS' => $d->COD_GPS->latitude.','.$d->COD_GPS->longitude,
	        				'COD_Time' => $d->created_at,
	        			];

	        	$this->db->update('tbl_mdrreport', $ret, ['mr_epi' => $id]);

	        	$i++;

	        	$att[] = [
	        			'empcheckin_empid' => get_that_data('tbl_employees', 'emp_id', 'emp_gmail', $d['created_by']),
	        			'empcheckin_date' => date('Y-m-d', strtotime($d->created_at)),
	        			'empcheckin_checkouttime' => $d->created_at,
	        			'empcheckin_checkintime' => $sod
	        		];
        	}

        	$this->load->model('hr_model');
	        $this->hr_model->save_attendance($att);

        	
        }

        return $i;
	}
}