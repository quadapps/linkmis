<?php if (!defined('BASEPATH'))

    exit('No direct script access allowed');

    

    

class Lp_api extends CI_Controller



{

    public function __construct()

    {

        parent::__construct();

        $this->ip = $this->input->ip_address();

    }
    

    public function save_attendance()

    {

        $fp = fopen('php://input', 'r');

        $rawData = stream_get_contents($fp);

        if($rawData) {

            $data = json_decode($rawData, true);

            foreach($data as $d) {

                if(!$this->log_exist($d['AttendanceLogId'])) {

                    $ins['empcheckin_devid'] = $d['AttendanceLogId'];

                    $ins['empcheckin_checkintime'] = $d['InTime'];

                    $ins['empcheckin_checkouttime'] = $d['OutTime'];

                    $ins['empcheckin_empid'] = $d['EmployeeId'];

                    $ins['empcheckin_devtime'] = $d['AttendanceDate'];

                    $this->db->insert('tbl_empcheckin', $ins);

                } else {

                    $ins['empcheckin_checkintime'] = $d['InTime'];

                    $ins['empcheckin_checkouttime'] = $d['OutTime'];

                    $ins['empcheckin_empid'] = $d['EmployeeId'];

                    $this->db->update('tbl_empcheckin', $ins, array('empcheckin_devid' => $d['AttendanceLogId']));

                }                

            }

            echo 1;

        } else {

            echo 0;

        }

    }

    public function log_attendance()

    {

        // $fp = fopen('php://input', 'r');

        // $rawData = stream_get_contents($fp);
        // $rawData = file_get_contents('php://input');
        $rawData = $_POST['data'];
        echo $rawData;

        if($rawData) {

            $data = json_decode($rawData, true);
            foreach ($data as $k => $v) {
                echo $k.PHP_EOL;
                foreach ($v as $d) {
                $date = date('Y-m-d', strtotime($d['DateTime']));
                if ($this->log_exist($d['PIN'], $date) == 0) {
                    //echo $date;
                    //$ins['empcheckin_devid'] = $d['AttendanceLogId'];

                    $ins['empcheckin_checkintime'] = $d['DateTime'];

                    $ins['empcheckin_checkouttime'] = $d['DateTime'];

                    $ins['empcheckin_date'] = $date;

                    $ins['empcheckin_empid'] = $d['PIN'];

                    //$ins['empcheckin_devtime'] = $d['AttendanceDate'];

                    $this->db->insert('tbl_empcheckin', $ins);
                } 
                }
            }
        }

    }

    public function attendance()
    {
        $rawData = $_POST['data'];
        //echo $rawData;
        if ($rawData) {
            $data = json_decode($rawData, true);
            foreach($data as $d) {
                if ($this->log_exist($d['empcheckin_empid'], $d['empcheckin_date']) > 0) {
                    $this->db->update('tbl_empcheckin', array('empcheckin_checkouttime' => $d['empcheckin_checkouttime']), array('empcheckin_empid' => $d['empcheckin_empid'], 'empcheckin_date' => $d['empcheckin_date']));
                    echo "EmployeeId ".$d['empcheckin_empid']." updated ".$d['empcheckin_checkouttime']." for date: ".$d['empcheckin_date'];
                } else {
                    $this->db->insert('tbl_empcheckin', $d);
                    echo json_encode($d)." inserted succsesfully";
                }
            }
        }
    }

    

    private function log_exist($log_id, $dt = false) {

        if ($dt) {
            $query = $this->db->get_where('tbl_empcheckin', array('empcheckin_empid' => $log_id, 'empcheckin_date' => $dt));
        } else {
            $query = $this->db->get_where('tbl_empcheckin', array('empcheckin_devid' => $log_id));
        }

        return $query->num_rows();

    }

    public function do_recons()
    {
        $i = 0;
        foreach(get_data('tbl_agents', "WHERE agent_active = 1") as $a) {
            $date = date('Y-m-d', strtotime('-1 day'));
            $row = get_data('tbl_agentrecons', "WHERE ar_date = '$date' AND ar_currency = 'SSP' AND ar_empid = ".$a['agent_empid'], '*', true);
            $fb = isset($row['ar_fb']) ? $row['ar_fb'] : 0;
            $cb = isset($row['ar_cb']) ? $row['ar_cb'] : 0;
            $this->db->insert('tbl_agentrecons', ['ar_date' => date('Y-m-d'), 'ar_empid' => $a['agent_empid'], 'ar_fb' => $fb, 'ar_cb' => $cb, 'ar_currency' => 'SSP']);

            $row = get_data('tbl_agentrecons', "WHERE ar_date = '$date' AND ar_currency = 'USD' AND ar_empid = ".$a['agent_empid'], '*', true);
            $fb = isset($row['ar_fb']) ? $row['ar_fb'] : 0;
            $cb = isset($row['ar_cb']) ? $row['ar_cb'] : 0;
            $this->db->insert('tbl_agentrecons', ['ar_date' => date('Y-m-d'), 'ar_empid' => $a['agent_empid'], 'ar_fb' => $fb, 'ar_cb' => $cb, 'ar_currency' => 'USD']);
            $i++;
        }
        echo $i;
        @send_mail('info@quadrant.co.ke', 'Recons Posted', "$i recons have been posted");
    }

    public function mobile_attendance()
    {
        if(empty($_POST)) return 0;
        $agent = $this->input->get_post('agent');
        $emp_id = get_that_data('tbl_agents', 'agent_empid', 'agent_code', $agent);
        $d = [
            'empcheckin_empid' => $emp_id,
            'empcheckin_devid' => $this->input->get_post('agent'),
            'empcheckin_date' => date('Y-m-d'),
            'empcheckin_checkintime' => date('H:i:s'),
            'empcheckin_checkouttime' => date('H:i:s'),
        ];
        if ($this->log_exist($d['empcheckin_empid'], $d['empcheckin_date']) > 0) {
                    $this->db->update('tbl_empcheckin', array('empcheckin_checkouttime' => $d['empcheckin_checkouttime']), array('empcheckin_empid' => $d['empcheckin_empid'], 'empcheckin_date' => $d['empcheckin_date']));
                    echo "EmployeeId ".$d['empcheckin_empid']." updated ".$d['empcheckin_checkouttime']." for date: ".$d['empcheckin_date'];
                } else {
                    $this->db->insert('tbl_empcheckin', $d);
                    echo json_encode($d)." inserted succsesfully";
                }
    }

}