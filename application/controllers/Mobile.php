<?php defined('BASEPATH') or exit("Direct script access not allowed!");

class Mobile extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->session->user_id)
			$this->load->view('mobile/home');
		else
			$this->load->view('mobile/login');
	}

	public function home()
	{
		if($this->session->user_id)
			$this->load->view('mobile/home');
		else
			$this->load->view('mobile/login');
	}
}