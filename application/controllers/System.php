<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
/**
 * System
 * 
 * @package CRMS
 * @author Alfred Korir
 * @copyright 2014
 * @version $Id$
 * @access public
 */
class System extends CI_Controller
{
    
    /**
     * System::act()
     * 
     * @param mixed $model
     * @param mixed $action
     * @return json
     */
     
    public function index(){
        die("Direct access to this page is not allowed");
    }
    
    public function act($model, $action)
    {
        $this->load->model($model);
        echo $this->$model->$action();
    }
    
    public function get_clientcontact($id)
    {
        $phone = get_that_data('tbl_clients', 'client_contactpersonphone', 'client_id', $id);
        $name = get_that_data('tbl_clients', 'client_contactperson', 'client_id', $id);
        
        echo $name.', '.$phone;
    }
    
    public function get_fromjson($table, $id="")
    {
        $key = get_tablekey($table);
        if($id=="") {
            $data1 = get_data($table);
            foreach($data1 as $d) {
                $data[] = array($d[$key] => $d['emp_fullname']);
            }
        } else {
            $data = get_row_data($table, $key, $id);
        }
        
        echo json_encode($data);
    }
    
    public function get_bulkjson($table, $id, $return="")
    {
        $arr = explode('-', $id);
        $key = $arr[0]; $val = end($arr);
        if($return!="") {
            $arr1 = explode('-', $return);
            $k = $arr1[0]; $v = end($arr1);
            foreach(get_data($table, "WHERE $key = '$val'") as $d) {
                $data[] = array($d[$k] => $d[$v]);
            }
        } else {
            $data = get_row_data($table, $key, $val);
        }
        echo json_encode($data);
    }
    
    public function get_designers()
    {
        $datas = get_data('tbl_employees', 'WHERE emp_designation LIKE "%DESIGNER"');
        foreach($datas as $d) {
            $data[] = array($d['emp_id'] => $d['emp_fullname']);
        }
        echo json_encode($data);
    }
    
    public function toggle_job()
    {
        if(count($_POST) == 0) {
            echo "We did not recieve a thing";
        } else {
            $job_id = $this->input->post('job_id');
            $status = $this->input->post('status');
            
            if($this->db->update('tbl_jobs', array('job_status' => $status), array('job_id' => $job_id))) {
                if($status==3) $this->db->update('tbl_jobs', array('job_completiondate' => date('Y-m-d')), array('job_id' => $job_id));
                echo "Job status changed successully.";
            } else {
                echo "Error! Job status was not changed, please try again later.";
            }
        }
    }
    
    public function check_username($uname)
    {
        $this->load->model('users_model');
        if($this->users_model->user_exists($uname)) {
            echo 0;
        } else {
            echo 1;
        }
    }
    
    public function get_papers($type)
    {
        $data = get_data('tbl_jobtypeitems', "WHERE jobtypeitem_jobtypeid = $type");
        $items = "";
        if(count($data) == 0) $items = "No items for selected type";
        foreach($data as $d) {
            $item_id = $d['jobtypeitem_itemid'];
            $items .= "<input type='checkbox' name='item[]' value='$item_id'/> ".get_that_data('tbl_wh_items', 'item_name', 'item_id', $item_id).' ('.get_that_data('tbl_wh_items', 'item_qty', 'item_id', $item_id).') &nbsp;';
        }
        
        echo $items;
    }
    
    public function change_price()
    {
        if(count($_POST) == 0) {
            echo "We did not recieve anything";
        } else {
            $item_id = $this->input->post('id');
            $price = $this->input->post('price');
            $old_price = get_that_data('tbl_jobitems', 'jobitem_amount', 'jobitem_id', $item_id);
            
            $details = get_row_data('tbl_jobitems', 'jobitem_id', $item_id);
            
            if($this->db->update('tbl_jobitems', array('jobitem_amount' => $price), array('jobitem_id' => $item_id))) {
                
                $job = get_row_data('tbl_jobs', 'job_id', $details['jobitem_jobid']);
                $marketer = get_that_data('tbl_employees', 'emp_email', 'emp_id', get_that_data('tbl_clients', 'clientmarketerid', 'client_id', $job['job_clientid']));
                $creator = get_that_data('tbl_employees', 'emp_email', 'emp_id', get_that_data('tbl_users', 'user_empid', 'user_id', $job['job_creator']));
                
                send_mail(array($marketer, $creator), 'Price updated', 'The price for '.$details['jobitem_description'].' has been updated by '.get_fullname().' you can now proceed to generate quotation.', '', false, site_url('welcome/page/job_details/'.$job['job_id']), 'View Job');
                
                echo 1;
            } else {
                echo 0;
            
            }
        }
    }

    public function test_mail()
    {
        echo send_mail('alfred@quadrantsoftwares.com', 'test_mail', 'received');
    }
    
    public function delete_act($model, $action,$id)
    {
        $this->load->model($model);
        echo $this->$model->$action($id);
    }
    
    public function gatepass()
    {
        if(count($_POST['jobs'])==0 || empty($_POST['jobs'])) { ?>
        <script>
            alert('Please select jobs to proceed');
            window.history.back();
        </script>
        <?php } else {
            $data['jobs'] = $_POST['jobs'];
            $this->load->view('gatepass', $data);
        }
    }
    
    public function approve($what, $how, $id)
    {
        if($what=='cashrequest') {
            $data['cashrequest_status'] = $how;
            if($how==3) {
                $data['cashrequest_approvedate'] = date('Y-m-d H:i:s');
                $data['cashrequest_approvedby'] = $this->session->userdata('user_id');
            }
            return $this->db->update('tbl_cashrequests', $data, array('cashrequest_id' => $id));
        } elseif($what=='itemrequest') {
            $data['request_status'] = $how;
            if($how==3) {
                $data['request_approvedate'] = date('Y-m-d H:i:s');
                $data['request_approvedby'] = $this->session->userdata('user_id');
            }
            return $this->db->update('tbl_itemrequests', $data, array('request_id' => $id));
        } elseif($what=='leave') {
            $data = array(
                    'leave_status' => $how,
                    'leave_approveby' => $this->session->userdata('user_id'),
                    'leave_approvedate' => date('Y-m-d H:i:s')
                );
            return $this->db->update('tbl_leaves', $data, array('leave_id' => $id));
        } elseif($what=='request') {
            $item_id = get_that_data('tbl_wh_requests', 'request_itemid', 'request_id', $id);
            $qty = get_that_data('tbl_wh_items', 'item_qty', 'item_id', $item_id);
            $rqty = get_that_data('tbl_wh_requests', 'request_qty', 'request_id', $id);
            $new_qty = $qty - $rqty;
            $this->db->update('tbl_wh_items', array('item_qty' => $new_qty), array('item_id' => $item_id));            
            $data['request_status'] = $how;
            $data['request_issuer'] = $this->session->userdata('user_id');
            $data['request_issuedate'] = date('Y-m-d H:i:s');
            return $this->db->update('tbl_wh_requests', $data, array('request_id' => $id));
        } elseif($what=='salary') {
            $data['sa_status'] = $how;
            $data['sa_approvedby'] = $this->session->userdata('user_id');
            $data['sa_approvedate'] = date('Y-m-d H:i:s');
            return $this->db->update('tbl_salaryadvance', $data, array('sa_id' => $id));
        } else {
            echo 0;
        }
    }
    
    public function change_designer()
    {
        if(count($_POST) == 0) {
            echo 0;
        } else {
            if($this->db->update('tbl_jobs', array('job_designerid' => $this->input->post('id')), array('job_id' => $this->input->post('job_id')))) echo 1;
            else echo 0;
        }
    }
    
    public function change_pass()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not recieve anyhting.');
        } else {
            $emp_id = $this->input->post('emp');
            $uid = get_that_data('tbl_users', 'user_id', 'user_empid', $emp_id);
            $pass = $this->input->post('pass'); $pass1 = $this->input->post('pass1');
            if($pass != $pass1 ) {
                $res = array('status' => 0, 'msg' => 'Passwords do not match.');
            } else {
                if($this->db->update('tbl_users', array('user_password' => md5($pass)), array('user_id' => $uid))) {
                    $res = array('status' => 1, 'msg' => 'Password changed successully.');
                } else {
                    $res = array('status' => 1, 'msg' => 'Sorry! Password could not be changed.');
                }
            }
        }
        
        echo json_encode($res);
    }
    
    public function get_jobitems($job)
    {
        $str = '';
        foreach(get_data('tbl_jobitems', "WHERE jobitem_jobid = $job") as $j) {
            $str .= '<input type="radio" name="item_id" value="'.$j['jobitem_id'].'"/> '.$j['jobitem_description'];
        }
        
        echo $str;
    }
    
    public function delete_item()
    {
        if(count($_POST) == 0) {
            //$res = array('status' => 0, 'Error: 101');
            echo 'Error: 101';
        } else {
            $id = $this->input->post('id');
            $table = $this->input->post('tbl');
            $key = get_tablekey($table);
            $field = reset(explode('_', $key)).'_active';
            if($this->db->update($table, array($field => 0), array($key => $id))) {
                //$res = array('status' => 1, 'msg' => 'Item deleted successfully');
                echo 'Item deleted successfully.';
            } else {
                echo 'Sorry! Item could not be deleted';
                //$res = array('status' => 0, 'msg' => 'Sorry! Item could not be deleted');
            }
        }
    }
    
    public function remove_item()
    {
        if(count($_POST) == 0) {
            //$res = array('status' => 0, 'Error: 101');
            echo 'Error: 101';
        } else {
            $id = $this->input->post('id');
            $table = $this->input->post('tbl');
            $key = get_tablekey($table);
            if($this->db->delete($table, array($key => $id))) {
                //$res = array('status' => 1, 'msg' => 'Item deleted successfully');
                echo 'Item deleted successfully.';
            } else {
                echo 'Sorry! Item could not be deleted';
                //$res = array('status' => 0, 'msg' => 'Sorry! Item could not be deleted');
            }
        }
    }
    
    public function resolve_complain()
    {
        if(count($_POST) == 0) {
            echo 0;
        } else {
            $id = $this->input->post('id');
            $how = $this->input->post('how');
            if($this->db->update('tbl_complains', array('complain_status' => $how), array('complain_id' => $id))) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }
    
    public function get_price($job_item, $qty=0)
    {
        //$details = get_row_data('tbl_jobitems', 'jobitem_id', $job_item);
        $prices = get_row_data('tbl_jobtypes', 'jobtype_id', $job_item);
        $x = $prices['jobtype_price'];
        $r = $prices['jobtype_range'] == 0 ? 1 : $prices['jobtype_range'];
        $disc = $prices['jobtype_discount']/100;
        //$qty = $details['jobitem_pcs'];
        $rounds = $qty > $prices['jobtype_limit'] ? $prices['jobtype_limit']/$r : $qty/$r; 
        for($i=0; $i<$rounds; $i++) {
            $d = $x-($x*$disc);
            //echo $d.'<br>';
            $x = $d;
        }
        $price = number_format($x, 2);
        echo $price;
    }
    
    public function job_lpoupdater()
    {
        $files = get_data('tbl_jobfiles', 'WHERE jobfile_type = "LPO"');
        
        foreach($files as $f) {
            $this->db->update('tbl_jobs', array('job_confirmdate' => $f['jobfile_createdate']), array('job_id' => $f['jobfile_jobid']));
            echo $f['jobfile_jobid'].' - '.$f['jobfile_createdate'].'<br/>';
        }
    }
    
    public function job_cashupdater()
    {
        $pmts = get_data('tbl_payments', 'GROUP BY payment_jobid');
        
        foreach($pmts as $p) {
            $client_type = $this->get_clienttype($p['payment_jobid']);
            if($client_type == 1) $this->db->update('tbl_jobs', array('job_confirmdate' => $p['payment_createdate']), array('job_id' => $p['payment_jobid'])); 
            echo $p['payment_createdate'].' - '.$p['payment_jobid'].' -cash- '.$p['payment_amt'].' - '.get_jobamt($p['payment_jobid']).' -client- '.$client_type.'<br/>';
        }
    }
    
    public function job_delupdater()
    {
        $dels = get_data('tbl_jobdeliveries');
        foreach($dels as $d) {
            $this->db->update('tbl_jobs', array('job_deliverydate' => $d['jd_createdate']), array('job_id' => $d['jd_jobid']));
        }
    }
    
    private function get_clienttype($job)
    {
        return get_that_data('tbl_clients', 'client_type', 'client_id', get_that_data('tbl_jobs', 'job_clientid', 'job_id', $job));
    }

    public function getcashrequest($id) {

        if (!empty($id)) {
            $rsql = "SELECT emp_id, emp_fullname, sa_id, sa_empid, sa_amt, sa_currency FROM tbl_salaryadvance JOIN tbl_employees ON emp_id = sa_empid WHERE sa_status = 3 AND sa_id = $id";
            $request = get_query($rsql);

            $installment = $this->getinstallmentlimit($request[0]['emp_id']);
            if (empty($installment)) {
                $installment = 0;
            }

            $res['payload'] = array(
                'empid' => $request[0]['emp_id'],
                'emp_fullname' => $request[0]['emp_fullname'],
                'cashrequest_id' => $request[0]['sa_id'],
                'cashrequest_amt' => $request[0]['sa_amt'],
                'cashrequest_currency' => $request[0]['sa_currency'],
                'loan_installment' => $installment
            );
            echo json_encode($res);
        }

    }

    public function getinstallmentlimit($emp_id) {

        $basic_salary = get_query("SELECT basic, currency FROM tbl_empsalary WHERE empid = $emp_id ");

        if (!empty($basic_salary)) {
            $limit = (float)$basic_salary[0]['basic'] / 3;
            return round($limit,2).'-'.strtoupper($basic_salary[0]['currency']);
        } else {
            return 'Salary details not set';
        }

    }

    public function show()
    {
        print_r($this->uri->segment_array());
    }

    public function get_checkin_status($emp, $date)
    {
        // $emp = $this->session->userdata('emp_id');
        // $date = date('Y-m-d');
        $data = $this->db->get_Where('tbl_empcheckin', ['empcheckin_date' => $date, 'empcheckin_empid' => $emp])->num_rows();
        return $data;
    }

    public function checkin()
    {
        $status = $this->input->post('how');
        $location = $this->input->post('location');
        $emp = $this->session->userdata('emp_id');
        $date = date('Y-m-d');
        $time = date('H:i:s');

        if($status == 1) {
            if($this->get_checkin_status($emp, $date) == 0) {
                $this->db->insert('tbl_empcheckin', ['empcheckin_date' => $date, 'empcheckin_checkintime' => $time, 'empcheckin_empid' => $emp, 'empcheckin_location' => $location]);
                echo 1;
            } else {
                echo "Already checked in for $date";
            }
        } else {
            $this->db->update('tbl_empcheckin', ['empcheckin_checkouttime' => $time], ['empcheckin_date' => $date, 'empcheckin_empid' => $emp]);
            echo 1;
        }
    }

    public function set_agent_recons()
    {
        $data = [
            'ar_empid' => $_POST['emp'],
            'ar_date' => $_POST['date'],
            'ar_currency' => 'SSP'
        ];


            $data1 = $data;
            $data1['ar_ts'] = date('Y-m-d H:i:s');
            $data1['ar_fb'] = $_POST['ssp_float'];
            $data1['ar_cb'] = $_POST['ssp_cash'];

        if($this->db->get_where('tbl_agentrecons', $data)->num_rows() > 0) {
            $this->db->update('tbl_agentrecons', $data1, $data);
            echo 'SSP Update <br>';
        } else {
           $this->db->insert('tbl_agentrecons', $data1); 
           echo 'SSP Update <br>';
        }


        $data = [
            'ar_empid' => $_POST['emp'],
            'ar_date' => $_POST['date'],
            'ar_currency' => 'USD'
        ];

        // $data1 = $data;
        $data1 = [
            'ar_empid' => $_POST['emp'],
            'ar_date' => $_POST['date'],
            'ar_fb' => $_POST['usd_float'],
            'ar_cb' => $_POST['usd_cash'],
            'ar_currency' => 'USD',
            'ar_ts' => date('Y-m-d H:i:s')
        ];

        if($this->db->get_where('tbl_agentrecons', $data)->num_rows() > 0) {
            $this->db->update('tbl_agentrecons', $data1, $data);
            echo 'USD Update';
        } else {
           $this->db->insert('tbl_agentrecons', $data1); 
           echo 'USD Set';
        }
        
    }

}