<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		if(!$this->session->userdata('user_id'))
			$this->load->view('login');
		else
			$this->load->view('home');
	}

	public function login()
	{
		if(empty($_POST)) echo json_encode(array('status' => 0, 'msg' => 'Please provide email and passsword'));
		else {
			$email = $this->input->post('email');
			$passsword = md5($this->input->post('password'));
			$row = $this->db->get_where('tbl_users', array('user_email' => $email, 'user_password' => $passsword))->row();
			if(empty($row)) echo json_encode(array('status' => 0, 'msg' => 'Incorrect email or password'));
			elseif ($row->user_active == 0) echo json_encode(array('status' => 0, 'msg' => 'User is deactivated'));
			else {
				$this->session->set_userdata(array('user_id' => $row->user_id, 'emp_id' => $row->user_empid));
				$this->db->insert('tbl_loginactivity', array('login_userid' => $row->user_id, 'login_time' => date('Y-m-d H:i:s'), 'login_ip' => $_SERVER['REMOTE_ADDR']));
				// redirect();
				echo json_encode(array('status' => 1, 'msg' => 'Login successfull'));
			}
		}

		//$this->load->view('home');
	}

	public function logout()
	{
		$this->db->update('tbl_loginactivity', array('logout_time' => date('Y-m-d H:i:s')), array('login_userid' => $this->session->userdata('user_id')));
		$this->session->sess_destroy();
		redirect();
	}

	public function view($page, $params = "")
	{
		if(!$this->session->userdata('user_id'))
			$this->load->view('login');
		else 
			$this->load->view("pages/$page", array('params' => $params));
	}
    
    public function page($page, $params = "")
	{
		if(!$this->session->userdata('user_id'))
			$this->load->view('login');
		else 
			$this->load->view("pages/$page", array('params' => $params));
	}

	public function modal($page, $params = "")
	{
		$this->load->view("modals/$page", array('params' => $params));
	}
	
	public function load_partial($page, $params = "")
	{
		$this->load->view("partial/$page", array('id' => $params));
	}

	public function report($page, $params="")
	{
		if(!$this->session->userdata('user_id'))
			$this->load->view('login');
		else $this->load->view("reports/$page", array('params' => $params));
	}

	public function sales($page, $params="")
	{
		if(!$this->session->userdata('user_id'))
			$this->load->view('login');
		else $this->load->view("sales/$page", array('params' => $params));
	}
}
