<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function get_data($table, $where="", $columns='*', $row=false)

{

    $A =& get_instance();

    $query = $A->db->query("SELECT $columns FROM $table $where");

    if($query->num_rows()==0) {

        return array();

    }

    if ($row) {
        return $query->row_array();
    }

    return $query->result_array();

}



function get_row_data($table, $key, $value="")

{

    $A =& get_instance();

    if(is_array($key) || $value=="") {

        $query = $A->db->get_where($table, $key);

    } else {

        $query = $A->db->get_where($table, array($key => $value));

    }

    if($query->num_rows()==0) {

        return array();

    }

    

    return $query->row_array();

}

function get_fullname($uid = "")

{

    $A =& get_instance();

    if($uid == "") {

        $uid = $A->session->userdata('user_id');

    } 

    $id = get_that_data('tbl_users', 'user_empid', 'user_id', $uid);

    return get_that_data('tbl_employees', 'emp_fullname', 'emp_id', $id);

}



function get_that_data($table, $column, $key, $value)

{

    $data = get_row_data($table, $key, $value);

    if(empty($data)) {

        return '';

    }

    

    return $data["$column"];

}

//The function returns the no. of business days between two dates and it skips the holidays
function get_working_days($startDate,$endDate){
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);


    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = ($endDate - $startDate) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
        }
        else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
   $workingDays = $no_full_weeks * 5.5;
    if ($no_remaining_days > 0 )
    {
      $workingDays += $no_remaining_days;
    }

    $holidays = get_data('tbl_holidays', " WHERE active=1 ", 'date');
    //We subtract the holidays
    foreach($holidays as $holiday){
        $time_stamp=strtotime($holiday['date']);
        //If the holiday doesn't fall in weekend
        if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
            $workingDays--;
    }

    return $workingDays;
}

function send_mail($to, $subject, $message, $file = "", $name = false, $url = '', $url_title = 'Read More', $get=true)
{
    if(is_array($to)) $to = json_encode($to);
    $data = [
            'emails' => $to,
            'subject' => $subject,
            'msg' => $message,
            'file' => $file,
            'name' => $name,
            'url' => $url,
            'url_title' => $url_title
        ];

    $A =& get_instance();
    return $A->db->insert('tbl_mailqueue', $data);

}

function send_email($to, $subject, $message, $file = "", $name = false, $url = '', $url_title = 'Read More', $get=true)
{
    require_once 'mail/PHPMailerAutoload.php';



    $mail = new PHPMailer;

    

    //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    //$body = '<img src="res/assets/img/header.png" style="width: 800px"/><br/><br/>'; // include our formatted email

    //$body .= $message;

    //$body .= '<br/><br/><br/>Regards, <br/> RAK Media Group <br/><br/>Adjacent to Co-op Bank Kololo, Airport Road,Off American Embassy<br/> thong Piny Area, Juba, South Sudan <br/>+211 (0) 957 321 060 | +211 (0) 977 321 060 <br/>info@rakmediagrouprss.com | www.rakmediagrouprss.com <br/><br/> <img src="res/assets/img/footer.png" style="width: 800px"/>';

    $mail->isSMTP();                                      // Set mailer to use SMTP

    $mail->Host = 'mail.quadrant.co.ke';  // Specify main and backup SMTP servers

    //$mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers

    $mail->SMTPAuth = true;                               // Enable SMTP authentication

    $mail->Username = 'no-reply@quadrant.co.ke';                 // SMTP username

    $mail->Password = 'Notify2020';                           // SMTP password

    //$mail->Username = 'rakmediamis@gmail.com'; // SMTP username

    //$mail->Password = 'Rakmis@2014'; // SMTP password

    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted

    $mail->Port = 465;                                    // TCP port to connect to

    

    $mail->From = 'no-reply@quadrant.co.ke';
    //$mail->From = 'rakmediamis@gmail.com';

    $mail->FromName = 'Links Pay MIS';

    if(is_array($to)) {

        if (count($to) > 2) {
            $min = min ( count ( $to ), 2 );

            $i = 0;
            foreach($to as $t) { 

                if (is_array($t)) {

                    foreach($t as $e) {

                        $mail->addAddress($e);
                    
                    }

                } else {

                    $mail->addAddress($t);     // Add a recipient

                };

                $i++;
                if ($i == $min) break;

            }

            $slice = array_slice($to, 2, count($to));

            foreach($slice as $t) { 

                if (is_array($t)) {

                    foreach($t as $e) {

                        $mail->addCC($e);
                    
                    }

                } else {

                    $mail->addCC($t);     // Add a recipient

                };

            }

        } else {
            foreach($to as $t) { 

                if (is_array($t)) {

                    foreach($t as $e) {

                        $mail->addAddress($e);
                    
                    }

                } else {

                    $mail->addAddress($t);     // Add a recipient

                };

            }
        }

    } else {

        $mail->addAddress($to);     // Add a recipient

    }

    //$mail->addAddress('ann.ruteere@rakmediagrouprss.com', 'Ann Ruteere');

    /*

    $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient

    $mail->addAddress('ellen@example.com');   */            // Name is optional

    //$mail->addReplyTo('rakmediahouse@yahoo.com', 'RAK Media Group Ltd');

    // $mail->addReplyTo('rakmediahouse@gmail.com', 'RAK Media Group Co. Ltd');
    //$mail->addCC('rakmediahouse@gmail.com', 'RAK Media Group Ltd');

    $mail->addCC('md@links-pay.com', 'Ann Ruteere');
   // $mail->addCC('ann.rutere@rakmediagroup.com', 'Ann Ruteere');
    $mail->addBCC('alfred@quadrantsoftwares.com', 'Alfo');

    //$mail->addBCC('support@quadrantsoftwares.com'); 

    if($file != "") {

        $mail->addAttachment($file);

    }
    if ($url == "") {
        $url = site_url();
    }

    

    $mail->isHTML(true);                                  // Set email format to HTML


    $mail->Subject = $subject;

    //$mail->Body    = $message;
    if (!$get) {
        $body = $message;
    } else {
        
        $CI =& get_instance();
        $body = $CI->load->view('modals/email', ['name' => $name, 'msg' => $message, 'url' => $url, 'url_title' => $url_title], TRUE);
    }

    $mail->MsgHTML($body);

    if(!$mail->send()) {

        return 'Mailer Error: ' . $mail->ErrorInfo;

    } else {

        return true;

    }
}

function timeAgo($time) {

    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");

    $lengths = array("60","60","24","7","4.35","12","10");

    $now = time();

    $difference = $now - strtotime($time);

    $tense = "ago";

    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {

        $difference /= $lengths[$j];

    }

    $difference = round($difference);

    if($difference != 1) {

        $periods[$j].= "s";

    }

    $return = "$difference $periods[$j] ago";

    if($return=="0 seconds ago"){

        return 'Just Now';

    }

    return $return;

} 

function time_dif($in, $out)
{
  $time1 = strtotime($in);
  $time2 = strtotime($out);
  $difference = round(abs($time2 - $time1) / 3600,2);
  return $difference;
}

function get_req_status($req)
{
    $amt = get_that_data('tbl_payments', 'payment_id', $req, 'payment_amt');
    if ($amt > 0) {
        return "<label class='alert alert-success small'>Paid ($amt)</label>";
    }

    $data = get_data('tbl_reqapprovals', "WHERE ra_typeid = '$req'");
    $c = count($data);
    switch ($c) {
        case 0:
            return "<label class='alert alert-warning text-xs'>Awaiting approval</label>";
            break;

        case 1:
            $des = get_that_data('tbl_employees', 'emp_designation', 'emp_id', end($data)['ra_empid']);
            return "<label class='alert alert-secondary text-xs'>Approved by $des<br>".end($data)['ra_remarks']."</label>";
            break;

        case 2:
            $des = get_that_data('tbl_employees', 'emp_designation', 'emp_id', end($data)['ra_empid']);
            return "<label class='alert alert-info text-xs'>Approved by $des<br>".end($data)['ra_remarks']."</label>";
            break;

        case 3:
            $des = get_that_data('tbl_employees', 'emp_designation', 'emp_id', end($data)['ra_empid']);
            return "<label class='alert alert-success text-xs'>Approved by $des<br>".end($data)['ra_remarks']."</label>";
            break;
        
        case 4:
            $des = get_that_data('tbl_employees', 'emp_designation', 'emp_id', end($data)['ra_empid']);
            return "<label class='alert alert-success text-xs'>Approved by $des<br>".end($data)['ra_remarks']."</label>";
            break;

        default:
            return "<label class='alert alert-primary text-xs'>Unknown</label>";
            break;
    }
}

function get_reqpaid($id, $type, $fogo=false)

{

    if ($fogo) {
        $arr = explode('-', $id);
        $id = end($arr); $item = reset($arr);
        switch ($item) {
            case 'item':
                $type = 1;
                break;

            case 'cash':
                $type = 2;
                break;

            case 'sa':
                $type = 3;
                break;

            case 'fuel':
                $type = 4;
                break;
            
            default:
                # code...
                break;
        }
    } 

    $data = get_data('tbl_requestpmts', "WHERE requestpmt_type = '$type' AND requestpmt_requestid = $id");

    $amt = 0;

    foreach($data as $d) {

        $amt += $d['requestpmt_amt'];

    }

    

    return $amt;

}

function to_words( $num = '' )
{
    $num    = ( string ) ( ( int ) $num );

    if( ( int ) ( $num ) && ctype_digit( $num ) )
    {
        $words  = array( );

        $num    = str_replace( array( ',' , ' ' ) , '' , trim( $num ) );

        $list1  = array('','one','two','three','four','five','six','seven',
            'eight','nine','ten','eleven','twelve','thirteen','fourteen',
            'fifteen','sixteen','seventeen','eighteen','nineteen');

        $list2  = array('','ten','twenty','thirty','forty','fifty','sixty',
            'seventy','eighty','ninety','hundred');

        $list3  = array('','thousand','million','billion','trillion',
            'quadrillion','quintillion','sextillion','septillion',
            'octillion','nonillion','decillion','undecillion',
            'duodecillion','tredecillion','quattuordecillion',
            'quindecillion','sexdecillion','septendecillion',
            'octodecillion','novemdecillion','vigintillion');

        $num_length = strlen( $num );
        $levels = ( int ) ( ( $num_length + 2 ) / 3 );
        $max_length = $levels * 3;
        $num    = substr( '00'.$num , -$max_length );
        $num_levels = str_split( $num , 3 );

        foreach( $num_levels as $num_part )
        {
            $levels--;
            $hundreds   = ( int ) ( $num_part / 100 );
            $hundreds   = ( $hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ( $hundreds == 1 ? '' : '' ) . ' ' : '' );
            $tens       = ( int ) ( $num_part % 100 );
            $singles    = '';

            if( $tens < 20 )
            {
                $tens   = ( $tens ? ' ' . $list1[$tens] . ' ' : '' );
            }
            else
            {
                $tens   = ( int ) ( $tens / 10 );
                $tens   = ' ' . $list2[$tens] . ' ';
                $singles    = ( int ) ( $num_part % 10 );
                $singles    = ' ' . $list1[$singles] . ' ';
            }
            $words[]    = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_part ) ) ? ' ' . $list3[$levels] . ' ' : '' );
        }

        $commas = count( $words );

        if( $commas > 1 )
        {
            $commas = $commas - 1;
        }

        $words  = implode( ', ' , $words );

        //Some Finishing Touch
        //Replacing multiples of spaces with one space
        $words  = trim( str_replace( ' ,' , ',' , trim_all( ucwords( $words ) ) ) , ', ' );
        if( $commas )
        {
            $words  = str_replace_last( ',' , ' and' , $words );
        }

        return $words;
    }
    else if( ! ( ( int ) $num ) )
    {
        return 'Zero';
    }
    return '';
}

function trim_all( $str , $what = NULL , $with = ' ' )
{
    if( $what === NULL )
    {
        //  Character      Decimal      Use
        //  "\0"            0           Null Character
        //  "\t"            9           Tab
        //  "\n"           10           New line
        //  "\x0B"         11           Vertical Tab
        //  "\r"           13           New Line in Mac
        //  " "            32           Space

        $what   = "\\x00-\\x20";    //all white-spaces and control chars
    }

    return trim( preg_replace( "/[".$what."]+/" , $with , $str ) , $what );
}

function str_replace_last( $search , $replace , $str ) {
    if( ( $pos = strrpos( $str , $search ) ) !== false ) {
        $search_length  = strlen( $search );
        $str    = substr_replace( $str , $replace , $pos , $search_length );
    }
    return $str;
}

function calculate_tax($amt, $currency, $rate = 360, $pays_pit = true, $pays_nssf = false) {
    $t_exempt = 2000;
    if($currency == 'USD') $amt = $amt*$rate;
    $ti = $amt - $t_exempt;
    
    $nssf = 0;
    if($pays_nssf) $nssf = $ti*0.08;
    $ta = $ti-$nssf;

    $pit = 0;
    if($pays_pit) {
        if($ta < 5001) $pit = $ta*0.05;
        if($ta < 10001) $pit = $ta*0.1;
        if($ta < 15001) $pit = $ta*0.15;
        if($ta > 15000) $pit = $ta*0.2;
    }

    if($currency == 'USD') {
        $ti = $ti/$rate;
        $nssf = $nssf/$rate;
        $ta = $ta/$rate;
        $pit = $pit/$rate;
        $t_exempt = $t_exempt/$rate;
    }

    return ['t_exempt' => $t_exempt, 't_income' => $ti, 'nssf' => $nssf, 't_amt' => $ta, 'pit' => $pit];
}

function get_tablekey($table)
{
    $A =& get_instance();
    $query = $A->db->query("SHOW KEYS FROM $table WHERE Key_name = 'PRIMARY'");
    $res = $query->row_array();
    return $res['Column_name'];
}

function colums_exists($table, $col)
{
    $A =& get_instance();
    $query = $A->db->query("SHOW COLUMNS FROM $table LIKE '$col'");
    return $query->num_rows() ? TRUE : FALSE;
}

function get_worked_days($emp, $month)
{
    $worked_days = get_data('tbl_empcheckin', "WHERE DATE_FORMAT(empcheckin_date,'%Y-%m') = '$month' AND empcheckin_empid = $emp", 'COUNT(empcheckin_id) AS total', true)['total'];
    return $worked_days;
}

function get_hod($dept_id)
{
    return get_data('tbl_employees', "WHERE emp_id = (SELECT dept_empid FROM tbl_departments WHERE dept_id = ".$dept_id.")", 'emp_email, emp_id, emp_fullname, emp_designation', true);
}

function get_bal($date, $emp_id, $currency)
{
    $uid = get_that_data('tbl_users', 'user_id', 'user_empid', $emp_id);
    if(empty($uid)) $uid = $emp_id;
    $currency = strtolower($currency);
    $cash_field = 'co_'.$currency.'amt';
    $float_field = 'co_'.$currency.'float';
    //$jana = date('Y-m-d', strtotime("$date - 1 day"));
    $float = get_data('tbl_cashouts', "WHERE co_agentname = $emp_id AND DATE(co_createdate) = '$date' AND co_active = 1 AND co_currency ='$currency' AND co_type = 1", "SUM(co_amt) AS total", true);

    $cash = get_data('tbl_cashouts', "WHERE co_agentname = $emp_id AND DATE(co_createdate) = '$date' AND co_active = 1 AND co_currency ='$currency' AND co_type = 0 ", "SUM(co_amt) AS total", true);

    $c = get_data('tbl_agenttxns', "WHERE at_creator = $uid AND at_active = 1 AND DATE(at_date) = '$date' AND at_currency = '$currency' AND at_type = 'Withdrawal'", 'SUM(at_amt) AS total', true);

    $d = get_data('tbl_agenttxns', "WHERE at_creator = $uid AND at_active = 1 AND DATE(at_date) = '$date' AND at_currency = '$currency' AND at_type = 'Deposit'", 'SUM(at_amt) AS total', true);

    //Float Balance
    $f_coll = get_data('tbl_collections', "WHERE collection_active = 1 AND collection_currency = '$currency' AND collection_creator = $uid AND DATE(collection_createdate) = '$date' AND collection_type = 'Float'", 'SUM(collection_amt) AS total', true);
    $f_bal = ($float['total']-$f_coll['total'])+($c['total']-$d['total']);

    ///Cash balance = deposits+cash_out-withdrawals-collections
    $coll = get_data('tbl_collections', "WHERE collection_active = 1 AND collection_currency = '$currency' AND collection_creator = $uid AND DATE(collection_createdate) = '$date' AND collection_type = 'Cash'", 'SUM(collection_amt) AS total', true);
    $c_bal = ($d['total']-$c['total'])+($cash['total']-$coll['total']);

    return ['float_bal' => $f_bal, 'cash' => $c_bal, 'deposits' => $d['total'], 'withdrawals' => $c['total'], 'co' => $cash['total'], 'float' => $float['total'], 'collections' => $coll['total'], 'fo' => $f_coll['total']];
}

function dateRanges( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {
    $dates = [];
    $current = strtotime( $first );
    $last = strtotime( $last );

    while( $current <= $last ) {

        $dates[] = date( $format, $current );
        $current = strtotime( $step, $current );
    }

    return $dates;
}

function has_checkedin($emp_id, $date) {
    $data = get_data('tbl_empcheckin', "WHERE empcheckin_empid = $emp_id AND DATE(empcheckin_date) = '$date'", '*', true);

    if(empty($data)) return false;

    //get previous checked in date
    $data = get_data('tbl_empcheckin', "WHERE empcheckin_empid = $emp_id AND DATE(empcheckin_date) < '$date' ORDER BY empcheckin_id DESC LIMIT 1", '*', true);
    return $data;
}

