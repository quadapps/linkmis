<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function force_ssl() {
    $CI =& get_instance();
    $class = $CI->router->fetch_class();
    $exclude =  array('emailer', 'reporter2', 'lp_api');  // add more controller name to exclude ssl.
    if(!in_array($class,$exclude)) {
        // redirecting to ssl.
        $CI->config->config['base_url'] = str_replace('http://', 'https://', $CI->config->config['base_url']);
        if ($_SERVER['SERVER_PORT'] != 443) redirect($CI->uri->uri_string());
    } else {
        // redirecting with no ssl.
        // $CI->config->config['base_url'] = str_replace('https://', 'http://', $CI->config->config['base_url']);
        // if ($_SERVER['SERVER_PORT'] == 443) 
        //redirect($CI->uri->uri_string());
    }
    //$CI->config->config['base_url'] = str_replace('http://', 'https://', $CI->config->config['base_url']);
    // if (!isset($_SERVER['SERVER_PORT'])) {
    //     $_SERVER['SERVER_PORT'] = getenv('SERVER_PORT');
    // }
    //     if ($_SERVER['SERVER_PORT'] != 443) redirect($CI->uri->uri_string());
}