<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
/**
 * Epicollect API
 * 
 * @package LinkMis
 * @author Alfred Korir
 * @copyright 2020
 * @version $Id$
 * @access public
 */

require APPPATH.'third_party/epicollect/vendor/autoload.php';
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class Epic {

	private $token = null;
	public function __construct($params)
	{
		// parent::__construct();
	    

	     $tokenClient = new Client(); //GuzzleHttp\Client
	     $tokenURL = 'https://five.epicollect.net/api/oauth/token';

	      //get token first
		 try {
		     $tokenResponse = $tokenClient->request('POST', $tokenURL, [
		         'headers' => ['Content-Type' => 'application/vnd.api+json'],
		         'body' => json_encode([
		             'grant_type' => 'client_credentials',
		             'client_id' => $params['id'],
		             'client_secret' => $params['secret']
		         ])
		     ]);

		     $body = $tokenResponse->getBody();
		     $obj = json_decode($body);
		     $this->token = $obj->access_token;
		 } catch (RequestException $e) {
		     //handle errors
		     die("errors");
		 }
	}


	function get_form_data($project, $form, $start = false, $end = false)
	{

		 //Configure filter
		 if(!$start) {
		 	$start = date('Y-m-d 00:00:00', strtotime('-3 months'));
		 	$end = date('Y-m-d 23:59:59');
		 }

		 //get entries now
		 $entriesURL = 'https://five.epicollect.net/api/export/entries/'.$project.'?form_ref='.$form.'&sort_by=created_at&sort_order=DESC&per_page=1000&filter_by=created_at&filter_from='.$start.'&filter_to='.$end;
		 // $entriesURL = 'https://five.epicollect.net/api/export/entries/'.$project.'?form_ref='.$form;
		 $entriesClient = new Client([
		     'headers' => [
		         'Authorization' => 'Bearer '.$this->token //this will last for 2 hours!
		     ]
		 ]);


	     try {
	         $response = $entriesClient-> request('GET', $entriesURL);

	         $body = $response->getBody();
	         $obj = json_decode($body);

	         return $obj->data->entries;
	         

	     } catch (RequestException $e) {
	         //handle errors
	         //...
	        return false;
	     }
	}

	function get_branch_data($project, $branch, $branch_owner_uuid=false, $start = false, $end = false)
	{

		 //Configure filter
		 if(!$start) {
		 	$start = date('Y-m-d 00:00:00', strtotime('-1 month'));
		 	$end = date('Y-m-d 23:59:59', strtotime('+1 month'));
		 }

		 //get entries now
		 $entriesURL = 'https://five.epicollect.net/api/export/entries/'.$project.'
		?branch_ref='.$branch.'&branch_owner_uuid='.$branch_owner_uuid;
		 $entriesClient = new Client([
		     'headers' => [
		         'Authorization' => 'Bearer '.$this->token //this will last for 2 hours!
		     ]
		 ]);


	     try {
	         $response = $entriesClient-> request('GET', $entriesURL);

	         $body = $response->getBody();
	         $obj = json_decode($body);

	         return $obj->data->entries;
	         

	     } catch (RequestException $e) {
	         //handle errors
	         //...
	        return false;
	     }
	}

	function get_form_with_branch($form, $branch, $start, $end)
	{
		$data = $this->get_form_data($form, $start, $end);
		foreach ($data as $key => $value) {
	        $branch = $this->get_branch_data($branch, $start, $end);
	        array_merge($data, $branch);
	    }

	    return $data;
	}

	function get_photos($project, $name)
	{
		$entriesURL = 'https://five.epicollect.net/api/export/media/'.$project.'
		?type=photo&format=entry_original&name='.$name;

		$entriesClient = new Client([
		     'headers' => [
		         'Authorization' => 'Bearer '.$this->token //this will last for 2 hours!
		     ]
		 ]);


	     try {
	         $response = $entriesClient-> request('GET', $entriesURL, ['sink' => 'uploads/'.$name]);
	         return true;
	         

	     } catch (RequestException $e) {
	         //handle errors
	         //...
	        return false;
	     }
	}

}