<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function create_fuel($car = false)
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing Received!']);

		if(!$car) $car = $this->input->post('car');
		$data = [
				'fuel_carid' => $car,
				'fuel_lts' => $this->input->post('lts'),
				'fuel_cost' => $this->input->post('cost'),
				'fuel_date' => $this->input->post('date'),
				'fuel_reading' => $this->input->post('reading'),
				'fuel_creator' => $this->session->userdata('user_id'),
				'fuel_createdate' => date('Y-m-d H:i:s')
			];

		if($this->db->insert('tbl_fueling', $data)) return json_encode(['status' => 1, 'msg' => 'Fueling posted successfully!']);

		else return json_encode(['status' => 0, 'msg' => 'Fueling could not be posted! Please try again later!']);
	}

	public function create_car()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing Received!']);

		$data = [
				'car_number' => $this->input->post('car_no'),
				'car_type' => $this->input->post('type'),
				'car_creator' => $this->session->userdata('user_id'),
				'car_createdate' => date('Y-m-d H:i:s')
			];

		if($this->db->insert('tbl_vehicles', $data)) {
			$id = $this->db->insert_id();
			$fuel = $this->input->post('lts');
			if($fuel) $this->create_fuel($id);
			return json_encode(['status' => 1, 'msg' => 'Car created successfully']);
		}

		else return json_encode(['status' => 0, 'msg' => 'Car could not be created! Please try again later!']);
	}

	public function update_car()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing Received!']);

		$car = $this->input->post('car');
		$data = [
				'car_number' => $this->input->post('car_no'),
				'car_type' => $this->input->post('type'),
				'car_updator' => $this->session->userdata('user_id')
			];

		if($this->db->update('tbl_vehicles', $data, ['car_id' => $car])) {
			return json_encode(['status' => 1, 'msg' => 'Car updated successfully']);
		}

		else return json_encode(['status' => 0, 'msg' => 'Car could not be updated! Please try again later!']);
	}
}