<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Appraisal_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function create()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing was received!']);

		$year = $this->input->post('year');
		$period = $this->input->post('period');
		$depts = implode(',', $this->input->post('depts'));

		$data = ['appraisal_year' => $year, 'appraisal_active' => 1, 'appraisal_period' => $period, 'appraisal_depts' => $depts];

		$rows = $this->db->get_where('tbl_app_appraisals', $data)->num_rows();

		if($rows) return json_encode(['status' => 0, 'msg' => 'Appraisal already exists!']);

		$data['appraisal_creator'] = $this->session->userdata('user_id');
		$data['appraisal_createdate'] = date('Y-m-d H:i:s');

		if($this->db->insert('tbl_app_appraisals', $data))
			return json_encode(['status' => 1, 'msg' => 'Appraisal created successfully!']);
		else
			return json_encode(['status' => 0, 'msg' => 'Appraisal could not be created!']);
	}

	public function update()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing was received!']);

		$year = $this->input->post('year');
		$period = $this->input->post('period');
		$depts = implode(',', $this->input->post('depts'));

		$data = ['appraisal_year' => $year, 'appraisal_active' => 1, 'appraisal_period' => $period, 'appraisal_depts' => $depts];
		$data['appraisal_updator'] = $this->session->userdata('user_id');

		if($this->db->update('tbl_app_appraisals', $data, ['appraisal_id' => $this->input->post('app_id')]))
			return json_encode(['status' => 1, 'msg' => 'Appraisal updated successfully!']);
		else
			return json_encode(['status' => 0, 'msg' => 'Appraisal could not be updated!']);
	}

	public function create_brand()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing was received!']);

		$name = $this->input->post('name');
		$amt = $this->input->post('amt');

		$data = ['ba_name' => $name, 'ba_active' => 1, 'ba_weightage' => $amt];

		$rows = $this->db->get_where('tbl_app_brandatts', $data)->num_rows();

		if($rows) return json_encode(['status' => 0, 'msg' => 'Attribute already exists!']);

		$data['ba_desc'] = $this->input->post('desc');
		$data['ba_creator'] = $this->session->userdata('user_id');
		$data['ba_createdate'] = date('Y-m-d H:i:s');

		if($this->db->insert('tbl_app_brandatts', $data))
			return json_encode(['status' => 1, 'msg' => 'Attribute created successfully!']);
		else
			return json_encode(['status' => 0, 'msg' => 'Attribute could not be created!']);
	}

	public function update_brand()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing was received!']);

		$name = $this->input->post('name');
		$amt = $this->input->post('amt');

		$data = ['ba_name' => $name, 'ba_active' => 1, 'ba_weightage' => $amt];
		$data['ba_desc'] = $this->input->post('desc');
		$data['ba_updator'] = $this->session->userdata('user_id');

		if($this->db->update('tbl_app_brandatts', $data, ['ba_id' => $this->input->post('ba_id')]))
			return json_encode(['status' => 1, 'msg' => 'Attribute updated successfully!']);
		else
			return json_encode(['status' => 0, 'msg' => 'Attribute could not be updated!']);
	}
}