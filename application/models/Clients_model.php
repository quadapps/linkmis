<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Clients_model
 * 
 * @package   
 * @author RAKMIS
 * @copyright Alfred Korir
 * @version 2014
 * @access public
 */
class Clients_model extends CI_Model
{
    /**
     * Clients_model::__construct()
     * 
     * @return
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Clients_model::create_client()
     * 
     * @return
     */
    public function create_client()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;

            $name = strtoupper($p->post('client_name'));
            if ($this->client_exists($name)) {
                $res = array('status' => 0, 'msg' => 'Client name already exists.');
            } else {
                $data = array(
                    'client_name' => $name,
                    'client_phone' => $p->post('client_phone'),
                    'client_email' => $p->post('client_email'),
                    'client_address' => $p->post('address'),
                    'client_contactperson' => $p->post('contact_person'),
                    'client_contactpersonphone' => $p->post('contact_phone'),
                    'client_contactpersonemail' => $p->post('contact_email'),
                    'client_marketerid' => $p->post('marketer'),
                    'client_catid' => $p->post('cat'),
                    'client_type' => strtoupper($p->post('type')),
                    'client_creator' => $this->session->userdata('user_id'));

                if ($this->db->insert('tbl_clients', $data)) {
                    $res = array('status' => 1, 'msg' => 'Client has been saved successfully');
                } else {
                    $res = array('status' => 0, 'msg' =>
                            'An error occured while saving client. Please try again.');
                }
            }
        }
        return json_encode($res);

    }

    /**
     * Clients_model::update_clients()
     * 
     * @return
     */
    public function update_clients()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('client_id');
            $p = $this->input;
            $data = array(
                'client_name' => $p->post('client_name'),
                'client_phone' => $p->post('client_phone'),
                'client_catid' => $p->post('cat'),
                'client_email' => $p->post('client_email'),
                'client_address' => $p->post('address'),
                'client_contactperson' => $p->post('contact_person'),
                'client_contactpersonphone' => $p->post('contact_phone'),
                'client_contactpersonemail' => $p->post('contact_email'),
                'client_marketerid' => $p->post('marketer'),
                'client_type' => strtoupper($p->post('type'))
              );

            if ($this->db->update('tbl_clients', $data, array('client_id' => $id))) {

                $res = array('status' => 1, 'msg' => 'Client has been updated successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while updating client. Please try again.');
            }

            return json_encode($res);

        }

    }


    /**
     * Clients_model::delete_clients()
     * 
     * @return
     */
    public function delete_clients($id)
    {
        
            $data = array('client_active' => 0, );

            if ($this->db->update('tbl_clients', $data, array('client_id' => $id))) {

                $res = array('status' => 1, 'msg' => 'Client has been deleted successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while deleting client. Please try again.');
            }
            return json_encode($res);

        }

            


    /**
     * Clients_model::create_suppliers()
     * 
     * @return
     */
    public function create_suppliers()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;

            $name = strtoupper($p->post('supplier_name'));
            if ($this->supplier_exists($name)) {
                $res = array('status' => 0, 'msg' => 'supplier name already exists.');
            } else {
                $data = array(
                    'supplier_name' => $name,
                    'supplier_phone' => $p->post('supplier_phone'),
                    'supplier_email' => $p->post('supplier_eamil'),
                    'supplier_address' => $p->post('supplier_address'),
                    'supplier_contactperson' => $p->post('supplier_contactperson'),
                    'supplier_contactpersonphone' => $p->post('supplier_contactpersonphone'),
                    'supplier_contactpersonemail' => $p->post('supplier_contactpersonemail'),
                    'supplier_creator' => $this->session->userdata('user_id'));

                if ($this->db->insert('tbl_suppliers', $data)) {
                    $res = array('status' => 1, 'msg' => 'supplier has been saved successfully');
                } else {
                    $res = array('status' => 0, 'msg' =>
                            'An error occured while saving supplier. Please try again.');
                }
            }
        }
        return json_encode($res);

    }

    /**
     * Clients_model::supplier_exists()
     * 
     * @param mixed $name
     * @return
     */
    private function supplier_exists($name)
    {
        $query = $this->db->get_where('tbl_suppliers', array('supplier_name' => $name));
        if ($query->num_rows() == 0)
            return false;
        else
            return true;
    }

    /**
     * Clients_model::update_suppliers()
     * 
     * @return
     */
    public function update_suppliers()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('supplier_id');
            $data = array(
                'supplier_name' => $name,
                'supplier_phone' => $p->post('supplier_phone'),
                'supplier_email' => $p->post('supplier_eamil'),
                'supplier_address' => $p->post('supplier_address'),
                'supplier_contactperson' => $p->post('supplier_contactperson'),
                'supplier_contactpersonphone' => $p->post('supplier_contactpersonphone'),
                'supplier_contactpersonemail' => $p->post('supplier_contactpersonemail'),
                'supplier_creator' => $this->session->userdata('user_id'));

            if ($this->db->update('tbl_suppliers', $data, array('supplier_id' => $id))) {

                $res = array('status' => 1, 'msg' => 'supplier has been updated successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while updating supplier. Please try again.');
            }

            return json_encode($res);

        }

    }
    /**
     * Clients_model::delete_suppliers()
     * 
     * @return
     */
    public function delete_suppliers()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('supplier_id');

            $data = array('supplier_active' => 0, );

            $query = $this->db->update('tbl_suppliers', $data, array('supplier_id' => $id));

            return json_encode($res);
        }
    }

    /**
     * Clients_model::client_exists()
     * 
     * @param mixed $name
     * @return
     */
    private function client_exists($name)
    {
        $query = $this->db->get_where('tbl_clients', array('client_name' => $name));
        if ($query->num_rows() == 0)
            return false;
        else
            return true;
    }
    
    public function create_debtor()
    {
         if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $data = array(
                    'pj_clientid' => $this->input->post('client'),
                    'pj_amt' => $this->input->post('amt'),
                    'pj_currency' => $this->input->post('currency'),
                    'pj_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_previousjobs', $data)) {
                $res = array('status' => 1, 'msg' => 'Debtor saved successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! Debtor could not be saved.');
            }
        }
        
        return json_encode($res);
    }
    
    public function pay_debt()
    {
         if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $data = array(
                    'pd_jobid' => $this->input->post('debt_id'),
                    'pd_amt' => $this->input->post('amt'),
                    'pd_mode' => $this->input->post('mode'),
                    'pd_remarks' => $this->input->post('remarks'),
                    'pd_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_previousdebts', $data)) {
                $res = array('status' => 1, 'msg' => 'Debt paid successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! Debt could not be saved.');
            }
        }
        
        return json_encode($res);
    }
    
    public function make_deposit()
    {
         if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            // $this->load->model('jobs_model');
            // //data from upload
            // $f = $this->jobs_model->do_upload();
            $data = array(
                    'deposit_amt' => $this->input->post('amt'),
                    'deposit_remarks' => $this->input->post('remarks'),
                    'deposit_mode' => $this->input->post('mode'),
                    'deposit_currency' => $this->input->post('currency'),
                    'deposit_date' => $this->input->post('date'),
                    'deposit_bank' => $this->input->post('bank'),
                    // 'deposit_acct' => $this->input->post('acct'),
                    // 'deposit_slipurl' => base_url('uploads/'.$f['file_name']),
                    // 'deposit_slipname' => $f['file_name'],
                    'deposit_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_deposits', $data)) {
                $res = array('status' => 1, 'msg' => 'Deposit paid successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! Deposit could not be saved.');
            }
        }
        
        return json_encode($res);
    }
    
    public function make_withdrawal()
    {
         if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            // $this->load->model('jobs_model');
            // //data from upload
            // $f = $this->jobs_model->do_upload();
            $data = array(
                    'withdrawal_amt' => $this->input->post('amt'),
                    'withdrawal_remarks' => $this->input->post('remarks'),
                    'withdrawal_mode' => $this->input->post('mode'),
                    'withdrawal_currency' => $this->input->post('currency'),
                    'withdrawal_date' => $this->input->post('date'),
                    'withdrawal_bank' => $this->input->post('bank'),
                    // 'withdrawal_acct' => $this->input->post('acct'),
                    // 'withdrawal_slipurl' => base_url('uploads/'.$f['file_name']),
                    // 'withdrawal_slipname' => $f['file_name'],
                    'withdrawal_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_withdrawals', $data)) {
                $res = array('status' => 1, 'msg' => 'withdrawal saved successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! withdrawal could not be saved.');
            }
        }
        
        return json_encode($res);
    }
    
    public function refund()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $pmt_id = $this->input->post('pmt_id');
            $refund_amt = $this->input->post('refund_amt');
            $paid_amt = get_that_data('tbl_payments', 'payment_amt', 'payment_id', $pmt_id);
            $new_amt = $paid_amt - $refund_amt;
            
            if($this->db->update('tbl_payments', array('payment_amt' => $new_amt), array('payment_id' => $pmt_id))) {
                $res = array('status' => 1, 'msg' => 'Payment refunded successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! Payment could not be refunded. Please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function reverse()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $pmt_id = $this->input->post('pmt_id');
            
            if($this->db->delete('tbl_payments', array('payment_id' => $pmt_id))) {
                $res = array('status' => 1, 'msg' => 'Payment reversed successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! Payment could not be reversed. Please try again later.');
            }
        }
        
        return json_encode($res);
    }

    public function create_log()
    {
        if (empty($_POST)) {
            return json_encode(array('status' => 0, 'msg' => 'We did not recieve anything.'));;
        } else {
            $data = array(
                    'log_clientid' => $this->input->post('client'),
                    'log_date' => $this->input->post('date'),
                    'log_title' => $this->input->post('title'),
                    'log_type' => $this->input->post('type'),
                    'log_details' => $this->input->post('details'),
                    'log_tags' => implode(',', $this->input->post('tags')),
                    'log_createdate' => date('Y-m-d H:i:s'),
                    'log_creator' => $this->session->userdata('user_id')
                );
            
            if($this->db->insert('tbl_clientlogs', $data)) {
                $is_tasked = $this->input->post('create_task');
                if ($is_tasked == 1) {
                    $details = array(
                        'task_creator' => $this->session->userdata('user_id'),
                        'task_clientid' => $this->input->post('client'),
                        'task_priority' => 'MEDIUM',
                        'task_description' => $data['log_type'].' follow up on '.$this->input->post('title'),
                        'task_start' => $this->input->post('date'),
                        'task_end' => $this->input->post('end'),
                        'task_tags' => implode(',', $this->input->post('tags')),
                        'task_createdate' => date('Y-m-d_H-i-s')
                    );
                    $this->load->model('users_model');
                    $this->users_model->create_task($details);
                }
                return json_encode(array('status' => 1, 'msg' => 'Log saved successfully'));
            } else {
                return json_encode(array('status' => 0, 'msg' => 'Error! Log could not be saved. Please try again later.'));;
            }
        }
    }

    public function create_quote()
    {
        if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything to save.');
         } else {
            $p = $this->input;
            
            $client = $p->post('client');
            $items = array(
                    'desc' => $p->post('desc'),
                    'qty' => $p->post('qty'),
                    'price' => $p->post('amt')
                );
            $data = array(
                    'quotation_clientid' => $client,
                    'quotation_ref' => $p->post('ref'),
                    'quotation_currency' => $p->post('currency'),
                    'quotation_terms' => preg_replace("/\r\n|\r|\n/",'<br/>',$p->post('terms')),
                    'quotation_items' => json_encode($items),
                    'quotation_createdate' => date('Y-m-d H:i:a'),
                    'quotation_creator' => $this->session->userdata('user_id')
                );
                
            if($this->db->insert('tbl_quotations', $data)) {
                $id = $this->db->insert_id();
                
                //get recipeints
                // $to = $this->get_recipients($id);
                $url = site_url('welcome/view/quotation/'.$id);
                
                // @send_mail($to, 'RAKMIS NEW JQUOTATION', 'A new quotation has been created please visit the link below to view details <br/>', '', $url, 'View it here');
                
                $res = array('status' => 1, 'msg' => 'Quote saved successfully.', 'id' => $id, 'url' => $url);
            } else {
                $res = array('status' => 0, 'msg' => 'An error occured while saving quote. Please try again later.');
            }
        }
        
        return json_encode($res);
    }

    public function import_bank_transactions()
    {
        if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);

        // $this->load->model('jobs_model');
        //     //data from upload
        // $f = $this->jobs_model->do_upload();
        // if($f['file_ext'] != '.csv')  {
        //     unlink($f['full_path']);
        //     return json_encode(['status' => 0, 'msg' => 'Invalid file. Please choose a file like the template!']);
        // }

        $bank = $this->input->post('bank');
        $currency = get_that_data('tbl_banks', 'bank_currency', 'bank_id', $bank);

        $csv = array_map('str_getcsv', $_FILES["userfile"]["tmp_name"]);
        array_walk($csv, function(&$a) use ($csv) {
          $a = array_combine($csv[0], $a);
        });
        array_shift($csv); # remove column header
        //print_r($csv);
        $deposits = ['amt' => array_column($csv, 'WITHDRAWAL AMT'), 'date' => array_column($csv, 'DATE'), 'ref' => array_column($csv, 'REFERENCE'), 'mode' => array_column($csv, 'MODE')];
        //print_r($deposits); exit();
        $data = [];
        for($i=0; $i<count($deposits['amt']); $i++) {
            if(!empty($deposits['amt'][$i])) {
                $data[] = [
                        'withdrawal_amt' => $deposits['amt'][$i],
                        'withdrawal_bank' => $bank,
                        'withdrawal_currency' => $currency,
                        'withdrawal_date' => $deposits['date'][$i],
                        'withdrawal_ref' => $deposits['ref'][$i],
                        'withdrawal_mode' => $deposits['mode'][$i],
                        'withdrawal_creator' => $this->session->userdata('user_id'),
                        'withdrawal_createdate' => date('Y-m-d H:i:s')
                    ];
            } 
        }
        //print_r($data); exit;
        $this->db->insert_batch('tbl_withdrawals', $data);

        $deposits = ['amt' => array_column($csv, 'DEPOSIT AMT'), 'date' => array_column($csv, 'DATE'), 'ref' => array_column($csv, 'REFERENCE'), 'mode' => array_column($csv, 'MODE')];
        $data = [];
        for($i=0; $i<count($deposits['amt']); $i++) {
            if(!empty($deposits['amt'][$i])) {
                $data[] = [
                        'deposit_amt' => $deposits['amt'][$i],
                        'deposit_bank' => $bank,
                        'deposit_currency' => $currency,
                        'deposit_date' => $deposits['date'][$i],
                        'deposit_mode' => $deposits['mode'][$i],
                        'deposit_creator' => $this->session->userdata('user_id'),
                        'deposit_createdate' => date('Y-m-d H:i:s')
                    ];
            } 
        }
        $this->db->insert_batch('tbl_deposits', $data);

        $deposits = ['amt' => array_column($csv, 'BANK CHARGES'), 'date' => array_column($csv, 'DATE'), 'ref' => array_column($csv, 'REFERENCE'), 'mode' => array_column($csv, 'MODE')];
        $data = [];
        for($i=0; $i<count($deposits); $i++) {
            if(!empty($deposits['amt'][$i])) {
                $data[] = [
                        'bc_amt' => $deposits['amt'][$i],
                        'bc_bankid' => $bank,
                        'bc_currency' => $currency,
                        'bc_date' => $deposits['date'][$i],
                        'bc_creator' => $this->session->userdata('user_id'),
                        'bc_createdate' => date('Y-m-d H:i:s')
                    ];
            } 
        }
        $this->db->insert_batch('tbl_bankingcharges', $data);

        return json_encode(['status' => 1, 'msg' => count($csv).' records uploaded']);
    }
}
