<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

    /**
     * 
     */
    class Finance_model extends CI_Model
    {
    	
    	function __construct()
    	{
    		parent::__construct();
    	}


    	function confirm_quote()
    	{
    		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);

    		$quote = $this->input->post('quote');
    		$po = $this->input->post('po');
    		$remarks = $this->input->post('remarks');

    		$this->db->update('tbl_quotations', ['quotation_status' => 1], ['quotation_id' => $quote]);
            $items = get_that_data('tbl_quotations', 'quotation_items', 'quotation_id', $quote);
            $arr = json_decode($items, true);
            $amt = 0;
            for($i=0; $i<count($arr['price']); $i++) {
                $amt += $arr['price'][$i]*$arr['qty'][$i];
            }
    		$data = [
    			'invoice_quoteid' => $quote,
    			'invoice_remarks' => $remarks,
    			'invoice_po' => $po,
    			'invoice_items' => $items,
                'invoice_value' => $amt,
    			'invoice_creator' => $this->session->user_id
    		];

    		if($this->db->insert('tbl_invoices', $data)) return json_encode(['status' => 1, 'msg' => 'Confirmed and invoce created']);

    		else return json_encode(['status' => 0, 'msg' => 'Error occured Please try again later']);
    	}

    	public function cancel_quote()
	    {
	        if(empty($_REQUEST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);
	        
	            $job_id = $this->input->post('quote');
	            $data['quotation_cancelremarks'] = $this->input->post('remarks');
	            $data['quotation_cancelby'] = $this->session->userdata('user_id');
	            $data['quotation_status'] = -1;
	            $data['quotation_canceldate'] = date('Y-m-d H:i:s');
	            if($this->db->update('tbl_quotations', $data, array('quotation_id' => $job_id))) {
	                $res = array('status' => 1, 'msg' => 'quotation Cancelled Successfully.');
	            } else {
	                $res = array('status' => 0, 'msg' => 'Error! quotation could not be cancelled');
	            }
	        
	        
	        return json_encode($res);
	    }

        public function pay_bill()
        {
            if(empty($_REQUEST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);
            
                $job_id = $this->input->post('exp');
                $data['payment_remarks'] = $this->input->post('remarks');
                $data['payment_creator'] = $this->session->userdata('user_id');
                $data['payment_amt'] = $this->input->post('amt');
                // $data['payment_payer'] = $this->input->post('payer');
                $data['payment_mode'] = $this->input->post('mode');
                $data['payment_txn'] = $this->input->post('txn');
                $data['payment_expid'] = $job_id;
                $data['payment_createdate'] = date('Y-m-d H:i:s');
                if($this->db->insert('tbl_payments', $data)) {
                    $res = array('status' => 1, 'msg' => 'Payment posted Successfully.');
                    if($data['payment_amt'] == $this->input->post('pmt_amt'))
                        $this->db->update('tbl_invoices', ['invoice_status' => 1], ['invoice_id => $job_id']);
                } else {
                    $res = array('status' => 0, 'msg' => 'Error! quotation could not be cancelled');
                }
            
            
            return json_encode($res);
        }

        public function create_exp()
        {
            if(empty($_REQUEST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);
            
                $job_id = $this->input->post('invoice');
                $data['exp_remarks'] = $this->input->post('remarks');
                $data['exp_creator'] = $this->session->userdata('user_id');
                $data['exp_amt'] = $this->input->post('amt');
                $data['exp_currency'] = $this->input->post('currency');
                $data['exp_catid'] = $this->input->post('cat');
                $data['exp_deptid'] = $this->input->post('dept');
                $data['exp_name'] = $this->input->post('name');
                $data['exp_billno'] = $this->input->post('bill_no');
                $data['exp_billdate'] = $this->input->post('bill_date');
                $data['exp_epd'] = $this->input->post('epd');
                $data['exp_details'] = $this->input->post('details');
                $data['exp_acctnumber'] = $this->input->post('acct');
                $data['exp_type'] = $this->input->post('type');
                $data['exp_createdate'] = date('Y-m-d H:i:s');
                if($this->db->insert('tbl_expenses', $data)) {
                    $res = array('status' => 1, 'msg' => 'Expense posted Successfully.');
                    // $hod = get_that_data('tbl_employees', "emp_email", 'emp_id', get_that_data('tbl_departments', 'dept_empid', 'dept_id', $data['exp_deptid']));
                    $mails = get_data('tbl_employees', "JOIN tbl_departments ON dept_id = emp_deptid JOIN tbl_users ON user_empid = emp_id WHERE emp_active = 1 AND user_levelid < 4 AND (emp_designation LIKE 'FINANCE MANAGER' OR emp_designation LIKE 'HEAD OF LINKS PAY' OR dept_empid = ".$this->session->userdata('emp_id').")", 'emp_email');

                    $hod = [];
                    foreach($mails as $e) $hod[] = $e['emp_email'];

                    @send_mail($hod, 'New Request made', 'New payment request is awaiting your approval. Please login to the system to approve', false, false, site_url('welcome/view/expenses'), 'View');
                } else {
                    $res = array('status' => 0, 'msg' => 'Error! Expense could not be created');
                }
            
            
            return json_encode($res);
        }

        public function reconcile()
        {
            if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Error! Nothing received']);

            $Comments = $this->input->post('comments');
            $id = $this->input->post('tdr_id');

            $this->db->update('tbl_recons', ['recon_comments' => $Comments], ['recon_id' => $id]);

            return json_encode(['status' => 1, 'msg' => 'Reconciled succcessfully!']);
        }

        public function confirm_collection()
        {
            if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Error! Nothing received']);

            $Comments = $this->input->post('comments');
            $id = $this->input->post('coll_id');
            $amt = $this->input->post('amt');

            $data = [
                'collection_remarks' => $Comments,
                // 'collection_ssprec' => $this->input->post('ssp_amt'),
                // 'collection_usdrec' => $this->input->post('usd_amt'),
                'collection_amtrec' => $amt,
                'collection_status' => 2,
                'collection_cashier' => $this->session->user_id
            ];

            $this->db->update('tbl_collections', $data, ['collection_id' => $id]);

            $data = get_row_data('tbl_collections', 'collection_id', $id);

            $last = get_data('tbl_cashbook', "WHERE cb_float = 0 AND cb_type = 1 AND cb_typeid = $id ORDER BY cb_id DESC LIMIT 1", 'cb_linebal, cb_id, cb_amt', true);

            if(!empty($last) && $last['cb_amt'] != $amt) {
                $bal = $last['cb_linebal']-$last['cb_amt']+$amt;
                $this->db->update('tbl_cashbook', ['cb_amt' => $amt, 'cb_linebal' => $bal], ['cb_id' => $last['cb_id']]);
            } else {
                $cb = ['cb_amt' => $amt, 'cb_linebal' => $amt, 'cb_float' => 0, 'cb_type' => 1, 'cb_typeid' => $id, 'cb_currency' => $data['collection_currency']];
                $this->db->insert('tbl_cashbook', $cb);
            }

                //$this->create_cb($id, 1, 0, $data['collection_currency']);

                $d = ['ar_date' => date('Y-m-d'), 'ar_empid' => $data['collection_agent'], 'ar_currency' => $data['collection_currency']];

                $rec = $this->db->get_where('tbl_agentrecons', $d)->row_array();
                if(empty($rec)) {
                    $this->db->insert('tbl_agentrecons', $d);
                    $rec = $this->db->get_where('tbl_agentrecons', ['ar_id' => $this->db->insert_id()])->row_array();
                }
                if($data['collection_type'] == 'Float') {
                    $fb = (int)$rec['ar_fb'] - (int)$data['collection_amt'];
                    // $cb = $rec['ar_cb']-$data['at_amt'];
                    $this->db->update('tbl_agentrecons', ['ar_fb' => $fb], ['ar_id' => $rec['ar_id']]);
                }

                if($data['collection_type'] == 'Cash') {
                    // $fb = $rec['ar_fb']-$data['at_amt'];
                    $cb = (int)$rec['ar_cb'] - (int)$data['collection_amt'];
                    $this->db->update('tbl_agentrecons', ['ar_cb' => $cb], ['ar_id' => $rec['ar_id']]);
                }

            return json_encode(['status' => 1, 'msg' => 'Confirmed succcessfully!']);
        }

        public function create_cashout()
        {
            if(empty($_REQUEST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);
            
                $data['co_agentcode'] = $this->input->post('code');
                $data['co_creator'] = $this->session->userdata('user_id');
                // $data['co_sspamt'] = $this->input->post('ssp_amt');
                // $data['co_usdamt'] = $this->input->post('usd_amt');
                // $data['co_sspfloat'] = $this->input->post('ssp_float');
                // $data['co_usdfloat'] = $this->input->post('usd_float');
                $data['co_collector'] = $this->input->post('collector');
                $data['co_currency'] = $this->input->post('currency');
                $data['co_amt'] = $this->input->post('amt');
                $data['co_type'] = $this->input->post('type');
                $data['co_agentline'] = $this->input->post('line');
                $data['co_agentname'] = $this->input->post('name');
                $data['co_createdate'] = $this->input->post('date');

                if($data['co_collector'] == 0)
                    $data['co_status'] = 1;

                if($this->db->insert('tbl_cashouts', $data)) {
                    $id = $this->db->insert_id();
                    $res = array('status' => 1, 'msg' => 'Cash Out posted Successfully.', 'url' => site_url('welcome/view/agent_reqs'));
                    

                    // $this->create_cb($id, 2, 0, $data['co_currency']);

                    // $last = get_data('tbl_cashbook', "WHERE cb_float = 0 AND cb_type = 1 AND cb_typeid = 2 ORDER BY cb_id DESC LIMIT 1", 'cb_linebal, cb_id, cb_amt', true);

                    // if(!empty($last) && $last['cb_amt'] != $amt) {
                    //     $bal = $last['cb_linebal']-$last['cb_amt']-$amt;
                    //     $this->db->update('tbl_cashbook', ['cb_amt' => $amt, 'cb_linebal' => $bal], ['cb_id' => $last['cb_id']]);
                    // } else {
                    //     $cb = ['cb_amt' => $amt, 'cb_linebal' => $amt, 'cb_float' => 0, 'cb_type' => 1, 'cb_typeid' => $id, 'cb_currency' => $data['co_currency']];
                    //     $this->db->insert('tbl_cashbook', $cb);
                    // }

                    // //$this->create_cb($id, 1, 0, $data['collection_currency']);

                    // $d = ['ar_date' => date('Y-m-d'), 'ar_empid' => $data['co_agentname'], 'ar_currency' => $data['co_currency']];

                    // $rec = $this->db->get_where('tbl_agentrecons', $d)->row_array();
                    // if(empty($rec)) {
                    //     $this->db->insert('tbl_agentrecons', $d);
                    //     $rec = $this->db->get_where('tbl_agentrecons', ['ar_id' => $this->db->insert_id()])->row_array();
                    // }
                    // if($data['collection_type'] == 'Float') {
                    //     $fb = (int)$rec['ar_fb'] + (int)$data['co_amt'];
                    //     // $cb = $rec['ar_cb']-$data['at_amt'];
                    //     $this->db->update('tbl_agentrecons', ['ar_fb' => $fb], ['ar_id' => $rec['ar_id']]);
                    // }

                    // if($data['collection_type'] == 'Cash') {
                    //     // $fb = $rec['ar_fb']-$data['at_amt'];
                    //     $cb = (int)$rec['ar_cb'] + (int)$data['co_amt'];
                    //     $this->db->update('tbl_agentrecons', ['ar_cb' => $cb], ['ar_id' => $rec['ar_id']]);
                    // }

                } else {
                    $res = array('status' => 0, 'msg' => 'Error! Cash Out could not be created');
                }
            
            
            return json_encode($res);
        }

        public function process_cashout()
        {
            $how = $this->input->post('how');
            $this->db->update('tbl_agentreqs', ['co_status' => $how, 'co_updator' => $this->session->user_id], ['co_id' => $this->input->post('co_id')]);

            if($how == 1) return $this->create_cashout();

            return json_encode(['status' => 1, 'Process completed succcessfully!']);
        }

        public function create_agent_request()
        {
            if(empty($_REQUEST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);
                $currency = $this->input->post('currency');
                $amt = $this->input->post('amt');
                $name = $this->input->post('agent');
                $data['co_currency'] = $currency;
                $data['co_amt'] = $amt;
                $data['co_type'] = $this->input->post('type');
                $data['co_agentname'] = $this->input->post('name');
                $data['co_createdate'] = date('Y-m-d H:i:s');
                if($this->db->insert('tbl_agentreqs', $data)) {
                    $id = $this->db->insert_id();
                    $res = array('status' => 1, 'msg' => 'Request posted Successfully.');
                    $type = $data['co_type'] == 0 ? 'Cash' : 'Float';
                    $msg = "A $type request for $currency $amt has been made by $name . Please process immediately!";
                    @send_mail(['liban@links-pay.com', 'amirali.khoja@links-pay.com'], "New Agent $type Request", $msg, false, false, site_url('welcome/view/agent_reqs/'.$id), 'Login Now');
                } else {
                    $res = array('status' => 0, 'msg' => 'Error! Request could not be created');
                }
            
            
            return json_encode($res);
        }

        private function create_cb($id, $typeid, $type, $currency=false, $amt=false)
        {
            if($amt == false) $amt = $this->input->post('amt');
            if($currency == false) $currency = $this->input->post('currency');

            $last = get_data('tbl_cashbook', "WHERE cb_float = $type ORDER BY cb_id DESC LIMIT 1", 'cb_linebal', true);
            empty($last) ? $last = 0 : $last = $last['cb_linebal'];
            $bal = $typeid == 1 ? $last+$amt : $last-$amt;
            $data = [
                'cb_typeid' => $id,
                'cb_amt' => $amt,
                'cb_type' => $typeid,
                'cb_currency' => $currency,
                'cb_linebal' => $bal,
                'cb_float' => $type,
                'cb_creator' => $this->session->user_id,
                'cb_createdate' => date('Y-m-d H:i:s')
            ];
            $typeid == 1 ? $data['cb_cashin'] = $amt : $data['cb_cashout'] = $amt;

            return $this->db->insert('tbl_cashbook', $data);
        }

        public function act_expense()
        {
            if(empty($_REQUEST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);

            $data['exp_status'] = $this->input->post('type');
            $data['exp_rejectremarks'] = $this->input->post('remarks');

            $id = $this->input->post('act_id');

            if($data['exp_status'] > 0)
                return $this->approve_request($id);

            else {
                if($this->db->update('tbl_expenses', $data, ['exp_id' => $id]))
                    return json_encode(['status' => 1, 'msg' => 'Action received']);

                else
                    return json_encode(['status' => 0, 'msg' => 'Action not received']);
            }
        }

        public function approve_request($typeid = false)
     {
        if (empty($_POST)) {
            return json_encode(array('status' => 0, 'msg' => 'Sorry we did not receive anything!'));
        }

        $data['ra_typeid'] = $typeid == false ? $this->input->post('typeid') : $typeid;
        $data['ra_remarks'] = $this->input->post('remarks');
        $data['ra_empid'] = $this->session->userdata('emp_id');
        $data['ra_designation'] = get_that_data('tbl_employees', 'emp_designation', 'emp_id', $data['ra_empid']);
        $data['ra_createdate'] = date('Y-m-d H:i:s');

        if ($this->db->insert('tbl_reqapprovals', $data)) {
            return json_encode(array('status' => 1, 'msg' => 'Action Applied', 'url' => site_url('welcome/view/expenses')));
        }

        else return json_encode(array('status' => 1, 'msg' => 'Action Impossible'));
     }

         public function bulk_approve_request()
         {
            if (empty($_POST)) return json_encode(array('status' => 0, 'msg' => 'Sorry we did not receive anything!'));

            $typeids = $this->input->post('typeid');
            $typeids = rtrim($typeids, ',');
            $arr = explode(',', $typeids);
            foreach ($arr as $key => $value) {
                $this->approve_request($value);
            }

            return json_encode(array('status' => 1, 'msg' => count($arr).' items approved'));
         }

         public function cashout_action()
         {
            if (empty($_POST)) return json_encode(array('status' => 0, 'msg' => 'Sorry we did not receive anything!'));

            $id = $this->input->post('id');
            $status = $this->input->post('status');

            $this->db->update('tbl_cashouts', ['co_status' => $status, 'co_updator' => $this->session->user_id], ['co_id' => $id]);

            if($status == 2) {
                $data = get_row_data('tbl_cashouts', 'co_id', $id);
                $this->create_cb($id, 2, $data['co_type'], $data['co_currency'], $data['co_amt']);

                    $d = ['ar_date' => date('Y-m-d', strtotime($data['co_createdate'])), 'ar_empid' => $data['co_agentname'], 'ar_currency' => $data['co_currency']];
            
                    $rec = $this->db->get_where('tbl_agentrecons', $d)->row_array();
                    if(empty($rec)) {
                        $this->db->insert('tbl_agentrecons', $d);
                        $rec = $this->db->get_where('tbl_agentrecons', ['ar_id' => $this->db->insert_id()])->row_array();
                    }
                    if($data['co_type'] == 1) { //Float
                        $fb = (int)$rec['ar_fb'] + (int)$data['co_amt'];
                        $this->db->update('tbl_agentrecons', ['ar_fb' => $fb], ['ar_id' => $rec['ar_id']]);
                    }

                    if($data['co_type'] == 0) { //Cash
                        // $fb = $rec['ar_fb']-$data['at_amt'];
                        $cb = (int)$rec['ar_cb'] + (int)$data['co_amt'];
                        $this->db->update('tbl_agentrecons', ['ar_cb' => $cb], ['ar_id' => $rec['ar_id']]);
                    }
            }

            return 'Operation Successfull';
        }

        public function create_txn()
        {
            if (empty($_POST)) return json_encode(array('status' => 0, 'msg' => 'Sorry we did not receive anything!'));

            $agent = get_row_data('tbl_agents', 'agent_empid', $this->session->emp_id);
            $data = [
                    'at_code' => $agent['agent_code'],
                    'at_line' => $agent['agent_line'],
                    'at_amt' => $this->input->post('amt'),
                    'at_date' => $this->input->post('date'),
                    'at_txn' => $this->input->post('txn'),
                    'at_natid' => $this->input->post('natid'),
                    'at_currency' => $this->input->post('currency'),
                    'at_type' => $this->input->post('type'),
                    'at_creator' => $this->session->user_id,
                    'at_createdate' => date('Y-m-d H:i:s')
                ];

            $this->db->insert('tbl_agenttxns', $data);

            $d = ['ar_date' => $data['at_date'], 'ar_empid' => $this->session->emp_id, 'ar_currency' => $data['at_currency']];
            $rec = $this->db->get_where('tbl_agentrecons', $d)->row_array();
            if(empty($rec)) {
                $this->db->insert('tbl_agentrecons', $d);
                $rec = $this->db->get_where('tbl_agentrecons', ['ar_id' => $this->db->insert_id()])->row_array();
            }
            if($data['at_type'] == 'Withdrawal') {
                $fb = (int)$rec['ar_fb'] + (int)$data['at_amt'];
                $cb = (int)$rec['ar_cb'] - (int)$data['at_amt'];
            }

            if($data['at_type'] == 'Deposit') {
                $fb = (int)$rec['ar_fb'] - (int)$data['at_amt'];
                $cb = (int)$rec['ar_cb'] + (int)$data['at_amt'];
            }

            $this->db->update('tbl_agentrecons', ['ar_fb' => $fb, 'ar_cb' => $cb], ['ar_id' => $rec['ar_id']]);

            return json_encode(['status' => 1, 'msg' => 'Transaction posted successfully!']);
        }

        public function reverse_txn()
        {
            if (empty($_POST)) return json_encode(array('status' => 0, 'msg' => 'Sorry we did not receive anything!'));

            // $agent = get_row_data('tbl_agents', 'agent_empid', $this->session->emp_id);
            $data = get_row_data('tbl_agenttxns', 'at_id', $this->input->post('id'));

            $this->db->update('tbl_agenttxns', ['at_active' => 0], ['at_id' => $data['at_id']]);

            $d = ['ar_date' => $data['at_date'], 'ar_empid' => $this->session->emp_id, 'ar_currency' => $data['at_currency']];
            $rec = $this->db->get_where('tbl_agentrecons', $d)->row_array();
            // if(empty($rec)) {
            //     $this->db->insert('tbl_agentrecons', $d);
            //     $rec = $this->db->get_where('tbl_agentrecons', ['ar_id' => $this->db->insert_id()])->row_array();
            // }
            if($data['at_type'] == 'Withdrawal') {
                $fb = (int)$rec['ar_fb'] - (int)$data['at_amt'];
                $cb = (int)$rec['ar_cb'] + (int)$data['at_amt'];
            }

            if($data['at_type'] == 'Deposit') {
                $fb = (int)$rec['ar_fb'] + (int)$data['at_amt'];
                $cb = (int)$rec['ar_cb'] - (int)$data['at_amt'];
            }

            $this->db->update('tbl_agentrecons', ['ar_fb' => $fb, 'ar_cb' => $cb], ['ar_id' => $rec['ar_id']]);

            return json_encode(['status' => 1, 'msg' => 'Transaction reveresed successfully!']);
        }

        public function create_collection()
        {
            if (empty($_POST)) return json_encode(array('status' => 0, 'msg' => 'Sorry we did not receive anything!'));

            $data = [
                    'collection_amt' => $this->input->post('amt'),
                    'collection_type' => $this->input->post('type'),
                    'collection_currency' => $this->input->post('currency'),
                    'collection_collector' => $this->input->post('collector'),
                    'collection_agent' => $this->session->emp_id,
                    'collection_creator' => $this->session->user_id,
                    'collection_createdate' => date('Y-m-d H:i:s')
                ];

            $this->db->insert('tbl_collections', $data);
            
            

            return json_encode(['status' => 1, 'msg' => 'Transaction posted successfully!']);
        }

        public function collect_cash()
        {
            if (empty($_POST)) return json_encode(array('status' => 0, 'msg' => 'Sorry we did not receive anything!'));

            $id = $this->input->post('id');
            $status = $this->input->post('status');

            $data = [
                    'collection_status' => $status,
                    'collection_collector'=> $this->session->user_id
                ];

            $this->db->update('tbl_collections', $data, ['collection_id' => $id]);

            if($status == 1) {
                $data = get_row_data('tbl_collections', 'collection_id', $id);
                $this->create_cb($id, 1, 0, $data['collection_currency']);

                $d = ['ar_date' => date('Y-m-d', strtotime($data['collection_createdate'])), 'ar_empid' => $this->session->emp_id, 'ar_currency' => $data['collection_currency']];

                $rec = $this->db->get_where('tbl_agentrecons', $d)->row_array();
                if(empty($rec)) {
                    $this->db->insert('tbl_agentrecons', $d);
                    $rec = $this->db->get_where('tbl_agentrecons', ['ar_id' => $this->db->insert_id()])->row_array();
                }
                if($data['collection_type'] == 'Float') {
                    $fb = (int)$rec['ar_fb'] - (int)$data['collection_amt'];
                    // $cb = $rec['ar_cb']-$data['at_amt'];
                    $this->db->update('tbl_agentrecons', ['ar_fb' => $fb], ['ar_id' => $rec['ar_id']]);
                }

                if($data['collection_type'] == 'Cash') {
                    // $fb = $rec['ar_fb']-$data['at_amt'];
                    $cb = (int)$rec['ar_cb'] - (int)$data['collection_amt'];
                    $this->db->update('tbl_agentrecons', ['ar_cb' => $cb], ['ar_id' => $rec['ar_id']]);
                }

            }

            return 'Operation Successfull';
        }

        public function create_agent()
        {
            if (empty($_POST)) return json_encode(array('status' => 0, 'msg' => 'Sorry we did not receive anything!'));
            $emp = $this->input->post('emp_id');
            $data = [
                    'agent_empid' => $emp,
                    'agent_name' => $this->input->post('name'),
                    'agent_code' => $this->input->post('code'),
                    'agent_line' => $this->input->post('line'),
                    'agent_creator' => $this->session->user_id,
                    'agent_createdate' => date('Y-m-d H:i:s')
                ];

            $this->db->insert('tbl_agents', $data);

            return json_encode(array('status' => 1, 'msg' => 'Agent created successfully.'));
        }
    }