<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/**
 * Hr_model
 * 
 * @package   RAKMIS
 * @author Larry Wambua
 * @modified by Alf Rirok
 * @copyright 2014
 * @version 1
 * @access public
 */
class Hr_model extends CI_Model
{
    /**
     * Hr_model::__construct()
     * 
     * @return
     */
    public function __construct()
    {

        parent::__construct();
        date_default_timezone_set('Africa/Nairobi');
    }


    /**
     * Hr_model::create_department()
     * 
     * @return
     */
    public function create_dept()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;

            $name = strtoupper($p->post('dept'));
            if ($this->department_exists($name)) {
                $res = array('status' => 0, 'msg' => 'Department name already exists.');
            } else {
                $data = array('dept_name' => $name, 'dept_empid' => $this->input->post('hod'), 'dept_creator' => $this->session->userdata('user_id'));

                if ($this->db->insert('tbl_departments', $data)) {
                    $res = array('status' => 1, 'msg' => 'department has been saved successfully');
                } else {
                    $res = array('status' => 0, 'msg' =>
                            'An error occured while saving department. Please try again.');
                }
            }
        }
        return json_encode($res);

    }

    /**
     * Hr_model::department_exists()
     * 
     * @param mixed $name
     * @return
     */
    private function department_exists($name)
    {
        $query = $this->db->get_where('tbl_departments', array('dept_name' => $name));
        if ($query->num_rows() == 0)
            return false;
        else
            return true;
    }

    /**
     * Hr_model::update_departments()
     * 
     * @return
     */
    public function update_dept()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('dept_id');
            $data = array('dept_name' => $this->input->post('dept'), 'dept_empid' => $this->input->post('hod'));

            if ($this->db->update('tbl_departments', $data, array('dept_id' => $id))) {

                $res = array('status' => 1, 'msg' => 'Department has been updated successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while updating dept. Please try again.');
            }
        }
        return json_encode($res);

    }


    /**
     * Hr_model::delete_departments()
     * 
     * @return
     */
    public function delete_departments($id)
    {
            $data = array('dept_active' => 0, );

            if($query = $this->db->update('tbl_departments', $data, array('dept_id' => $id))){
                $res = array('status' => 1, 'msg' => 'department has been deleted successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while deleting supplier. Please try again.');
            }

            return json_encode($res);
    }


    /**
     * Hr_model::create_educationalbackground()
     * 
     * @return
     */
    public function create_educationalbackground()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $data = array(
                'edu_degree' => $p->post('degree'),
                'edu_institution' => $p->post('institution'),
                'edu_majors' => $p->post('majors'),
                'edu_from' => $p->post('from'),
                'edu_to' => $p->post('to'),
                'edu_empid' => $p->post('emp')
                );

            if ($this->db->insert('tbl_educationalbackground', $data)) {
                $res = array('status' => 1, 'msg' =>
                        'Educational background has been saved successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while saving educational background. Please try again.');
            }

        }
        return json_encode($res);

    }

    /**
     * Hr_model::update_educationalbackgrounds()
     * 
     * @return
     */
    public function update_educationalbackground()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('edu_id');
            $data = array(
                'edu_degree' => $p->post('degree'),
                'edu_institution' => $p->post('institution'),
                'edu_majors' => $p->post('majors'),
                'edu_from' => $p->post('from'),
                'edu_to' => $p->post('to')
                );

            if ($this->db->update('tbl_educationalbackground', $data, array('edu_id' =>
                    $id))) {

                $res = array('status' => 1, 'msg' =>
                        'educational background has been updated successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while updating background. Please try again.');
            }

            return json_encode($res);

        }

    }


    /**
     * Hr_model::delete_educationalbackgrounds()
     * 
     * @return
     */
    public function delete_educationalbackgrounds()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('educationalbackground_id');

            $data = array('educationalbackground_active' => 0, );

            if ($this->db->update('tbl_educationalbackground', $data, array('educationalbackground_id' =>
                    $id))) {

                $res = array('status' => 1, 'msg' =>
                        'educational background has been delete successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while deleting background. Please try again.');
            }

            return json_encode($res);
        }
    }


    /**
     * Hr_model::create_emphistory()
     * 
     * @return
     */
    public function create_emphistory()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anyhting');
        } else {
            $p = $this->input;
            $data = array(
                'history_employer' => $p->post('employer'),
                'history_position' => $p->post('position'),
                'history_duties' => $p->post('duties'),
                'history_from' => $p->post('from'),
                'history_to' => $p->post('to'),
                'history_reason' => $p->post('reason'),
                'history_empid' => $p->post('emp'),
                'history_creator' => $this->session->userdata('user_id'));

            if ($this->db->insert('tbl_emphistory', $data)) {
                $res = array('status' => 1, 'msg' =>
                        'Employment history has been saved successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while saving employment history. Please try again.');
            }
        }

        return json_encode($res);

    }

    /**
     * Hr_model::emphistory_exists()
     * 
     * @param mixed $name
     * @return
     */
    private function emphistory_exists($name)
    {
        $query = $this->db->get_where('tbl_emphistory', array('emphistory_name' => $name));
        if ($query->num_rows() == 0)
            return false;
        else
            return true;
    }

    /**
     * Hr_model::update_emphistorys()
     * 
     * @return
     */
    public function update_emphistory()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('history_id');
            $data = array(
                'history_employer' => $p->post('employer'),
                'history_position' => $p->post('position'),
                'history_duties' => $p->post('duties'),
                'history_from' => $p->post('from'),
                'history_to' => $p->post('to'),
                'history_reason' => $p->post('reason')
                );

            if ($this->db->update('tbl_emphistory', $data, array('emphistory_id' => $id))) {

                $res = array('status' => 1, 'msg' =>
                        'employee history. has been updated successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while updating employee history. Please try again.');
            }

            return json_encode($res);

        }

    }


    /**
     * Hr_model::create_employee()
     * 
     * @return
     */
    public function create_employee()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;

            $emp_id = $p->post('bio_id');
            if(empty($emp_id) || $emp_id == 0) return json_encode(['status' => 0, 'msg' => 'Please provide staff employee ID']);

            $name = strtoupper($p->post('full_name'));
            $data = array(
                    'emp_fullname' => $name,
                    'emp_residence' => $p->post('address'),
                    'emp_homephone' => $p->post('home_phone'),
                    'emp_workphone' => $p->post('work_phone'),
                    'emp_email' => $p->post('email'),
                    'emp_gmail' => $p->post('gmail'),
                    //'emp_cellphone' => $p->post('cell_phone'),
                    'emp_birthdate' => $p->post('birth_date'),
                    'emp_ppno' => $p->post('ppid'),
                    'emp_nationality' => $p->post('nationality'),
                    'emp_maritalstatus' => $p->post('marital_status'),
                    'emp_designation' => strtoupper($p->post('designation')),
                    'emp_empdate' => $p->post('emp_date'),
                    'emp_visano' => $p->post('visano'),
                    'emp_permitno' => $p->post('permit'),
                    'emp_visaexpiry' => $p->post('visa_date'),
                    'emp_permitexpiry' => $p->post('permit_date'),
                    'emp_deptid' => $p->post('dept'),
                    'emp_duties' => $p->post('duties'),
                    'emp_salary' => $p->post('salary'),
                    'emp_incentive' => $p->post('incentive'),
                    'emp_payspit' => $p->post('pays_pit'),
                    'emp_paysnssf' => $p->post('pays_nssf'),
                    'emp_currency' => $p->post('currency'),
                    'emp_id' => $emp_id,
                    'emp_creator' => $this->session->userdata('user_id'),
                    'emp_gender' => $p->post('gender'),
                    'emp_createdate' => date('Y-m-d H:i:s'),
                    'emp_creator' => $this->session->userdata('user_id')
                );

                if ($this->db->insert('tbl_employees', $data)) {
                    $emp_id = $this->db->insert_id();
                    $login = $p->post('login');
                    
                    if ($login == 1) {
                        $this->load->model('users_model');
                        $this->users_model->create($emp_id);
                        //$this->db->delete('tbl_employees', array('emp_id' => $emp_id));
                    } 
                    $res = array('status' => 1, 'msg' => 'Employee has been saved successfully', 'id' => $emp_id, 'url' => site_url('welcome/view/employees'));
                } else {
                    $res = array('status' => 0, 'msg' =>
                            'An error occured while saving employee. Please try again.');
                }
            
        }
        return json_encode($res);

    }

    /**
     * Hr_model::employee_exists()
     * 
     * @param mixed $name
     * @return
     */
    private function employee_exists($name)
    {
        $query = $this->db->get_where('tbl_employees', array('emp_fullname' => $name));
        if ($query->num_rows() == 0)
            return false;
        else
            return true;
    }

    /**
     * Hr_model::update_employees()
     * 
     * @return
     */
    public function update_employee()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $id = $this->input->post('employee_id');
            $data = array(
                    'emp_fullname' => strtoupper($p->post('full_name')),
                    'emp_residence' => $p->post('address'),
                    'emp_homephone' => $p->post('home_phone'),
                    'emp_workphone' => $p->post('work_phone'),
                    'emp_email' => $p->post('email'),
                    'emp_gmail' => $p->post('gmail'),
                    // 'emp_cellphone' => $p->post('cell_phone'),
                    'emp_birthdate' => $p->post('birth_date'),
                    'emp_ppno' => $p->post('ppid'),
                    'emp_issueplace' => $p->post('place'),
                    'emp_nationality' => $p->post('nationality'),
                    'emp_maritalstatus' => $p->post('marital_status'),
                    'emp_designation' => strtoupper($p->post('designation')),
                    'emp_empdate' => $p->post('emp_date'),
                    'emp_duties' => $p->post('duties'),
                    'emp_visano' => $p->post('visano'),
                    'emp_permitno' => $p->post('permit'),
                    'emp_visaexpiry' => $p->post('visa_date'),
                    'emp_permitexpiry' => $p->post('permit_date'),
                    'emp_deptid' => $p->post('dept'),
                    'emp_salary' => $p->post('salary'),
                    'emp_incentive' => $p->post('incentive'),
                    'emp_payspit' => $p->post('pays_pit'),
                    'emp_paysnssf' => $p->post('pays_nssf'),
                    'emp_currency' => $p->post('currency'),
                    // 'emp_id' => $p->post('bio_id'),
                    'emp_gender' => $p->post('gender'),
                    'emp_updator' => $this->session->userdata('user_id')
                    );

            if ($this->db->update('tbl_employees', $data, array('emp_id' => $id))) {

                $res = array('status' => 1, 'msg' => 'Employee has been updated successfully', 'url' => site_url('welcome/view/employees'));
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while updating employee. Please try again.');
            }

            return json_encode($res);

        }

    }


    /**
     * Hr_model::delete_employees()
     * 
     * @return
     */
    public function delete_employees($id)
    {

            $data = array('emp_active' => 0);

            if ($this->db->update('tbl_employees', $data, array('emp_id' => $id))) {

                $res = array('status' => 1, 'msg' => 'employee has been deleted successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while deleted employee. Please try again.');
            }

            return json_encode($res);

    }


    /**
     * Hr_model::create_leave()
     * 
     * @return
     */
    public function create_leave()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $emp_id = $this->input->post('emp_id');
            $data = array(
                'leave_empid' => $emp_id,
                'leave_from' => $this->input->post('leave_from'),
                'leave_to' => $this->input->post('leave_to'),
                'leave_type' => $this->input->post('type'),
                'leave_reason' => $this->input->post('leave_reason'),
                'leave_resuming' => $this->input->post('resuming'),
                'leave_backstopper' => $this->input->post('backstopper'),
                'leave_createdate' => date('Y-m-d H:i:s'),
                'leave_creator' => $this->session->userdata('user_id'));
    
            if ($this->db->insert('tbl_leaves', $data)) {
                $res = array('status' => 1, 'msg' => 'Leave has been applied successfully');
                $e = get_data('tbl_employees', "WHERE emp_id = $emp_id", 'emp_email, emp_fullname, emp_deptid');
                $hod = get_hod($e['emp_deptid']);
                $hr = get_data('tbl_employees', "WHERE emp_designation = 'HR MANAGER'", 'emp_email', true);
                $to = [$hod['emp_email'], $hr['emp_email'], 'hr@links-pay.com'];
                @send_mail($to, 'Leave Application', "New leave application has been made for ".$e['emp_fullname'].". Please login to the system to approve!", false, false, site_url('welcome/view/leaves'), 'Approve Now!');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while applying leave. Please try again.');
    
            }
        }
        return json_encode($res);

    }


    /**
     * Hr_model::update_leaves()
     * 
     * @return
     */
    public function update_leaves()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $id = $p->post('leave_id');
            $data = array(
                'leave_from' => $p->post('leave_from'),
                'leave_to' => $p->post('leave_to'),
                'leave_type' => $this->input->post('type'),
                'leave_reason' => $p->post('leave_reason'));

            if ($this->db->update('tbl_leaves', $data, array('leave_id' => $id))) {

                $res = array('status' => 1, 'msg' => 'leave has been updated successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while updating leave. Please try again.');
            }
        }
        return json_encode($res);

    }

    public function leave_action()
    {
        if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);

        $remarks = $this->input->post('remarks');
        $leave = $this->input->post('leave_id');
        $status = $this->input->post('status');

        if($status == 'approve') {
            $emp = get_data('tbl_employees', "JOIN tbl_users ON user_empid = emp_id WHERE emp_id = ".$this->session->emp_id, 'emp_id, emp_designation, emp_deptid, user_levelid', true);

            if($emp['emp_designation'] == 'HR MANAGER' || $emp['emp_designation'] == 'GENERAL MANAGER') $status = 2;

            elseif(strpos($emp['emp_designation'], 'MANAGER') || $emp['user_levelid'] == 0) $status = 1;

            else return json_encode(['status' => 0, 'msg' => 'You do not have permission to approve leave']);
        }

        $this->db->update('tbl_leaves', ['leave_status' => $status, 'leave_approveremarks' => $remarks], ['leave_id' => $leave]);

        return json_encode(['status' => 1, 'msg' => 'Leave action updated successfully']);
    }

    public function create_training()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $data = array(
                'training_type' => $this->input->post('type'),
                'training_date' => $this->input->post('date'),
                'training_cost' => $this->input->post('cost'),
                'training_currency' => $this->input->post('currency'),
                'training_purpose' => $this->input->post('purpose'),
                'training_duration' => $this->input->post('duration'),
                'training_emps' => implode(',', $this->input->post('staff')),
                'training_createdate' => date('Y-m-d H:i:s'),
                'training_creator' => $this->session->userdata('user_id'));
    
            if ($this->db->insert('tbl_trainings', $data)) {
                $res = array('status' => 1, 'msg' => 'training has been applied successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while saving training. Please try again.');
    
            }
        }
        return json_encode($res);

    }



    /**
     * Hr_model::delete_leaves()
     * 
     * @return
     */
    public function delete_leaves()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('leave_id');

            $data = array('leave_active' => 0, );

            $query = $this->db->update('tbl_leaves', $data, array('leave_id' => $id));

            return json_encode($res);
        }
    }

    /**
     * Hr_model::create_leaveday()
     * 
     * @return
     */
    public function create_leaveday()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            
            $p = $this->input;
            
            $data = array(
                'leaveday_empid' => $p->post('leaveday_empid'),
                'leaveday_amt' => $p->post('leaveday_amt'),
                'leaveday_year' => $p->post('leaveday_year'),
                'leaveday_creator' => $this->session->userdata('user_id'));

            if ($this->db->insert('tbl_leavedays', $data)) {
                $res = array('status' => 1, 'msg' => 'leaveday has been saved successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while saving leaveday. Please try again.');

            }
        }
        return json_encode($res);

    }


    /**
     * Hr_model::update_leavedays()
     * 
     * @return
     */
    public function update_leavedays()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('leaveday_id');
            $data = array(

                'leaveday_empid' => $p->post('leaveday_phone'),
                'leaveday_amt' => $p->post('leaveday_email'),
                'leaveday_year' => $p->post('address'),
                'leaveday_creator' => $this->session->userdata('user_id'));

            if ($this->db->update('tbl_leavedays', $data, array('leaveday_id' => $id))) {

                $res = array('status' => 1, 'msg' => 'leaveday has been updated successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while updating leaveday. Please try again.');
            }

            return json_encode($res);

        }

    }


    /**
     * Hr_model::delete_leavedays()
     * 
     * @return
     */
    public function delete_leavedays()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('leaveday_id');

            $data = array('leaveday_active' => 0, );

            $query = $this->db->update('tbl_leavedays', $data, array('leaveday_id' => $id));

            return json_encode($res);
        }
    }

    /**
     * Hr_model::create_nextofkin()
     * 
     * @return
     */
    public function create_nextofkin()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;

            $name = strtoupper($p->post('nextofkin_name'));
            if ($this->nextofkin_exists($name)) {
                $res = array('status' => 0, 'msg' => 'next of kin name already exists.');
            } else {
                $data = array(
                    'nextofkin_name' => $name,
                    'nextofkin_age' => $p->post('nextofkin_age'),
                    'nextofkin_relation' => $p->post('nextofkin_relation'),
                    'nextofkin_occupation' => $p->post('nextofkin_occupation'),
                    'nextofkin_percentage' => $p->post('nextofkin_percentage'),
                    'nextofkin_phone' => $p->post('nextofkin_phone'),
                    'nextofkin_email' => $p->post('nextofkin_email'),
                    'nextofkin_creator' => $this->session->userdata('user_id'));

                if ($this->db->insert('tbl_nextofkins', $data)) {
                    $res = array('status' => 1, 'msg' => 'next of kin has been saved successfully');
                } else {
                    $res = array('status' => 0, 'msg' =>
                            'An error occured while saving next of kin. Please try again.');
                }
            }
        }
        return json_encode($res);

    }

    /**
     * Hr_model::nextofkin_exists()
     * 
     * @param mixed $name
     * @return
     */
    private function nextofkin_exists($name)
    {
        $query = $this->db->get_where('tbl_nextofkins', array('nextofkin_name' => $name));
        if ($query->num_rows() == 0)
            return false;
        else
            return true;
    }

    /**
     * Hr_model::update_nextofkins()
     * 
     * @return
     */
    public function update_nextofkins()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('nextofkin_id');
            $data = array(
                'nextofkin_name' => $name,
                'nextofkin_age' => $p->post('nextofkin_age'),
                'nextofkin_relation' => $p->post('nextofkin_relation'),
                'nextofkin_occupation' => $p->post('nextofkin_occupation'),
                'nextofkin_percentage' => $p->post('nextofkin_percentage'),
                'nextofkin_phone' => $p->post('nextofkin_phone'),
                'nextofkin_email' => $p->post('nextofkin_email'),
                'nextofkin_creator' => $this->session->userdata('user_id'));

            if ($this->db->update('tbl_nextofkins', $data, array('nextofkin_id' => $id))) {

                $res = array('status' => 1, 'msg' => 'nextofkin has been updated successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while updating nextofkin. Please try again.');
            }

            return json_encode($res);

        }

    }
    
    public function upload_empphoto()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'jpg|png|jpeg|bmp';
        $config['max_size'] = '0';
        $config['max_width']  = '0';
        $config['max_height']  = '0';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            $err = "";
            foreach($error as $e) {
                $err .= $e.": or invalid file";
            }
            die($err);
        }
        else
        {
            $data = $this->upload->data();
            $empid = $this->input->post('emp_id');
            if($this->db->update('tbl_employees', array('emp_photourl' => base_url('uploads/'.$data['file_name'])), array('emp_id' => $empid))) {
                $res = array('status' => 1, 'msg' => 'Photo uploaded successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Photo could not be uploaded.');
            }
            return json_encode($res);
        }
    }
    
    public function approve_vehicle()
    {
            if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $id = $this->input->post('vehiclelog_id');
            $data = array(
                'vehiclelog_confirmedby' => $this->session->userdata('user_empid'));

            if ($this->db->update('tbl_vehiclelog', $data, array('vehiclelog_id' => $id))) {

                $res = array('status' => 1, 'msg' => 'log has been approved successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while approving log. Please try again.');
            }

            return json_encode($res);

        } 
        
        

        

    }
    public function create_vehiclelog()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;

                $data = array(
                    'vehiclelog_vehicleid' => $p->post('vehiclelog_vehicleid'),
                    'vehiclelog_empid' => $p->post('vehiclelog_empid'),
                    'vehiclelog_milagestart' => $p->post('vehiclelog_milagestart'),
                    'vehiclelog_milagefinish' => $p->post('vehiclelog_milagefinish'),
                    'vehiclelog_timein' => $p->post('vehiclelog_timein'),
                    'vehiclelog_timeout' => $p->post('vehiclelog_timeout'),
                    'vehiclelog_dateoffillingtank' => $p->post('vehiclelog_dateoffillingtank'),
                    'vehiclelog_consumption' => $p->post('vehiclelog_consumption'),
                    'vehiclelog_workdone' => $p->post('vehiclelog_workdone'),
                    'vehiclelog_createdate' => $p->post('log_date'),
                    'vehiclelog_creator' => $this->session->userdata('user_id'));

                if ($this->db->insert('tbl_vehiclelog', $data)) {
                    $res = array('status' => 1, 'msg' => 'vehicle log has been saved successfully');
                } else {
                    $res = array('status' => 0, 'msg' =>
                            'An error occured while saving vehicle log. Please try again.');
                }
            
        }
        return json_encode($res);

    }
    
    public function create_vehicle()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;

                $data = array(
                    'vehicle_regno' => $p->post('vehicle_regno'),
                    'vehicle_type' => $p->post('vehicle_type'),
                    'vehicle_remarks' => $p->post('vehicle_remarks'),
                    'vehicle_creator' => $this->session->userdata('user_id'));

                if ($this->db->insert('tbl_vehicles', $data)) {
                    $res = array('status' => 1, 'msg' => 'vehicle has been saved successfully');
                } else {
                    $res = array('status' => 0, 'msg' =>
                            'An error occured while saving vehicle. Please try again.');
                }
            
        }
        return json_encode($res);

    }
    public function delete_vehicle($id)
    {
            $data = array('vehicle_active' => 0, );

            if($query = $this->db->update('tbl_vehicles', $data, array('vehicle_id' => $id))){
                $res = array('status' => 1, 'msg' => 'vehicle has been deleted successfully');
            } else {
                $res = array('status' => 0, 'msg' =>
                        'An error occured while deleting vehicle. Please try again.');
            }

            return json_encode($res);
    }
    
    public function add_bankdetails()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $data = array(
                    'bank_name' => $p->post('bank_name'),
                    'bank_branch' => $p->post('branch'),
                    'bank_acctname' => $p->post('acct_name'),
                    'bank_acctno' => $p->post('acct_no'),
                    'bank_empid' => $p->post('emp'),
                    'bank_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_empbanks', $data)) {
                $res = array('status' => 1, 'msg' => 'Bank details added successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Bank details could not be saved. Please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function update_bankdetails()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $id = $p->post('bank_id');
            $data = array(
                    'bank_name' => $p->post('bank_name'),
                    'bank_branch' => $p->post('branch'),
                    'bank_acctname' => $p->post('acct_name'),
                    'bank_acctno' => $p->post('acct_no')
                );
            if($this->db->update('tbl_empbanks', $data, array('bank_id' => $id))) {
                $res = array('status' => 1, 'msg' => 'Bank details updated successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Bank details could not be updated. Please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function apply_advance()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $emp = $p->post('emp_id');
            $currency = $p->post('currency');

            $e = get_data('tbl_employees', "WHERE emp_id = $emp", 'emp_fullname, emp_currency', true);
            if($e['emp_currency'] != $currency)
                return json_encode(['status' => 0, 'msg' => 'Employee salary currency does not match the applied currency']);

            $data = array(
                    'sa_empid' => $emp,
                    'sa_amt' => $p->post('amt'),
                    'sa_reason' => $p->post('reason'),
                    'sa_currency' => $currency,
                    'sa_creator' => $this->session->userdata('user_id'),
                    'sa_createdate' => date('Y-m-d H:i:s'),
                    'sa_isloan' => $p->post('isloan')
                );
            if($this->db->insert('tbl_salaryadvance', $data)) {
                $res = array('status' => 1, 'msg' => 'Advance applied successfully.');
                $hr = get_data('tbl_employees', "WHERE emp_designation = 'HR MANAGER'", 'emp_email', true);
                $to = [$hr['emp_email']];
                @send_mail($to, 'New Salary Advance Application', "Salary Advance has been applied by ".$e['emp_fullname'].". Please login to the system to approve.", false, false, site_url('welcome/view/loans'), 'View');

            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Advance not applied, please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function make_enquiry()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $data = array(
                    'enquiries_clientname' => $p->post('name'),
                    'enquiries_phone' => $p->post('phone'),
                    'enquiries_email' => $p->post('email'),
                    'enquiries_jobtypeid' => $p->post('job'),
                    'enquiries_remarks' => $p->post('details'),
                    'enquiries_feedback' => $p->post('feedback'),
                    'enquiries_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_enquiries', $data)) {
                $res = array('status' => 1, 'msg' => 'Enquiry saved successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Enquiry not saved, please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function make_complain()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $data = array(
                    'complain_deptid' => $p->post('dept'),
                    'complain_details' => $p->post('details'),
                    'complain_creator' => $this->session->userdata('user_id'),
                    'complain_createdate' => date('Y-m-d H:i:s')
                );
            if($this->db->insert('tbl_complains', $data)) {
                $res = array('status' => 1, 'msg' => 'Complain saved successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Complain not saved, please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function create_bill()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $data = array(
                    'mb_empid' => $p->post('emp'),
                    'mb_amt' => $p->post('amt'),
                    'mb_invoiceno' => $p->post('invoice'),
                    'mb_hospital' => $p->post('hospital'),
                    'mb_date' => $p->post('date'),
                    'mb_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_medicalbills', $data)) {
                $res = array('status' => 1, 'msg' => 'Bill saved successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Bill not saved, please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function update_bill()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $id = $p->post('mb_id');
            $data = array(
                    'mb_empid' => $p->post('emp'),
                    'mb_amt' => $p->post('amt'),
                    'mb_invoiceno' => $p->post('invoice'),
                    'mb_hospital' => $p->post('hospital'),
                    'mb_date' => $p->post('date')
                );
            if($this->db->update('tbl_medicalbills', $data, array('mb_id' => $id))) {
                $res = array('status' => 1, 'msg' => 'Bill updated successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Bill not updated, please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function allocate_bill()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $p = $this->input;
            $data = array(
                    'ma_empid' => $p->post('emp'),
                    'ma_amt' => $p->post('amt'),
                    'ma_year' => $p->post('year'),
                    'ma_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_medicalallocation', $data)) {
                $res = array('status' => 1, 'msg' => 'Bill allocated successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Bill not allocated, please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function update_permit()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not recieve anything.');
        } else {
            $p = $this->input;
            $what = $p->post('what');
            $date = $p->post('expiry_date');
            $emp_id = $p->post('emp');
            if($what=='visa') {
                $this->db->update('tbl_employees',array('emp_visaexpiry' => $date), array('emp_id' => $emp_id));
                $res = array('status' => 1, 'msg' => "Visa updated successully");
            } elseif($what=='work') {
                $this->db->update('tbl_employees',array('emp_permitexpiry' => $date), array('emp_id' => $emp_id));
                $res = array('status' => 1, 'msg' => "Work permit updated successully");
            } else {
                $res = array('status' => 0, 'msg' => "Sorry! Command not understood");
            }
        }
        
        return json_encode($res);
    }
    
    public function feedback()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not recieve anything.');
        } else {
            $p = $this->input;
            $data = array(
                    'feedback_deptid' => $p->post('dept'),
                    'feedback_module' => $p->post('module'),
                    'feedback_details' => $p->post('remarks'),
                    'feedback_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_feedback', $data)) {
                $res = array('status' => 1, 'msg' => "Thank you for your feedback.");
            } else {
                $res = array('status' => 0, 'msg' => "Sorry! Feedback not understood");
            }
        }
        
        return json_encode($res);
    }
    
    public function create_task()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not recieve anything.');
        } else {
            $p = $this->input;
            $data = array(
                    'task_title' => $p->post('title'),
                    'task_description' => $p->post('desc'),
                    'task_start' => $p->post('start'),
                    'task_end' => $p->post('end'),
                    'task_complexity' => $p->post('complexity'),
                    'task_status' => $p->post('status'),
                    'task_empid' => $p->post('empid'),
                    'task_creator' => $this->session->userdata('user_id'),
                    'task_createdate' => date('Y-m-d H:i:s')
                );
            if($this->db->insert('tbl_tasks', $data)) {
                $id = $this->db->insert_id();
                $with_actions = $this->input->post('actions');
                if(!is_null($with_actions)) $res = $this->create_action($id);
                else $res = array('status' => 1, 'msg' => "Task created successfully.");
            } else {
                $res = array('status' => 0, 'msg' => "Sorry! Task not created, please try again later.");
            }
        }
        
        return json_encode($res);
    }

    public function update_task()
    {
        if(empty($_POST)) return json_encode(array('status' => 0, 'msg' => 'Sorry! We did not recieve anything.'));

        if ($this->db->update('tbl_tasks', array('task_status' => $this->input->post('status')), array('task_id' => $this->input->post('task')))) {
            return json_encode(array('status' => 1, 'msg' => "Task updated successfully."));
        } 

        else return json_encode(array('status' => 0, 'msg' => "Task not updated."));
    }
    
    public function create_action($task)
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not recieve anything.');
        } else {
            $p = $this->input;
            
            $actions = $p->post('actions');
            foreach($actions as $a) {
                $data['ta_taskid'] = $task;
                $data['ta_details'] = $a;
                $data['ta_creator'] = $this->session->userdata('user_id');
                $data['ta_createdate'] = date('Y-m-d H:i:s');
                $this->db->insert('tbl_taskactions', $data);
            }
            $res = array('status' => 1, 'msg' => 'Task actions added successfully.');
        }
        
        return $res;
    }
    
    public function complete_task()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not recieve anything.');
        } else {
            $p = $this->input;
            $data['ta_remarks'] = $p->post('remarks');
            $data['ta_status'] = 1;
            if($this->db->update('tbl_taskactions', $data, array('ta_id' => $p->post('id')))) {
                $res = array('status' => 1, 'msg' => 'Task updated successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! Please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function enq()
    {
        echo json_encode(array('status' => 1, 'msg' => 'Follow up posted successfully'));
    }

    public function upload_empdoc() {

        if (count($_POST) == 0) {

            $res = array('status' => 0, 'msg' => 'Sorry! we did not receive anything.');

        } else {

            $p = $this->input;

            // save uploaded file 
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'doc|docx|pdf|xls|xlsx|jpeg|png|JPG|jpg|JPEG';
            $config['remove_spaces'] = true;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {

                $errors = $this->upload->display_errors();
                $res = array('status' => 0, 'msg' => $errors);

            } else { 

                $upload_ = $this->upload->data();
                $filename = $upload_['file_name'];

                $pid = $p->post('pid');

                // insert db record
                $data = array(
                    'title' => $p->post('title'),
                    'filename' => $filename,
                    'empid' => $p->post('empid')       
                );

                if ($this->db->insert('tbl_employeedocs', $data)) {
                    $res = array('status' => 1, 'msg' => 'File uploaded successfully.');
                } else {
                    $res = array('status' => 0, 'msg' => 'Error! could not upload file.');
                }

            }

        }

        return json_encode($res);

    }

    public function delete_empdoc() {

        $doc_id = $this->input->post('doc_id');
        $pid = $this->input->post('pid');
        $filename = '';

        // get filename and unlink it
        $this->db->select('filename')
            ->from('tbl_employeedocs')
            ->where('id', $doc_id);
        $query = $this->db->get();
        
        if ($row = $query->result_array()) {

            $filename = $row[0]['filename'];

        }
        @unlink("./uploads/$filename");

        // delete record
        $this->db->where('id', $doc_id);
        if ($this->db->delete('tbl_employeedocs')) {
            $res = array('status' => 1, 'msg' => 'Success! The operation was successfull.');
        } else {
            $res = array('status' => 0, 'msg' => 'Error! The operation has failed.');
        }

        echo json_encode($res);

    }
    
    public function toggle_leave()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything.');
        } else {
            $leave_id = $this->input->post('leave_id');
            $status = $this->input->post('how');
            if($status == 1) {
                $data = array('leave_approvedate' => date('Y-m-d H:i:s'), 'leave_approveremarks' => $this->input->post('remarks'), 'leave_approveby' => $this->session->userdata('user_id'), 'leave_status' => $status);
            } else {
                $data = array('leave_rejectdate' => date('Y-m-d H:i:s'), 'leave_rejectremarks' => $this->input->post('remarks'), 'leave_rejectby' => $this->session->userdata('user_id'), 'leave_status' => $status);
            }
            
            if($this->db->update('tbl_leaves', $data, array('leave_id' => $leave_id))) {
                $res = array('status' => 1, 'msg' => 'Success! The operation was successfull.');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! The operation has failed.');
            }
        }
        
        return json_encode($res);
    }
    
    public function create_meeting()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything.');
        } else {
            $agenda = $this->input->post('agenda');
            $start = $this->input->post('start');
            $end = $this->input->post('end');
            //people tagged to this meeting
            $pips = $this->input->post('with');
            if(!is_array($pips)) die('With not array');
            $with = '';
            $emails = array();
            foreach($pips as $p) {
                $with .= $p.',';
                $emails[] = array(get_that_data('tbl_employees', 'emp_email', 'emp_id', $p));
            }
            
            $data = array(
                        'meeting_agenda' => $agenda,
                        'meeting_start' => $start,
                        'meeting_end' => $end,
                        'meeting_with' => rtrim($with, ','),
                        'meeting_creator' => $this->session->userdata('user_id'),
                        'meeting_createdate' => date('Y-m-d H:i:s')
                    );
            if($this->db->insert('tbl_meetings', $data)) {
                foreach($pips as $pp) {
                    $details = get_row_data('tbl_employees', 'emp_id', $pp);
                    $message = 'Dear '.$details['emp_fullname'].'<br/>';
                    $message .= "We have a meeting on ".date('d/m/Y', strtotime($start)).' at '.date('g:i a', strtotime($start)).' expected to end at '.date('g:i a', strtotime($end)).'<br/>';
                    $message .= "The agenda of the meeting is '$agenda'. Please plan to attend. <br/><br/>Kind Regards,<br/>".get_fullname();
                    send_mail($details['emp_email'], 'Scheduled Meeting', $message);
                }
                $res = array('status' => 1, 'msg' => 'Success! Meeting scheduled successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! Meeting could not be scheduled. Please try again later.');
            }
        }
        
        return json_encode($res);
    }
    
    public function create_memo()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything.');
        } else {
            
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'doc|docx|rtf|pdf|doc|ppt|pptx';
            $config['max_size'] = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
    
            $this->load->library('upload', $config);
    
            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors());
                $err = "";
                foreach($error as $e) {
                    $err .= $e.": or invalid file";
                }
                $res = array('status' => 0, 'msg' => $err);
            } else {
              $udata = $this->upload->data();
              
              if($this->db->update('tbl_meetings', array('meeting_filename' => $data['file_name'], 'meeting_fileurl' => site_url('uploads/'.$data['file_name'])), array('meeting_id' => $minute))) $res = array('status' => 1, 'msg' => 'Success! Minutes uploaded.');
              else $res = array('status' => 1, 'msg' => 'Error! Minutes could not be uploaded.');
              
                $subject = $this->input->post('subject');
                
                $pips = $this->input->post('cc');
                $to = $this->input->post('to');
                if(!is_array($pips)) die('With not array');
                $with = '';
                $emails = array();
                foreach($pips as $p) {
                    $with .= $p.',';
                    $emails[] = array(get_that_data('tbl_employees', 'emp_email', 'emp_id', $p));
                }
                
                foreach($to as $t) {
                    $tos .= $t.',';
                    $emails[] = array(get_that_data('tbl_employees', 'emp_email', 'emp_id', $t));
                }
                
                $data = array(
                            'memo_subject' => $subject,
                            'memo_cc' => rtrim($with, ','),
                            'memo_to' => rtrim($tos, ','),
                            'memo_filename' => $udata['file_name'],
                            'memo_fileurl' => base_url('uploads/'.$udata['file_name']),
                            'memo_creator' => $this->session->userdata('user_id'),
                            'memo_createdate' => date('Y-m-d H:i:s')
                        );
                if($this->db->insert('tbl_memos', $data)) {
                    
                        $message .= "A memo concerning <strong>$subject</strong> has been created. Please login to the system for more details.";
                        $message .= "<br/><br/>Kind Regards,<br/>".get_fullname();
                        send_mail($emails, 'New Memo', $message, $udata['full_path']);
                    
                    $res = array('status' => 1, 'msg' => 'Success! Memo created successfully.');
                } else {
                    $res = array('status' => 0, 'msg' => 'Error! Memo could not be created. Please try again later.');
                }   
            }
        }
        
        return json_encode($res);
    }
    
    public function cancel_meeting()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything.');
        } else {
            $meeting_id = $this->input->post('id');
            $reason = $this->input->post('reason');
            
            if($this->db->update('tbl_meetings', array('meeting_status' => 0), array('meeting_id' => $meeting_id))) {
                $meeting = get_row_data('tbl_meetings', 'meeting_id', $meeting_id);
                $pips = explode(',', $meeting['meeting_with']);
                foreach($pips as $pp) {
                    $details = get_row_data('tbl_employees', 'emp_id', $pp);
                    $message = 'Dear '.$details['emp_fullname'].'<br/>';
                    $message .= "The meeting on ".date('d/m/Y', strtotime($meeting['meeting_start'])).' at '.date('g:i a', strtotime($meeting['meeting_start'])).' expected to end at '.date('g:i a', strtotime($meeting['meeting_end']))." has been cancelled due to $reason<br/>";
                    $message .= "Apologies for any inconviniences caused.<br/><br/>Kind Regards,<br/>".get_fullname();
                    send_mail('alfred@quadrantsofwares.com', 'Cancelled Meeting', $message);
                }
                $res = array('status' => 1, 'msg' => 'Success! Meeting cancelled successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! Meeting could not be cancelled. Please try again later.');
            }
            
            
        }
        
        return json_encode($res);
    }
    
    public function upload_minutes()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything.');
        } else {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'doc|docx|rtf|pdf|doc|ppt|pptx';
            $config['max_size'] = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
    
            $this->load->library('upload', $config);
    
            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors());
                $err = "";
                foreach($error as $e) {
                    $err .= $e.": or invalid file";
                }
                $res = array('status' => 0, 'msg' => $err);
            } else {
              $minute = $this->input->post('minute_id');
              $data = $this->upload->data();
              
              if($this->db->update('tbl_meetings', array('meeting_filename' => $data['file_name'], 'meeting_fileurl' => base_url('uploads/'.$data['file_name'])), array('meeting_id' => $minute))) $res = array('status' => 1, 'msg' => 'Success! Minutes uploaded.');
              else $res = array('status' => 1, 'msg' => 'Error! Minutes could not be uploaded.');
              
            }
        }
        
        return json_encode($res);
    }

    /*
     * Payroll Functions
     *
     */

    public function setup_payroll()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything.');
        } else {
                        
            $data = array(
                    'daily_rate_basis' => $this->input->post('daily_rate_basis'),
                    'national_security_rate' => $this->input->post('national_security_rate'),
                    'usd_ssp_rate' => $this->input->post('usd_ssp_rate'),
                    'tax_exempt_income' => $this->input->post('tax_exempt_income'),
                    'tax_level_one_threshold' => $this->input->post('tax_level_one_threshold'),
                    'tax_level_one_rate' => $this->input->post('tax_level_one_rate'),
                    'tax_level_two_threshold' => $this->input->post('tax_level_two_threshold'),
                    'tax_level_two_rate' => $this->input->post('tax_level_two_rate'),
                    'allowances' => $this->input->post('allowances'),
                    'valid_from' => $this->input->post('valid_from'),
                    'valid_to' => $this->input->post('valid_to')
                );
                    
            if($this->db->insert('tbl_payrollsetup', $data)) $res = array('status' => 1, 'msg' => 'Payroll setup successfully.');
            else $res = array('status' => 0, 'msg' => 'Error! Payroll could not be setup.');
        }
        
        return json_encode($res);
    }

    public function update_payroll_setup()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything.');
        } else {
            
            $id = $this->input->post('id');

            $data = array(
                'daily_rate_basis' => $this->input->post('daily_rate_basis'),
                'national_security_rate' => $this->input->post('national_security_rate'),
                'usd_ssp_rate' => $this->input->post('usd_ssp_rate'),
                'tax_exempt_income' => $this->input->post('tax_exempt_income'),
                'tax_level_one_threshold' => $this->input->post('tax_level_one_threshold'),
                'tax_level_one_rate' => $this->input->post('tax_level_one_rate'),
                'tax_level_two_threshold' => $this->input->post('tax_level_two_threshold'),
                'tax_level_two_rate' => $this->input->post('tax_level_two_rate'),
                'allowances' => $this->input->post('allowances'),
                'valid_from' => $this->input->post('valid_from'),
                'valid_to' => $this->input->post('valid_to')
            );

            $this->db->where('id', $id);
            if($this->db->update('tbl_payrollsetup', $data)) $res = array('status' => 1, 'msg' => 'Payroll setup successfully.');
            else $res = array('status' => 0, 'msg' => 'Error! Payroll could not be setup.');
        }
        
        return json_encode($res);
    }

    public function generate_payroll() {

        $view = "";
        $data = array();

        //if(count($_POST) == 0) {
        //    $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything.');
        //} else {
            
            $officeid = 'Rak Media Group';

            // get payroll setup
            $now = date('Y-m-d');
            $pr_sql = "SELECT * FROM tbl_payrollsetup WHERE date('$now') <= date(valid_to) AND date('$now') >= date(valid_from)";
            $settings = get_query($pr_sql);

            // get office employees
            $emp_sql = "SELECT emp_id, emp_fullname, emp_designation, emp_nationality FROM tbl_employees WHERE emp_active = 1";
            $employees = get_query($emp_sql);

            // if date < 20th payroll is for last month
            // if date > 20th payroll is for this month
            $d = date('d', strtotime($now));
            if ($d < 20) {
                $now = date('Y-m-d', strtotime("-1 month"));
            } else {
                $now = $now;
            }

            // check if payroll has been authorize, 
            // if it has, do not generate it again
            // else generate it
            $_m = date('m', strtotime($now)); $_y = date('Y', strtotime($now));
            $_data = get_data('tbl_payrolls', "WHERE month = $_m AND year = $_y ");

            if (count($_data) > 0) {
                // payroll exists

                if ($_data[0]['status'] == 0) {
                    // payroll has not been checked or authorized

                    $data['settings'] = $settings;
                    $data['employees'] = $employees;
                    $data['request'] = array(
                        'officeid' => $officeid,
                        'month' => date('m', strtotime($now)),
                        'year' => date('Y', strtotime($now))
                    );
                    
                    $view = $this->load->view('partial/office_payroll', $data, TRUE);

                    try {

                        // save payroll to db
                        $month = date('m', strtotime($now));
                        $year = date('Y', strtotime($now));
                        $data = addslashes(trim($view));
                        $creator = $this->session->userdata('user_id');
                        $createdate = date('Y-m-d H:i:s');
                        $sql = "INSERT INTO tbl_payrolls (month, year, data, creator, createdate) VALUES ('$month', '$year', '$data', $creator, '$createdate') ON DUPLICATE KEY UPDATE data = '$data' ";
                        insert_query($sql);

                    } catch(Exception $e) {
                        echo $e->getMessage();
                    }

                    echo $view;

                } elseif ($_data[0]['status'] == 2 || $_data[0]['status'] == 1) {

                    $view = $_data[0]['data'];
                    echo $view;
                    echo '<br><label class="label label-danger">NOTE:</label>&nbsp;<strong class="text-danger">This payroll has been checked or authorized and cannot be changed.</strong>';

                }

            } else {
                // payroll exists

                    $data['settings'] = $settings;
                    $data['employees'] = $employees;
                    $data['request'] = array(
                        'officeid' => $officeid,
                        'month' => date('m', strtotime($now)),
                        'year' => date('Y', strtotime($now))
                    );
                    
                    $view = $this->load->view('partial/office_payroll', $data, TRUE);

                    try {

                        // save payroll to db
                        $month = date('m', strtotime($now));
                        $year = date('Y', strtotime($now));
                        $data = addslashes(trim($view));
                        $creator = $this->session->userdata('user_id');
                        $createdate = date('Y-m-d H:i:s');
                        $sql = "INSERT INTO tbl_payrolls (month, year, data, creator, createdate) VALUES ('$month', '$year', '$data', $creator, '$createdate') ON DUPLICATE KEY UPDATE data = '$data' ";
                        insert_query($sql);


                    } catch(Exception $e) {
                        echo $e->getMessage();
                    }

                    echo $view;
                    
            }

    }

    public function employee_salary_form() {

        if (count($_POST) == 0) {

            $view = '<div class="alert alert-info">Please select an employee first</div>';
            echo $view;

        } else {

            $empid = $this->input->post('empid');

            $data = array(
                'emp_id' => $empid
            );

            $view = $this->load->view('partial/modsal_container', $data, TRUE);

            echo $view;

        }

    }

    public function employee_salary()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything.');
        } else {

            $id = $this->input->post('id');
            $allows = $this->input->post('allowance');
            $allowances = 0;
            foreach ($allows as $key => $value) {
                $allowances = $allowances + $value;
            }
            $fields = serialize($allows);

            $empid = $this->input->post('empid');
            $basic = $this->input->post('basic');
            $bonus = $this->input->post('bonus');
            $allowances = $allowances;
            $loan_repayment = $this->input->post('loan_repayment');
            $advance_repayment = $this->input->post('advance_repayment');
            $month = $this->input->post('month');
            $year = date('Y');
            $currency = $this->input->post('currency');
            $creator = $this->session->userdata('user_id');
            $is_taxable = $this->input->post('is_taxable');
            $active = 1;

            $sql = "INSERT INTO tbl_empsalary (empid, basic, bonus, allowances, allowances_allocation, loan_repayment, advance_repayment, month, year, currency, creator, active, is_taxable)
                    Values ($empid, $basic, $bonus, $allowances, '$fields', $loan_repayment, $advance_repayment, '$month', '$year', '$currency', $creator, $active, $is_taxable) 
                    ON DUPLICATE KEY UPDATE 
                    basic = $basic, bonus = $bonus, allowances = $allowances, allowances_allocation = '$fields', loan_repayment = $loan_repayment, advance_repayment = $advance_repayment, month = '$month', year = '$year', currency = '$currency', is_taxable = $is_taxable ";

            if ($this->db->query($sql)) $res = array('status' => 1, 'msg' => 'The operation was successful.');
                else $res = array('status' => 0, 'msg' => 'Error! The operation was not successful.');   
            
        }
        
        return json_encode($res);
    }

    public function generate_payslips() {

        $view = '';
        $data = array();

        
            
            $officeid = 'Rak Media Group';

            // get payroll setup
            $now = date('Y-m-d');
            $pr_sql = "SELECT * FROM tbl_payrollsetup WHERE date('$now') <= date(valid_to) AND date('$now') >= date(valid_from)";
            $settings = get_query($pr_sql);

            // get office employees
            $emp_sql = "SELECT emp_id, emp_fullname, emp_designation, emp_nationality, dept_name, emp_empdate, '123' as emp_nationalid FROM tbl_employees 
                JOIN tbl_departments ON dept_id = emp_deptid
                WHERE emp_active = 1";
            $employees = get_query($emp_sql);

            // if date < 20th payroll is for last month
            // if date > 20th payroll is for this month
            $d = date('d', strtotime($now));
            if ($d < 20) {
                $now = date('Y-m-d', strtotime("-1 month"));
            } else {
                $now = $now;
            }

            // check if payroll has been authorize, 
            // if it has, generate payslips, otherwise
            // dont generate payslips
            $_m = date('m', strtotime($now)); $_y = date('Y', strtotime($now));
            $_data = get_data('tbl_payrolls', "WHERE month = $_m AND year = $_y ");

            if (count($_data) > 0) {

                if ($_data[0]['status'] == 2) {
                    // payroll authorized 
                    
                    $data['settings'] = $settings;
                    $data['employees'] = $employees;
                    $data['request'] = array(
                        'officeid' => $officeid,
                        'month' => date('M', strtotime($now)),
                        'year' => date('Y', strtotime($now))
                    ); 
                
                    $view = $this->load->view('partial/office_payslips', $data, TRUE);
                    echo $view;

                } else {

                    echo "<div class='alert alert-info'>Payslips cannot be generated since the payroll for $_m/$_y has not yet been authorized.</div>";

                }

            } else {

                echo "<div class='alert alert-info'>Payslips cannot be generated since the payroll for $_m/$_y has not yet been generated.</div>";
            
            }

    }

    public function get_payslip($emp_id, $range) {

        $view = '';
        $data = array();

        if(!empty($emp_id) && !empty($range)) {
            
            // generate date from range
            $_date = explode('ziz', urldecode($range));
            $_m = trim($_date[0]);
            $_y = trim($_date[1]);
            $_date_string = $_y.'-'.$_m.'-20';

            $officeid = 'Rak Media Group';

            // get payroll setup
            $now = date('Y-m-d', strtotime($_date_string));
            $pr_sql = "SELECT * FROM tbl_payrollsetup WHERE date('$now') <= date(valid_to) AND date('$now') >= date(valid_from)";
            $settings = get_query($pr_sql);

            // get office employees
            $emp_sql = "SELECT emp_id, emp_fullname, emp_designation, emp_nationality, dept_name, emp_empdate, '123' as emp_nationalid FROM tbl_employees 
                JOIN tbl_departments ON dept_id = emp_deptid
                WHERE emp_active = 1";
            $employees = get_query($emp_sql);
            
            $data['settings'] = $settings;
            $data['employees'] = $employees;
            $data['request'] = array(
                'officeid' => $officeid,
                'month' => date('M', strtotime($now)),
                'year' => date('Y', strtotime($now))
            );

        }
        
        $view = $this->load->view('partial/email_office_payslips', $data, TRUE);
        return $view;

    }

    public function check_payroll() {

        if (!empty($_POST)) {
                
                $_param = $this->input->post('id');
                $params = explode('ziz', $_param);
                $action = $params[0];
                $id = $params[1];

                if ($action == 'verify') {
                    // verify - hr
                    $data = array(
                        'status' => 1,
                        'verifiedby' => $this->session->userdata('user_id'),
                        'verifydate' => date('Y-m-d H:i:s')
                    );

                } elseif ($action == 'check') {
                    // check - finance
                    $data = array(
                        'status' => 2,
                        'checkedby' => $this->session->userdata('user_id'),
                        'checkdate' => date('Y-m-d H:i:s')
                    );

                } elseif ($action == 'approve') {
                    // approve - gm
                    $data = array(
                        'status' => 3,
                        'approvedby' => $this->session->userdata('user_id'),
                        'approvedate' => date('Y-m-d H:i:s')
                    );

                } elseif ($action == 'authorize') {
                    // authorize - director
                    $data = array(
                        'status' => 4,
                        'authorizedby' => $this->session->userdata('user_id'),
                        'authorizedate' => date('Y-m-d H:i:s')
                    );

                } elseif ($action == 'reject') {
                    // authorize - director
                    $data = array(
                        'status' => -1,
                        'rejectedby' => $this->session->userdata('user_id'),
                        'rejectdate' => date('Y-m-d H:i:s')
                    );
                }

                $this->db->where('id', $id);
                if ($this->db->update('tbl_payrolls', $data)) {
                    $res = array('status' => 1, 'msg' => 'The operation was successful.');
                } else {
                    $res = array('status' => 0, 'msg' => 'Error! The operation was not successful.');
                }
            
        } else {

            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything.');
        
        }

        return json_encode($res);

    }

    public function record_payments() {

        if (count($_POST) == 0) {

            $res = array('status' => 0, 'msg' => 'Sorry!, we did not receive anything.');

        } else {

            /*
            $data = array(
                'payslip_id' => $this->input->post('payslip_id'),
                'amount_paid' => $this->input->post('amount'),
                'date' => date('Y-m-d H:i:s'),
                'creator' => $this->session->userdata('user_id'),
                'createdate' => date('Y-m-d H:i:s')
            );
            */

            $pid = $this->input->post('payslip_id');
            $amt = $this->input->post('amount');
            $empid = $this->input->post('empid');
            $date = date('Y-m-d H:i:s');
            $creator = $this->session->userdata('user_id');
            $createdate = date('Y-m-d H:i:s');

            $arp = $this->input->post('arp');
            $lrp = $this->input->post('lrp');
            $lcur = $this->input->post('lcur');

            // record advance repayment
            if ($arp > 0) {
                $data = array(
                    'empid'=>$empid,
                    'amount'=>$arp,
                    'month'=>date('m'),
                    'year'=>date('Y'),
                    'status'=>1,
                    'creator'=>0,
                    'createdate'=>date('Y-m-d H:i:s')
                );
                $this->db->insert('tbl_advances', $data);
            }

            // exchange rate
            $exr = get_exchange_rate();
            if (strtolower($lcur) == 'ssp') {
                $sspamt = $lrp;
                $usdamt = round( ($lrp / $exr), 2);
            } else if (strtolower($lcur) == 'usd') {
                $sspamt = round( ($lrp * $exr), 2);
                $usdamt = $lrp;
            }
            
            // record loan repayment
            if ($lrp > 0) {
                $datas = array(
                    'empid' => $empid,
                    'amount' => $lrp,
                    'date' => date('Y-m-d'),
                    'usd_amt' => $usdamt,
                    'ssp_amt' => $sspamt
                );
                $this->db->insert('tbl_loanrepayment', $datas);
            }

            $sql = "INSERT INTO tbl_payroll_payments (payslip_id, amount_paid, emp_id, date, creator, createdate) VALUES ";
            $sql .= " ($pid, $amt, $empid, '$date', $creator, '$createdate') ";
            $sql .= "ON DUPLICATE KEY UPDATE amount_paid = $amt, date = '$date'";

            if ($this->db->query($sql)) {

                $res = array('status' => 1, 'msg' => 'The operation was successful.');

            } else {

                $res = array('status' => 0, 'msg' => 'Error! The operation was not successful.'); 

            }  

        }

        return json_encode($res);
    }

    public function update_payslip($payslip_id, $sent_status, $net_amount) {

        if (!empty($payslip_id) && !empty($sent_status) && !empty($net_amount)) {

            $data = array(
                'is_sent' => $sent_status,
                'net_amount' => $net_amount
            );
            

        }

    }

    public function staff_loan() {

        if (count($_POST) == 0) {

            $res = array('status' => 0, 'msg' => 'Sorry!, we did not receive anything.');

        } else {

            $data = array(
                'requestid' => $this->input->post('request_id'),
                'empid' => $this->input->post('emp_id'),
                'amount' => $this->input->post('loan_amount'),
                'currency' => $this->input->post('loan_currency'),
                'installment' => $this->input->post('loan_installment'),
                'creator' => $this->session->userdata('user_id'),
                'createdate' => date('Y-m-d H:i:s')
            );

            if ($this->db->insert('tbl_loans', $data)) {
                $res = array('status' => 1, 'msg' => 'The operation was successful.');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! The operation was not successful.'); 
            }

        }

        return json_encode($res);

    }

    public function create_loan() {

        if (count($_POST) == 0) {

            $res = array('status' => 0, 'msg' => 'Sorry!, we did not receive anything.');

        } else {

            $emp = $this->input->post('empid');
            $currency = $this->input->post('currency');

            $e = get_data('tbl_employees', "WHERE emp_id = $emp", 'emp_fullname, emp_currency, emp_deptid', true);
            if($e['emp_currency'] != $currency)
                return json_encode(['status' => 0, 'msg' => 'Employee salary currency does not match the applied currency']);

            $data = array(
                'loan_type' => $this->input->post('type'),
                'loan_empid' => $emp,
                'loan_amt' => $this->input->post('amt'),
                'loan_currency' => $currency,
                'loan_duration' => $this->input->post('duration'),
                'loan_reason' => $this->input->post('reason'),
                'loan_creator' => $this->session->userdata('user_id'),
                'loan_createdate' => date('Y-m-d H:i:s')
            );

            if ($this->db->insert('tbl_loans', $data)) {
                $res = array('status' => 1, 'msg' => 'The operation was successfull.');
                $hod = get_hod($e['emp_deptid']);
                $hr = get_data('tbl_employees', "WHERE emp_designation = 'HR MANAGER'", 'emp_email', true);
                $to = [$hod['emp_email'], $hr['emp_email']];
                @send_mail($to, 'New Salary Advance Application', "Salary Advance has been applied by ".$e['emp_fullname'].". Please login to the system to approve.", false, $hod['emp_fullname'], site_url('welcome/view/loans'), 'Approve Now');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! The operation was not successful.'); 
            }

        }

        return json_encode($res);

    }

    public function toggle_loan()
    {
        if(empty($_POST)) return json_encode(['status' => o, 'msg' => 'Nothign received!']);

        $status = $this->input->post('status');
        $loan = $this->input->post('loan');
        $remarks = $this->input->post('remarks');
        $data['loan_status'] = $status;

        switch ($status) {
            case '-1':
                $data['loan_rejectby'] = $this->session->userdata('user_id');
                $data['loan_rejectremarks'] = $remarks;
                $data['loan_rejectdate'] = date('Y-m-d H:i:s');
                $msg = "Loan/Salary Advance has been rejected. <br>Remarks: $remarks";
                break;

            case '1':
                $data['loan_forwardedby'] = $this->session->userdata('user_id');
                $data['loan_forwardremarks'] = $remarks;
                $data['loan_forwarddate'] = date('Y-m-d H:i:s');
                $msg = "Loan/Salary Advance has been forwarded. <br>Remarks: $remarks";
                break;

            case '2':
                $data['loan_approvedby'] = $this->session->userdata('user_id');
                $data['loan_approveremarks'] = $remarks;
                $data['loan_approveddate'] = date('Y-m-d H:i:s');
                $msg = "Loan/Salary Advance has been approved. <br>Remarks: $remarks";
                break;

            case '3':
                $data['loan_updator'] = $this->session->userdata('user_id');;
                $msg = "Loan/Salary Advance has been paid. <br>Remarks: $remarks";
                break;
            
            default:
                return json_encode(['status' => 0, 'msg' => 'Unknown status!']);
                break;
        }

        if($this->db->update('tbl_loans', $data, ['loan_id' => $loan])) {
            $e = get_data('tbl_loans', "JOIN tbl_employees ON emp_id = loan_empid WHERE loan_id = $loan",'emp_email, emp_fullname', true);
            @send_mail($e['emp_email'], 'Salary Advance/Loan Acted on', $msg, false, $e['emp_fullname'], site_url('welcome/view/loans'), 'View Status');

            return json_encode(['status' => 1, 'msg' => 'Operation successful']);
        }

        else return json_encode(['status' => 0, 'msg' => 'Operation failed! Please try again later.']);
    }

    public function get_payroll() {

        if (count($_POST) == 0) {

            $res = array('status' => 0, 'msg' => 'Sorry!, we did not receive anything.');

        } else {

            $data = array(
                'month' => $this->input->post('month'),
                'year' => $this->input->post('year')
            );

            $ret = $this->db->get_where('tbl_payrolls', $data)->result(); var_dump($ret);
            if (!empty($ret) && is_array($ret)) {

                $ret = $ret[0];

                $res = array(
                    'status' => 1,
                    'data' => $ret->data,
                    'pid' => $ret->id
                );

            } else {

                $res = array(
                    'status' => 0,
                    'data' => '<div class="alert alert-info"><strong>Payroll Not Found</strong></div>',
                    'pid' => 0
                ); 
            
            }

        }

        return json_encode($res);
    }

    public function add_holiday() {

        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry!, we did not receive anything.');
        } else {

            $data = array(
                'date' => $this->input->post('hdate'),
                'name' => $this->input->post('hname'),
                'creator' => $this->session->userdata('user_id'),
                'createdate' => date('Y-m-d H:i:s')
            );

            if ($this->db->insert('tbl_holidays', $data)) {
                $res = array('status' => 1, 'msg' => 'The operation was successful.');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! The operation was not successful.'); 
            }
        }

        return json_encode($res);

    }

    public function remove_holiday() {

        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry!, we did not receive anything.');
        } else {

            $data = array(
                'id' => $this->input->post('id')
            );

            if ($this->db->delete('tbl_holidays', $data)) {
                $res = array('status' => 1, 'msg' => 'The operation was successful.');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! The operation was not successful.'); 
            }
        }

    }

    public function save_short_day() {

        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry!, we did not receive anything.');
        } else {

            $data = array(
                'empid' => $this->input->post('empid'),
                'days' => $this->input->post('days'),

            );

            if ($this->db->delete('tbl_holidays', $data)) {
                $res = array('status' => 1, 'msg' => 'The operation was successful.');
            } else {
                $res = array('status' => 0, 'msg' => 'Error! The operation was not successful.'); 
            }
        }

    }
    
    public function upload_report()
    {
        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry!, we did not receive anything.');
        } else {

            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'xls|xlsx|docx|rtf|pdf|doc|ppt|pptx';
            $config['max_size'] = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
    
            $this->load->library('upload', $config);
    
            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors());
                $err = "";
                foreach($error as $e) {
                    $err .= $e.": or invalid file";
                }
                $res = array('status' => 0, 'msg' => $err);
            } else {
              
              $udata = $this->upload->data();
              $type = $this->input->post('type');
              $date = $this->input->post('date');
              
              $file_name = rename($udata['full_path'], $udata['file_path'].get_username().'_'.$type.'_'.$date.$udata['file_ext']) ? get_username().'_'.$type.'_'.$date.$udata['file_ext'] : $udata['file_name']; //rename the file to matcsh user and timestamp
              
              $data = array(
                        'tracker_empid' => get_that_data('tbl_users', 'user_empid', 'user_id', $this->session->userdata('user_id')),
                        'tracker_type' => $type,
                        'tracker_title' => $this->input->post('title'),
                        'tracker_reportdate' => $date,
                        'tracker_desc' => $this->input->post('desc'),
                        'tracker_creator' => $this->session->userdata('user_id'),
                        'tracker_createdate' => date('Y-m-d H:i:s'),
                        'tracker_filename' => $file_name,
                        'tracker_filepath' => $udata['full_path']
                    );
                    
              if($this->db->insert('tbl_tracker', $data)) $res = array('status' => 1, 'msg' => 'Report uploaded successfully');
              else $res = array('status' => 0, 'msg' => 'Error! Report could not be sent!');
              
            }
            
        }
        
        return json_encode($res);
    }
    
    public function send_report()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive anything!');
        } else {
            $track_id = $this->input->post('track_id');
            $details = get_row_data('tbl_tracker', 'tracker_id', $track_id);
            
            $emp = get_row_data('tbl_employees', 'emp_id', get_that_data('tbl_users', 'user_empid', 'user_id', $this->session->userdata('user_id')));
            
            $to = array('diwakar.pandey@rakmediagrouprss.com',  'ann.ruteere@rakmediagrouprss.com', $emp['emp_email']);
            
            $msg = $this->input->post('msg').'<br/><br/> Sent by '.$emp['emp_fullname'];
            
            send_mail($to, $details['tracker_title'], $msg, $details['tracker_filepath']);
            
            if($this->db->update('tbl_tracker', array('tracker_sent' => 1, 'tracker_sentdate' => date('Y-m-d H:i:s')), array('tracker_id' => $track_id))) $res = array('status' => 1, 'msg' => 'Report sent successfully!');
            else $res = array('status' => 0, 'msg' => 'Error! Report could not be sent!');
        }
        
        return json_encode($res);
    }

    public function create_repo()
    {
        if(empty($_POST) && empty($_FILES)) return json_encode(array('status' => 1, 'msg' => 'Nothing recieved!'));

        $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|xls|xlsx|csv|jpeg|zip';
                $config['max_size']             = 100;
                //$config['max_width']            = 1024;
                //$config['max_height']           = 768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        //$error = array('msg' => $this->upload->display_errors());

                        return json_encode(array('msg' => $this->upload->display_errors(), 'status' => 0));
                }
                else
                {
                        $data = array(
                                'repo_title' => $this->input->post('name'),
                                'repo_deptid' => $this->input->post('dept'),
                                'repo_url' => base_url('uploads/'.$this->upload->data('file_name')),
                                'repo_creator' => $this->session->userdata('user_id'),
                                'repo_createdate' => date('Y-m-d H:I:s')
                            );

                    if ($this->db->insert('tbl_docsrepo', $data)) {
                        return json_encode(array('status' => 1, 'msg' => 'File uploaded successfully'));
                    } else {
                        return json_encode(array('status' => 0, 'msg' => 'File not saved in DB'));
                    }
                }
    }

    public function terminate()
    {
        if(empty($_POST)) return json_encode(array('status' => 1, 'msg' => 'Sorry! We did not recieve anything!'));

        $data = array(
                'emp_active' => 0,
                'emp_terminatedon' => $this->input->post('date'),
                'emp_terminationreason' => $this->input->post('status'),
                'emp_cleared' => $this->input->post('cleared'),
                'emp_terminationremarks' => $this->input->post('remarks')
            );

        if ($this->db->update('tbl_employees', $data, array('emp_id' => $this->input->post('emp_id')))) {
            return json_encode(array('status' => 1, 'msg' => 'Emp terminated'));
        }

        else return json_encode(array('status' => 0, 'msg' => 'Emp not terminated'));
    }

    public function transfer()
    {
        if(empty($_POST)) return json_encode(array('status' => 1, 'msg' => 'Sorry! We did not recieve anything!'));

        $emp = $this->input->post('emp_id');
        $b = get_that_data('tbl_employees', 'emp_branch', 'emp_id', $emp);
        $new_b = $b.','.$this->input->post('branch');

        if ($this->db->update('tbl_employees', ['emp_branch' => $new_b], array('emp_id' => $emp))) {
            return json_encode(array('status' => 1, 'msg' => 'Employee transfered'));
        }

        else return json_encode(array('status' => 0, 'msg' => 'Employee not transfered'));
    }

    public function change_status()
    {
        if(empty($_POST)) return json_encode(array('status' => 1, 'msg' => 'Sorry! We did not recieve anything!'));

        $emp = $this->input->post('emp_id');
        $new_b = $this->input->post('status');

        if ($this->db->update('tbl_employees', ['emp_status' => $new_b], array('emp_id' => $emp))) {
            return json_encode(array('status' => 1, 'msg' => 'Employee updated'));
        }

        else return json_encode(array('status' => 0, 'msg' => 'Employee not updated'));
    }

    public function user_report()
    {
        if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing recieved']);

        $files = false;
        if (!empty($_FILES)) {
            $data = $this->do_upload();
            if (isset($data['status']) && $data['status'] == 0) {
                return json_encode($data);
            } else {
                $files = './uploads/'.$data['file_name'];
            }
        }

        $mode = $this->input->post('mode');
        $remarks = $this->input->post('remarks');

        $emp = get_data('tbl_employees', "WHERE emp_id = ".$this->session->userdata('emp_id'), 'emp_id, emp_fullname, emp_email, emp_deptid', true);

        $hod = get_hod($emp['emp_deptid']);

        $data = $this->get_report($mode);

        $tasks = count($data);
        $html = $emp['emp_fullname']." was able to do $tasks tasks.<br>";

        $html .= '<table style="width:100%; border: 1px solid black;">
                        <tr>
                        <th>Report Date</th>
                        <th>Task</th>
                        <th>Description</th>
                        <th>Duration</th>
                        <th>Status</th>
                      </tr>';
                  foreach($data as $e) {
                      $html .= '<tr>
                        <td>'.date('Y-m-d', strtotime($e['task_createdate'])).'</td>
                        <td>'.$e['task_title'].'</td>
                        <td>'.$e['task_description'].'</td>
                        <td>'.date('Y-m-d g:i a', strtotime($e['task_start'])).' - '.date('Y-m-d g:i a', strtotime($e['task_end'])).'</td>
                        <td>'.get_task_progress($e['task_status'])[1].'</td>
                      </tr>';
                      // if ($e['task_attachment'] != null) {
                      //     $files[] = './uploads/'.$e['task_attachment'];
                      // }
                  }
            $html .= '</table>';

        $msg = "$remarks<br/>$html";

        $to = [$emp['emp_email'], $hod['emp_email']];

        $r = send_mail($to, "RAKMIS: $mode report for ".$hod['emp_fullname'], $msg.'<br/>Check the link below to details.', $files, $hod['emp_fullname'], site_url('welcome/view/tasks/'), 'View Them Here');

        if($r == 1) {
            @unlink($files);
            return json_encode(['status' => 1, 'msg' => 'Report Sent to '.$hod['emp_email']]);
        }
        else return json_encode(['status' => 0, 'msg' => 'Error sending report with response '.$r]);
    }

    private function get_report($mode)
    {
        $start = $end = date('Y-m-d');
        if ($mode == 'Daily') 
            $start = date('Y-m-d', strtotime('-1 day'));
        if($mode == 'Weekly')
            $start = date('Y-m-d', strtotime('-1 week'));
        if($mode == 'Monthly')
            $start = date('Y-m-d', strtotime('-1 month'));

        return get_data('tbl_tasks', "WHERE DATE(task_start) BETWEEN '$start' AND '$end' AND task_creator = ".$this->session->userdata('user_id'));
    }

    private function do_upload()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|xls|xlsx|csv|jpeg|zip';
        $config['max_size'] = '0';
        $config['max_width']  = '0';
        $config['max_height']  = '0';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        //$error = array('msg' => $this->upload->display_errors());

                        return array('msg' => $this->upload->display_errors(), 'status' => 0);
                }
                else
                {
                    return $this->upload->data();
                }
    }


    public function appraise()
    {
        if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing recieved']);

        if (!$this->upload->do_upload()) {

                $errors = $this->upload->display_errors();
                $res = array('status' => 0, 'msg' => $errors);

            } else { 

                $upload_ = $this->upload->data();
                $filename = $upload_['file_name'];

            $emp = $this->input->post('emp_id');
            $data = [
                    'app_empid' => $emp,
                    'app_remarks' => $this->input->post('remarks'),
                    'app_value' => $this->input->post('perf'),
                    'app_designation' => get_that_data('tbl_employees', 'emp_designation', 'emp_id', $emp),
                    'app_fileurl' => 'uploads/'.$filename,
                    'app_creator' => $this->session->userdata('user_id')
                ];

            if($this->db->insert('tbl_appraisals', $data)) {
                return json_encode(['status' => 1, 'msg' => 'Appraisal done']);
            } else {
                return json_encode(['status' => 0, 'msg' => 'Appraisal failed '.$this->db->error()->message]);
            }
        }
    }

    public function save_attendance($data)
    {
        foreach($data as $d) {
                if ($this->log_exist($d['empcheckin_empid'], $d['empcheckin_date']) > 0) {
                    $this->db->update('tbl_empcheckin', array('empcheckin_checkouttime' => $d['empcheckin_checkouttime']), array('empcheckin_empid' => $d['empcheckin_empid'], 'empcheckin_date' => $d['empcheckin_date']));
                    //echo "EmployeeId ".$d['empcheckin_empid']." updated ".$d['empcheckin_checkouttime']." for date: ".$d['empcheckin_date'];
                } else {
                    $this->db->insert('tbl_empcheckin', $d);
                    //echo json_encode($d)." inserted succsesfully";
                }
            }
    }

    private function log_exist($log_id, $dt = false) {

        if ($dt) {
            $query = $this->db->get_where('tbl_empcheckin', array('empcheckin_empid' => $log_id, 'empcheckin_date' => $dt));
        } else {
            $query = $this->db->get_where('tbl_empcheckin', array('empcheckin_devid' => $log_id));
        }

        return $query->num_rows();

    }

}