<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    

class Marketing_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function create_target()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => "We did not recieve anyhting.");
        } else {
            $month = $this->input->post('month');
            $emp_id = $this->input->post('emp_id');
            if($this->has_target($month, $emp_id)) {
                $res = array('status' => 0, 'msg' => 'Sorry! Targets for that month have already been set.');
            } else {
                $data = array(
                        'target_empid' => $emp_id,
                        'target_month' => $month,
                        'target_qty' => $this->input->post('qty'),
                        'target_regs' => $this->input->post('regs'),
                        'target_acts' => $this->input->post('acts'),
                        'target_usdamt' => $this->input->post('usd'),
                        'target_sspamt' => $this->input->post('ssp'),
                        'target_visits' => $this->input->post('visits'),
                        'target_merchants' => $this->input->post('merchants'),
                        'target_year' => date('Y'),
                        'target_creator' => $this->session->userdata('user_id')
                    );
                    
                if($this->db->insert('tbl_targets', $data)) {
                    $res = array('status' => 1, 'msg' => 'Target assigned successfully.');
                } else {
                    $res = array('status' => 0, 'msg' => 'Sorry! Target could not be saved. Please try again later.');
                }
            }
        }
        
        return json_encode($res);
    }
    
    public function has_target($month, $emp_id)
    {
        $year = date('Y');
        $data = get_data('tbl_targets', "WHERE target_month = '$month' AND target_empid = $emp_id AND target_year = $year");
        if(empty($data)) {
            return false;
        } else {
            return true;
        }
    }
    
    public function delete_target()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => "We did not recieve anyhting.");
        } else {
            $id = $this->input->post('target_id');
            if($this->db->delete('tbl_targets', array('target_id' => $id)))
                $res = array('status' => 1, 'msg' => 'Target has been unset successfully.');
            else 
                $res = array('status' => 0, 'msg' => 'Sorry! Target could not be unset. Please try again later.');    
        }
        
        return json_encode($res);
    }
    
    public function create_visit()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => "We did not recieve anyhting.");
        } else {
                $data = array(
                        'visit_empid' => get_that_data('tbl_users', 'user_empid', 'user_id', $this->session->userdata('user_id')),
                        'visit_reason' => $this->input->post('reason'),
                        'visit_remarks' => $this->input->post('remarks'),
                        'visit_clientid' => $this->input->post('client'),
                        'visit_creator' => $this->session->userdata('user_id')
                    );
                    
                if($this->db->insert('tbl_visits', $data)) {
                    $res = array('status' => 1, 'msg' => 'Visit made successfully.');
                } else {
                    $res = array('status' => 0, 'msg' => 'Sorry! Visit could not be saved. Please try again later.');
                }
        }
        
        
        return json_encode($res);
        
    }
    
    public function follow_up()
    {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => "We did not recieve anyhting.");
        } else {
                
                $data['rm_comments'] = $this->input->post('remarks');
                if($this->db->update('tbl_rmactivities', $data, ['rm_id' => $this->input->post('rm_id')])) {
                    $res = array('status' => 1, 'msg' => 'Followup made successfully.');
                } else {
                    $res = array('status' => 0, 'msg' => 'Sorry! Followup could not be saved. Please try again later.');
                }
        }
        
        
        return json_encode($res);
    }
}