<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Payroll_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function create()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing was received!']);

		$year = $this->input->post('year');
		$month = $this->input->post('month');
		$rate = $this->input->post('rate');

		$dur = $year.'-'.$month;

		$rows = $this->db->get_where('tbl_payroll', ['pr_month' => $dur, 'pr_active' => 1])->num_rows();

		if($rows) return json_encode(['status' => 0, 'msg' => 'Payroll already exists!']);

		$data['pr_month'] = $dur;
		$data['pr_rate'] = $rate;
		$data['pr_creator'] = $this->session->userdata('user_id');
		$data['pr_createdate'] = date('Y-m-d H:i:s');

		if($this->db->insert('tbl_payroll', $data)) 
			if($this->create_payslips($this->db->insert_id(), $rate))
				return json_encode(['status' => 1, 'msg' => 'Payroll created successfully!' ]);

		else return json_encode(['status' => 0, 'msg' => 'Payroll could not be created. Please try again later!']);
	}

	private function create_payslips($pr_id, $rate)
	{
		$emps = get_data('tbl_employees', "WHERE emp_active = 1", 'emp_id, emp_salary, emp_currency, emp_incentive, emp_payspit, emp_paysnssf');
		$data = [];
		foreach($emps as $e) {
			$allowances = get_data('tbl_allowances', "WHERE allowance_active = 1 AND allowance_empid = ".$e['emp_id'], 'SUM(allowance_amt) AS total', true)['total'];
			$tax = calculate_tax($e['emp_salary'], $e['emp_currency'], $rate, $e['emp_payspit'], $e['emp_paysnssf']);
			$loans = $this->get_loan($e['emp_id']);
			$total = ($e['emp_salary']+$e['emp_incentive']+$allowances)-($tax['pit']+$tax['nssf']+$loans);
			// $worked_days = $this->db->query("SELECT COUNT(empcheckin_id) AS total FROM tbl_empcheckin WHERE DATE_FORMAT(empcheckin_date,'%Y%m') = '2021-01' AND empcheckin_empid = ".$e['emp_id'])->row_array()['total'];
			$data[] = [
					'ps_empid' => $e['emp_id'],
					'ps_prid' => $pr_id,
					'ps_basic' => $e['emp_salary'],
					'ps_incentive' => $e['emp_incentive'],
					'ps_allowances' => $allowances,
					'ps_pit' => $tax['pit'],
					'ps_taxexempt' => $tax['t_exempt'],
					'ps_taxableincome' => $tax['t_income'],
					'ps_taxableamt' => $tax['t_amt'],
					'ps_nssf' => $tax['nssf'],
					'ps_due' => $total,
					'ps_loans' => $loans,
					'ps_creator' => $this->session->userdata('user_id'),
					'ps_createdate' => date('Y-m-d H:i:s')
				];
		}

		return $this->db->insert_batch('tbl_payslips', $data);
	}

	public function create_allowance()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing was received!']);

		$data['allowance_name'] = $this->input->post('name');
		$data['allowance_empid'] = $this->input->post('emp');
		$data['allowance_amt'] = $this->input->post('amt');
		$data['allowance_creator'] = $this->session->userdata('user_id');
		$data['allowance_createdate'] = date('Y-m-d H:i:s');

		if($this->db->insert('tbl_allowances', $data))
			return json_encode(['status' => 1, 'msg' => 'Allowance created successfully!' ]);
		else
			return json_encode(['status' => 0, 'msg' => 'Allowance not created! Please try again later!' ]);
	}

	private function get_loan($emp)
	{
		$paid = get_data('tbl_payslips', "WHERE ps_active = 1 AND ps_empid = $emp", 'SUM(ps_loans) AS total', true)['total'];
		$loan = get_data('tbl_loans', "WHERE loan_active = 1 AND loan_status = 3 AND loan_empid = $emp", 'SUM(loan_amt) AS amt, SUM(loan_amt/loan_duration) AS installment', true);
		$bal = $loan['amt'] - $paid;

		if($bal == 0) return 0;

		return $loan['installment'];
	}

	public function upload_salos()
	{
		// if(empty($_FILES)) return json_encode(['status' => 0, 'msg' => 'Please attach file']);

		if(isset($_FILES['import'])) {
			$file = $_FILES['import']['tmp_name'];
			$csv = array_map('str_getcsv', file($file));
			// var_dump($csv); exit;
			for($i=1; $i<count($csv); $i++) {
				$row = $csv[$i];
				$emp_id = preg_replace('~\D~', '', $row[0]);
				$data = [
						'emp_currency' => $row[4],
						'emp_salary' => $row[5],
						'emp_incentive' => $row[6],
						'emp_payspit' => $row[9],
						'emp_paysnssf' => $row[10]
					];
				$this->db->update('tbl_employees', $data, ['emp_id' => $emp_id]);

				if((int)$row[7] > 0) {
					$allowances = ['allowance_amt' => $row[7], 'allowance_empid' => $emp_id, 'allowance_name' => $csv[0][7]];
					$row = $this->db->get_where('tbl_allowances', $allowances)->row_array();
					if(empty($row)) {
						$allowances['allowance_creator'] = $this->session->userdata('user_id');
						$allowances['allowance_createdate'] = date('Y-m-d H:i:s');
						$this->db->insert('tbl_allowances', $allowances);
					} else {
						$this->db->update('tbl_allowances', $allowances, ['allowance_id' => $row['allowance_id']]);
					}
				}
			}

			return json_encode(['status' => 1, 'msg' => "$i records uploaded successfully!"]);
		}

		return json_encode(['status' => 0, 'msg' => "File not valid!"]);

	}

	public function delete_payroll()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);

		$pr_id = $this->input->post('pr_id');
		$this->db->update('tbl_payslips', ['ps_active' => 0], ['ps_prid' => $pr_id]);
		$this->db->update('tbl_payroll', ['pr_active' => 0], ['pr_id' => $pr_id]);

		return json_encode(['status' => 0, 'msg' => 'Payroll deactivated successfully!']);
	}

	public function process()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing received']);

		$slips = $this->input->post('slips');
		$status = $this->input->post('status');
		$this->db->where_in('ps_id', $slips, false)->update('tbl_payslips', ['ps_status' => $status, 'ps_updator' => $this->session->user_id]);

		$emps = get_data('tbl_employees', "WHERE emp_designation LIKE 'HEAD%' OR emp_designation LIKE 'FINANCE MANAGER' OR emp_designation LIKE 'OPERATIONS MANAGER'", 'emp_email');
		$to = array_column($emps, 'emp_email');
		@send_mail($to, 'Payroll Processed for Payment', "Payroll has been processed for payment. Login to the system to pay.", false, false, site_url('welcome/view/payroll'), 'Login Now');
		return json_encode(['status' => 1, 'msg' => 'Slips processed successfully!']);
	}
}