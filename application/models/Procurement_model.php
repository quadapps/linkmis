<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


 class Procurement_model extends CI_Model

 {
    public function __construct()

    {

        parent::__construct();
        date_default_timezone_set('Africa/Nairobi');

    }

    public function create_request()
    {

      if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.'); }
        else {
            $qty = $this->input->post('qty');
            $desc = $this->input->post('desc');
            $price = $this->input->post('price');
            $amt = 0;
            for($i=0; $i<count($qty); $i++) {
                $amt += $qty[$i]*$price[$i];
            }
            //print_r($_POST); exit;
                $data = array(
                        'req_deptid' => $this->input->post('deptid'),
                        'req_type' => 1,
                        'req_items' => json_encode(['qty' => $qty, 'price' => $price, 'desc' => $desc]),
                        'req_amt' => $amt,
                        'req_currency' => $this->input->post('currency'),
                        'req_exphead' => $this->input->post('head'),
                        'req_creator' => $this->session->userdata('user_id'),
                        'req_createdate' => date('Y-m-d H:i:s')
                    );
                    

                if ($this->db->insert('tbl_reqs', $data)) {

                    $res = array('status' => 1, 'msg' => 'Request has been saved successfully');

                    $mails = get_data('tbl_employees', "JOIN tbl_departments ON dept_id = emp_deptid JOIN tbl_users ON user_empid = emp_id WHERE emp_active = 1 AND emp_deptid = 5", 'emp_email');

                    $hod = [];
                    foreach($mails as $e) $hod[] = $e['emp_email'];

                    @send_mail($hod, 'New Request made', 'New requisition is awaiting your approval. Please login to the system to approve', false, false, site_url('welcome/view/reqs'), 'View');

                } else {
                    $res = array('status' => 0, 'msg' => 'An error occured while saving request. Please try again.');
                }
            return json_encode($res);
        }
    }

    private function add_items($req_id)
    {
        $items = $this->input->post('item');
        $qty = $this->input->post('qty');
        $price = $this->input->post('price');
        $unit = $this->input->post('unit');
        
        $items_ = '';
        $cost_ = 0;

        if(is_array($items)) {
            $i = 0;
            foreach($items as $itm) {
                $data['requestitem_requestid'] = $req_id;
                $data['requestitem_qty'] = $qty[$i];
                $data['requestitem_description'] = $itm;
                $data['requestitem_price'] = $price[$i];
                $data['requestitem_units'] = $unit[$i];
                $data['requestitem_createdate'] = date('Y-m-d H:i:s');
                $data['requestitem_creator'] = $this->session->userdata('user_id');
                
                $items_ .= $qty[$i].' '.$unit[$i].' '.$itm.'<br>';
                $cost_ += ($qty[$i] * $price[$i]);

                $this->db->insert('tbl_requestitems', $data);
                $i++;
            }
            
            return array('request_description' => $items_, 'request_amt' => $cost_);
        }
        
        return 'You cannot loop through a string man!';
    }
        
    
    
    public function update_requests()
     {    if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.'); }
        else {     
            $id =$this->input->post('request_id');
               $data = array(
                                'request_dept' => $id->post('request_dept'),
                                'request_empid' => $id->post('request_empid'),
                                'request_creator' => $this->session->userdata('user_id')
                            );

       if ($this->db->update('tbl_requests', $data, array('request_id' => $id))){
        
        $res = array('status' => 1, 'msg' => 'request has been updated successfully');
                } else {
                    $res = array('status' => 0, 'msg' => 'An error occured while updating supplier. Please try again.');
                 }
                 
                 return json_encode($res);
     
       }                   					  
        
    }
    
    
     public function delete_requests()
    {  if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.'); }
        else {     
            $id =$this->input->post('request_id');
        
        $data = array(
                      'request_active' => 0,);
 	                  
   $query = $this->db->update('tbl_requests', $data, array('request_id' => $id));
   
  return json_encode($res);
        }
        }
        

    public function create_requestfiles()
    {
      
                $data = array(
                        'requestfiles_type' => $id->post('requestfiles_type'),
                        'requestfiles_path' => $id->post('requestfiles_path'),
                        'requestfiles_name' => $id->post('requestfiles_name'),
                        'requestfiles_url' => $id->post('requestfiles_url'),
                        'requestfiles_creator' => $this->session->userdata('user_id')
                    );
                    
                if($this->db->insert('tbl_requestfiles', $data)) {
                    $res = array('status' => 1, 'msg' => 'request has been saved successfully');
                } else {
                    $res = array('status' => 0, 'msg' => 'An error occured while saving request. Please try again.');
                }
            return json_encode($res);
        }
        
        
    
    
    public function update_requestfiles()
     {    if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.'); }
        else {     
            $id =$this->input->post('requestfiles_id');
               $data = array(
                                'requestfiles_type' => $id->post('requestfiles_type'),
                                'requestfiles_path' => $id->post('requestfiles_path'),
                                'requestfiles_name' => $id->post('requestfiles_name'),
                                'requestfiles_url' => $id->post('requestfiles_url'),
                                'requestfiles_creator' => $this->session->userdata('user_id')
                            );

       if ($this->db->update('tbl_requestfiles', $data, array('requestfiles_id' => $id))){
        
        $res = array('status' => 1, 'msg' => 'request has been updated successfully');
                } else {
                    $res = array('status' => 0, 'msg' => 'An error occured while updating supplier. Please try again.');
                 }
                 
                 return json_encode($res);
     
       }                   					  
        
    }
    

        
        
      public function create_requestitems()
    {
      
                $data = array(
                        'requestitems_requestid' => $id->post('requestitems_requestid'),
                        'requestitems_qty' => $id->post('requesitems_qty'),
                        'requestitems_description' => $id->post('requesitems_description'),
                        'requestitems_price' => $id->post('requesitems_price'),
                        'requestitems_creator' => $this->session->userdata('user_id')
                    );
                    
                if($this->db->insert('tbl_requestitems', $data)) {
                    $res = array('status' => 1, 'msg' => 'request has been saved successfully');
                } else {
                    $res = array('status' => 0, 'msg' => 'An error occured while saving request. Please try again.');
                }
            return json_encode($res);
        }
        
        
    
    
    public function update_requestitems()
     {    if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.'); }
        else {     
            $id =$this->input->post('request_id');
               $data = array(
                                'requestitems_requestid' => $id->post('requestitems_requestid'),
                                'requestitems_qty' => $id->post('requesitems_qty'),
                                'requestitems_description' => $id->post('requesitems_description'),
                                'requestitems_price' => $id->post('requesitems_price'),
                                'requestitems_creator' => $this->session->userdata('user_id')
                            );

       if ($this->db->update('tbl_requestitems', $data, array('requestitems_id' => $id))){
        
        $res = array('status' => 1, 'msg' => 'request has been updated successfully');
                } else {
                    $res = array('status' => 0, 'msg' => 'An error occured while updating supplier. Please try again.');
                 }
                 
                 return json_encode($res);
     
       }                   					  
        }
        
     public function create_cashrequest()
     {
        if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $data = array(
                    'req_deptid' => $this->input->post('deptid'),
                    'req_exphead' => $this->input->post('head'),
                    'req_amt' => $this->input->post('amt'),
                    'req_currency' => $this->input->post('currency'),
                    'req_mode' => $this->input->post('mode'),
                    'req_items' => $this->input->post('purpose'),
                    'req_type' => 2,
                    'req_creator' => $this->session->userdata('user_id'),
                    'req_createdate' => date('Y-m-d H:i:s')
                );
            if($this->db->insert('tbl_reqs', $data)) {
                $res = array('status' => 1, 'msg' => 'Request posted successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Request was not posted. Please try again later.');
            }
        }
        
        return json_encode($res);
     }
     
     public function create_fuelrequest()
     {
        if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything!');
        } else {
            $data = array(
                    'fuelrequest_vehicleid' => $this->input->post('car'),
                    'fuelrequest_lts' => $this->input->post('lts'),
                    'fuelrequest_amt' => $this->input->post('amt'),
                    'fuelrequest_remarks' => $this->input->post('remarks'),
                    'fuelrequest_creator' => $this->session->userdata('user_id'),
                    'fuelrequest_createdate' => date('Y-m-d H:i:s')
                );
            if($this->db->insert('tbl_fuelrequests', $data)) {
                $res = array('status' => 1, 'msg' => 'Request posted successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Request was not posted. Please try again later.');
            }
        }
        
        return json_encode($res);
     }
     
     public function add_remarks()
     {
        if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $data = array(
                    'requestremark_requestid' => $this->input->post('req_id'),
                    'requestremark_type' => $this->input->post('type'),
                    'requestremark_remarks' => $this->input->post('remarks'),
                    'requestremark_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_requestremarks', $data)) {
                $res = array('status' => 1, 'msg' => 'Remarks posted successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Remarks were not posted. Please try again later.');
            }
        }
        
        return json_encode($res);
     }
     
     public function pay_request()
     {
        if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $req_id = $this->input->post('req_id');
            $type = $this->input->post('type');
            $data = array(
                    'requestpmt_requestid' => $req_id,
                    'requestpmt_type' => $type,
                    'requestpmt_mode' => $this->input->post('mode'),
                    'requestpmt_amt' => $this->input->post('amt'),
                    'requestpmt_ledger' => $this->input->post('ledger'),
                    'requestpmt_remarks' => $this->input->post('remarks'),
                    'requestpmt_creator' => $this->session->userdata('user_id'),
                    'requestpmt_createdate' => date('Y-m-d H:i:s')
                );
            if($this->db->insert('tbl_requestpmts', $data)) {
                if($type==1) {
                    $this->db->update('tbl_itemrequests', array('request_status' => 5), array('request_id' => $req_id));
                } elseif($type==2) {
                    $this->db->update('tbl_cashrequests', array('cashrequest_status' => 5), array('cashrequest_id' => $req_id));
                } elseif($type==3) {
                    $this->db->update('tbl_salaryadvance', array('sa_status' => 3), array('sa_id' => $req_id));
                } elseif($type==4) {
                    $this->db->update('tbl_fuelrequests', array('fuelrequest_status' => 5), array('fuelrequest_id' => $req_id));
                }
                $res = array('status' => 1, 'msg' => 'Payment posted successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Payments were not posted. Please try again later.');
            }
        }
        
        return json_encode($res);
     }
     
     public function request_action()
     {
        if(count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not receive anything.');
        } else {
            $status = $this->input->post('status');
            $type = $this->input->post('type');
            $id = $this->input->post('req_id');
            $remarks = $this->input->post('remarks');
            $uid = $this->session->userdata('user_id');
            if($status==3) {
                if($type=='cashrequest') {
                    $data['cashrequest_status'] = $status;
                    $data['cashrequest_approvedby'] = $uid;
                    $data['cashrequest_approvedate'] = date('Y-m-d H:i:s');
                    $data['cashrequest_approveremarks'] = $remarks;
                    $this->db->update('tbl_cashrequests', $data, array('cashrequest_id' => $id));
                } elseif($type=='itemrequest') {
                    $data['request_status'] = $status;
                    $data['request_approvedby'] = $uid;
                    $data['request_approvedate'] = date('Y-m-d H:i:s');
                    $data['request_approveremarks'] = $remarks;
                    $this->db->update('tbl_itemrequests', $data, array('request_id' => $id));
                } elseif($type=='fuelrequest') {
                    $data['fuelrequest_status'] = $status;
                    $data['fuelrequest_rejectby'] = $uid;
                    $data['fuelrequest_rejectdate'] = date('Y-m-d H:i:s');
                    $data['fuelrequest_rejectremarks'] = $remarks;
                    $this->db->update('tbl_fuelrequests', $data, array('fuelrequest_id' => $id));
                } else {
                    $res = array('status' => 0, 'msg' => 'Request not understood. Please try again later.');
                }
                $res = array('status' => 1, 'msg' => 'Request  successful.');
            } elseif($status==4) {
                if($type=='cashrequest') {
                    $data['cashrequest_status'] = $status;
                    $data['cashrequest_rejectby'] = $uid;
                    $data['cashrequest_rejectdate'] = date('Y-m-d H:i:s');
                    $data['cashrequest_rejectremarks'] = $remarks;
                    $this->db->update('tbl_cashrequests', $data, array('cashrequest_id' => $id));
                } elseif($type=='itemrequest') {
                    $data['request_status'] = $status;
                    $data['request_rejectby'] = $uid;
                    $data['request_rejectdate'] = date('Y-m-d H:i:s');
                    $data['request_rejectremarks'] = $remarks;
                    $this->db->update('tbl_itemrequests', $data, array('request_id' => $id));
                } else {
                    $res = array('status' => 0, 'msg' => 'Request not understood. Please try again later.');
                }
                $res = array('status' => 1, 'msg' => 'Request rejected successfully.');
            } elseif($status==2) {
                if($type=='cashrequest') {
                    $data['cashrequest_status'] = $status;
                    $data['cashrequest_forwardby'] = $uid;
                    $data['cashrequest_forwarddate'] = date('Y-m-d H:i:s');
                    $data['cashrequest_forwardremarks'] = $remarks;
                    $this->db->update('tbl_cashrequests', $data, array('cashrequest_id' => $id));
                } elseif($type=='itemrequest') {
                    $data['request_status'] = $status;
                    $data['request_forwardby'] = $uid;
                    $data['request_forwarddate'] = date('Y-m-d H:i:s');
                    $data['request_forwardremarks'] = $remarks;
                    $this->db->update('tbl_itemrequests', $data, array('request_id' => $id));
                } elseif($type=='fuelrequest') {
                    $data['fuelrequest_status'] = $status;
                    $data['fuelrequest_approvedby'] = $uid;
                    $data['fuelrequest_approvedate'] = date('Y-m-d H:i:s');
                    $data['fuelrequest_approveremarks'] = $remarks;
                    $this->db->update('tbl_fuelrequests', $data, array('fuelrequest_id' => $id));
                } else {
                    $res = array('status' => 0, 'msg' => 'Request not understood. Please try again later.');
                }
                $res = array('status' => 1, 'msg' => 'Request  successful.');
            }
        }
        
        return json_encode($res);
     }
     
     public function upload_file()
     {
        if(count($_POST) == 0) {
            $res  = array('status' => 0, 'msg' => 'Sorry! We did not recieve anything.');
        } else {
            $this->load->model('jobs_model');
            //data from upload
            $res = $this->jobs_model->do_upload();
            $data['requestfiles_requestid'] = $this->input->post('req_id');
            $data['requestfiles_type'] = $this->input->post('type');
            $data['requestfiles_requesttype'] = $this->input->post('req_type');
            $data['requestfiles_remarks'] = $this->input->post('remarks');
            $data['requestfiles_name'] = $res['file_name'];
            $data['requestfiles_path'] = $res['full_path'];
            $data['requestfiles_url'] = base_url('uploads/'.$res['file_name']);
            $data['requestfiles_creator'] = $this->session->userdata('user_id');
            
            if($this->db->insert('tbl_requestfiles', $data)) {
                $res = array('status' => 1, 'msg' => 'Files uploaded successfully.');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! File could not be uplaoded.');
            }
        }
        
        return json_encode($res);
     }
     
     public function received_goods()
     {
        if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $data = array(
                    'fj_itemid' => $this->input->post('item'),
                    'fj_qty' => $this->input->post('qty'),
                    'fj_remarks' => $this->input->post('remarks'),
                    'fj_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_finishedjobs', $data)) {
                $res = array('status' => 1, 'msg' => 'Item posted successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Items were not posted. Please try again later.');
            }
        }
        
        return json_encode($res);
     }
     
     public function create_ledger()
     {
        if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $data = array(
                    'ledger_name' => $this->input->post('ledger'),
                    'ledger_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_tallyledgers', $data)) {
                $res = array('status' => 1, 'msg' => 'Ledger posted successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Ledger was not posted. Please try again later.');
            }
        }
        
        return json_encode($res);
     }
     
     public function refund()
     {
        if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything.');
        } else {
            $req_id = $this->input->post('req_id');
            $amt_used = $this->input->post('amt_used');
            $amt_refunded = $this->input->post('amt_refunded');
            $pmt_id = $this->input->post('pmt_id');
            
            $this->load->model('jobs_model');
            //data from upload
            $file = $this->jobs_model->do_upload();
            $data = array(
                    'refund_amt' => $amt_refunded,
                    'refund_remarks' => $this->input->post('remarks'),
                    'refund_pmtid' => $pmt_id,
                    'refund_creator' => $this->session->userdata('user_id')
                );
            if($this->db->insert('tbl_reqrefunds', $data)) {
                
                $this->db->update('tbl_requestpmts', array('requestpmt_used' => $amt_used, 'requestpmt_receipturl' => base_url('uploads/'.$file['file_name'])), array('requestpmt_id' => $pmt_id));
                
                $res = array('status' => 1, 'msg' => 'Refund posted successfully');
            } else {
                $res = array('status' => 0, 'msg' => 'Sorry! Refund was not posted. Please try again later.');
            }
        }
        
        return json_encode($res);
     }

    public function add_price() {

        if (count($_POST) == 0) {

            $res = array('status' => 0, 'msg' => 'We did not receive anything.');

        } else {

            $uid = $this->session->userdata('user_id');
            $data = array(
                'type' => $this->input->post('type'),
                'name' => $this->input->post('name'),
                'price' => $this->input->post('price'),
                'currency' => $this->input->post('currency'),
                'brand' => $this->input->post('brand'),
                'category' => $this->input->post('category'),
                'active' => 1,
                'creator' => $uid
            );

            if ($this->db->insert('tbl_price_index', $data)) {

                $res = array('status' => 1, 'msg' => 'Price added successfully');

            } else {

                $res = array('status' => 1, 'msg' => 'Price was not added');

            }

        }

        return json_encode($res);

    }

    public function approve()
    {
        if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing received!']);

        $data = [
            'app_remarks' => $this->input->post('remarks'),
            'app_reqid' => $this->input->post('req_id'),
            'app_creator' => $this->session->user_id
        ];

        if ($this->db->insert('tbl_reqapprovals', $data)) {
            return json_encode(['status' => 1, 'msg' => 'Approved successfully!']);
        }

        else return json_encode(['status' => 0, 'msg' => 'Approving failed. Try again later!']);
    }

    public function reject()
    {
        if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing received!']);

        $data = [
            'req_rejectremarks' => $this->input->post('remarks'),
            'req_rejectdate' => date('Y-m-d H:i:s'),
            'req_rejectby' => $this->session->user_id,
            'req_status' => -1
        ];

        if ($this->db->update('tbl_reqs', $data, ['req_id' => $this->input->post('req_id')])) {
            return json_encode(['status' => 1, 'msg' => 'Rejected successfully!']);
        }

        else return json_encode(['status' => 0, 'msg' => 'Rejecting failed. Try again later!']);
    }

    public function import_index() {

        if (count($_POST) == 0) {

            $res = array('status' => 0, 'msg' => 'We did not receive anything.');

        } else {

            // load reader lib
            require_once './rakmis/libraries/spreadsheet-reader-master/SpreadsheetReader.php';

            // upload file
            $file = $this->do_upload(); // file_name, full_path 

            // params
            $uid = $this->session->userdata('user_id');
            $type = $this->input->post('type');
            $count = 0;
            $target_file = './uploads/'.trim($file['file_name']); 

            // reader
            $reader = new SpreadsheetReader($target_file);
            $reader->changeSheet(0);
            
            $i = 0;
            foreach ($reader as $row) {

                if ($i > 0) {

                    $name = $row[0];
                    $price = $row[1];
                    $currency = $row[2];
                    $date = date('Y-m-d H:i:s');
                    
                    $sql = 'INSERT INTO tbl_price_index (name, price, currency, type, creator, active) ';
                    $sql .= "VALUES ('$name', $price, '$currency', '$type', $uid, 1) ON DUPLICATE KEY ";
                    $sql .= "UPDATE name = '$name', price = $price, currency = '$currency', type = '$type', updator = $uid, updatedate = '$date' ";

                    if ($this->db->query($sql)) {
                        $count++;
                    }

                    $name = '';
                    $price = 0;
                    $currency = '';
                    $sql = '';

                }

                $i++;
            
            }

            unlink($target_file);

            if ($count > 0) {

                $res = array('status' => 1, 'msg' => 'Price added successfully');

            } else {

                $res = array('status' => 0, 'msg' => 'Price was not added');

            }

        }

        return json_encode($res);

    }

    public function do_upload()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '0';
        $config['max_width']  = '0';
        $config['max_height']  = '0';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            $err = "";
            foreach($error as $e) {
                $err .= $e.": or invalid file";
            }
            die($err);
        }
        else
        {
            return $this->upload->data();
        }
    }

    public function create_po() {

        if (count($_POST) == 0) {
            $res = array('status' => 0, 'msg' => 'We did not receive anything.');
        } else {

            $req_no = $this->input->post('req_no');
            $supplier_name = $this->input->post('supplier_name');
            $supplier_address = $this->input->post('supplier_address');
            $quotation_ref = $this->input->post('quotation_ref');
            $quotation_date = $this->input->post('quotation_date');
            $completion_count = $this->input->post('completion_count');
            $completion_unit = $this->input->post('completion_unit');
            $warranty_period = $this->input->post('waranty_period');

            $items = get_query("select * from tbl_requestitems where requestitem_requestid = $req_no ");

            if (empty($items)) {

                $item_arr = get_query("select request_description, request_amt FROM tbl_itemrequests WHERE request_id = $req_no");
                $items[0] = array(
                    'requestitem_description' => $item_arr[0]['request_description'] , 
                    'requestitem_units'=> 'item', 
                    'requestitem_qty' => 1, 
                    'requestitem_price' => $item_arr[0]['request_amt']
                );

            }

            $contract_sum = get_query("SELECT CONCAT(request_amt,' ',request_currency) AS sum FROM tbl_itemrequests WHERE request_id = $req_no");
            $sum = $contract_sum[0]['sum'];


            $emp = get_query("select emp_fullname, emp_email, emp_cellphone from tbl_employees where emp_designation like '%admin officer%' and emp_active = 1 ");
            $contact_person = array($emp[0]['emp_fullname'], $emp[0]['emp_email'], $emp[0]['emp_cellphone']);

            $rak_address = 'Adjacent to Co-op Bank Kololo, Airport Road, Off American Embassy, Thong Ping Area, Juba, South Sudan';

            $req_currency = get_that_data('tbl_itemrequests', 'request_currency', 'request_id', $req_no);

            $payload = array(
                'req_no' => $req_no,
                'supplier_name' => $supplier_name,
                'supplier_address' => $supplier_address,
                'quotation_ref' => $quotation_ref,
                'quotation_date' => $quotation_date,
                'completion_unit' => $completion_unit,
                'completion_count' => $completion_count,
                'warranty_period' => $warranty_period,
                'items' => $items,
                'contract_sum' => $sum,
                'contact_person' => $contact_person,
                'delivery_address' => $rak_address,
                'currency' => $req_currency
            );

            $data = serialize($payload);
            $sql = "INSERT INTO tbl_lpo (lpo_req, lpo_data) VALUES ( $req_no, '$data' ) ON DUPLICATE KEY UPDATE lpo_data = '$data' ";

            if ($this->db->query($sql)) {

                $id = $this->db->query("select id from tbl_lpo where lpo_data = '$data' ")->result();
                $payload['po_no'] = $id[0]->id;

                $this->lpo($payload);

            }

        }

    }

    private function lpo($payload)
    {         
        
        $this->load->library('fpdf');
        $pdf = new FPDF();
        
        $p = $payload;

        $pdf->AliasNbPages();
        
        $pdf->AddPage();

        $pdf->SetFillColor(81,50,42);
        $pdf->SetTextColor(255);
        $pdf->SetFont('Times','B',12);
        $pdf->Cell(130,5," ",0,0,'R');
        $pdf->Cell(60,5,"LOCAL PURCHASE ORDER",0,1,'C',1);

        $pdf->Ln();
        $pdf->SetTextColor(0);
        $pdf->Cell(190,5,"Procurement Number",1,1,'C');
        $pdf->Cell(60,5,"Department",1,0,'C');
        $pdf->Cell(50,5,"REQ No",1,0,'C');
        $pdf->Cell(40,5,"PO No",1,0,'C');
        $pdf->Cell(40,5,"Fiscal Year",1,0,'C');

        $pdf->Ln();
        $pdf->SetFont('Times','',11);
        $pdf->Cell(60,5,"Procurement",1,0,'C');
        $pdf->Cell(50,5,$p['req_no'],1,0,'C');
        $pdf->Cell(40,5,$p['po_no'],1,0,'C');
        $pdf->Cell(40,5,"2015",1,0,'C');

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',12);
        $pdf->Cell(20,5,"To:",0,0,'L');

        $pdf->Ln();
        $pdf->SetFont('Times','B',11);
        $pdf->cell(130,5,$p['supplier_name'],0,1,'L');
        $pdf->cell(130,5,$p['supplier_address'],0,1,'L');

        $pdf->Ln();
        $pdf->SetFont('Times','',12);
        $pdf->Write(5, "Your quotation reference $p[quotation_ref] dated $p[quotation_date] is accepted and your are required to supply the goods to perform the works or services as detailed on the attached Schedule of Requirements against the terms contained in this Purchase Order. This order is placed subject to the attached General Conditions of Contract for Local Purchase Orders, except where modified by the terms below.");

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',12);
        $pdf->Write(5, "Specific Terms of this Purchase Order:");

        $pdf->Ln();
        $pdf->Ln();
        $pdf->cell(35,5,'1)  Contract Sum: ',0,0,'L');
        $pdf->SetFont('Times','',12);
        $pdf->cell(20,5,"The Contract Sum is $p[contract_sum] ");

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',12);
        $pdf->cell(45,5,'2)  Completion Period: ',0,0,'L');
        $pdf->SetFont('Times','',12);
        $pdf->Write(5,"The goods are to be delivered or the works or services performed within $p[completion_count] $p[completion_unit] from the date of this purchase order");

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',12);
        $pdf->cell(30,5,'3)  Warranty: ',0,0,'L');
        $pdf->SetFont('Times','',12);
        $pdf->Write(5,"The warranty/guarantee period is $p[warranty_period] months");

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',12);
        $pdf->cell(35,5,'4)  Delivery point: ',0,0,'L');
        $pdf->SetFont('Times','',12);
        $pdf->Write(5,"The goods are to be delivered to, or the works or services provided at $p[delivery_address] ");

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',12);
        $pdf->cell(35,5,'5)  Contact Person: ',0,0,'L');
        $pdf->SetFont('Times','',12);
        $name = $p['contact_person'][0];
        $email = $p['contact_person'][1];
        $tele = $p['contact_person'][2];
        $pdf->Write(5,"Enquiries and documentation should be addressed to $name at $p[delivery_address] ");
        $pdf->Ln();
        $pdf->Write(5,"Telephone Number: $tele Email: $email");

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',12);
        $pdf->cell(45,5,'6)  Payment to Supplier: ',0,0,'L');
        $pdf->SetFont('Times','',12);
        $pdf->Write(5,"Payment will be made in full within forty-five (45) days on completion of satisfactory performance of the contract. The following documentation must be supplied for payment to be made: ");
        $pdf->Ln();
        $pdf->cell(10,5,' ',0,0,'L');
        $pdf->cell(35,5,'i) An original and two copies of an Invoice,',0,0,'L');
        $pdf->Ln();
        $pdf->cell(10,5,' ',0,0,'L');
        $pdf->cell(35,5,'ii) A delivery note evidencing dispatch of the goods,',0,0,'L');
        $pdf->Ln();
        $pdf->cell(10,5,' ',0,0,'L');
        $pdf->cell(35,5,'iii) A completion certificate signed by ',0,0,'L');
        
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',12);
        $pdf->Write(5,'Purchase Order Authorised by:');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','',12);
        $pdf->Write(5,'Signature:................................................................ Name:......................................................................');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Write(5,'Position:................................................................... Date:.........................................................................');


        $pdf->AddPage();

        $pdf->Cell(10,10,'',0,1,'C');
        $pdf->SetTextColor(0);
        $pdf->SetFont('Times','B',11);
        $pdf->Cell(190,5,"Procurement Number",1,1,'C');
        $pdf->Cell(60,5,"Department",1,0,'C');
        $pdf->Cell(50,5,"REQ No",1,0,'C');
        $pdf->Cell(40,5,"PO No",1,0,'C');
        $pdf->Cell(40,5,"Fiscal Year",1,0,'C');

        $pdf->Ln();
        $pdf->SetFont('Times','',11);
        $pdf->Cell(60,5,"Procurement",1,0,'C');
        $pdf->Cell(50,5,$p['req_no'],1,0,'C');
        $pdf->Cell(40,5,$p['po_no'],1,0,'C');
        $pdf->Cell(40,5,"2015",1,0,'C');

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',12);
        $pdf->Write(5,"SCHEDULE OF REQUIREMENTS/ACTIVITY SCHEDULE");

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',11);
        $header = array('Item No.', 'Description', 'Unit of Measure', 'Quantity', 'Unit Price', 'Total Price');
        // Column widths
        $w = array(20, 60, 30, 20, 30, 30);

        // Header
        for($i=0;$i<count($header);$i++) {
            
            if ($i == 4) {
                $pdf->Cell($w[$i],9,$header[$i]." ($p[currency])",1,0,'C');
            } elseif ($i == 5) {
                $pdf->Cell($w[$i],9,$header[$i]." ($p[currency])",1,0,'C');
            } else {
                $pdf->Cell($w[$i],9,$header[$i],1,0,'C');
            }

        }
        $pdf->Ln();
        // items
        foreach ($p['items'] as $it) {
            $i = 1;
            $data[] = array(
                $i, 
                $it['requestitem_description'], 
                $it['requestitem_units'], 
                $it['requestitem_qty'], 
                $it['requestitem_price'],
                ( (float)$it['requestitem_qty'] * (float)$it['requestitem_price'])
            );

        }


        $pdf->SetFont('Times','',11);
        // Data
        foreach($data as $row) {

            $pdf->Cell($w[0],6,$row[0],1,0,'1');
            $pdf->Cell($w[1],6,$row[1],1,0,'LR');
            $pdf->Cell($w[2],6,$row[2],1,0,'C');
            $pdf->Cell($w[3],6,$row[3],1,0,'C');
            $pdf->Cell($w[4],6,number_format($row[4],2,'.',','),1,0,'C');
            $pdf->Cell($w[5],6,number_format($row[5],2,'.',','),1,0,'C');
            $pdf->Ln();

        }
        // Closing line*/
        //$pdf->Cell(array_sum($w),0,'','T');

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','B',12);
        $pdf->Write(5,'Receipt of Order by the Supplier:');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times','',12);
        $pdf->Write(5,'Signature:.......................................................................... Name:......................................................................');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Write(5,'Position:..............................................................................................................................................................');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Write(5,'On behalf of:...................................................................... Date:.........................................................................');

        $pdf->Output();
        
    }

}

/*

Notifications 
    -> cancelled job, send daily notifications until feedback from client is uploaded
    -> in progress job milestones, notifications for each milestones

in progress jobs
    -> define milestones for each job
    -> 

completed jobs -> press and inhouse and outsourced


delivered -> job creator, production department, client (notify job delivery & req fro payment)

car tracking -> move to admin dept 

Due Payments - payment status
    -> 7, 15, 30, 45, 60, 90 days sfter delivery 
    -> after 90 days, moved to bad debts (daily notifications)
    
    Notifications (after delivery) - to company & customer (attach invoice) - add preferable mode of payment (corporate - cheque, walkin - cash)
    Notify when cheque is ready for collection
    -> 7 days - 3x
    -> after 45 days, penalty of X% per month

Debt Collector 
    -> daily payment collection + invoice

Monthly followup emails to all clients - 15th of every month
Reminders of public and special holidays -
Promotions - emails sent



*/