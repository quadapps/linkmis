<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Larry Wambua
 * @copyright Quadrant Softwares 2014
 */

class Users_model extends CI_Model
{
    var $table = 'tbl_users';
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function create($emp_id="")
    {
        if(count($_POST) == 0) {
            return false;
        } else {
            $email = strtolower($this->input->post('email'));
            if($this->user_exists($email)) {
                return false;
            } else {
                $name = get_that_data('tbl_employees', 'emp_fullname', 'emp_id', $emp_id);
                $data['user_fullname'] = $name;
                $data['user_email'] = $email;
                $data['user_password'] = md5('Password@20');
                $data['user_empid'] = $emp_id;
                $data['user_levelid'] = $this->input->post('user_level');
                $data['user_creator'] = $this->session->userdata('user_id');
                $data['user_key'] = base64_encode(random_bytes(10));
                $data['user_createdate'] = date('Y-m-d_H-i-s');

                $this->db->insert('tbl_users', $data);

                $msg = "Welcome to LINKMIS - a centralized Management Information System for Links Pay Ltd.";
                @send_email($email, 'Welcome to LINKMIS', $msg, false, $name, site_url('welcome/modal/set_pass/'.$data['user_key']), 'Set Password');
                return true;
            }
        }
    }

    public function update()
    {
        if (empty($_POST)) {
            return json_encode(['status' => 0, 'msg' => 'Nothting received!']);
        }

        $data = [
            'user_fullname' => $this->input->post('name'),
            'user_email' => $this->input->post('email')
        ];

        if($this->db->update('tbl_users', $data, ['user_id' => $this->input->post('uid')])) return json_encode(['status' =>1, 'msg' => 'User update']);

        else return json_encode(['status' => 0, 'msg' => 'User not updated!']);
    }

    public function set_pass()
    {
        if (empty($_POST)) {
            return json_encode(array('status' => 0, 'msg' => 'Nothing received'));
        }

        $key = $this->input->post('key');
        $user = get_row_data('tbl_users', 'user_key', $key);

        if(empty($user)) return json_encode(array('status' => 0, 'msg' => 'Invalid key'));

        //if(days_between_dates($user['user_createdate'], date('Y-m-d_H-i-s')) > 3) return json_encode(array('status' => 0, 'msg' => 'Expired key'));

        $this->db->update('tbl_users', array('user_password' => md5($this->input->post('password'))), array('user_id' => $user['user_id']));

        //return json_encode(array('status' => 1, 'msg' => 'Confirmed'));
        redirect();
    }
    
    public function user_exists($email)
    {
        $query = $this->db->get_where('tbl_users', array('user_email' => $email));
        if($query->num_rows() == 0) return false;
        else return true;
    }

    public function reset_passkey()
    {
        if (empty($_POST)) {
            return json_encode(['status' => 0, 'msg' => 'Nothting received!']);
        }

        $uid = $this->input->post('uid');
        $user = get_row_data('tbl_users', 'user_id', $uid);
        $data['user_key'] = $this->random_token();

        $msg = "Welcome to LINKMIS - a centralized Management Information System for Links Pay Ltd. Click the link below to login";
        @send_email($user['user_email'], 'Welcome to LINKMIS', $msg, false, $user['user_fullname'], site_url('welcome/modal/set_pass/'.$data['user_key']) , 'Login Now');
        if ($this->db->update('tbl_users', $data, ['user_id' => $uid])) {
            return json_encode(['status' => 1, 'msg' => 'Password link Sent!']);

        } else return json_encode(['status' => 0, 'msg' => 'Error occured!']);
    }
    
    public function db_backup()
    {
        $DBUSER=$this->db->username;
        $DBPASSWD=$this->db->password;
        $DATABASE=$this->db->database;
    
        $filename = $DATABASE . "-" . date("Y-m-d_H-i-s") . ".sql.gz";
        $mime = "application/x-gzip";
    
        header( "Content-Type: " . $mime );
        header( 'Content-Disposition: attachment; filename="' . $filename . '"' );
    
        // $cmd = "mysqldump -u $DBUSER --password=$DBPASSWD $DATABASE | gzip --best";   
        $cmd = "mysqldump -u $DBUSER --password=$DBPASSWD --no-create-info --complete-insert $DATABASE | gzip --best";
    
        passthru( $cmd );
        
        //fopen($file) or die('Could not open file');
    
        exit(0);
    }
    
    public function toggle_user()
    {
        if(empty($_POST)) {
            $res = array('status' => 0, 'msg' => 'Sorry! We did not receive a thing!!');
        } else {
            $uid = $this->input->post('uid');
            $status = $this->input->post('status');
            
            if($this->db->update('tbl_users', array('user_active' => $status), array('user_empid' => $uid))) $res = array('status' => 1, 'msg' => 'User updated successfully!');
            else $res = array('status' => 0, 'msg' => 'Error!! User could not be updated. Please try again later!');
        }
        
        return json_encode($res);
    }

    public function create_task($details = false)
    {
        if($details) {
            return $this->db->insert('tbl_tasks', $details);
        } else {
            if(!empty($_POST)) {
                $data = array(
                        'task_creator' => $this->session->userdata('user_id'),
                        'task_clientid' => $this->input->post('client'),
                        'task_priority' => $this->input->post('priority'),
                        'task_description' => $this->input->post('desc'),
                        'task_start' => $this->input->post('start'),
                        'task_end' => $this->input->post('end'),
                        'task_tags' => $this->input->post('tags'),
                        'task_createdate' => date('Y-m-d_H-i-s')
                    );
                if ($this->db->insert('tbl_tasks', $data)) {
                    return json_encode(array('status' => 1, 'msg' => 'Task created successfully'));
                } else {
                    return json_encode(array('status' => 0, 'msg' => 'Task could not be created. Please try again!'));
                }
            } else {
                return json_encode(array('status' => 0, 'msg' => 'Nothing was received'));
            }
        }
    }

    public function remove_things()
    {
        if (empty($_POST)) {
            return json_encode(array('status' => 0, 'msg' => 'Nothing received!'));
        } 

        $tbl = $this->input->post('tbl');

        $arr = explode('_', $tbl);
        if($arr[0] != 'tbl') $tbl = 'tbl_'.$tbl;

        $id = $this->input->post('id');
        $key = get_tablekey($tbl);
        $arr = explode('_', $key);
        $pref = reset($arr);

        $data = array($pref.'_active' => 0);
        $updator = $pref.'_updator';
        if(colums_exists($tbl, $updator)) $data[$updator] = $this->session->userdata('user_id');

        if ($this->db->update($tbl, $data, array($key => $id))) {
            return json_encode(array('status' => 1, 'msg' => 'success'));
        } else {
            return json_encode(array('status' => 0, 'msg' => 'Operation failed!'));
        }
    }

    public function log_comment()
    {
        if (empty($_POST)) {
            return json_encode(array('status' => 0, 'msg' => 'Nothing received!'));
        } 

        $data = array(
                'clc_details' => $this->input->post('comment'),
                'clc_logid' => $this->input->post('id'),
                'clc_creator' => $this->session->userdata('user_id'),
                'clc_createdate' => date('Y-m-d H:i:s')
            );

        if ($this->db->insert('tbl_clogcomments', $data)) {
            return json_encode(array('status' => 1, 'msg' => 'success'));
        } else {
            return json_encode(array('status' => 0, 'msg' => 'Operation failed!'));
        }
    }

    private function random_token($length = 32){
        if(!isset($length) || intval($length) <= 8 ){
          $length = 32;
        }
        if (function_exists('random_bytes')) {
            return bin2hex(random_bytes($length));
        }
        if (function_exists('mcrypt_create_iv')) {
            return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
        }
        if (function_exists('openssl_random_pseudo_bytes')) {
            return bin2hex(openssl_random_pseudo_bytes($length));
        }
    }
}

