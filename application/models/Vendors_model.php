<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendors_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function create()
	{
		if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing Received']);

		$data = [
				'supplier_name' => strtoupper($this->input->post('name')),
				'supplier_phone' => $this->input->post('phone'),
				'supplier_email' => $this->input->post('email'),
				'supplier_address' => $this->input->post('address'),
				'supplier_contactperson' => $this->input->post('cp'),
				'supplier_contactpersonphone' => $this->input->post('cp_phone'),
				'supplier_contactpersonemail' => $this->input->post('cp_email'),
				'supplier_country' => $this->input->post('country'),
				'supplier_creator' => $this->session->userdata('user_id'),
				'supplier_createdate' => date('Y-m-d H:i:s')
			];

		if($this->db->insert('tbl_suppliers', $data))
			return json_encode(['status' => 1, 'msg' => 'Supplier created successfully']);
		else
			return json_encode(['status' => 0, 'msg' => 'Error creating supplier '.$this->db->error()->message]);
	}

	public function update()
	{
		if(empty($_POST)) return ['status' => 0, 'msg' => 'Nothing Received'];

		$data = [
				'supplier_name' => strtoupper($this->input->post('name')),
				'supplier_phone' => $this->input->post('phone'),
				'supplier_email' => $this->input->post('email'),
				'supplier_address' => $this->input->post('address'),
				'supplier_contactperson' => $this->input->post('cp'),
				'supplier_contactpersonphone' => $this->input->post('cp_phone'),
				'supplier_contactpersonemail' => $this->input->post('cp_email'),
				'supplier_country' => $this->input->post('country'),
				'supplier_updator' => $this->session->userdata('user_id')
			];

		if($this->db->update('tbl_suppliers', $data, ['supplier_id' => $this->input->post('supplier_id')]))
			return json_encode(['status' => 1, 'msg' => 'Supplier updated successfully']);
		else
			return json_encode(['status' => 0, 'msg' => 'Supplier updated successfully']);
	}

	public function create_po()
	{
		if(empty($_POST)) return ['status' => 0, 'msg' => 'Nothing Received'];


		$p = $this->input;
            
            $client = $p->post('supplier');
            $type = $p->post('type');
            if($type == 2) $mats = json_encode(array(
                    'desc' => $p->post('desc'),
                    'qty' => $p->post('qty'),
                    'price' => $p->post('amt')
                ));
            else $mats = $p->post('mats');
            $data = array(
                    'po_supplierid' => $client,
                    'po_ref' =>strtoupper( $p->post('ref')),
                    'po_currency' => $p->post('currency'),
                    'po_amt' => array_sum($p->post('amt')),
                    // 'po_cpname' => strtoupper($p->post('cp_name')),
                    // 'po_cpphone' => $p->post('cp_phone'),
                    // 'po_cpemail' => $p->post('cp_email'),
                    'po_terms' => preg_replace("/\r\n|\r|\n/",'<br/>',$p->post('terms')),
                    'po_items' => $mats,
                    'po_type' => $type,
                    'po_createdate' => date('Y-m-d H:i:a'),
                    'po_creator' => $this->session->userdata('user_id')
                );

            if($this->db->insert('tbl_po', $data)) {
            	$id = $this->db->insert_id();
            	$url = site_url('welcome/view/lpo/'.$id);
            	return json_encode(['status' => 1, 'msg' => 'PO created', 'url' => $url]);
			} else
				return json_encode(['status' => 0, 'msg' => 'PO not saved '.$this->db->error()->message]);
    }

    public function update_po()
    {
        if(count($_POST) == 0) { $res = array('status' => 0, 'msg' => 'We did not recieve anything to save.');
         } else {
            $p = $this->input;
            $id = $p->post('po_id');
            $client = $p->post('supplier');
             $type = $p->post('type');
            if($type == 2) $mats = json_encode(array(
                    'desc' => $p->post('desc'),
                    'qty' => $p->post('qty'),
                    'price' => $p->post('amt')
                ));
            else $mats = $p->post('mats');
            $data = array(
                    'po_supplierid' => $client,
                    'po_ref' => $p->post('ref'),
                    'po_currency' => $p->post('currency'),
                    'po_terms' => preg_replace("/\r\n|\r|\n/",'<br/>',$p->post('terms')),
                    'po_items' => $mats,
                    'po_updator' => $this->session->userdata('user_id')
                );
                
            if($this->db->update('tbl_po', $data, array('po_id' => $id))) {

                $url = site_url('welcome/view/lpo/'.$id);
                
                $res = array('status' => 1, 'msg' => 'PO updated successfully.', 'id' => $id, 'url' => $url);
            } else {
                $res = array('status' => 0, 'msg' => 'An error occured while saving PO. Please try again later.');
            }
        }
        
        return json_encode($res);
    }

    public function confirm_po()
    {
    	if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing Received']);

    	$data['po_status'] = 1;
    	$data['po_ref'] = $this->input->post('ref');
        $po = $this->input->post('quote');

    	if($this->db->update('tbl_po', $data, ['po_id' => $po])) {
            $this->create_requisition($po);
            return json_encode(['status' => 1, 'msg' => 'PO Confirmed successfully']);
        }
    	else
    		return json_encode(['status' => 0, 'msg' => 'PO confirmation failed '.$this->db->error()->message]);
    }

    public function cancel_po()
    {
        if(empty($_POST)) return json_encode(['status' => 0, 'msg' => 'Nothing Received']);

        $data['po_status'] = -1;
        $data['po_cancelremarks'] = $this->input->post('remarks');
        $data['po_cancelby'] = $this->session->userdata('user_id');
        $po = $this->input->post('po');

        if($this->db->update('tbl_po', $data, ['po_id' => $po])) {
            //$this->create_requisition($po);
            return json_encode(['status' => 1, 'msg' => 'PO cancelled successfully']);
        }
        else
            return json_encode(['status' => 0, 'msg' => 'PO cancelled failed '.$this->db->error()->message]);
    }

   private function create_requisition($po)
    {
        $row = get_row_data('tbl_po', 'po_id', $po);
        $job_id = $this->input->post('ref');
                $data['exp_remarks'] = $this->input->post('remarks');
                $data['exp_creator'] = $this->session->userdata('user_id');
                $data['exp_amt'] = $row['po_amt'];
                $data['exp_currency'] = $row['po_currency'];
                $data['exp_catid'] = $this->input->post('cat');
                $data['exp_deptid'] = $this->input->post('dept');
                $data['exp_name'] = get_that_data('tbl_suppliers', 'supplier_name', 'supplier_id', $row['po_supplierid']);
                $data['exp_billno'] = $row['po_ref'];
                $data['exp_poid'] = $row['po_id'];
                $data['exp_billdate'] = $row['po_createdate'];
                $data['exp_epd'] = $this->input->post('epd');
                // $data['exp_details'] = $this->input->post('details');
                $data['exp_acctnumber'] = $this->input->post('acct');
                $data['exp_type'] = $this->input->post('type');
                $data['exp_createdate'] = date('Y-m-d H:i:s');
                if($this->db->insert('tbl_expenses', $data)) {
                    $res = array('status' => 1, 'msg' => 'Expense posted Successfully.');
                    $hod = get_that_data('tbl_employees', "emp_email", 'emp_id', get_that_data('tbl_departments', 'dept_empid', 'dept_id', $data['exp_deptid']));

                    @send_mail($hod, 'New Request made', 'New bill request is awaiting your approval. Please login to the system to approve', false, false, site_url('welcome/view/expenses'), 'Approve Now');

                } else {
                    $res = array('status' => 0, 'msg' => 'Error! Expense could not be created');
                }

        return json_encode($res);
    }

    private function add_items($req_id, $row)
    {
        $items = '';
        if($row['po_type'] == 2) $items = json_decode($row['po_items']);
        
        $items_ = '';
        $cost_ = 0;

        if(isset($items['desc'])) {
            $i = 0;
            for($j=0; $j<count($items['desc']); $j++) {
                $data['requestitem_requestid'] = $req_id;
                $data['requestitem_qty'] = $items['qty'][$j];
                $data['requestitem_description'] = $items['desc'][$j];
                $data['requestitem_price'] = $items['price'][$j];
                // $data['requestitem_units'] = $items['units'][$j];
                $data['requestitem_createdate'] = date('Y-m-d H:i:s');
                $data['requestitem_creator'] = $this->session->userdata('user_id');
                
                $items_ .= $items['qty'][$j].' '.$items['desc'][$j].'<br>';
                $cost_ += ((int)$items['qty'][$j] * (int)$items['price'][$j]);

                $this->db->insert('tbl_requestitems', $data);
                $i++;
            }
            
            return array('request_description' => $items_, 'request_amt' => $cost_);
        }
        
        return 'You cannot loop through a string man!';
    }
}