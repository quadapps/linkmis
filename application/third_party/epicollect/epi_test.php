<?php
require 'vendor/autoload.php';

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

 $tokenClient = new Client(); //GuzzleHttp\Client
 $tokenURL = 'https://five.epicollect.net/api/oauth/token';

 //get token first
 try {
     $tokenResponse = $tokenClient->request('POST', $tokenURL, [
         'headers' => ['Content-Type' => 'application/vnd.api+json'],
         'body' => json_encode([
             'grant_type' => 'client_credentials',
             'client_id' => 2072,
             'client_secret' => 'FrlcCGduwkqGB2811rKGWVgig7g385lSvU3XO3zu'
         ])
     ]);

     $body = $tokenResponse->getBody();
     $obj = json_decode($body);
     $token = $obj->access_token;
 } catch (RequestException $e) {
     //handle errors
     die("errors");
 }

 //get entries now
 $entriesURL = 'https://five.epicollect.net/api/export/entries/city-review-paper-distribution
?form_ref=8c64ef46b70c49f8ae5007ed7ce5fafd_5fab915ca1326';
 $entriesClient = new Client([
     'headers' => [
         'Authorization' => 'Bearer '.$token //this will last for 2 hours!
     ]
 ]);

 try {
     $response = $entriesClient-> request('GET', $entriesURL);

     $body = $response->getBody();
     $obj = json_decode($body);

     //do something with the entries
     echo '<pre>';
     print_r($obj->data->entries);
     echo '</pre>';

 } catch (RequestException $e) {
     //handle errors
     //...
 }