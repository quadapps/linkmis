<?php
$this->load->view('layout/header', ['title' => 'Dashboard', 'page' => 1]);
$uid = $this->session->userdata('user_id');

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-01');
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d');

$emp = get_data('tbl_employees', "JOIN tbl_users ON user_empid = emp_id WHERE user_id = $uid", 'emp_designation, user_levelid, emp_id, emp_fullname, emp_gmail', true);

$des = strtolower($emp['emp_designation']);


?>

<div class="my-3 my-md-5">
					<div class="container">
						
						<?php if(strtolower($emp['emp_designation']) == 'agent') {
						
						$this->load->view('agents_home', ['emp' => $emp]);

						 } elseif($des == 'cash collector') { ?>

							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Cash Collections</h3>

										<ul class="card-options">
											<!-- <li>
												<button class="btn btn-secondary" data-toggle="modal" data-target="#collModal">Issue Cash</button>
											</li> -->
										</ul>
									</div>
									<div class="table-responsive">
										<table class="table card-table table-vcenter text-nowrap data-table">
											<thead>
												<tr>
													<th>Date</th>
													<th class="w-1">Agent Code</th>
													<th>Agent Name</th>
													<th>Amount</th>
													<th>Status</th>
													<th class="text-right">Action</th>
												</tr>
											</thead>
											<tbody>
											<?php foreach(get_data('tbl_collections', "JOIN tbl_agents ON collection_agent = agent_empid WHERE collection_collector = ".$emp['emp_id']) as $d) : ?>
												<tr>
													<td><span class="text-muted"><?php echo date('d F, Y', strtotime($d['collection_createdate']));?></span></td>
													<td><?php echo $d['agent_code'];?></td>
													<td><?php echo $d['agent_name'];?></td>
													<td><?php echo $d['collection_currency'].$d['collection_amt'];?></td>

													<td>
													 <?php 
													 if($d['collection_status'] == 0) echo '<span class="status-icon bg-primary"></span> Pending';
													 if($d['collection_status'] == 1) echo '<span class="status-icon bg-info"></span> Collected';
													 if($d['collection_status'] == 2) echo '<span class="status-icon bg-success"></span> Received';
													 if($d['collection_status'] == -1) echo '<span class="status-icon bg-danger"></span> Declined';
													 ?>
													 	
													 </td>
													 
													<td class="text-right">

														<?php if($d['collection_status'] == 0 && $des == 'cash collector') : ?>
															<a href="javascript:void(0)" class="btn btn-green btn-sm collected" data-id="<?php echo $d['collection_id'];?>" data-amt="<?php echo $d['collection_amt'];?>" data-currency="<?php echo $d['collection_currency'];?>" data-status="1"><i class="fa fa-thumbs-up"></i> Collect</a>
															<a href="javascript:void(0)" class="btn btn-danger btn-sm not-collected" data-id="<?php echo $d['collection_id'];?>" data-amt="<?php echo $d['collection_amt'];?>" data-currency="<?php echo $d['collection_currency'];?>" data-status="1"><i class="fa fa-thumbs-down"></i> Decline</a>
														<?php endif; ?>

													</td>
												</tr>
											<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>

							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Cash Delivery</h3>

										<ul class="card-options">
											<!-- <li>
												<button class="btn btn-secondary" data-toggle="modal" data-target="#collModal">Issue Cash</button>
											</li> -->
										</ul>
									</div>
									<div class="table-responsive">
										<table class="table card-table table-vcenter text-nowrap data-table">
											<thead>
												<tr>
													<th>Date</th>
													<th class="w-1">Agent Code</th>
													<th>Agent Name</th>
													<th>Amount</th>
													<th>Status</th>
													<th class="text-right">Action</th>
												</tr>
											</thead>
											<tbody>
											<?php foreach(get_data('tbl_cashouts', "JOIN tbl_agents ON agent_empid = co_agentname WHERE co_type = 0 AND co_collector = ".$emp['emp_id']) as $d) : ?>
												<tr>
													<td><span class="text-muted"><?php echo date('d F, Y', strtotime($d['co_createdate']));?></span></td>
													<td><?php echo $d['agent_code'];?></td>
													<td><?php echo $d['agent_name'];?></td>
													<td><?php echo $d['co_currency'].$d['co_amt'];?></td>

													<td>
													 <?php 
													 if($d['co_status'] == 0) echo '<span class="status-icon bg-primary"></span> In Transit';
													 if($d['co_status'] == 1) echo '<span class="status-icon bg-success"></span> Received';
													 if($d['co_status'] == -1) echo '<span class="status-icon bg-danger"></span> Declined';
													 ?>
													 	
													 </td>
													 
													<td class="text-right btn-group">

														<?php if($d['co_status'] == 0) : ?>
															<a href="javascript:void(0)" class="btn btn-green btn-sm accept" data-id="<?php echo $d['co_id'];?>" data-status="1"><i class="fa fa-thumbs-up"></i> Accept</a>
															<a href="javascript:void(0)" class="btn btn-danger btn-sm reject" data-id="<?php echo $d['co_id'];?>" data-status="-1"><i class="fa fa-thumbs-down"></i> Decline</a>
														<?php endif; ?>

													</td>
												</tr>
											<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						<?php } else {

$txns = get_data('tbl_agenttxns', "WHERE at_active = 1 AND DATE(at_date) BETWEEN '$start' AND '$end'", "COUNT(at_id) AS total", true)['total'];
$ssp = get_data('tbl_agenttxns', "WHERE at_active = 1 AND at_currency = 'SSP' AND DATE(at_date) BETWEEN '$start' AND '$end'", "SUM(at_amt) AS total", true)['total'];
$usd = get_data('tbl_agenttxns', "WHERE at_active = 1 AND at_currency = 'USD' AND DATE(at_date) BETWEEN '$start' AND '$end'", "SUM(at_amt) AS total", true)['total'];

$tdr = get_data('tbl_tdractivities', "WHERE DATE(tdr_sodtime) BETWEEN '$start' AND '$end'", 'SUM(tdr_regsach) AS regs, SUM(tdr_actsach) AS agents', true);
$tdr_all = get_data('tbl_tdractivities', "", 'SUM(tdr_regsach) AS regs, SUM(tdr_actsach) AS agents', true);

$mdr = get_data('tbl_mdrreport', "WHERE DATE(mr_createdat) BETWEEN '$start' AND '$end'", 'SUM(Achieved_Recruitment) AS recs, SUM(Achieved_Activations) AS acts', true);
$mdr_all = get_data('tbl_mdrreport', "", 'SUM(Achieved_Recruitment) AS recs, SUM(Achieved_Activations) AS acts', true);
 ?>
						<div class="row row-cards row-deck">
              <div class=" col-lg-12">
                <div class="row">
                  <div class="col-sm-12 col-lg-3">
                    <div class="card">
                      <div class="card-body">
                        <div class="card-value float-right text-purple"><i class="fe fe-percent"></i></div>
                        <h3 class="mb-1"><?php echo number_format($txns, 0);?></h3>
                        <div class="text-muted">No of Transactions (MTD)</div>
                      </div>
                      <!-- <div class="card-chart-bg">
                        <div id="chart-bg-users-1"></div>
                      </div> -->
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-3">
                    <div class="card">
                      <div class="card-body">
                        <div class="card-value float-right text-green"><i class="fe fe-activity"></i></div>
                        <h3 class="mb-1"><?php echo number_format($ssp, 2);?></h3>
                        <div class="text-muted">Total SSP Value (MTD)</div>
                      </div>
                      <!-- <div class="card-chart-bg">
                        <div id="chart-bg-users-2" style="height: 100%"></div>
                      </div> -->
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-3">
                    <div class="card">
                      <div class="card-body">
                        <div class="card-value float-right text-orange"><i class="fe fe-dollar-sign"></i></div>
                        <h3 class="mb-1"><?php echo number_format($usd, 2);?></h3>
                        <div class="text-muted">Total USD Value (MTD)</div>
                      </div>
                      <!-- <div class="card-chart-bg">
                        <div id="chart-bg-users-3" style="height: 100%"></div>
                      </div> -->
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-3">
                    <div class="card">
                      <div class="card-body">
                        <div class="card-value float-right text-lightpink-red"><i class="fe fe-briefcase"></i></div>
                        <h3 class="mb-1"><a href="#"><?php echo number_format(100000, 2);?></a></h3>
                        <div class="text-muted">Total Expenditure (MTD)</div>
                      </div>
                      
                    </div>

                  </div>
                </div>
              </div>
            </div>

						<div class="row row-cards">
							<div class="col-sm-12 col-md-6">
								<div class="card ">
									<div class="card-body">
										<div id="chart-area-spline21" class="chart-visitors"></div>
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-md-6">
								<div class="card ">
									<div class="card-body">
										<div id="chart-area-spline22" class="chart-visitors"></div>
									</div>
								</div>
							</div>
						</div>
							
						<div class="row">
							<div class="col-sm-12 col-lg-4">
								<div class="card p-3">
									<div class="d-flex align-items-center">
										<span class="stamp stamp-md bg-cyan mr-3">
											<i class="fa fa-users"></i>
										</span>
										<div>
											<h4 class="m-0"><a href="javascript:void(0)"><strong><?php echo number_format($tdr_all['regs'], 0);?></strong> <small>Customers</small></a></h4>
											<small class="text-muted"><?php echo number_format($tdr['regs'], 0);?> new customers (MTD) </small>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-lg-4">
								<div class="card p-3">
									<div class="d-flex align-items-center">
										<span class="stamp stamp-md bg-orange mr-3">
											<i class="fa fa-cart-arrow-down"></i>
										</span>
										<div>
											<h4 class="m-0"><a href="javascript:void(0)"><strong><?php echo number_format($tdr_all['agents'], 0);?></strong> <small>Agents Onboard</small></a></h4>
											<small class="text-muted"><?php echo number_format($tdr['agents'], 0);?> new agents (MTD)</small>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-lg-4">
								<div class="card p-3">
									<div class="d-flex align-items-center">
										<span class="stamp stamp-md bg-teal mr-3">
											<i class="fa fa-eye"></i>
										</span>
										<div>
											<h4 class="m-0"><a href="javascript:void(0)"><strong><?php echo number_format($mdr_all['recs'], 0);?> </strong><small>Merchants Onboard</small></a></h4>
											<small class="text-muted"><?php echo number_format($mdr['acts'], 0);?> activated (MTD)</small>
										</div>
									</div>
								</div>
							</div>
							
							
						</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
	let options = {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Value Transacted (YTD)'
	    },
	    subtitle: {
	        text: 'In USD'
	    },
	    xAxis: {
	        categories: [],
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Amount (USD)'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0"><b>{point.y:.1f}</b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0
	        }
	    },
	    series: [{}]
	}

	let chart = Highcharts.chart('chart-area-spline21', options);
	chart.subtitle.update({text: "In SSP"});
	chart.yAxis[0].setTitle({
        text: 'Amount (SSP)'
    });

	let chart1 = Highcharts.chart('chart-area-spline22', options);
	chart1.subtitle.update({text: "In USD"});
	chart1.yAxis[0].setTitle({
        text: 'Amount (USD)'
    });

	$.getJSON('<?php echo site_url('data/get_chart_data');?>', function(data) {
		chart.xAxis[0].setCategories(data.ssp.cats);
		chart.series[0].setData(data.ssp.series);

		chart1.xAxis[0].setCategories(data.usd.cats);
		chart1.series[0].setData(data.usd.series);
	})
</script>
						
						<?php } ?>
						
					</div>
				</div>



<script type="text/javascript">
	
	$('.accept, .reject').click(function() {
		let id = $(this).data('id');
		let status = $(this).data('status');
        let amt = $(this).data('amt');
        let currency = $(this).data('currency');
		if(confirm("Are you sure?")) {
			$.post('<?php echo site_url('system/act/finance_model/cashout_action');?>', {id: id, status: status, amt: amt, currency: currency}).done(function(r) {
				swal('Success', r, 'success');
				location.reload();
			}).fail(function(e, r) {
				swal('Error', "Error: " + e.responseText, 'danger');
			})
		}
	});

	$('.collected, .not-collected').click(function() {
		let id = $(this).data('id');
		let status = $(this).data('status');
        let amt = $(this).data('amt');
        let currency = $(this).data('currency');
		if(confirm("Are you sure?")) {
			$.post('<?php echo site_url('system/act/finance_model/collect_cash');?>', {id: id, status: status, amt: amt, currency: currency}).done(function(r) {
				swal('Success', r, 'success');
				location.reload();
			}).fail(function(e, r) {
				swal('Error', "Error: " + e.responseText, 'danger');
			})
		}
	});
</script>

<?php
$this->load->view('layout/footer', ['dash' => 1, 'dtt' => 1]);
?>