<?php $emp = get_row_data('tbl_employees', 'emp_id', $this->session->userdata('emp_id')); ?>
				
				<aside class="app-sidebar">
					<div class="app-sidebar__user">
						<div class="dropdown">
							<a class="nav-link p-0 leading-none d-flex" data-toggle="dropdown" href="#">
								<span class="avatar avatar-md brround" style="background-image: url(<?php echo $emp['emp_photourl']?>)"></span>
								<span class="ml-2 "><span class="text-dark app-sidebar__user-name font-weight-semibold"><?php echo $emp['emp_fullname'];?></span><br>
									<span class="text-muted app-sidebar__user-name text-sm"> <?php echo $emp['emp_designation'];?></span>
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
								<a class="dropdown-item" href="<?php echo site_url('welcome/view/emp_details/'.$emp['emp_id'])?>"><i class="dropdown-icon mdi mdi-account-outline"></i> Profile</a>
								
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?</a>
								<a class="dropdown-item" href="<?php echo site_url('welcome/logout')?>"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
							</div>
						</div>
					</div>
					<ul class="side-menu">
						<li>
							<a class="side-menu__item <?php echo $page == 1 ? 'active' : '';?>" href="<?php echo site_url();?>"><i class="side-menu__icon fa fa-dashboard"></i><span class="side-menu__label">Dashboard</span></a>
						</li>
						<li class="slide">
							<a class="side-menu__item <?php echo $page == 2 ? 'active' : '';?>" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-users"></i><span class="side-menu__label">Customer Service</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('welcome/view/create_quote');?>" class="slide-item">Create Quotation</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/quotations');?>" class="slide-item">View Quotations</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/jobs');?>" class="slide-item">View Jobs</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/clients');?>" class="slide-item">Clients & Contacts</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/enquiries');?>" class="slide-item">Enquiries</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/report/activity_feed');?>" class="slide-item">Activity Report</a>
								</li>
								
							</ul>
						</li>
						
						<li class="slide">
							<a class="side-menu__item <?php echo $page == 3 ? 'active' : '';?>" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-building"></i><span class="side-menu__label">Production</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('welcome/view/jobs/1');?>" class="slide-item">Pending Jobs</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/jobs/2');?>" class="slide-item">In Design Jobs</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/jobs/6');?>" class="slide-item">PrePress Jobs</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/jobs/7');?>" class="slide-item">Press Jobs</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/jobs/8');?>" class="slide-item">Post Press Jobs</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/jobs/3');?>" class="slide-item">Completed Jobs</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/jobs/5');?>" class="slide-item">Delivered Jobs</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/jobs/4');?>" class="slide-item">Cancelled Jobs</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item <?php echo $page == 4 ? 'active' : '';?>" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-snowflake-o"></i><span class="side-menu__label">Sales &amp; Marketing</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('welcome/view/clients');?>" class="slide-item">Clients &amp; Contacts</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/visits');?>" class="slide-item">Activity Log</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/targets');?>" class="slide-item">Targets</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/performance');?>" class="slide-item">Sales Performance</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/report/activity_feed');?>" class="slide-item">Sales Report</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item <?php echo $page == 5 ? 'active' : '';?>" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-user"></i><span class="side-menu__label">Human Resource</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('welcome/view/depts');?>" class="slide-item">Departments</a>
								</li>
								
								<li>
									<a href="<?php echo site_url('welcome/view/employees');?>" class="slide-item">Employees</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/leaves');?>" class="slide-item">Leave Management</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/attendance');?>" class="slide-item">Attendance</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/trainings');?>" class="slide-item">Employee Trainings</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/complains');?>" class="slide-item">Employee Complaints</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/repos');?>" class="slide-item">Documents Repository</a>
								</li>

								<li>
									<a href="<?php echo site_url('welcome/reports/termations');?>" class="slide-item">Archived Staff</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/reports/hr_report');?>" class="slide-item">Report</a>
								</li>

							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item <?php echo $page == 6 ? 'active' : '';?>" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-lock"></i><span class="side-menu__label">Accounts &amp; Finance</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('welcome/view/req_pmts');?>" class="slide-item">Payables</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/payments');?>" class="slide-item">Receivables</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/debts');?>" class="slide-item">Outstanding Debts</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/balances');?>" class="slide-item">Outstanding Balances</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/deposits');?>" class="slide-item">Bank Deposits &amp; Withdrawals</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/store_req');?>" class="slide-item">Store Requisitions</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item <?php echo $page == 11 ? 'active' : '';?>" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-newspaper-o"></i><span class="side-menu__label">Project Management</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('welcome/view/projects');?>" class="slide-item">Projects</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/resources');?>" class="slide-item">Resources</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/project_tasks');?>" class="slide-item">Tasks</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/risks');?>" class="slide-item">Risks/Issues</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/project_report');?>" class="slide-item">Report</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item <?php echo $page == 7 ? 'active' : '';?>" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-newspaper-o"></i><span class="side-menu__label">Warehouse Management</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('welcome/view/items');?>" class="slide-item">Inventory &amp; Job Types</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/receivings');?>" class="slide-item">Receivings</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/store_req');?>" class="slide-item">Requisitions</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/issuings');?>" class="slide-item">Issuings</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item <?php echo $page == 8 ? 'active' : '';?>" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-table"></i><span class="side-menu__label">User Management</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								
								<li>
									<a href="<?php echo site_url('welcome/view/users');?>" class="slide-item">Users</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/activity');?>" class="slide-item">User Activity</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/backups');?>" class="slide-item">Backups</a>
								</li>
							</ul>
						</li>
						
						<li>
							<a class="side-menu__item <?php echo $page == 9 ? 'active' : '';?>" href="<?php echo site_url('welcome/view/req');?>" tooltip="Cash, Purchase, Fuel or Salary Advance"><i class="side-menu__icon fa fa-question-circle"></i><span class="side-menu__label">Requisitions</span></a>
						</li>

						<li class="slide">
							<a class="side-menu__item <?php echo $page == 12 ? 'active' : '';?>" data-toggle="slide" href="#"><i class="side-menu__icon fa fa-car"></i><span class="side-menu__label">Vehicle Logs</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="<?php echo site_url('welcome/view/vehicles');?>" class="slide-item">Vehicles &amp; Machines</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/vehicle_log');?>" class="slide-item">Mileage/Usage Logs</a>
								</li>
								<li>
									<a href="<?php echo site_url('welcome/view/vehicle_report');?>" class="slide-item">Vehicles Report</a>
								</li>
							</ul>
						</li>
					</ul>
				</aside>