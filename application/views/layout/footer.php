
</div>

      <!--footer-->
      <footer class="footer">
        <div class="container">
          <div class="row align-items-center flex-row-reverse">
            <div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
              Copyright &copy; <?php echo date('Y');?> <a href="https://links-pay.com" target="_blank">Links Pay Ltd.</a> All rights reserved. Built by <a href="https://quadrantsoftwares.com" target="_blank">Quadrant</a>
            </div>
          </div>
        </div>
      </footer>
      <!-- End Footer-->
    </div>
    
    <!-- Back to top -->
    <!-- <a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a> -->

<!-- Modal -->
<div class="modal fade" id="taskModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="ajax-form" method="post" action="<?php echo site_url('system/act/hr_model/create_task')?>">
        <div class="modal-body">
        <div class="form-group">
          <label>Task</label>
          <input type="text" name="title" class="form-control" required="">
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Start</label>
            <input type="text" name="start" class="form-control datetimepicker" required="">
          </div>
          <div class="form-group col-md-6">
            <label>End</label>
            <input type="text" name="end" class="form-control datetimepicker" required="">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Complexity</label>
            <select name="complexity" class="form-control">
              <option>Easy</option>
              <option>Medium</option>
              <option>Hard</option>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>Status</label>
            <select name="status" class="form-control">
              <option value="0">To Do</option>
              <option value="1">Started</option>
              <option value="2">In Progress</option>
              <option value="3">Done</option>
            </select>
          </div>
        </div>
<input type="hidden" name="actions" value="none">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modals -->
<div class="modal fade" id="ajaxModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="ajax-content"></div>
    </div>
  </div>
</div>

<div class="modal fade" id="client-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add/Modify Client</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" id="frm-clients" method="post" action="<?php echo
site_url('system/act/clients_model/create_client') ?>" class="ajax-form">
      <div class="modal-body">
        
          <div class="form-group">
            <label for="client_name">Client Name</label>
            <input type="text" name="client_name" class="form-control" id="client_name" placeholder="Client Name" required="">
          </div>
          <div class="form-group">
            <label for="address">Address</label>
            <input type="text" name="address" class="form-control" id="address" placeholder="Physical Address" />
          </div>
          <div class="row no-gutter">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="client_email">Client Email</label>
                    <input type="email" name="client_email" class="form-control" id="client_email" placeholder="Client Email" required="" />
                  </div>
            </div>
            <div class="col-md-6">
                  <div class="form-group">
                    <label for="client_phone">Client Phone</label>
                    <input type="text" name="client_phone" class="form-control" placeholder="Client Phone" id="client_phone" required="" />
                  </div>
            </div>
          </div>
          <div class="form-group">
            <label for="contact_person">Contact Person Name</label>
            <input type="text" name="contact_person" class="form-control" placeholder="Contact Person" id="contact_person" required="" />
          </div>
          <div class="row no-gutter">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="contact_email">Contact Person Email</label>
                    <input type="email" name="contact_email" class="form-control" id="contact_email" placeholder="Contact Email" required="" />
                  </div>
            </div>
            <div class="col-md-6">
                  <div class="form-group">
                    <label for="contact_phone">Contact Person Phone</label>
                    <input type="text" name="contact_phone" class="form-control" placeholder="Contact Phone" id="contact_phone" required="" />
                  </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row no-gutter">
                <div class="col-md-6">
                    <label>Client Type</label>
                    <select name="type" class="form-control">
                        <option value="1">Walk-In</option>
                        <option value="2">Corporate</option>
                        <option value="3">Staff Trustee</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Client Category</label>
                    <select name="cat" class="form-control">
                        <option value="0">--Select Client Category---</option>
                        <?php foreach(get_data('tbl_clientcategories', "WHERE cat_active = 1", 'cat_id, cat_name') as $cats) { ?>
                        <option value="<?php echo $cats['cat_id']?>"><?php echo $cats['cat_name']?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
          </div>
          <div class="form-group">
            <label for="marketer">Assign Marketer</label>
            <select name="marketer" class="form-control">
                <option value="0">--Select Marketer--</option>
                <?php foreach (get_data('tbl_employees',
'WHERE emp_active = 1 ORDER BY emp_fullname ASC', 'emp_id, emp_fullname') as $e) { ?>
                <option value="<?php echo $e['emp_id'] ?>"><?php echo $e['emp_fullname'] ?></option>
                <?php } ?>
            </select>
          </div>
        <input type="hidden" name="client_id" id="client_id"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" id="btn-clients" class="btn btn-theme">Save Client</button>
      </div>
      </form>
    </div>
  </div>
</div>

		<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>
		<!-- Dashboard js -->
		<script src="<?php echo base_url('res/assets/js/vendors/jquery.sparkline.min.js');?>"></script>
		<script src="<?php echo base_url('res/assets/js/vendors/selectize.min.js');?>"></script>
		<script src="<?php echo base_url('res/assets/js/vendors/jquery.tablesorter.min.js');?>"></script>
		<script src="<?php echo base_url('res/assets/js/vendors/circle-progress.min.js');?>"></script>
		<script src="<?php echo base_url('res/assets/plugins/rating/jquery.rating-stars.js');?>"></script>

		<!-- Custom scroll bar Js-->
		<script src="<?php echo base_url('res/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js');?>"></script>

		<!-- c3.js Charts Plugin -->
		<script src="<?php echo base_url('res/assets/plugins/charts-c3/d3.v5.min.js');?>"></script>
		<script src="<?php echo base_url('res/assets/plugins/charts-c3/c3-chart.js');?>"></script>

		<!-- Input Mask Plugin -->
		<script src="<?php echo base_url('res/assets/plugins/input-mask/jquery.mask.min.js');?>"></script>

        <!-- Index Scripts -->
		<script src="<?php echo base_url('res/assets/js/index.js');?>"></script>

		<!-- Custom js -->
		<script src="<?php echo base_url('res/assets/js/custom.js');?>"></script>

    <script type="text/javascript">
      $('#ajaxModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var title = button.data('title') // Extract info from data-* attributes
        var url = button.data('url') // Extract info from data-* attributes
        var modal = $(this);
        var content_div = modal.find('#ajax-content');
        modal.find('.modal-title').text(title)
        content_div.html('<h1 class="text-center"><i class="fa fa-spin fa-spinner"></i> </h1>');
        $.get(url, function(data) {
            content_div.html(data);
        })
      })
    </script>
    <?php if (isset($dtp)) { //datetime picker ?>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>
    $(function() {
      $('.datepickers').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        locale: {
          format: 'YYYY-MM-DD'
        },
        //maxYear: parseInt(moment().format('YYYY'),10)
      }, function(start, end, label) {
        //var years = moment().diff(start, 'years');
        //alert("You are " + years + " years old!");
      });

      $('.datetimepicker').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        showDropdowns: true,
        minYear: 1901,
        locale: {
          format: 'YYYY-MM-DD hh:mm'
        },
        //maxYear: parseInt(moment().format('YYYY'),10)
      }, function(start, end, label) {
        //var years = moment().diff(start, 'years');
        //alert("You are " + years + " years old!");
      });

      $('#reportrange').daterangepicker({
          startDate: moment().subtract(1, 'year'),
          endDate: moment().endOf('year'),
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Week': [moment().startOf('week'), moment().endOf('week')],
             'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
             'This Year': [moment().startOf('year'), moment().endOf('year')],
             'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
          }
      }, function cb(start, end) {
        var url = $('#reportrange').data('url');
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          let f = '<?php echo isset($_GET['f']) ? $_GET['f'] : '';?>';
          if(url !== undefined) location.href = url+'/?s='+start.format('YYYY-MM-DD')+'&e='+end.format('YYYY-MM-DD')+'&f='+f;
      });

      $('.reportrange').daterangepicker({
          startDate: moment().subtract(1, 'year'),
          endDate: moment().endOf('year'),
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
             'This Year': [moment().startOf('year'), moment().endOf('year')],
             'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
          }
      }, function cb(start, end) {
          $('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          var url = $('.reportrange').data('url');
          let f = '<?php echo isset($_GET['f']) ? $_GET['f'] : '';?>';
          if(url !== undefined) location.href = url+'/?s='+start.format('YYYY-MM-DD')+'&e='+end.format('YYYY-MM-DD')+'&f='+f;
      });

    });
    </script>
    <?php } ?>

    <?php if (isset($dtt)) { //datatables ?>      
    <!-- Data table css -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-flash-1.6.1/b-html5-1.6.1/r-2.2.3/rg-1.1.1/sl-1.3.1/datatables.min.css"/>
    <!-- Data tables -->
 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-flash-1.6.1/b-html5-1.6.1/r-2.2.3/rg-1.1.1/sl-1.3.1/datatables.min.js"></script>

    <!-- Data table js -->
        <script>
          $(function(e) {
            var table = $('#example').DataTable({
              dom: 'Blfrtip',
              order : [[0, 'desc']],
              buttons: {
                buttons: [
                    { extend: 'copy', className: 'btn-primary' },
                    { extend: 'excel', className: 'btn-primary' },
                    { extend: 'pdf', className: 'btn-primary' }
                ]
              }
            });

            table.buttons( 0, null ).containers().appendTo( '.panel-toolbox, .panel_toolbox' );

            $('.data-table-buttons').DataTable({
                dom: 'Blfrtip',
                order : [[0, 'desc']],
                buttons: {
                  buttons: [
                      { extend: 'copy', className: 'btn btn-default' },
                      { extend: 'excel', className: 'btn btn-default' },
                      { extend: 'pdf', className: 'btn btn-default' }
                  ]
                }
              });
            $('.data-table').DataTable({order : [[0, 'desc']]});
          } );
        </script>
    <?php } ?>
    
    <?php if(isset($tabs)) : ?>
    <!-- Tabs Style -->
		<link href="<?php echo base_url('res/assets/plugins/tabs/style.css')?>" rel="stylesheet" />
    <!---Tabs JS-->
		<script src="<?php echo base_url('res/assets/plugins/tabs/jquery.multipurpose_tabcontent.js')?>"></script>
        <!---Tabs scripts-->
		<script>
			$(function(e) {
				$(".first_tab").champ();
				$(".accordion_example").champ({
					plugin_type: "accordion",
					side: "left",
					active_tab: "3",
					controllers: "true"
				});

				$(".second_tab").champ({
					plugin_type: "tab",
					side: "right",
					active_tab: "1",
					controllers: "false"
				});

			});
		</script>
    <?php endif; ?>

    <?php if (isset($wys)) { ?>
      <!-- WYSIWYG Editor css -->
    <link href="<?php echo base_url('res/assets/plugins/wysiwyag/richtext.min.css');?>" rel="stylesheet" />
    <!-- WYSIWYG Editor js -->
    <script src="<?php echo base_url('res/assets/plugins/wysiwyag/jquery.richtext.js');?>"></script>
    <!-- WYSIWYG Editor js -->
    <script>
      $(function(e) {
        $('.content').richText();
      });
    </script>
    <?php } ?>

    <?php if(isset($bst)) : ?>
    <link href="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.css" rel="stylesheet">

    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.js"></script>
    <script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
    <script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF/jspdf.min.js"></script>
    <script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>

    <script type="text/javascript">
      var $table = $('#table');

      $('#excel-export').click(function() {
         $table.tableExport({
           type: 'excel',
           escape: false,
           exportDataType: 'all',
           refreshOptions: {
             exportDataType: 'all'
           },
           exportOptions: {
              fileName: function () {
                 return $(this).data('name')
              }
           }
         });
      });

      $('#pdf-export').click(function() {
         $table.tableExport({
           type: 'pdf',
           escape: false,
           exportDataType: 'all',
           refreshOptions: {
             exportDataType: 'all'
           },
           exportOptions: {
              fileName: function () {
                 return $(this).data('name')
              }
           }
         });
       });

      $(function() {
        $('#toolbar').find('select').change(function () {
          $table.bootstrapTable('destroy').bootstrapTable({
            exportDataType: $(this).val(),
            exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf']
          })
        }).trigger('change')
      })
        
    </script>


    <?php endif; ?>

    <?php if (isset($sel)) { ?>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script type="text/javascript">
      $(function(e) {
        $('body').on('shown.bs.modal', '.modal', function() {
          $(this).find('select').each(function() {
            var dropdownParent = $(document.body);
            if ($(this).parents('.modal.in:first').length !== 0)
              dropdownParent = $(this).parents('.modal.in:first');
            $(this).select2({
              dropdownParent: dropdownParent
              // ...
            });
          });
        });
      });
    </script>
    <?php } ?>

    <script type="text/javascript">
      function remove_things(tbl, id, reload) {
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this record!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.post( "<?php echo site_url('system/act/users_model/remove_things');?>", { id: id, tbl: tbl })
            .done(function( data ) {
              swal("Poof! Your record has been deleted!", {
                icon: "success",
              });
              location.reload();
            });
            
          } else {
            swal("Your record is safe!");
          }
        });
      }
    </script>

    <!-- Notifications  Css -->
    <link href="<?php echo base_url('res/assets/plugins/notify/css/jquery.growl.css');?>" rel="stylesheet" />
    <!-- Notifications js -->
    <script src="<?php echo base_url('res/assets/plugins/notify/js/rainbow.js')?>"></script>         
    <script src="<?php echo base_url('res/assets/plugins/notify/js/jquery.growl.js');?>"></script>  
    
    <script type="text/javascript">
      $('.ajax-form').submit(function(e) {
        e.preventDefault();
        let frm = $(this);
        let url = frm.attr('action');
        let data = frm.serialize();
        frm.find('input, button, textarea, select').attr('disabled', true);
        $.post( url, data)
        .done(function( data ) {
          console.log(data);
          let r = JSON.parse(data);
          frm.find('input, button, textarea, select').attr('disabled', false);
          
          show_done(r, frm.data('no-reload'));
        })
        .fail(function(e, r) {
          // alert(e.responseText);
          frm.find('input, button, textarea, select').attr('disabled', false);
          $.growl.error({
            message: "Error occured "+e.responseText
          });
        });
      });

      $('.delete-this').click(function() {
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this record!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            let tbl = $(this).data('tbl');
            let id = $(this).data('id');
            $.post( "<?php echo site_url('system/act/users_model/remove_things');?>", { id: id, tbl: tbl })
            .done(function( data ) {
              swal("Poof! Your record has been deleted!", {
                icon: "success",
              });
              location.reload();
            }).fail(function(e, r) {
              $.growl.error({
                message: "Error occured "+e.responseText
              });
            });
            
          } else {
            swal("Your record is safe!");
          }
        });
        
      });

      function show_done(r, nr) {
        if(r.status == 1) {
            $.growl.notice({
              message: "Hooray! "+r.msg
            });
            if(r.url != undefined) location.href = r.url;
            else location.reload();
          } else {
            $.growl.warning({
              message: "Ooops! "+r.msg
            });
          }
      }
    </script>
	</body>
</html>