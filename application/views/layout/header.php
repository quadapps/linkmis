
<?php
$user = get_data('tbl_users', "JOIN tbl_employees ON emp_id = user_empid WHERE user_id = ".$this->session->userdata('user_id'), 'user_id, user_empid, user_levelid, emp_id, emp_fullname, emp_designation, emp_photourl, emp_deptid', true);
// $emp = get_data('tbl_employees', "WHERE emp_id = ".$user['user_empid'], 'emp_id, emp_fullname, emp_designation, emp_photourl', true);
?>
<!doctype html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="msapplication-TileColor" content="#ffcc29">
		<meta name="theme-color" content="#ffcc29">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

		<!-- Title -->
		<title>LinkMis : <?php echo isset($title) ? $title : 'welcome';?></title>
		<link rel="stylesheet" href="<?php echo base_url('res/assets/fonts/fonts/font-awesome.min.css');?>">

		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

		<!-- Dashboard Css -->
		<link href="<?php echo base_url('res/assets/css/dashboard.css'); ?>" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="<?php echo base_url('res/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css'); ?>" rel="stylesheet" />

		<!-- Sidemenu Css -->
		<!-- <link href="<?php echo base_url('res/assets/plugins/toggle-sidebar/css/sidemenu.css');?>" rel="stylesheet"> -->

		<!-- c3.js Charts Plugin -->
		<link href="<?php echo base_url('res/assets/plugins/charts-c3/c3-chart.css'); ?>" rel="stylesheet" />

		<!---Font icons-->
		<link href="<?php echo base_url('res/assets/plugins/iconfonts/plugin.css'); ?>" rel="stylesheet" />

		<!-- Slect2 css -->
		<link href="<?php echo base_url('res/assets/plugins/select2/select2.min.css');?>" rel="stylesheet" />

		<!--Important JS -->

		<script src="<?php echo base_url('res/assets/js/vendors/jquery-3.2.1.min.js'); ?>"></script>
		<script src="<?php echo base_url('res/assets/js/vendors/bootstrap.bundle.min.js');?>"></script>

		<!--Swal -->
		<link href="<?php echo base_url('res/assets/plugins/sweet-alert/sweetalert.css');?>" rel="stylesheet" />
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


	</head>
	<body class="" >
		<div id="global-loader" ></div>
		<div class="page">
			<div class="page-main">
				<div class="header py-4">
					<div class="container">
						<div class="d-flex">
							<a class="header-brand" href="<?php echo site_url();?>">
								<img src="<?php echo base_url('res/assets/images/brand/lp-logo.png');?>" class="header-brand-img" alt="LinkMis logo">
							</a>
							<div class="d-flex order-lg-2 ml-auto">
								<div class=" ">
									<a href="<?php echo site_url('welcome/view/tasks');?>" class="btn btn-primary btn-rounded"><i class="fa fa-cog"></i> My Tasks </a>
								</div>
								
								<div class="dropdown">
									<a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
										<span class="avatar avatar-md brround" style="background-image: url(<?php echo $user['emp_photourl'];?>)"></span>
										<span class="ml-2 d-none d-lg-block">
											<span class="text-dark"><?php echo $user['emp_fullname'];?></span>
										</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item" href="<?php echo site_url('welcome/view/emp_details/'.$user['emp_id']);?>">
											<i class="dropdown-icon mdi mdi-account-outline "></i> Profile
										</a>
										
										<a class="dropdown-item" href="#">
											<i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?
										</a>
										<a class="dropdown-item" href="<?php echo site_url('welcome/logout');?>">
											<i class="dropdown-icon mdi  mdi-logout-variant"></i> Sign out
										</a>
									</div>
								</div>
							</div>
							<a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
							<span class="header-toggler-icon"></span>
							</a>
						</div>
					</div>
				</div>
				<div class="vobilet-navbar fixed-heade" id="headerMenuCollapse">
					<div class="container-fluid">
						<ul class="nav">
							
							<li class="nav-item">
								<a class="nav-link <?php echo $page==1 ? 'active' : ''?>" href="<?php echo site_url();?>">
									<i class="fa fa-home"></i>
									<span>Dashboard</span>
								</a>
							</li>
							
							<?php if($user['emp_deptid'] == 4 || $user['emp_deptid'] == 6 || $user['user_levelid'] < 3) :?>
							<li class="nav-item with-sub">
								<a class="nav-link <?php echo $page==2 ? 'active' : ''?>" href="#">
									<i class="fa fa-window-restore"></i>
									<span> Sales</span>
								</a>
								<div class="sub-item">
									<ul>
										<li>
											<a href="<?php echo site_url('welcome/view/targets');?>">Targets</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/sales?t=ASM');?>">ASM/RM</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/sales?t=CLUSTER');?>">BDR</a>
										</li>
										<!-- <li>
											<a href="<?php echo site_url('welcome/view/sales?t=TDR');?>">TDR/Agents Management</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/sales?t=MDR_act');?>">Merchants Management</a>
										</li> -->

										<li class="sub-with-sub">
											<a href="#">Staff Agents Management</a>
											<ul>
												<li>
													<a href="<?php echo site_url('welcome/view/agents');?>">Agents List</a>
												</li>
												<li>
													<a href="<?php echo site_url('welcome/view/agent_attendance');?>">Attendance</a>
												</li>
												<li>
													<a href="<?php echo site_url('welcome/view/agent_reqs');?>">Cash/Float Requests</a>
												</li>
												<li>
													<a href="<?php echo site_url('welcome/view/agent_recons');?>">Reconciliations</a>
												</li>
												<li>
													<a href="<?php echo site_url('welcome/report/agents_performance');?>">Performance</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/sales?t=MDR_act');?>">Agency Banking Management</a>
										</li>

										<!-- <li class="sub-with-sub">
											<a href="#">Direct Channels</a>
											<ul>
												<li>
													<a href="<?php echo site_url('welcome/view/sales?t=ASM');?>">HDC</a>
												</li>
												<li>
													<a href="<?php echo site_url('welcome/view/sales?t=TDR');?>">TDR Management</a>
												</li>
											</ul>
										</li>

										<li class="sub-with-sub">
											<a href="#">Merchant Channels</a>
											<ul>
												<li>
													<a href="<?php echo site_url('welcome/view/sales?t=ASM');?>">ASM/RM</a>
												</li>
												<li>
													<a href="<?php echo site_url('welcome/view/sales?t=MDR_act');?>">MDR Management</a>
												</li>
											</ul>
										</li> -->
										
										
										
									</ul>
								</div><!-- sub-item -->
							</li>
							<?php endif; ?>

							<?php if($user['emp_designation'] == 'SALES MANAGER') : ?>
							<li class="nav-item with-sub">
								<a class="nav-link <?php echo $page==3 ? 'active' : ''?>" href="#"><i class="fa fa-money"></i> <span>Finance</span></a>
								<div class="sub-item">
									<ul>
										<li>
											<a href="<?php echo site_url('welcome/view/agents');?>">Agents List</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/agent_txns');?>">Transactions Book</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/report/agents');?>">Agents Report</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/collections');?>">Collections</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/cash_out');?>">Cash Out</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/cashbook');?>">Cash Book</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/floatbook');?>">Float Book</a>
										</li>
										
									</ul>
								</div>
								<!-- dropdown-menu -->
							</li>

						<?php endif; ?>
							
							<?php if($user['emp_deptid'] == 1 || $user['user_levelid'] < 3) : ?>
							<li class="nav-item with-sub">
								<a class="nav-link <?php echo $page==3 ? 'active' : ''?>" href="#"><i class="fa fa-money"></i> <span>Finance</span></a>
								<div class="sub-item">
									<ul>
										<li>
											<a href="<?php echo site_url('welcome/view/agents');?>">Agents List</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/agent_txns');?>">Transactions Book</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/agent_reqs');?>">Agent Requests</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/report/agents');?>">Agents Report</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/collections');?>">Collections</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/cash_out');?>">Cash Out</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/cashbook');?>">Cash Book</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/floatbook');?>">Float Book</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/bankings');?>">Bankings</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/report/recons');?>">Reconciliations</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/expenses');?>">Expenses/Payment Vouchers</a>
										</li>
										
									</ul>
								</div>
								<!-- dropdown-menu -->
							</li>

							<li class="nav-item with-sub">
								<a class="nav-link <?php echo $page==7 ? 'active' : ''?>" href="#"><i class="fa fa-money"></i> <span>Admin</span></a>
								<div class="sub-item">
									<ul>
										<li>
											<a href="<?php echo site_url('welcome/view/vendors');?>">Vendors/Suppliers</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/lpos');?>">LPOs</a>
										</li>
										
										<li>
											<a href="<?php echo site_url('welcome/view/reqs');?>">Requisitions</a>
										</li>

										<li>
											<a href="<?php echo site_url('welcome/view/expenses');?>">Expenses/Vouchers</a>
										</li>

										<li class="sub-with-sub">
											<a href="#">Fleet Management</a>
											<ul>
												<li>
													<a href="<?php echo site_url('welcome/view/cars');?>">Vehicles</a>
												</li>
												<li>
													<a href="<?php echo site_url('welcome/view/fueling');?>">Fueling</a>
												</li>
												
											</ul>
										</li>
										
									</ul>
								</div>
								<!-- dropdown-menu -->
							</li>
							<?php endif; ?>

							<?php if($user['emp_deptid'] == 5  && $user['user_levelid'] > 2) : ?>
							
							<li class="nav-item with-sub">
								<a class="nav-link <?php echo $page==7 ? 'active' : ''?>" href="#"><i class="fa fa-money"></i> <span>Admin</span></a>
								<div class="sub-item">
									<ul>
										<li>
											<a href="<?php echo site_url('welcome/view/vendors');?>">Vendors/Suppliers</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/lpos');?>">LPOs</a>
										</li>
										
										<li>
											<a href="<?php echo site_url('welcome/view/reqs');?>">Requisitions</a>
										</li>

										<li>
											<a href="<?php echo site_url('welcome/view/expenses');?>">Expenses/Vouchers</a>
										</li>

										<li class="sub-with-sub">
											<a href="#">Fleet Management</a>
											<ul>
												<li>
													<a href="<?php echo site_url('welcome/view/cars');?>">Vehicles</a>
												</li>
												<li>
													<a href="<?php echo site_url('welcome/view/fueling');?>">Fueling</a>
												</li>
												
											</ul>
										</li>
										
									</ul>
								</div>
								<!-- dropdown-menu -->
							</li>
							<?php endif; ?>

							<?php if($user['emp_designation'] == 'BACKEND SUPPORT') : ?>
							<li class="nav-item with-sub">
								<a class="nav-link <?php echo $page==4 ? 'active' : ''?>" href="#"><i class="fa fa-desktop"></i> <span>Backend Support</span></a>
								<div class="sub-item">
									<ul>
										<li>
											<a href="<?php echo site_url('welcome/view/deliveries');?>">Application Forms</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/expenses');?>">Approved Codes &amp; SIM Cards</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/expenses');?>">Reports</a>
										</li>
										
									</ul>
								</div>
								<!-- dropdown-menu -->
							</li>
							<?php endif; ?>

							<?php if($user['emp_deptid'] == 3 || $user['user_levelid'] < 3) :?>
							<li class="nav-item with-sub">
								<a class="nav-link <?php echo $page==4 ? 'active' : ''?>" href="#"><i class="fa fa-snowflake-o"></i> <span>Marketing</span></a>
								<div class="sub-item">
									<ul>
										<li>
											<a href="<?php echo site_url('welcome/view/clients');?>">Clients</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/visits');?>">Market Visits</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/targets');?>">Targets</a>
										</li>
										<!-- <li>
											<a href="<?php echo site_url('welcome/view/activations');?>">Trainings &amp; Activations</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/reports/marketing');?>">Reports</a>
										</li> -->
										
									</ul>
								</div>
								<!-- dropdown-menu -->
							</li>
							<?php endif; ?>

							<?php if(strpos($user['emp_designation'], 'FOREX') !== false || $user['user_levelid'] < 3) : ?>
							<!-- <li class="nav-item with-sub">
								<a class="nav-link <?php echo $page==4 ? 'active' : ''?>" href="#"><i class="fa fa-usd"></i> <span>Forex</span></a>
								<div class="sub-item">
									<ul>
										<li>
											<a href="<?php echo site_url('welcome/view/deliveries');?>">Daily Transactions</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/expenses');?>">Reconciliations</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/expenses');?>">Reports</a>
										</li>
										
									</ul>
								</div>
								
							</li> -->
							<?php endif; ?>

							<?php if($user['user_levelid'] > 2) : ?>
							<li class="nav-item">
								<a class="nav-link <?php echo $page==7 ? 'active' : ''?>" href="<?php echo site_url('welcome/view/reqs');?>">
									<i class="fa fa-user"></i>
									<span>Requisitions</span>
								</a>
							</li>
							<?php endif; ?>

							
							<li class="nav-item with-sub">
								<a class="nav-link <?php echo $page==5 ? 'active' : ''?>" href="#"><i class="fa fa-users"></i> <span>Human Resource</span></a>
								<div class="sub-item">
									<ul>
										<li>
											<a href="<?php echo site_url('welcome/view/depts');?>">Departments</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/employees');?>">Employees</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/attendance');?>">Attendance</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/leaves');?>">Leave Management</a>
										</li>
										<li>
											<a href="<?php echo site_url('welcome/view/loans');?>">Loans &amp; Salary Advances</a>
										</li>
										<!-- <li>
											<a href="<?php echo site_url('welcome/view/appraisals');?>">Appraisal</a>
										</li> -->
										<li>
											<a href="<?php echo site_url('welcome/view/payroll');?>">Payroll</a>
										</li>

										<li>
											<a href="<?php echo site_url('welcome/view/meetings');?>">Meetings/Memos</a>
										</li>

										<li>
											<a href="<?php echo site_url('welcome/view/tasks');?>">Task Management</a>
										</li>
										<?php if($user['emp_deptid'] == 2 || $user['user_levelid'] < 3) : ?>
										<li>
											<a href="<?php echo site_url('welcome/view/user_activity');?>">Users</a>
										</li>
										<?php endif; ?>
										
									</ul>
								</div>
								<!-- dropdown-menu -->
							</li>
							
							
							<!-- <li class="nav-item">
								<a class="nav-link <?php echo $page==6 ? 'active' : ''?>" href="<?php echo site_url('welcome/view/users');?>">
									<i class="fa fa-user"></i> 
									<span>User Management</span>
									<span class="square-8"></span>
								</a>
							</li> -->
						</ul>
					</div>
				</div>