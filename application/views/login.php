<!doctype html>
<html lang="en" dir="ltr">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<meta name="msapplication-TileColor" content="#ffcc29">
		<meta name="theme-color" content="#ffcc29">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
		
		<!-- Title -->
		<title>LinkMis - Links Pay Ltd's Management Information System</title>
		<link rel="stylesheet" href="<?php echo base_url('res/assets/fonts/fonts/font-awesome.min.css'); ?>">
		
		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
		
		<!-- Custom scroll bar css-->
		<link href="<?php echo base_url('res/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css');?>" rel="stylesheet" />
		
		<!-- Dashboard Css -->
		<link href="<?php echo base_url('res/assets/css/dashboard.css'); ?>" rel="stylesheet" />
		
		<!-- c3.js Charts Plugin -->
		<link href="<?php echo base_url('res/assets/plugins/charts-c3/c3-chart.css'); ?>" rel="stylesheet" />
		
		<!---Font icons-->
		<link href="<?php echo base_url('res/assets/plugins/iconfonts/plugin.css'); ?>" rel="stylesheet" />
	
  </head>
	<body class="login-img-1" style="">
		<div id="global-loader" ></div>
		<div class="page">
			<div class="page-single">
				<div class="container">
					<div class="row">
						<div class="col col-login mx-auto">
							
							<div id="login">
							    <form class="card ajax-frm" method="post" action="<?php echo site_url('welcome/login'); ?>">
							        <div class="text-center mb-12">
        								<img src="<?php echo base_url('res/assets/images/brand/lp-logo.png');?>" class="" alt="CRMS logo">
        							</div>
    								<div class="card-body p-6">
    									<div class="card-title text-center">Welcome to LinkMis</div>
    									<div class="error-box"></div>
    									<div class="form-group">
    										<label class="form-label">Email address</label>
    										<input type="email" name="email" class="form-control" id="exampleInputEmail1"  placeholder="Enter email" required>
    									</div>
    									<div class="form-group">
    										<label class="form-label">Password
    											<a href="#" id="f-pwd" class="float-right small">I forgot password</a>
    										</label>
    										<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
    									</div>
    									<div class="form-group">
    										<label class="custom-control custom-checkbox">
    											<input type="checkbox" class="custom-control-input" />
    											<span class="custom-control-label">Remember me</span>
    										</label>
    									</div>
    									<div class="form-footer">
    										<button type="submit" class="btn btn-primary btn-block">Sign in</button>
    									</div>
    									<!--<div class="text-center text-muted mt-3">
    										Don't have account yet? <a href="./register.html">Sign up</a>
    									</div>-->
    								</div>
    								
    							</form>
							</div>
							<div id="forgot">
							    <form class="card ajax-frm"  method="post" action="<?php echo site_url('welcome/forgot_pass'); ?>">
							        <div class="text-center mb-12">
        								<img src="<?php echo base_url('res/assets/images/brand/lp-logo.png');?>" class="" alt="">
        							</div>
    								<div class="card-body p-6">
    									<div class="text-center card-title">Regain your password</div>
    									<div class="error-box"></div>
    									<div class="form-group">
    										<label class="form-label" for="exampleInputEmail1">Email address</label>
    										<input type="email" class="form-control" id="exampleInputEmail1"  placeholder="Enter email">
    									</div>
    									<div class="form-footer">
    										<button type="submit" class="btn btn-primary btn-block">Send</button>
    									</div>
    									<div class="text-center text-muted mt-3 ">
    									Forget it, <a href="#" id="send-back">send me back</a> to the sign in screen.
    								</div>
    								</div>
    								
    							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- Dashboard js -->
		<script src="<?php echo base_url('res/assets/js/vendors/jquery-3.2.1.min.js'); ?>"></script>
		<script src="<?php echo base_url('res/assets/js/vendors/bootstrap.bundle.min.js'); ?>"></script>
		<script src="<?php echo base_url('res/assets/js/vendors/jquery.sparkline.min.js'); ?>"></script>
		<script src="<?php echo base_url('res/assets/js/vendors/selectize.min.js'); ?>"></script>
		<script src="<?php echo base_url('res/assets/js/vendors/jquery.tablesorter.min.js'); ?>"></script>
		<script src="<?php echo base_url('res/assets/js/vendors/circle-progress.min.js'); ?>"></script>
		<script src="<?php echo base_url('res/assets/plugins/rating/jquery.rating-stars.js'); ?>"></script>
		
		<!-- Custom scroll bar Js-->
		<script src="<?php echo base_url('res/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
		
		<!-- custom js -->
		<script src="<?php echo base_url('res/assets/js/custom.js'); ?>"></script>
		<script>
		    $(document).ready(function() {
		        $('#forgot').hide();
		        $('#f-pwd').click(function() {
    		        $('#login').fadeOut();
    		        $('#forgot').fadeIn();
    		    });
    		    
    		    $('#send-back').click(function() {
    		        $('#forgot').fadeOut();
    		        $('#login').fadeIn();
    		    });
		    });
		</script>

		<script src="<?php echo base_url('res/assets/js/jquery.form.js');?>"></script> 
 
	    <script> 
	        $(document).ready(function() { 
			    // bind form using ajaxForm 
			    $('.ajax-frm').ajaxForm({ 
			        // dataType identifies the expected content type of the server response 
			        dataType:  'json', 
			        beforeSubmit: function(formData, jqForm, options) {
			        	
			        	jqForm.find('input, button').attr('disabled', true);
			        },
			 
			        // success identifies the function to invoke when the server response 
			        // has been received 
			        success:   function(d, statusText, xhr, $form) {
			        	$form.find('input, button').attr('disabled', false);
			        	if (d.status == 1) {
			        		location.reload();
			        	} else {
			        		$('.error-box').addClass('alert alert-danger').html(d.msg);
			        		setTimeout(function() {
			        			$('.error-box').removeClass('alert alert-danger').empty();
			        		}, 4000);
			        	}
			        },
			        error: function(e, r) {
			        	//alert(e.responseText);
			        	location.reload();
			        }
			    }); 
			}); 
	    </script>
	</body>
</html>
 