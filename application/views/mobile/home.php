<?php
$this->load->view('layout/header', ['title' => 'Dashboard', 'page' => 1]);
$uid = $this->session->userdata('user_id');

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-01');
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d');

$txns = get_data('tbl_agentreports', "WHERE DATE(created_at) BETWEEN '$start' AND '$end'", "SUM(No_of_transaction) AS total", true)['total'];

?>

<div class="my-3 my-md-5">
					<div class="container">
										
						
						
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Latest Cash Ins</h3>
									</div>
									<div class="table-responsive">
										<table class="table card-table table-vcenter text-nowrap">
											<thead>
												<tr>
													<th class="w-1">Agent No</th>
													<th>Agent Name</th>
													<th>SSP Collected</th>
													<th>USD Collected</th>
													<th>Date</th>
													<th>Cashier</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><span class="text-muted">001</span></td>
													<td><a href="store.html" class="text-inherit">Maria</a></td>
													<td>320,000 </td>
													<td>2,500</td>
													<td>17 Feb 2021</td>
													<td><span class="status-icon bg-primary"></span> James</td>
													<td class="text-right">
														<a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Edit</a>
														<a href="javascript:void(0)" class="btn btn-green btn-sm"><i class="fa fa-link"></i> Add</a>
														<a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>
													</td>
												</tr>
												<tr>
													<td><span class="text-muted">002</span></td>
													<td><a href="store.html" class="text-inherit">Grace </a></td>
													<td>789,100</td>
													<td>78,956</td>
													<td>12 Feb 2021</td>
													<td><span class="status-icon bg-secondary"></span> Ladu</td>
													<td class="text-right">
														<a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Edit</a>
														<a href="javascript:void(0)" class="btn btn-green btn-sm"><i class="fa fa-link"></i> Add</a>
														<a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>
													</td>
												</tr>
												<tr>
													<td><span class="text-muted">003</span></td>
													<td><a href="store.html" class="text-inherit">Zoe </a></td>
													<td>192,200</td>
													<td>23,487</td>
													<td>12 Feb 2021</td>
													<td><span class="status-icon bg-success"></span> Susan</td>
													<td class="text-right">
														<a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Edit</a>
														<a href="javascript:void(0)" class="btn btn-green btn-sm"><i class="fa fa-link"></i> Add</a>
														<a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>
													</td>
												</tr>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<?php
$this->load->view('layout/footer', ['dash' => 1]);
?>