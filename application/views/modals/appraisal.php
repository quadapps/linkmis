<?php
$arr = explode("_", $params);
$dept = $arr[0]; $start = $arr[1]; $end = end($arr);
$and = '';
if(get_that_data('tbl_users', 'user_levelid', 'user_id', $this->session->userdata('user_id')) == 4) $end = " AND user_id = ".$this->session->userdata('user_id');

$emps = get_data('tbl_employees', "JOIN tbl_users ON emp_id = user_empid WHERE emp_active = 1 AND emp_deptid = '$dept' $and ", 'emp_fullname, user_id, emp_id');

function get_last($emp)
{
  $data = get_data('tbl_appraisals', "WHERE app_empid = $emp ORDER BY app_id DESC LIMIT 1", '*', true);
  return empty($data) ? 'Not yet' : timeAgo($data['app_createdate']).' by '.get_fullname($data['app_creator']).'<br><a href="'.base_url($data['app_filename']).'">View</a>';
}
?>
<div class="modal-body">
	<?php if($dept == 4) {

		?>

		<table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                  <tr>
                                      <th> Name</th>
                                      <th>Emails</th>
                                      <th>Calls</th>
                                      <th>Total Followups</th>
                                      <th>Market Visits</th>
                                      <th>Tasks</th>
                                      <th>Jobs</th>
                                      <th>Quotations</th>
                                      <th>Last Appraised</th>
                                      <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($emps as $d) { $a = get_data('tbl_followup', "WHERE followup_creator = ".$d['user_id']." AND DATE(followup_createdate) BETWEEN '$start' AND '$end'", 'followup_id, followup_mode');
                                  $v = array_count_values(array_column($a, 'followup_mode')); 
                                  //print_r($v); exit; ?>
                                    <tr>
                                      <td><a href="<?php echo site_url('welcome/view/followups');?>"><?php echo $d['emp_fullname'];?></td>
                                      <td><?php echo isset($v['Email']) ? $v['Email'] : 0;?></td>
                                      <td><?php echo isset($v['Call']) ? $v['Call'] : 0;?></td>
                                      <td><?php echo count($a);?></td>

                                      <td><a href="#" data-toggle="modal" data-target="#ajaxModal" data-url="<?php echo site_url('welcome/page/visit_details/'.$d['emp_id']."_".$start.'_'.$end)?>" data-title="Activity Details"><?php echo count(get_data('tbl_visits', "WHERE DATE(visit_createdate) BETWEEN '$start' AND '$end' AND visit_creator = ".$d['user_id'], 'visit_id'));?></a></td>

                                      <td><a href="<?php echo site_url('welcome/view/tasks/?f='.$d['user_id']);?>"><?php echo count(get_data('tbl_tasks', "WHERE DATE(task_createdate) BETWEEN '$start' AND '$end' AND task_active = 1 AND task_creator = ".$d['user_id'], 'task_id'))?></a></td>

                                      <td><?php echo count($j = get_data('tbl_jobs', "JOIN tbl_clients ON client_id = job_clientid WHERE DATE(job_createdate) BETWEEN '$start' AND '$end' AND job_active = 1 AND (job_creator = ".$d['user_id']." OR client_marketerid = ".$d['emp_id']." )", 'job_id'));?></td>

                                      <td><?php echo count($j = get_data('tbl_quotations', "JOIN tbl_clients ON client_id = quotation_clientid WHERE DATE(quotation_createdate) BETWEEN '$start' AND '$end' AND quotation_status != -1 AND (quotation_creator = ".$d['user_id']." OR client_marketerid = ".$d['emp_id']." )", 'quotation_id'));?></td>

                                      <td><a href="#" class="" data-id="<?php echo $d['emp_id'];?>" data-toggle="modal" data-target="#ajaxModal" data-url="<?php echo site_url('welcome/modal/emp_appraisal/'.$d['emp_id']);?>" data-title="Appraisal Details [<?php echo $d['emp_fullname'];?>]"> <?php echo get_last($d['emp_id']);?></a></td>

                                      <td><a href="#" class="btn btn-default appraise" data-id="<?php echo $d['emp_id'];?>"> Appraise</a></td>
                                      
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>

        <?php } elseif($dept == 2) { ?>

        		<table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                  <tr>
                                      <th>Name</th>
                                      <th>Jobs Assigned</th>
                                      <th>Jobs Completed</th>
                                      <th>Assigned Qty</th>
                                      <th>Completed Qty</th>
                                      <th>Other Tasks</th>
                                      <th>Last Appraised</th>
                                      <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($emps as $d) { 
                                    $completed = get_data('tbl_jobschedules', "WHERE DATE(js_end) != '0000-00-00' AND js_empid = ".$d['emp_id']." AND DATE(js_createdate) BETWEEN '$start' AND '$end'", 'COUNT(js_id) AS count, SUM(js_qty) AS qty, js_empid', true);
                                    $all = get_data('tbl_jobschedules', "WHERE js_empid = ".$d['emp_id']." AND DATE(js_createdate) BETWEEN '$start' AND '$end'", 'COUNT(js_id) AS count, SUM(js_qty) AS qty, js_empid', true);
                                   ?>
                                    <tr>
                                      <td><a href="<?php echo site_url('welcome/view/job_schedules');?>"><?php echo $d['emp_fullname'];?></td>
                                      <td><?php echo $all['count'];?></td>
                                      <td><?php echo $completed['count'];?></td>
                                      <td><?php echo $all['qty'];?></td>
                                      <td><?php echo $completed['qty'];?></td>

                                      <td><a href="<?php echo site_url('welcome/view/tasks/?f='.$d['user_id']);?>"><?php echo count(get_data('tbl_tasks', "WHERE DATE(task_createdate) BETWEEN '$start' AND '$end' AND task_active = 1 AND task_creator = ".$d['user_id'], 'task_id'))?></a></td>

                                      <td><a href="#" class="" data-id="<?php echo $d['emp_id'];?>" data-toggle="modal" data-target="#ajaxModal" data-url="<?php echo site_url('welcome/modal/emp_appraisal/'.$d['emp_id']);?>" data-title="Appraisal Details [<?php echo $d['emp_fullname'];?>]"> <?php echo get_last($d['emp_id']);?></a></td>

                                      <td><a href="#" class="btn btn-default appraise" data-id="<?php echo $d['emp_id'];?>"> Appraise</a></td>
                                      
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>

        <?php } else { ?>

        	<table id="" class="table table-hover data-table table-bordered">
                              <thead>
                                  <th>Name</th>
                                  <th>Tasks</th>
                                  <th>Completed</th>
                                  <th>On Going</th>
                                  <th>Pending</th>
                                  <th>Performance</th>
                                  <th>Last Appraised</th>
                                  <th>Action</th>
                              </thead>
                              <tbody>
                              <?php $i=1; foreach($emps as $d) { $tasks = get_data('tbl_tasks', "WHERE DATE(task_start) BETWEEN '$start' AND '$end' AND task_active = 1 AND task_creator = ".$d['user_id'], 'task_id'); ?>
                                  <tr>
                                      <td>
                                        <div><a href="#"><?php echo $d['emp_fullname']?></a></div>
                                      </td>
                                      <td>
                                        <?php echo $t = count($tasks); ?>
                                      </td>
                                      <td class="text-center">
                                        <?php echo $c = count(get_data('tbl_tasks', "WHERE DATE(task_start) BETWEEN '$start' AND '$end' AND task_active = 1 AND task_status = 3 AND task_creator = ".$d['user_id'], 'task_id')); ?>
                                      </td>

                                      <td class="text-center">
                                        <?php echo count(get_data('tbl_tasks', "WHERE task_active = 1 AND task_status = 2 AND task_creator = ".$d['user_id'], 'task_id')); ?>
                                      </td>
                                      <td class="text-center">
                                        <?php echo count(get_data('tbl_tasks', "WHERE task_active = 1 AND task_status = 1 AND task_creator = ".$d['user_id'], 'task_id')); ?>
                                      </td>
                                      <td class="text-center">
                                        <?php echo $t > 0 ? number_format($c/$t*100) : 0?>%
                                      </td>

                                      <td><a href="#" class="" data-id="<?php echo $d['emp_id'];?>" data-toggle="modal" data-target="#ajaxModal" data-url="<?php echo site_url('welcome/modal/emp_appraisal/'.$d['emp_id']);?>" data-title="Appraisal Details [<?php echo $d['emp_fullname'];?>]"> <?php echo get_last($d['emp_id']);?></a></td>

                                      <td><a href="#" class="btn btn-default appraise" data-id="<?php echo $d['emp_id'];?>"> Appraise</a></td>
                                  </tr>
                              <?php $i++; } ?>
                              </tbody>
                             </table>

        <?php } ?>


</div>

<!-- Modal -->
<div class="modal fade" id="appraise-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Appraise Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <form method="post" action="<?php echo site_url('system/act/hr_model/appraise');?>" class="ajax-form" enctype="multipart/form-data">
        
        <div class="modal-body">

        <div class="form-group">
          <label>Overall Performance <small>(In percentage)</small></label>
          <input type="number" name="perf" class="form-control" required="">
        </div>
        <div class="form-group">
          <label>Remarks</label>
          <textarea name="remarks" class="form-control" required=""></textarea>
        </div>

        <div class="form-group">
          <label>Attach Appraisal Form</label>
          <input type="file" name="userfile" required="">
        </div>

        <input type="hidden" name="emp_id" id="emp-id">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>

      </form>

    </div>
  </div>
</div>


<script type="text/javascript">

  $('.appraise').click(function() {
    let id = $(this).data('id');
    // alert(id);
    $('#emp-id').val(id);
    $('#appraise-modal').modal('show');
  });

  $('.table').DataTable();
</script>