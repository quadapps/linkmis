<?php $row = get_row_data('tbl_jobitems', 'jobitem_id', $params);?>

<form method="post" action="<?php echo site_url('system/act/jobs_model/update_jobitem')?>" class="ajax-form">

      <div class="modal-body">

        <input type="hidden" id="job_id" name="job_id" value="<?php echo $row['jobitem_jobid']?>"/>

        <input type="hidden" id="jt_id" name="jt_id"  value="<?php echo $row['jobitem_id']?>"/>

        <label>Description</label>

        <input type="text" name="desc" id="jt_desc" class="form-control" required="" value="<?php echo $row['jobitem_description']?>"/>

        <div class="form-row no-gutter">

            <div class="col-md-6 form-group">

                <label>Quantity</label>

                <input type="number" name="qty" id="jt_qty" class="form-control" required="" value="<?php echo $row['jobitem_pcs']?>">

            </div>

            <div class="col-md-6 form-group">

                <label>Select Job Type</label>

                <select name="job_type" class="form-control select" onchange="get_papers(this.value)">

                    <option value="<?php echo $row['jobitem_jobtypeid']?>"> <?php echo get_that_data('tbl_jobtypes', 'jobtype_name', 'jobtype_id', $row['jobitem_jobtypeid']);?></option>

                    <?php foreach(get_data('tbl_jobtypes', 'WHERE jobtype_active = 1  ORDER BY jobtype_name ASC') as $jt) { ?>

                    <option value="<?php echo $jt['jobtype_id']?>"><?php echo $jt['jobtype_name']?></option>

                    <?php } ?>

                </select>

            </div>

        </div>

        <br />

        <label>Select Media</label>

        <div id="media"></div>

        <br />

        <div class="form-row">
            <div class="col-md-12"><strong>Production</strong></div> <br>
            <div class="col-md-4 form-group">
                
                    <label>Color</label>
                    <select name="colors" class="form-control">
                        <option>None</option>
                        <option>Full Color</option>
                        <option>Spot Color/Pantone</option>
                        <option>Single Color</option>
                        <option>UV</option>
                        <option>Other</option>
                    </select>
                
                    <label>Materials Type</label>
                    <input type="text" name="paper" id="paper" class="form-control" value="<?php echo $row['jobitem_paper']?>"/>
                
            </div>
            <div class="col-md-4 form-group">
                
                    <label>Material size</label>
                    <input type="text" name="size" id="size" class="form-control" value="<?php echo $row['jobitem_size']?>"/>
               
                    <label> Type</label>
                    <input type="text" name="type" id="type" class="form-control" value="<?php echo $row['jobitem_type']?>"/>
                
            </div>
            <div class="col-md-4 form-group">
                
                    <label>Finishing</label>
                    <select name="finishing[]" class="form-control" multiple="">
                        <option>None</option>
                        <option>Saddle Stitch</option>
                        <option>Hard Binding</option>
                        <option>Perfect Binding</option>
                        <option>Padding</option>
                        <option>O-Wire</option>
                        <option>Side Binding</option>
                        <option>Top Binding</option>
                        <option>Numbering</option>
                        <option>Perforation</option>
                        <option>Lamination</option>
                        <option>Punching</option>
                        <option>Crazing</option>
                        <option>Cornering</option>
                        <option>Eyelets</option>
                        <option>Other</option>
                    </select>
                
            </div>
        </div>

        

        <div class="form-row">

        <div class="col-md-6 form-group">

            <label>GMS <small>Optional</small></label>

            <input type="text" name="gsm" id="gsm" class="form-control" value="<?php echo $row['jobitem_gsm']?>"/>

        </div>

        <div class="col-md-6 form-group">

            <label>Job done at:</label> <br />

            <select name="site" class="form-control">
                <option>Offset Printing (SM 74)</option>
                <option>Offset Printing (GTO 46)</option>
                <option>Offset Printing (MOZP)</option>
                <option>Large Format</option>
                <option>Konica</option>
                <option>Epson</option>
                <option>ID Card Printer</option>
                <option>Hot Foiled</option>
                <option>Embroidery</option>
                <option>Heat Press</option>
                <option>Screen Printer</option>
                <option>Stamp</option>
            </select>

        </div>

        </div>

        <br />

        <label>Special Instructions: <small>e.g. Numbering, binding and other details</small></label>

        <textarea class="form-control" name="remarks" required="" id="jt_remarks"><?php echo $row['jobitem_remarks']?></textarea>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        <button type="submit" class="btn btn-primary btn-main">Save changes</button>

      </div>

      </form>
      <script src="http://malsup.github.com/jquery.form.js"></script>
      <script>
          // prepare the form when the DOM is ready 
            $(document).ready(function() { 
                var frm = $('.ajax-form');
                // bind form using ajaxForm 
                frm.ajaxForm({ 
                    // dataType identifies the expected content type of the server response 
                    dataType:  'json', 
                    beforeSubmit: function() {
                        frm.find('input, select, button, textarea').attr('disabled', true);
                    }
             
                    // success identifies the function to invoke when the server response 
                    // has been received 
                    success:   function(d) {
                        frm.find('input, select, button, textarea').attr('disabled', false);
                        if (d.status == 1) {
                            location.href = d.url;
                        } else {
                            swal('Ooops!', 'Something went wrong');
                        }
                    },
                    error: function(e, r) {
                        frm.find('input, select, button, textarea').attr('disabled', false);
                        alert(r.responseText);
                    } 
                }); 
            });
      </script>