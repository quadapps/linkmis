<?php
$kpis = get_data('tbl_app_kpis');
?>

<div class="modal-body">
	<table class="table table-responsive table-bordered table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Weightage</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($kpis as $k) : ?>
				<tr>
					<td><?php echo $k['kpi_name'];?></td>
					<td><?php echo $k['kpi_weightage'];?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>