<?php
$data = get_row_data('tbl_po', "po_id", $params);
$supplier = get_row_data('tbl_suppliers', 'supplier_id', $data['po_supplierid']);
$Total = 0;
?>
<div class="table-responsive push">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr class=" ">
                      <th class="text-center " style="width: 1%">NO</th>
                      <th>Description</th>
                      <th class="text-center" style="width: 1%">Qty</th>
                      <th class="text-right" style="width: 5%">Unit  (<?php echo $data['po_currency'];?>)</th>
                      <th class="text-right" style="width: 5%">Amount  (<?php echo $data['po_currency'];?>)</th>
                    </tr>
                    </thead>
                   <tbody>
                    <?php if($data['po_type'] == 1) { $arr = explode(',', $data['po_items']);  
                    $j = 1; foreach($arr as $a) : $items = explode('#', $a); ?>
                    <tr>
                      <td class="text-center"><?php echo $j;?></td>
                      <td>
                        <strong class=""><?php echo get_that_data('tbl_wh_items', 'item_name', 'item_id', $items[0])?></strong>
                      </td>
                      <td class="text-center"><STRONG><?php echo $items[1]?></STRONG></td>
                      <td class="text-right"><strong><?php echo number_format($items[2], 2);?></strong></td>
                      <td class="text-right"><strong><?php $amt = $items[1]*$items[2]; echo number_format($amt, 2)?></strong></td>
                    </tr>
                    <?php $Total += $amt; $j++; endforeach; ?>
                    <tr>
                      <td colspan="4" class="font-w600 text-right"><strong>Subtotal</strong></td>
                      <td class="text-right"><strong><?php echo number_format($Total, 2)?></strong></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="font-w600 text-right"><strong>Vat</strong></td>
                      <td class="text-right"><strong>00.00</strong></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="font-weight-bold text-uppercase text-right"><strong>Total</strong> </td>
                      <td class="font-weight-bold text-right"><strong><?php echo number_format($Total, 2)?></strong></td>
                    </tr>
                    <?php } else { $items = json_decode($data['po_items'], true);
                     $j = 1; for($i=0; $i<count($items['desc']); $i++) : if(isset($items['desc'][$i])) : ?>
                    <tr>
                      <td class="text-center"><?php echo $j;?></td>
                      <td>
                        <strong class=""><?php echo $items['desc'][$i]?></strong>
                      </td>
                      <td class="text-center"><STRONG><?php echo $items['qty'][$i]?></STRONG></td>
                      <td class="text-right"><strong><?php echo number_format($items['price'][$i], 2);?></strong></td>
                      <td class="text-right"><strong><?php $amt = $items['qty'][$i]*$items['price'][$i]; echo number_format($amt, 2)?></strong></td>
                    </tr>
                    <?php $Total += $amt; $j++; endif; endfor; ?>
                    <tr>
                      <td colspan="4" class="font-w600 text-right"><strong>Subtotal</strong></td>
                      <td class="text-right"><strong><?php echo number_format($Total, 2)?></strong></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="font-w600 text-right"><strong>Vat</strong></td>
                      <td class="text-right"><strong>00.00</strong></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="font-weight-bold text-uppercase text-right"><strong>Total</strong> </td>
                      <td class="font-weight-bold text-right"><strong><?php echo number_format($Total, 2)?></strong></td>
                    </tr>
                  <?php } ?>
                    <!--<tr>
                      <td colspan="5" class="text-right">
                        
                      </td>
                    </tr>-->
                   </tbody>
                  </table>
                </div>