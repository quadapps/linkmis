<?php
$p = get_row_data('tbl_users', 'user_id', $params);
?>
<form method="post" action="<?php echo site_url('system/act/users_model/update')?>" class="ajax-form" id="jsonForm">
        <div class="modal-body">
          <div class="form-group">
            <label>Full Name</label>
            <input type="text" class="form-control" name="name" value="<?php echo $p['user_fullname'];?>" required="">
          </div>

          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" name="email" value="<?php echo $p['user_email'];?>" required>
          </div>
<input type="hidden" name="uid" value="<?php echo $p['user_id']?>">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
      </form>

<script src="<?php echo base_url('res/assets/js/vendors/jquery-3.2.1.min.js')?>"></script>
<script type="text/javascript">
  // prepare the form when the DOM is ready 
$(document).ready(function() { 
    $('#jsonForm').submit(function(e) {
                e.preventDefault();
                var frm = $(this);
                var url = frm.attr('action');
                //console.log('URL: ', url);
                var data = frm.serialize();
                frm.find('button, input, select, textarea').attr('disabled', true);
                $.post(url, data).done(function(r) {
                    frm.find('button, input, select, textarea').attr('disabled', false);
                    var resp = JSON.parse(r);
                    if (resp.status == 1) {
                        swal({
                          icon: 'success',
                          title: 'Yeeei...',
                          text: resp.msg
                        });
                        // $('#ajaxModal').modal('hide');
                        location.reload();
                    } else {
                        swal('error', resp.msg);
                    }
                })
            }); 
});
</script>