<?php
$dets = ['title' => 'Agents Transactions Book', 'page' => 3];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-d', strtotime('-30 days')); 
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d');

$user = get_data('tbl_users', "JOIN tbl_employees ON emp_id = user_empid WHERE user_id = $uid", 'user_empid, user_levelid, user_id, emp_designation',  true);

if(isset($_GET['f']) && !empty($_GET['f'])) {
	$emp = $_GET['f'];
	$data = get_data('tbl_agenttxns', "JOIN tbl_agents ON agent_code = at_code WHERE at_active = 1 AND agent_empid = '$emp' AND DATE(at_createdate) BETWEEN '$start' AND '$end'") ;
} else {
	$data = get_data('tbl_agents', "WHERE agent_active = 1") ;
}

?>

<div class="my-3 my-md-5">
					<div class="container">
						
																		
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											
											<li>
												<a class="btn btn-secondary" id="reportrange" href="javascript: void()" data-url="<?php echo site_url('welcome/view/agent_txns')?>"><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end)) ;?></a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<?php if(isset($_GET['f']) && !empty($_GET['f'])) { ?>

											<table id="example" class="table card-table table-vcenter text-nowrap">
											<thead>
												<tr>
													<th>Date</th>
													<th class="w-1">Agent Code</th>
													<th>Agent Name</th>
													<th>Currency</th>
													<th>Amount</th>
													<th>Type</th>
													<th class="text-right">Action</th>
												</tr>
											</thead>
											<tbody>
											<?php foreach($data as $d) : ?>
												<tr>
													<td><span class="text-muted"><?php echo $d['at_createdate'];?></span></td>
													<td><?php echo $d['at_code'];?></td>
													<td><?php echo $d['agent_name'];?></td>
													<td><?php echo $d['at_currency'];?></td>
													<td><?php echo $d['at_amt'];?></td>
													<td><?php echo $d['at_type'];?></td>
													 
													<td class="text-right">
														
														
													</td>
												</tr>
											<?php endforeach; ?>
												
											</tbody>
										</table>

											<?php } else { ?>

											<table id="example" class="table card-table table-vcenter text-nowrap">
											<thead>
												<tr>
													<th class="w-1" rowspan="2">Agent Code</th>
													<th rowspan="2">Agent Name</th>
													<th colspan="2">Total Withdrawals</th>
													<th colspan="2">Total Deposits</th>
													<th class="text-right" rowspan="2">Action</th>
												</tr>
												<tr>
													<th>SSP</th>
													<th>USD</th>
													<th>SSP</th>
													<th>USD</th>
												</tr>
											</thead>
											<tbody>
											<?php foreach($data as $d) : $txns = get_data('tbl_agenttxns', "WHERE at_active = 1 AND DATE(at_createdate) BETWEEN '$start' AND '$end' AND at_code = ".$d['agent_code']." GROUP BY at_currency, at_type", 'SUM(at_amt) AS total, at_currency, at_type', true); ?>
												<tr>
													<td><span class="text-muted"><?php echo $d['agent_code'];?></span></td>
													<td><?php echo $d['agent_name'];?></td>
													<td><?php echo isset($txns[0]['total']) ? $txns[0]['total'] : 0;?></td>
													<td><?php echo isset($txns[2]['total']) ? $txns[2]['total'] : 0;?></td>
													<td><?php echo isset($txns[1]['total']) ? $txns[1]['total'] : 0;?></td>
													<td><?php echo isset($txns[3]['total']) ? $txns[3]['total'] : 0;?></td>
													 
													<td class="text-right">
														
														<a href="<?php echo site_url('welcome/view/agent_txns?f='.$d['agent_empid']);?>">View</a>
													</td>
												</tr>
											<?php endforeach; ?>
												
											</tbody>
										</table>

									<?php } ?>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Agent</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/finance_model/create_agent');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Employee</label>
	        	<select name="emp_id" class="form-control" id="agent">
	        		<option>Select Employee</option>
	        		<?php foreach(get_data('tbl_employees', "WHERE emp_designation  = 'Agent' || emp_designation  LIKE 'CASH%' AND emp_active = 1 ") as $e) : ?>
	        			<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>Agent Code</label>
	        	<input type="number" name="code" placeholder="" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>Agent Line</label>
	        	<input type="number" name="line" placeholder="" class="form-control" required="">
	        </div>
	        
	        <!-- <label>Opening Cash</label> <br>
	        <div class="form-row">
	        	
	        	<div class="form-group col">
		        	<label>SSP Amount</label>
		        	<input type="number" name="ssp_cash" step="any" class="form-control" >
		        </div>
		        <div class="form-group col">
		        	<label>USD Amount</label>
		        	<input type="number" name="usd_cash" step="any" class="form-control" >
		        </div>
	        </div>

	        <label>Opening Float</label> <br>
	        <div class="form-row">
	        	
	        	<div class="form-group col">
		        	<label>SSP Amount</label>
		        	<input type="number" name="ssp_float" step="any" class="form-control" >
		        </div>
		        <div class="form-group col">
		        	<label>USD Amount</label>
		        	<input type="number" name="usd_float" step="any" class="form-control" >
		        </div>
	        </div> -->

	        <input type="hidden" name="name" id="agent-name">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="recon-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Confirm Collection</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/finance_model/confirm_collection')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      		<div class="form-group">
	        	<label>SSP Recieved</label>
	        	<input type="number" name="ssp_amt" id="ssp-amt" class="form-control" required="">
	        </div>
	        
	        <div class="form-group">
	        	<label>USD Recieved</label>
	        	<input type="number" name="usd_amt" id="usd-amt" class="form-control" required="">
	        </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Comments</label>
            <textarea name="comments" class="form-control" required="" id="comments"></textarea>
          </div>

          <div class="form-group">
            <input type="hidden" name="coll_id" required="" class="form-control" id="recon-id" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
</div>


<script type="text/javascript">
	$('#sync').click(function(){
		swal('submitted');
		$.get('<?php echo site_url('epicollect/sync_collections')?>', function(r) {
			//if(r > 0)
				location.reload();
		});
	});

	$('#agent').change(function() {
		$('#agent-name').val($(this).find('option:selected').text());
	})

	$('.reconcile').click(function() {
		let id = $(this).data('id');
		let comm = $(this).data('comm');
		let usd = $(this).closest('tr').find("td:nth-child(5)").text();
		let ssp = $(this).closest('tr').find("td:nth-child(6)").text();
		$('#recon-id').val(id);
		$('#comments').text(comm);
		$('#ssp-amt').val(ssp);
		$('#usd-amt').val(usd);
		$('#recon-modal').modal('show');
	})
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>