<?php
$dets = ['title' => 'Agents', 'page' => 3];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');


$data = get_data('tbl_agents', "JOIN tbl_employees ON emp_id = agent_empid WHERE agent_active =1 ", 'tbl_agents.*, emp_fullname');
?>

<div class="my-3 my-md-5">
					<div class="container">
						
																		
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											
											<li>
												<a href="javascript:void()" class="btn btn-secondary" id="" data-toggle="modal" data-target="#deptModal" ><i class="fa fa-plus"></i> New Agent</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter table-bordered">
											<thead>
												<tr>
													<th>Staff ID</th>
													<th>Agent Name</th>
													<th>Code</th>
													<th>Line</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : ?>
													<tr>
														<td>LPL<?php echo $d['agent_empid'];?></td>
														<td><?php echo $d['emp_fullname'];?></td>
														<td><?php echo $d['agent_code'];?></td>
														<td><?php echo $d['agent_line'];?></td>
														
														<td class="btn-group">
															<button class="btn btn-secondary btn-sm edit" data-id="<?php echo $d['agent_id'];?>" data-comm="" title="Edit"><i class="fa fa-pencil"></i></button>
															<button class="btn btn-danger btn-sm delete-this" data-id="<?php echo $d['agent_id'];?>" data-tbl="agents" title="Delete"><i class="fa fa-trash-o"></i></button>
															
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Agent</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/finance_model/create_agent');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Employee</label>
	        	<select name="emp_id" class="form-control" id="agent">
	        		<option>Select Employee</option>
	        		<?php foreach(get_data('tbl_employees', "WHERE emp_designation  = 'Agent' || emp_designation  LIKE 'CASH%' AND emp_active = 1 ") as $e) : ?>
	        			<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>Agent Code</label>
	        	<input type="number" name="code" placeholder="" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>Agent Line</label>
	        	<input type="number" name="line" placeholder="" class="form-control" required="">
	        </div>
	        
	        <!-- <label>Opening Cash</label> <br>
	        <div class="form-row">
	        	
	        	<div class="form-group col">
		        	<label>SSP Amount</label>
		        	<input type="number" name="ssp_cash" step="any" class="form-control" >
		        </div>
		        <div class="form-group col">
		        	<label>USD Amount</label>
		        	<input type="number" name="usd_cash" step="any" class="form-control" >
		        </div>
	        </div>

	        <label>Opening Float</label> <br>
	        <div class="form-row">
	        	
	        	<div class="form-group col">
		        	<label>SSP Amount</label>
		        	<input type="number" name="ssp_float" step="any" class="form-control" >
		        </div>
		        <div class="form-group col">
		        	<label>USD Amount</label>
		        	<input type="number" name="usd_float" step="any" class="form-control" >
		        </div>
	        </div> -->

	        <input type="hidden" name="name" id="agent-name">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="recon-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Confirm Collection</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/finance_model/confirm_collection')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      		<div class="form-group">
	        	<label>SSP Recieved</label>
	        	<input type="number" name="ssp_amt" id="ssp-amt" class="form-control" required="">
	        </div>
	        
	        <div class="form-group">
	        	<label>USD Recieved</label>
	        	<input type="number" name="usd_amt" id="usd-amt" class="form-control" required="">
	        </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Comments</label>
            <textarea name="comments" class="form-control" required="" id="comments"></textarea>
          </div>

          <div class="form-group">
            <input type="hidden" name="coll_id" required="" class="form-control" id="recon-id" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
</div>


<script type="text/javascript">
	$('#sync').click(function(){
		swal('submitted');
		$.get('<?php echo site_url('epicollect/sync_collections')?>', function(r) {
			//if(r > 0)
				location.reload();
		});
	});

	$('#agent').change(function() {
		$('#agent-name').val($(this).find('option:selected').text());
	})

	$('.reconcile').click(function() {
		let id = $(this).data('id');
		let comm = $(this).data('comm');
		let usd = $(this).closest('tr').find("td:nth-child(5)").text();
		let ssp = $(this).closest('tr').find("td:nth-child(6)").text();
		$('#recon-id').val(id);
		$('#comments').text(comm);
		$('#ssp-amt').val(ssp);
		$('#usd-amt').val(usd);
		$('#recon-modal').modal('show');
	})
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>