<?php 
$dets['page'] = 3; $dets['title'] = 'Appraisals';
$this->load->view('layout/header', $dets); 
$user = get_data('tbl_users', 'JOIN tbl_employees ON emp_id = user_empid WHERE user_id='.$this->session->userdata('user_id'), 'user_id, user_levelid, user_empid, emp_designation, emp_deptid', true);

$start = date('Y-01-01');
$end = date('Y-m-d');

if (!empty($params)) {
  $arr = explode('_', $params);
  $start = reset($arr); $end = end($arr);
}

$data = get_data('tbl_app_appraisals', "LEFT JOIN tbl_app_results ON appraisal_id = app_appraisalid WHERE appraisal_active = 1 GROUP BY appraisal_id", 'COUNT(app_id) AS total, tbl_app_appraisals.*');

if($user['user_levelid'] < 4 || $user['emp_deptid'] == 2)
$depts = get_data('tbl_departments', "WHERE dept_active = 1");
else 
$depts = get_data('tbl_departments', "WHERE dept_active = 1 AND dept_empid = ".$user['user_empid']);

function get_pr_details($id, $currency)
{
  $data = get_data('tbl_payslips', "LEFT JOIN tbl_employees ON emp_id = ps_empid WHERE pr_active = 1 AND ps_active = 1 AND emp_currency = '$currency' AND ps_prid = $id ", 'SUM(ps_due) AS payable, SUM(ps_pit) AS tax, SUM(ps_nssf) AS nssf, SUM(ps_allowances) AS allowances, SUM(ps_loans) AS loans');

  return $data;
}

?>
     <div class="app-content my-3 my-md-5">
          <div class="container">
                       
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">

                    <li>
                      <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#bank-modal"><i class="fa fa-plus"></i> Create Appraisal</a>
                    </li>

                    <li>
                      <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#file-modal"><i class="fa fa-file"></i> Create Brand Attribute</a>
                    </li>
                    
                    <!-- <li class="dropdown">
                    <a aria-expanded="false" aria-haspopup="true" href="javascript: void()" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-plus"></i> New Transaction</a>
                              <div class="dropdown-menu">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal1">Deposit</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal">Withdrawal</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal2">Banking Charges</a>
                              </div>
                    </li> -->

                    <li>
                      <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#ajaxModal" data-title="KPIs" data-url="<?php echo site_url('welcome/modal/kpis');?>"><i class="fa fa-file"></i> Key Performance Indicators</a>
                    </li>
                    
                  </ul>
                </div>
                <div class="card-body p-6">
                    <div class="panel panel-primary">
                      <div class=" tab-menu-heading">
                        <div class="tabs-menu1 ">
                          <!-- Tabs -->
                          <ul class="nav panel-tabs">
                            <li class=""><a href="#tab5" class="active" data-toggle="tab">Appraisals </a></li>
                            <li><a href="#tab6" data-toggle="tab">Company Brand Attributes</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="panel-body tabs-menu-body">
                        <div class="tab-content">
                          <div class="tab-pane active " id="tab5">
                            <div class="table-responsive">
                              <table class="table table-hover table-outline table-vcenter text-nowrap card-table data-table-buttons">
                                <thead>
                                  <tr>
                                    
                                    <th>NO</th>
                                    <th>Year</th>
                                    <th>Period</th>
                                    <th>No of Participants</th>
                                    <th>Results Posted</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  
                                  <?php foreach($data as $b) : ?>
                                    <tr>
                                    
                                    <td >
                                      <div><a href="<?php echo site_url('welcome/view/appraisal/?c=SSP&f='.$b['appraisal_id']);?>">LPL/HR/<?php echo $b['appraisal_id'];?></a></div>
                                    </td>
                                    <td >
                                      <div> <?php echo $b['appraisal_year'];?> </div>
                                    </td>

                                    <td >
                                      <div><?php echo $b['appraisal_period'];?></div>
                                    </td>
                                    <td >
                                      <div><?php echo count(get_data('tbl_employees', "WHERE emp_active = 1 AND emp_deptid IN (".$b['appraisal_depts'].")", 'emp_id'));?></div>
                                    </td>
                                    <td >
                                      <div><?php echo $b['total'];?></div>
                                    </td>
                                    <td class="">
                                      <button class="btn btn-danger btn-sm" onclick="remove_things('tbl_app_appraisals', <?php echo $b['appraisal_id'];?>)"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                  </tr>
                                  <?php endforeach; ?>
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="tab-pane " id="tab6">
                            <div class="table-responsive">
                              <table class="table table-hover table-outline table-vcenter text-nowrap card-table data-table-buttons">
                                <thead>
                                  <tr>
                                    
                                    <th>NO</th>
                                    <th>Attribute Name</th>
                                    <th>Weightage</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  
                                  <?php foreach(get_data('tbl_app_brandatts', "WHERE ba_active = 1") as $b) : ?>
                                    <tr>
                                    
                                    <td >
                                      <div><a href="#"><?php echo $b['ba_id'];?></a></div>
                                    </td>
                                    <td >
                                      <div> <?php echo $b['ba_name'];?> </div>
                                      <div class="text-muted"> <?php echo $b['ba_desc'];?> </div>
                                    </td>

                                    <td >
                                      <div><?php echo $b['ba_weightage'];?></div>
                                    </td>
                                    <td class="">
                                      <button class="btn btn-danger btn-sm" onclick="remove_things('tbl_app_brandatts', <?php echo $b['ba_id'];?>)"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                  </tr>
                                  <?php endforeach; ?>
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
              
            </div>
          </div>


  <div class="modal fade" id="bank-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">New Appraisal</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" enctype="multipart/form-data" method="post" action="<?php echo site_url('system/act/appraisal_model/create')?>">
      <div class="modal-body">
          
          <div class="form-group">
            <label>Year</label>
            <input type="text" class="form-control" name="year" value="<?php echo date('Y');?>" required=""/>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Period</label>
            <select name="period" class="form-control">
                <option value="1st Quarter">1st Quarter</option>
                <option value="2nd Quarter">2nd Quarter</option>
                <option value="3rd Quarter">3rd Quarter</option>
                <option value="4th Quarter">4th Quarter</option>
                <option value="1st Half">1st Half</option>
                <option value="2nd Half">2nd Half</option>
            </select>
          </div>
          
          <div class="form-group">
            <label>Participating Departments</label>
            <select name="depts[]" class="form-control" multiple="">
            	<?php foreach($depts as $d) : ?>
            		<option value="<?php echo $d['dept_id'];?>"><?php echo $d['dept_name'];?></option>
            	<?php endforeach; ?>
            </select>
          </div>
          
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

  <div class="modal fade" id="file-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Create Brand Attribute</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" method="post" action="<?php echo site_url('system/act/appraisal_model/create_brand')?>">
      <div class="modal-body">
          
          <div class="form-group">
            <label for="inputPassword3">Attribute Name:</label>
            <input type="text" class="form-control" name="name" required="" />
          </div>

          <div class="form-group">
            <label for="inputPassword3">Attribute Description:</label>
            <textarea name="desc" class="form-control" required=""></textarea>
          </div>

          <div class="form-group">
            <label for="inputPassword3">Weightage:</label>
            <input type="number" step="any" class="form-control" name="amt" max="100" required="" />
          </div>
          
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Create</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

<?php $this->load->view('layout/footer', array('dtt' => 1, 'sel' => 1, 'dtp' => 1)); ?>