<?php
$dets = ['title' => 'Attendance List', 'page' => 5];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-d', strtotime('-30 days')); 
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d', strtotime('-1 day'));
// if (!empty($params)) {
//   $arr = explode('_', $params);
//   $start = reset($arr); $end = end($arr);
// } 

$user = get_data('tbl_users', "JOIN tbl_employees ON emp_id = user_empid WHERE user_id = ".$this->session->userdata('user_id'), 'user_empid, user_levelid, user_id, emp_deptid',  true);
$and = '';
if ($user['user_levelid'] == 4) {
  $and = " AND emp_id = ".$user['user_empid'];
} 
if ($user['user_levelid'] == 3) {
  $and = " AND emp_deptid = ".$user['emp_deptid'];
}

if(isset($_GET['f']) && !empty($_GET['f'])) {
	$and .= " AND emp_id = '".$_GET['f']."' ";
}

// $data = get_data('tbl_empcheckin', "JOIN tbl_employees ON emp_id = empcheckin_empid WHERE DATE(empcheckin_date) BETWEEN '$start' AND '$end' $and", 'tbl_empcheckin.*, emp_fullname, emp_deptid, emp_id');
$data = get_data('tbl_employees', "LEFT JOIN tbl_departments ON emp_deptid = dept_id WHERE emp_active = 1 $and ", 'emp_id, emp_fullname, emp_deptid, dept_name, emp_designation');

function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {
    $dates = [];
    $current = strtotime( $first );
    $last = strtotime( $last );

    while( $current <= $last ) {

        $dates[] = date( $format, $current );
        $current = strtotime( $step, $current );
    }

    return $dates;
}

$range = dateRange($start, $end);
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
						                    <li><a class="btn btn-secondary" href="<?php echo site_url('welcome/report/attendance_sheet')?>"><i class="fa fa-file"></i> Report</a>
						                    </li>

						                    <li><a class="btn btn-secondary" href="javascript: void(0);" id="syncer" data-toggle="modal" data-target="#att-Modal"><i class="fa fa-refresh"></i> Sync</a>
						                    </li>
											<li>
                      <a class="btn btn-secondary" id="reportrange" href="javascript: void()" data-url="<?php echo site_url('welcome/view/attendance')?>"><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end)) ;?></a>
                    </li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table table-striped table-bordered" style="width:100%">
			                                <thead>
			                                  <tr>
			                                      <th>Date</th>
			                                      <th>Name</th>
			                                      <th>Department</th>
			                                      <th>Designation</th>
			                                      <th>Time In</th>
			                                      <th>Time Out</th>
			                                      <th>Total Hrs</th>
			                                      <th>Overtime In</th>
			                                      <th>Overtime Out</th>
			                                  </tr>
			                                </thead>
			                                <tbody>
			                                  <?php foreach ($data as $d) { 
			                                    // $e = get_data('tbl_employees', "JOIN tbl_departments ON dept_id = emp_deptid WHERE emp_id = ".$d['empcheckin_empid'], 'emp_id, emp_fullname, dept_name', true);
			                                    //$a = get_data('tbl_empcheckin', "WHERE empcheckin_empid = ".$d['emp_id']." AND DATE(empcheckin_date) BETWEEN '$start' AND '$end'");
			                                    foreach($range as $r) :
			                                      $e = get_data('tbl_empcheckin', "WHERE empcheckin_empid = ".$d['emp_id']." AND DATE(empcheckin_date) = '$r' ORDER BY empcheckin_id DESC LIMIT 1", 'tbl_empcheckin.*', true);
			                                   ?>
			                                    <tr>
			                                      <td><a href="#" ><?php echo date('Y-m-d, l', strtotime($r));?></td>
			                                      <td><?php echo $d['emp_fullname'];?></td>
			                                      <td><?php echo $d['dept_name'];?></td>
			                                      <td><?php echo $d['emp_designation'];?></td>
			                                      <td><?php echo isset($e['empcheckin_checkintime']) ? date('g:i a', strtotime($e['empcheckin_checkintime'])) : 'Not checked in';?></td>
			                                      <td><?php echo isset($e['empcheckin_checkouttime']) && $e['empcheckin_checkouttime'] !== '00:00:00' ? date('g:i a', strtotime($e['empcheckin_checkouttime'])) : 'Not checked out';?></td>
			                                      <td><?php echo isset($e['empcheckin_checkintime']) ? time_dif($e['empcheckin_checkintime'], $e['empcheckin_checkouttime']) : 0;?></td>
			                                      <td><?php echo isset($e['empcheckin_otin']) && $e['empcheckin_otin'] !== '00:00:00' ? date('g:i a', strtotime($e['empcheckin_otin'])) : 'None';?></td>
			                                      <td><?php echo isset($e['empcheckin_otout']) && $e['empcheckin_otout'] !== '00:00:00' ? date('g:i a', strtotime($e['empcheckin_otout'])) : 'None';?></td>
			                                      
			                                  </tr>
			                                  <?php endforeach; } ?>
			                                </tbody>
			                              </table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

<!-- Modal -->
<div class="modal fade" id="att-Modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sync Missing Attendance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
            <div class="form-group col">
                <label>From</label>
                <input type="date" class="form-control" name="start" id="start" required value="2021-05-27" />
            </div>
            <div class="form-group col">
                <label>To</label>
                <input type="date" class="form-control" name="end" id="end" required value="2021-05-27" />
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="sync-att" class="btn btn-primary">Sync</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	    $('#sync-att').click(function() {
	        let s = $('#start').val();
	        let e = $('#end').val();
	       // $.get('http://192.168.101.74/tad/thl.php?s='+s+'&e='+e, function(resp) {
	       //     alert(resp);
	       // });
	       location.href = 'http://192.168.101.74/tad/lp.php?s='+s+'&e='+e;
	    });
} );
</script>


<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>