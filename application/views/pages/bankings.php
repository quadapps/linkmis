<?php 
$dets['page'] = 3; $dets['title'] = 'Banking';
$this->load->view('layout/header', $dets); 
$user = get_row_data('tbl_users', 'user_id', $this->session->userdata('user_id'));

$start = date('Y-01-01');
$end = date('Y-m-d');

if (!empty($params)) {
  $arr = explode('_', $params);
  $start = reset($arr); $end = end($arr);
}

$total_withdrawals = get_data('tbl_withdrawals', "WHERE withdrawal_active = 1 AND DATE(withdrawal_date) BETWEEN '$start' AND '$end' GROUP BY withdrawal_currency", 'SUM(withdrawal_amt) AS amt, withdrawal_currency');

$total_deposits = get_data('tbl_deposits', "WHERE deposit_active = 1 AND DATE(deposit_date) BETWEEN '$start' AND '$end' GROUP BY deposit_currency", 'SUM(deposit_amt) AS amt, deposit_currency');

// $total_payments = get_data('tbl_payments', "WHERE payment_active = 1 AND DATE(payment_createdate) BETWEEN '$start' AND '$end' GROUP BY payment_currency", 'SUM(payment_amt) AS amt, payment_currency');

$total_collections = get_data('tbl_collections', "", "SUM(collection_sspamt) AS ssp, SUM(collection_usdamt) AS usd", true);

function get_bank_details($id, $start, $end)
{
  $total_withdrawals = get_data('tbl_withdrawals', "WHERE withdrawal_active = 1 AND withdrawal_bank = $id AND DATE(withdrawal_date) BETWEEN '$start' AND '$end' GROUP BY withdrawal_currency", 'SUM(withdrawal_amt) AS amt, withdrawal_currency', true);
  $total_deposits = get_data('tbl_deposits', "WHERE deposit_active = 1 AND deposit_bank = $id AND DATE(deposit_date) BETWEEN '$start' AND '$end'", 'SUM(deposit_amt) AS amt', true);
  $total_charges = get_data('tbl_bankingcharges', " WHERE bc_active = 1 AND bc_bankid = $id AND DATE(bc_createdate) BETWEEN '$start' AND '$end'", 'SUM(bc_amt) AS amt', true);

  $w = isset($total_withdrawals['amt']) ? $total_withdrawals['amt'] : 0;
  $d = isset($total_deposits['amt']) ? $total_deposits['amt'] : 0;
  $c = isset($total_charges['amt']) ? $total_charges['amt'] : 0;

  return array($w, $d, $c);
}

$banks = get_data('tbl_banks', "WHERE bank_active = 1");
$emps = get_data('tbl_employees', "WHERE emp_deptid = 1", 'emp_id, emp_fullname');

?>
     <div class="app-content my-3 my-md-5">
          <div class="container">
            
            <div class="row row-cards row-deck">
              <div class="col-lg-4 col-md-12 m-b-3">
                <div class="widget-info card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12 col-12">
                        <p>Cash At Hand</p>
                        <h4 class="font-weight-bold">USD <?php echo isset( $total_collections['usd']) && isset($total_deposits[1]) ? number_format(($total_collections['usd']-$total_deposits[1]['amt']), 2) : $total_collections['usd'];?></h4>
                        <h4 class="font-weight-bold">SSP <?php echo isset($total_collections['ssp']) && isset($total_deposits[0]) ? number_format(($total_collections['ssp']-$total_deposits[0]['amt']), 2) : $total_collections['ssp'];?></h4>
                        <!-- <a href="#" class="text-white">View Statement</a> -->
                      </div>
                      <!-- <div class="col-sm-3 col-3 m-t-2 text-right"> <i class="fa fa-usd fa-3x text-white"></i> </div> -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-12 m-b-3">
                <div class="widget-info card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 ">
                        <p>Total Deposits</p>
                        <h4 class="font-weight-bold"><?php echo isset($total_deposits[0]) ? $total_deposits[0]['deposit_currency'].' '.number_format($total_deposits[0]['amt'], 2) : 0;?></h4>
                        <h4 class="font-weight-bold"><?php echo isset($total_deposits[1]) ? $total_deposits[1]['deposit_currency'].' '.number_format($total_deposits[1]['amt'], 2) : 0;?></h4>
                        <!-- <a href="#" class="text-white">View Statement</a> -->
                      </div>
                      <!-- <div class="col-3 m-t-2 text-right">  <i class="fa fa-globe fa-3x text-white"></i>  </div> -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-12 m-b-3">
                <div class="widget-info card ">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 ">
                        <<!-- p>Bank Balance</p>
                        <h4 class="font-weight-bold"><?php echo isset($total_deposits[0]) && isset($total_withdrawals[0]) ? $total_deposits[0]['deposit_currency'].' '.number_format($total_deposits[0]['amt']-$total_withdrawals[0]['amt'], 2) : 0;?></h4>
                        <h4 class="font-weight-bold"><?php echo isset($total_deposits[1]) && isset($total_withdrawals[1]) ? $total_deposits[1]['deposit_currency'].' '.number_format($total_deposits[1]['amt']-$total_withdrawals[1]['amt'], 2) : 0;?></h4> -->
                        <!-- <a href="#" class="text-white">View Statement</a> -->
                      </div>
                      <!-- <div class="col-3 m-t-2 text-right"><i class="fa fa-pie-chart fa-3x text-white" ></i></div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">

                    <li>
                      <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#bank-modal"><i class="fa fa-plus"></i> Add Bank</a>
                    </li>

                    <li>
                      <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#file-modal"><i class="fa fa-file"></i> Import Transactions</a>
                    </li>
                    
                    <li class="dropdown">
                    <a aria-expanded="false" aria-haspopup="true" href="javascript: void()" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-plus"></i> New Transaction</a>
                              <div class="dropdown-menu">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal1">Deposit</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal">Withdrawal</a>
                                <!-- <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal2">Banking Charges</a> -->
                              </div>
                    </li>

                    <li>
                      <a class="btn btn-secondary" id="reportrange" href="javascript: void()" data-url="<?php echo site_url('welcome/view/banking')?>"><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end)) ;?></a>
                    </li>
                    
                  </ul>
                </div>
                  <div class="table-responsive">
                    <table id="example" class="table table-hover table-outline table-vcenter text-nowrap card-table">
                      <thead>
                        <tr>
                          <th class="text-center w-1"><i class="icon-people"></i></th>
                          <th>Cashier Name</th>
                          <th>SSP Collected</th>
                          <th>SSP CashOut</th>
                          <th>SSP Balance</th>
                          <th>USD Collected</th>
                          <th>USD CashOut</th>
                          <th>USD Balance</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        <?php foreach($emps as $b) :  
                          $coll = get_data('tbl_collections', "JOIN tbl_users ON user_id = collection_cashier WHERE DATE(collection_date) BETWEEN '$start' AND '$end' AND user_empid = ".$b['emp_id'], 'SUM(collection_sspamt) AS ssp_amt, SUM(collection_usdamt) AS usd_amt', true);
                          $co = get_data('tbl_cashouts', "JOIN tbl_users ON user_id = co_creator WHERE DATE(co_createdate) BETWEEN '$start' AND '$end' AND user_empid = ".$b['emp_id'], 'SUM(co_sspamt) AS ssp_amt, SUM(co_usdamt) AS usd_amt', true);
                          ?>
                          <tr>
                          <td class="text-center">
                            <div class="avatar brround d-block" style="background-image: url(assets/images/faces/female/26.jpg)">
                              <span class="avatar-status bg-green"></span>
                            </div>
                          </td>
                          <td >
                            <div><a href="<?php echo site_url('welcome/view/deposits/?f='.$b['emp_id']);?>"><?php echo $b['emp_fullname'];?></a></div>
                          </td>
                          <td >
                            <div> <?php echo number_format($coll['ssp_amt']);?> </div>
                          </td>

                          <td >
                            <div><?php echo number_format($co['ssp_amt']);?></div>
                          </td>
                          <td >
                            <div><?php echo number_format($coll['ssp_amt']-$co['ssp_amt']);?></div>
                          </td>
                          <td>
                            <!-- <div class="small text-muted">Last login</div> -->
                            <div> <?php echo number_format($coll['usd_amt']);?> </div>
                          </td>
                          <td class="">
                            <div> <?php echo number_format($coll['usd_amt']);?> </div>
                          </td>
                          <td class="">
                            <div><?php echo number_format($coll['usd_amt']-$co['usd_amt']);?></div>
                          </td>
                        </tr>
                        <?php endforeach; ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              
            </div>
          </div>

<?php //$banks = get_data('tbl_banks', "WHERE bank_active = 1"); ?>
<!-- Modal -->
<div class="modal fade" id="enq-modal1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Make Deposit</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" enctype="multipart/form-data" method="post" action="<?php echo site_url('system/act/clients_model/make_deposit')?>">
      <div class="modal-body">
          
          <div class="form-group">
            <label>Amount Deposited</label>
            <input type="number" class="form-control" name="amt" required=""/>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Currency:</label>
            <select name="currency" class="form-control">
                <option>SSP</option>
                <option>USD</option>
            </select>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Deposit mode:</label>
            <select name="mode" class="form-control">
                <option>Cash</option>
                <option>Cheque</option>
                <option>BankTransfer</option>
            </select>
          </div>
          
          <div class="form-group">
            <label>Date Deposited</label>
            <input type="text" class="form-control datepickers" name="date" required=""/>
          </div>

          <div class="form-group">
            <label>Bank Deposited</label>
            <select name="bank" class="form-control">
              <?php foreach($banks as $b) { ?>
                <option value="<?php echo $b['bank_id'];?>"><?php echo $b['bank_name'].' - '.$b['bank_acctno'].' - '.$b['bank_currency'];?></option>
              <?php } ?>
            </select>
          </div>

          <!-- <div class="form-group">
            <label>Account Deposited</label>
            <input type="text" class="form-control " name="acct" required=""/>
          </div> -->
          
          <div class="form-group">
            <label for="inputPassword3">Deposit Slip:</label>
            <input type="file" name="userfile" />
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Remarks:</label>
            <textarea class="form-control" name="remarks" placeholder=""></textarea>
          </div>
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Save Deposit</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

<div class="modal fade" id="enq-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Make Withdrawal</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" enctype="multipart/form-data" method="post" action="<?php echo site_url('system/act/clients_model/make_withdrawal')?>">
      <div class="modal-body">
          
          <div class="form-group">
            <label>Amount Withdrawn</label>
            <input type="number" class="form-control" name="amt" required=""/>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Currency:</label>
            <select name="currency" class="form-control">
                <option>SSP</option>
                <option>USD</option>
            </select>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Mode:</label>
            <select name="mode" class="form-control">
                <option>Cash</option>
                <option>Cheque</option>
                <option>BankTransfer</option>
            </select>
          </div>
          
          <div class="form-group">
            <label>Date Withdrawn</label>
            <input type="text" class="form-control datepickers" name="date" required=""/>
          </div>

          <div class="form-group">
            <label>Bank Withdrawn</label>
            <select name="bank" class="form-control">
              <?php foreach($banks as $b) { ?>
                <option value="<?php echo $b['bank_id'];?>"><?php echo $b['bank_name'].' - '.$b['bank_acctno'].' - '.$b['bank_currency'];?></option>
              <?php } ?>
            </select>
          </div>

          <!-- <div class="form-group">
            <label>Account Withdrawn</label>
            <input type="text" class="form-control " name="acct" required=""/>
          </div> -->
          
          <div class="form-group">
            <label for="inputPassword3">Bank Slip:</label>
            <input type="file" name="userfile" />
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Remarks:</label>
            <textarea class="form-control" name="remarks" placeholder=""></textarea>
          </div>
          </div>    
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Save Withdrawal</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

  <div class="modal fade" id="enq-modal2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">New Bank Charges</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" method="post" action="<?php echo site_url('system/act/clients_model/create_bankcharges')?>">
      <div class="modal-body">
          
          <div class="form-group">
            <label>Amount Charged</label>
            <input type="number" class="form-control" name="amt" required=""/>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Currency:</label>
            <select name="currency" class="form-control">
                <option>SSP</option>
                <option>USD</option>
            </select>
          </div>
          
          <div class="form-group">
            <label>Date </label>
            <input type="text" class="form-control datepickers" name="date" required=""/>
          </div>

          <div class="form-group">
            <label>Bank Account</label>
            <select name="bank" class="form-control">
              <?php foreach($banks as $b) { ?>
                <option value="<?php echo $b['bank_id'];?>"><?php echo $b['bank_name'].' - '.$b['bank_acctno'].' - '.$b['bank_currency'];?></option>
              <?php } ?>
            </select>
          </div>

          <!-- <div class="form-group">
            <label>Account Deposited</label>
            <input type="text" class="form-control " name="acct" required=""/>
          </div> -->
          
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Save </button>
          </div>
      </form> 
    </div>
  </div>
  </div>

  <div class="modal fade" id="bank-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">New Bank Account</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" enctype="multipart/form-data" method="post" action="<?php echo site_url('system/act/clients_model/create_bank')?>">
      <div class="modal-body">
          
          <div class="form-group">
            <label>Bank Name</label>
            <input type="text" class="form-control" name="bank_name" required=""/>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Currency:</label>
            <select name="currency" class="form-control">
                <option>SSP</option>
                <option>USD</option>
            </select>
          </div>
          
          <div class="form-group">
            <label>Account No</label>
            <input type="number" class="form-control " name="acct_no" required=""/>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Account Name:</label>
            <input type="text" name="acct_name" class="form-control" required="" />
          </div>
          
          <div class="form-group">
            <label>Opening Balance</label>
            <input type="number" class="form-control " name="bal" value="0" />
          </div>
          
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

  <div class="modal fade" id="file-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Import Transactions</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" enctype="multipart/form-data" method="post" action="<?php echo site_url('system/act/clients_model/import_bank_transactions')?>">
      <div class="modal-body">


          <div class="form-group">
            <label>Bank Account</label>
            <select name="bank" class="form-control">
              <?php foreach($banks as $b) { ?>
                <option value="<?php echo $b['bank_id'];?>"><?php echo $b['bank_name'].' - '.$b['bank_acctno'].' - '.$b['bank_currency'];?></option>
              <?php } ?>
            </select>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Select File:</label>
            <input type="file" name="userfile" required="" />
            <a href="<?php echo base_url('TRANSACTIONS_TEMPLATE.csv');?>" class="btn btn-info"><i class="fa fa-file-excel"></i> Download Template</a>
          </div>
          
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Upload</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

<?php $this->load->view('layout/footer', array('dtt' => 1, 'sel' => 1, 'dtp' => 1)); ?>