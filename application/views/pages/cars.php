<?php
$dets = ['title' => 'Vehicles', 'page' => 7];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');


$data = get_data('tbl_vehicles', "WHERE car_active =1 ");
?>

<div class="my-3 my-md-5">
					<div class="container">
						
																		
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											
											<li>
												<a href="javascript:void()" class="btn btn-secondary" id="" data-toggle="modal" data-target="#deptModal" ><i class="fa fa-plus"></i> New Vehicle</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter">
											<thead>
												<tr>
													<th>Number</th>
													<th>Type</th>
													<th>Last Modified</th>
													<th>Added By</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : ?>
													<tr>
														<td><?php echo $d['car_number'];?></td>
														<td><?php echo $d['car_type'];?></td>
														<td><?php echo $d['car_updatedate'];?></td>
														<td><?php echo get_fullname($d['car_creator']);?></td>
														<td class="btn-group">
															<button class="btn btn-secondary btn-sm reconcile" data-id="<?php echo $d['car_id'];?>" data-comm="" title="Edit"><i class="fa fa-pencil"></i></button>
															
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Vehicle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/admin_model/create_car');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Number Plate</label>
	        	<input type="text" name="car_no" placeholder="SSD001AA" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>Vehicle Type</label>
	        	<select name="type" class="form-control">
	        		<option>Motor Vehicle</option>
	        		<option>Motor Cycle</option>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>Last Refuel Date</label>
	        	<input type="date" name="date" class="form-control" >
	        </div>
	        <div class="form-group">
	        	<label>Litres Fueled</label>
	        	<input type="number" name="lts" class="form-control"  value="0">
	        </div>
	        <div class="form-group">
	        	<label>Cost Per Ltd</label>
	        	<input type="number" name="cost" class="form-control"  value="0">
	        </div>

	        <div class="form-group">
	        	<label>Last Odometer Reading</label>
	        	<input type="number" name="reading" class="form-control" value="0">
	        </div>

	        
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="recon-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Confirm Collection</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/finance_model/confirm_collection')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      		<div class="form-group">
	        	<label>SSP Recieved</label>
	        	<input type="number" name="ssp_amt" id="ssp-amt" class="form-control" required="">
	        </div>
	        
	        <div class="form-group">
	        	<label>USD Recieved</label>
	        	<input type="number" name="usd_amt" id="usd-amt" class="form-control" required="">
	        </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Comments</label>
            <textarea name="comments" class="form-control" required="" id="comments"></textarea>
          </div>

          <div class="form-group">
            <input type="hidden" name="coll_id" required="" class="form-control" id="recon-id" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
</div>


<script type="text/javascript">
	$('#sync').click(function(){
		swal('submitted');
		$.get('<?php echo site_url('epicollect/sync_collections')?>', function(r) {
			//if(r > 0)
				location.reload();
		});
	});

	$('.reconcile').click(function() {
		let id = $(this).data('id');
		let comm = $(this).data('comm');
		let usd = $(this).closest('tr').find("td:nth-child(5)").text();
		let ssp = $(this).closest('tr').find("td:nth-child(6)").text();
		$('#recon-id').val(id);
		$('#comments').text(comm);
		$('#ssp-amt').val(ssp);
		$('#usd-amt').val(usd);
		$('#recon-modal').modal('show');
	})
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>