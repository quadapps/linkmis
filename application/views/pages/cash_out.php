<?php
$dets = ['title' => 'Cash Out Transactions', 'page' => 3];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');
$user = get_data('tbl_users', "JOIN tbl_employees ON emp_id = user_empid WHERE user_id = $uid", 'user_id, user_levelid, user_empid, emp_designation', true);
// $emp = get_data('tbl_employees', "WHERE emp_id = ".$user['user_empid'], 'emp_id, emp_designation', true);
// $des = strtolower($emp['emp_designation']);
// if (strpos($des, 'project') !== false || $des == 'production manager') {
// 	$this->load->view('dashboards/projects');
// } else {
// 	$this->load->view('dashboards/main');
// }\\

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-01');
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d');

$ssp = get_data('tbl_cashouts', "WHERE co_currency = 'SSP' AND DATE(co_createdate) BETWEEN '$start' AND '$end'", "SUM(co_amt) AS ssp", true)['ssp'];
$usd = get_data('tbl_cashouts', "WHERE co_currency = 'USD' AND DATE(co_createdate) BETWEEN '$start' AND '$end'", "SUM(co_amt) AS usd", true)['usd'];

$data = get_data('tbl_cashouts', "JOIN tbl_agents ON agent_empid = co_agentname WHERE co_active =1 AND DATE(co_createdate) BETWEEN '$start' AND '$end'");
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="card-body">
									<h5><strong>Total Cash Outs</strong> </h5>
									<p><strong>USD: </strong> <?php echo number_format($usd, 2);?></p>
									<p><strong>SSP: </strong> <?php echo number_format($ssp, 2);?></p>
								</div>
							</div>
						</div>
																		
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="javascript:void()" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/view/cash_out');?>" ><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end));?></a>
											</li>
											<li>
												<a href="javascript:void()" class="btn btn-secondary" id="" data-toggle="modal" data-target="#deptModal" ><i class="fa fa-plus"></i> New CashOut</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter">
											<thead>
												<tr>
													<th>Date</th>
													<th>Agent Code</th>
													<th>Agent Name</th>
													<th>Agent Line</th>
													<th>Currency</th>
													<th> Amount</th>
													<th>Type</th>
													<th>Issued By</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : ?>
													<tr>
														<td><?php echo $d['co_createdate'];?></td>
														<td><?php echo $d['agent_code'];?></td>
														<td><?php echo get_that_data('tbl_employees', 'emp_fullname', 'emp_id', $d['agent_empid']);?></td>
														<td><?php echo $d['agent_line'];?></td>
														<td><?php echo $d['co_currency'];?></td>
														<td><?php echo $d['co_amt'];?></td>
														<td><?php echo $d['co_type'] == 1 ? 'Float' : 'Cash';?></td>
														
														<td><?php echo get_fullname($d['co_creator']);?></td>
														<td class="btn-group">
															<?php 
													 if($d['co_status'] == 0) echo '<span class="status-icon bg-primary"></span> In Transit';
													 if($d['co_status'] == 1) echo '<span class="status-icon bg-info"></span> In Transit';
													 if($d['co_status'] == 2) echo '<span class="status-icon bg-success"></span> Received';
													 if($d['co_status'] == -1) echo '<span class="status-icon bg-danger"></span> Declined';
													 ?>
															
														</td>
														<td>
															<?php
															if($d['co_status'] < 1 ) : ?>
																<button class="btn btn-danger btn-sm delete-this" data-id="<?php echo $d['co_id'];?>" data-tbl="cashouts"><i class="fa fa-trash-o"></i> Void</button>
															<?php endif; ?>
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Issue Cash/Float</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/finance_model/create_cashout');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Agent Name</label>
	        	<select name="name" class="form-control">
	        		<?php foreach(get_data('tbl_agents', "JOIN tbl_employees ON emp_id = agent_empid WHERE agent_active = 1", 'agent_empid, emp_fullname') as $e) : ?>
	        			<option value="<?php echo $e['agent_empid'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>
	        
	        <div class="form-group">
	        	<label>Type</label>
	        	<select name="type" class="form-control">
	        		<option value="0">Cash</option>
	        		<option value="1">Float</option>
	        	</select>
	        </div>

	        <div class="form-group">
	        	<label>Currency</label>
	        	<input type="radio" name="currency" class="form-control" required="" value="SSP" checked=""> SSP<br>
	        	<input type="radio" name="currency" class="form-control" required="" value="USD"> USD<br>
	        </div>

	        <div class="form-group">
	        	<label>Amount Issued</label>
	        	<input type="number" step="any" name="amt" class="form-control" required="" value="0">
	        </div>

	        <div class="form-group">
	        	<label>Collector</label>
	        	<select name="collector" class="form-control">
	        		<option value="0"></option>
	        		<?php foreach(get_data('tbl_employees', "WHERE emp_active = 1 AND emp_designation LIKE 'CASH%'") as $e) : ?>
	        			<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="recon-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Confirm Collection</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/finance_model/confirm_collection')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      		<div class="form-group">
	        	<label>SSP Recieved</label>
	        	<input type="number" name="ssp_amt" id="ssp-amt" class="form-control" required="">
	        </div>
	        
	        <div class="form-group">
	        	<label>USD Recieved</label>
	        	<input type="number" name="usd_amt" id="usd-amt" class="form-control" required="">
	        </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Comments</label>
            <textarea name="comments" class="form-control" required="" id="comments"></textarea>
          </div>

          <div class="form-group">
            <input type="hidden" name="coll_id" required="" class="form-control" id="recon-id" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
</div>


<script type="text/javascript">
	$('#sync').click(function(){
		swal('submitted');
		$.get('<?php echo site_url('epicollect/sync_collections')?>', function(r) {
			//if(r > 0)
				location.reload();
		});
	});

	$('.reconcile').click(function() {
		let id = $(this).data('id');
		let comm = $(this).data('comm');
		let usd = $(this).closest('tr').find("td:nth-child(5)").text();
		let ssp = $(this).closest('tr').find("td:nth-child(6)").text();
		$('#recon-id').val(id);
		$('#comments').text(comm);
		$('#ssp-amt').val(ssp);
		$('#usd-amt').val(usd);
		$('#recon-modal').modal('show');
	});
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>