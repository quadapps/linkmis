<?php
$this->load->view('layout/header', ['title' => 'Home', 'page' => 2]);
$uid = $this->session->userdata('user_id');
// $user = get_data('tbl_users', "WHERE user_id = $uid", 'user_id, user_levelid, user_empid', true);
// $emp = get_data('tbl_employees', "WHERE emp_id = ".$user['user_empid'], 'emp_id, emp_designation', true);
// $des = strtolower($emp['emp_designation']);
// if (strpos($des, 'project') !== false || $des == 'production manager') {
// 	$this->load->view('dashboards/projects');
// } else {
// 	$this->load->view('dashboards/main');
// }
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Our Customers</h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#client-modal" ><i class="fa fa-plus"></i> Add New</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter">
											<thead>
												<tr>
													<th>Name</th>
													<th>Category</th>
													<th>Address</th>
													<th>Contacts</th>
													<th>Contact Person</th>
													<th>Marketer</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php foreach(get_data('tbl_clients', "LEFT JOIN tbl_clientcategories ON cat_id = client_catid LEFT JOIN tbl_employees ON client_marketerid = emp_id WHERE client_active = 1", 'tbl_clients.*, cat_name, emp_fullname') as $d) : ?>

													<tr>
													<td><a href="<?php echo site_url('welcome/view/client_details/'.$d['client_id']);?>" class="text-inherit"><?php echo $d['client_name'];?></a></td>
													<td><?php echo $d['cat_name'];?></td>
													<td><?php echo $d['client_address'];?></td>
													<td><?php echo $d['client_phone'].', '.$d['client_email'];?></td>
													<td><?php echo $d['client_contactperson'].', '.$d['client_contactpersonphone'].', '.$d['client_contactpersonemail'];?></td>
													<td><?php echo $d['emp_fullname'];?></td>
													<td class="text-right dropdown">
														<button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														    Action
														  </button>
														  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														    <a href="javascript:void(0)" class="dropdown-item"><i class="fa fa-pencil"></i> Edit</a>
															<a href="javascript:void(0)" class="dropdown-item"><i class="fa fa-link"></i> Follow Up</a>
															<a href="javascript:void(0)" class="dropdown-item"><i class="fa fa-trash"></i> Delete</a>
														  </div>
														
													</td>
												</tr>

											<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>