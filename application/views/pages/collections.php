<?php
$dets = ['title' => 'Cash Collections', 'page' => 3];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');
$emp = get_data('tbl_employees', "JOIN tbl_users ON user_empid = emp_id WHERE user_id = $uid", 'emp_id, emp_designation', true);

$des = strtolower($emp['emp_designation']);

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-01');
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d');

$ssp = get_data('tbl_collections', "WHERE DATE(collection_createdate) BETWEEN '$start' AND '$end' AND collection_currency = 'SSP'", "SUM(collection_amt) AS ssp ", true)['ssp'];
$usd = get_data('tbl_collections', "WHERE DATE(collection_createdate) BETWEEN '$start' AND '$end' AND collection_currency = 'USD'", "SUM(collection_amt) AS usd", true)['usd'];

$data = get_data('tbl_collections', "LEFT JOIN tbl_users ON user_id = collection_creator JOIN tbl_agents ON collection_agent = agent_empid WHERE collection_active =1 AND DATE(collection_createdate) BETWEEN '$start' AND '$end'", 'tbl_collections.*, agent_name, agent_code, user_fullname');
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="card-body">
									<h5><strong>Total Collections</strong> </h5>
									<p><strong>USD: </strong> <?php echo number_format($usd, 2);?></p>
									<p><strong>SSP: </strong> <?php echo number_format($ssp, 2);?></p>
								</div>
							</div>
						</div>
																		
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="javascript:void()" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/view/collections');?>" ><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end));?></a>
											</li>
											<!-- <li>
												<a href="javascript:void()" class="btn btn-secondary" id="sync" ><i class="fa fa-refresh"></i> Sync</a>
											</li> -->
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter">
											<thead>
												<tr>
													<th>Date</th>
													<th>Agent Code</th>
													<th>Agent Name</th>
													<th>Amount Given</th>
													<th>Amount Received</th>
													<th>Type</th>
													<th>Issued By</th>
													<th>Collected By</th>
													<th>Status</th>
													<th>Received By</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : ?>
													<tr>
														<td><?php echo $d['collection_createdate'];?></td>
														<td><?php echo $d['agent_code'];?></td>
														<td><?php echo $d['agent_name'];?></td>
														<td><?php echo $d['collection_currency'].$d['collection_amt'];?></td>
														<td><?php echo $d['collection_currency'].$d['collection_amtrec'];?></td>
														<td><?php echo $d['collection_type'];?></td>
														<td><?php echo get_fullname($d['collection_creator']);?></td>
														<td><?php echo $d['collection_collector'] == 0 ? 'Not Collected' : get_fullname($d['collection_collector']);?></td>
														<td>
															 <?php 
																 if($d['collection_status'] == 0) echo '<span class="status-icon bg-primary"></span> Pending';
																 if($d['collection_status'] == 1) echo '<span class="status-icon bg-info"></span> Collected';
																 if($d['collection_status'] == 2) echo '<span class="status-icon bg-success"></span> Received'.'<br>'.$d['collection_remarks'];
																 if($d['collection_status'] == -1) echo '<span class="status-icon bg-danger"></span> Declined';
																 ?>
														</td>
														<td><?php echo $d['collection_cashier'] == 0 ? '<span class="text-danger">Not Recevied</span>' : get_fullname($d['collection_cashier']);?></td>
														<td class="btn-group">
															<?php if(($d['collection_status'] == 1 || $d['collection_collector'] == 0) && $d['collection_status'] != 2 ) : ?>
																<button class="btn btn-secondary btn-sm reconcile" data-id="<?php echo $d['collection_id'];?>" data-amt="<?php echo $d['collection_amt'];?>" data-comm="<?php echo $d['collection_remarks'];?>" title="Confirm/Reject"><i class="fa fa-check"></i> Confirm</button>
															<?php endif; ?>
															
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Department</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/hr_model/create_dept');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Deparment Name</label>
	        	<input type="text" name="dept" class="form-control" required="">
	        </div>
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="recon-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Confirm Collection</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/finance_model/confirm_collection')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      		<div class="form-group">
	        	<label>Amount Recieved</label>
	        	<input type="number" name="amt" id="ssp-amt" class="form-control" required="">
	        </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Comments</label>
            <textarea name="comments" class="form-control" required="" id="comments"></textarea>
          </div>

          <div class="form-group">
            <input type="hidden" name="coll_id" required="" class="form-control" id="recon-id" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
</div>


<script type="text/javascript">
	$('#sync').click(function(){
		swal('submitted');
		$.get('<?php echo site_url('epicollect/sync_collections')?>', function(r) {
			//if(r > 0)
				location.reload();
		});
	});

	$('.reconcile').click(function() {
		let id = $(this).data('id');
		let comm = $(this).data('comm');
		let amt = $(this).data('amt');
		let ssp = $(this).closest('tr').find("td:nth-child(4)").text();
		$('#recon-id').val(id);
		$('#comments').text(comm);
		// $('#ssp-amt').val(ssp);
		$('#ssp-amt').val(amt);
		$('#recon-modal').modal('show');
	})
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>