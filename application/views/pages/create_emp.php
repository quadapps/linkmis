<?php
$this->load->view('layout/header', ['title' => 'Employee Information', 'page' => 5]);
$uid = $this->session->userdata('user_id');
$id = $params;
$user = get_data('tbl_users', "WHERE user_id = ".$this->session->userdata('user_id'), 'user_id, user_levelid, user_empid', true);
$user_level = $user['user_levelid'];
$dept = get_that_data('tbl_employees', 'emp_deptid', 'emp_id', $user['user_empid']);

$emp_id = 0;
$e = get_data('tbl_employees', "ORDER BY emp_id DESC LIMIT 1", 'emp_id', true);
if(!empty($e)) $emp_id = $e['emp_id']+1;

if(!empty($id))
$data = get_row_data('tbl_employees', 'emp_id', $id);
?>

<div class="my-3 my-md-5">
          <div class="container">
            <!-- <div class="page-header">
              <h4 class="page-title">Dashboard</h4>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
              </ol>
            </div> -->
                                    
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Employee Information</h3>
                    <!-- <ul class="card-options panel-toolbox">
                      <li>
                        <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#client-modal" ><i class="fa fa-plus"></i> Add New</a>
                      </li>
                    </ul> -->
                  </div>
                  <div class="table-responsive card-body">
                    <form class="form-horizontal ajax-form" role="form" method="post" action="<?php echo empty($id) ? site_url('system/act/hr_model/create_employee') : site_url('system/act/hr_model/update_employee')?>" id="ajax-formZ">
                         <h3>Personal Details</h3>
                          <div class="row no-gutter">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Full Name:</label>
                                <div class="col-sm-9">
                                  <input type="text" name="full_name" class="form-control" required="" value="<?php echo isset($data['emp_fullname']) ? $data['emp_fullname'] : ''?>" />
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Address:</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control" name="address" required="" rows="5" placeholder="Enter adress here ...."><?php echo isset($data['emp_residence']) ? $data['emp_residence'] : ''?></textarea>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Home Phone<i class="fa fa-phone-square"></i>:</label>
                                <div class="col-sm-9">
                                  <input type="text" name="home_phone" class="form-control" value="<?php echo isset($data['emp_homephone']) ? $data['emp_homephone'] : ''?>" />
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Work Phone<i class="fa fa-mobile"></i>:</label>
                                <div class="col-sm-9">
                                  <input type="text" name="work_phone" class="form-control" value="<?php echo isset($data['emp_workphone']) ? $data['emp_workphone'] : ''?>" />
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Work Email <i class="fa fa-envelope"></i>:</label>
                                <div class="col-sm-9">
                                  <input type="text" name="email" class="form-control" placeholder="email@links-pay.com" value="<?php echo isset($data['emp_email']) ? $data['emp_email'] : ''?>" required/>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Gmail <i class="fa fa-envelope"></i>:</label>
                                <div class="col-sm-9">
                                  <input type="text" name="gmail" class="form-control" placeholder="name@gmail.com" value="<?php echo isset($data['emp_email']) ? $data['emp_gmail'] : ''?>" required/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row no-gutter">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Date of Birth:</label>
                                <div class="col-sm-8">
                                  <input type="date" name="birth_date" class="form-control datepickers" value="<?php echo isset($data['emp_birthdate']) ? $data['emp_birthdate'] : ''?>" />
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Citizenship:</label>
                                <div class="col-sm-8">
                                  <input type="text" name="nationality" class="form-control" value="<?php echo isset($data['emp_nationality']) ? $data['emp_nationality'] : ''?>" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row no-gutter">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Passport/ID NO:</label>
                                <div class="col-sm-8">
                                  <input type="text" name="ppid" class="form-control" value="<?php echo isset($data['emp_ppno']) ? $data['emp_ppno'] : ''?>" />
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Place of Issue:</label>
                                <div class="col-sm-8">
                                  <input type="text" name="place" class="form-control" value="<?php echo isset($data['emp_issueplace']) ? $data['emp_issueplace'] : ''?>" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row no-gutter">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Gender:</label>
                                <div class="col-sm-8">
                                  <select name="gender" class="form-control">
                                    <option>Male</option>
                                    <option>Female</option>
                                    <option>Other</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Marital Status:</label>
                                <div class="col-sm-8">
                                  <input type="text" name="marital_status" class="form-control" value="<?php echo isset($data['emp_maritalstatus']) ? $data['emp_maritalstatus'] : ''?>" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php if ($user_level < 3 || $dept == 2) : ?>
                          <h3>Employment Details</h3>
                          <div class="row no-gutter">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-9 control-label">Duties/Responsibilities</label>
                                    <div class="col-sm-9">
                                    <textarea class="form-control" name="duties" required="" rows="6"><?php echo isset($data['emp_duties']) ? $data['emp_duties'] : ''?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">Designation:</label>
                                <div class="col-sm-8">
                                  <select name="designation" class="form-control">
                                    <option value="<?php echo isset($data['emp_designation']) ? $data['emp_designation'] : '0'?>"><?php echo isset($data['emp_designation']) ? $data['emp_designation'] : '--Select Designation--'?></option>
                                    <option>MANAGING DIRECTOR</option>
                                    <option>HEAD OF LINKS PAY</option>
                                    
                                    <option>HR MANAGER</option>
                                    <option>MARKETING MANAGER</option>
                                    <option>HEAD OF AGENT &amp; DIRECT CHANNEL</option>
                                    <option>MERCHANT HEAD</option>
                                    <option>FOREX HEAD</option>
                                    <option>BACKEND SUPPORT</option>
                                    <option>OPERATIONS MANAGER</option>
                                    <option>PROCUREMENT &amp; LOGISTICS MANAGER</option>
                                    <option>SUB EDITOR-CITY REVIEW NEWSLETTER</option>
                                    <option>ADMIN OFFICER</option>
                                    <option>HR OFFICER</option>
                                    <option>PROCUREMENT OFFICER</option>
                                    <option>LOGISTIC OFFICER</option>
                                    <option>ACCOUNTANT</option>
                                    <option>SENIOR ACCOUNTANT</option>
                                    <option>CASHIER</option>
                                    <option>CASH COLLECTOR</option>

                                    <option>CLUSTER MANAGER</option>
                                    <option>REGIONAL MANAGER</option>
                                    <option>ASM</option>
                                    <option>ANM</option>
                                    <option>MDR</option>
                                    <option>TDR</option>
                                    <option>AGENT</option>
                                    <option>BA</option>
                                    
                                    <option>CUSTOMER CARE EXECUTIVE</option>
                                    <option>STORE KEEPER</option>
                                    <option>DRIVER</option>
                                  
                                    <option>SECURITY PERSONNEL</option>                                 
                                    <option>OFFICE CLEANER</option>
                                    <option>CHEF</option>   
                                    <option>GENERAL STAFF</option>
                                  
                                    
                                  </select>
                                </div>
                                
                              </div>
                            </div>
                          
                            <div class="col-md-6">
                              <div class="form-row">
                                <div class="form-group col">
                                  <label>Emp ID</label>
                                  <input type="number" name="bio_id" class="form-control" value="<?php echo $emp_id;?>" <?php echo isset($data['emp_id']) ? 'readonly' : '';?> required>
                                </div>
                                <div class="form-group col">
                                <label for="inputPassword3" class="control-label">Employment Date:</label>
                                <div class="">
                                  <input type="date" name="emp_date" class="form-control datepickers" value="<?php echo isset($data['emp_empdate']) ? $data['emp_empdate'] : ''?>" />
                                </div>
                              </div>
                              </div>

                              <div class="form-row">
                                <div class="form-group col">
                                  <label for="inputPassword3" class="">Basic Salary:</label>
                                  <div class="">
                                    <input type="number" class="form-control" name="salary" value="<?php echo isset($data['emp_salary']) ? $data['emp_salary'] : ''?>" />
                                  </div>
                                </div>
                                <div class="form-group col">
                                  <label for="inputPassword3" class="">Incentive:</label>
                                  <div class="">
                                    <input type="number" class="form-control" name="incentive" value="<?php echo isset($data['emp_incentive']) ? $data['emp_incentive'] : ''?>" />
                                  </div>
                                </div>
                              </div>
                            
                              <div class="form-row">
                                <div class="form-group col-3">
                                  <label for="inputPassword3" class="control-label">Salary Currency:</label>
                                  <div class="">
                                    <input type="radio" value="SSP" name="currency"> SSP
                                    <input type="radio" value="USD" name="currency"> USD
                                  </div>
                                </div>
                                <div class="form-group col-9">
                                  <label for="inputPassword3" class="control-label">Check where applicable:</label>
                                  <div class="">
                                    <input type="checkbox" value="1" name="pays_pit"> Pays PIT?
                                    <input type="checkbox" value="1" name="pays_nssf"> Pays NSSF?
                                  </div>
                                </div>
                              </div>
                            
                                <div class="form-row">
                                
                                <div class="form-group col-sm-8">
                                  <label for="inputPassword3" class="control-label">Department:</label>
                                  <select class="form-control" name="dept">
                                    <option value="<?php echo isset($data['emp_deptid']) ? $data['emp_deptid'] : '0'?>"><?php echo isset($data['emp_deptid']) ? get_that_data('tbl_departments', 'dept_name', 'dept_id', $data['emp_deptid']) : '--Select Department--'?></option>
                                    <?php foreach(get_data('tbl_departments', 'WHERE dept_active = 1') as $d) { ?>
                                    <option value="<?php echo $d['dept_id']?>"><?php echo $d['dept_name']?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                                <div class="form-group col-sm-4">
                                  <label>&nbsp;</label>
                                  <button class="btn btn-default" data-toggle="modal" data-target="#dept-modal">New</button></div>
                              </div>
                            </div>
                          </div>
                        <?php endif; ?>
                          <h3>Visa/Work Permit Details</h3>
                          <div class="row no-gutter">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Visa Number</label>
                                    <div class="col-sm-8">
                                    <input type="text" name="visano" class="form-control" value="<?php echo isset($data['emp_visano']) ? $data['emp_visano'] : ''?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Work Permit Number</label>
                                    <div class="col-sm-8">
                                    <input type="text" name="permit" class="form-control" value="<?php echo isset($data['emp_permitno']) ? $data['emp_permitno'] : ''?>" />
                                    </div>
                                </div>
                            </div>
                          
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 control-label">Visa Expiry Date:</label>
                                    <div class="col-sm-8">
                                      <input type="date" name="visa_date" class="form-control datepickers" value="<?php echo isset($data['emp_visaexpiry']) ? $data['emp_visaexpiry'] : ''?>" />
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 control-label">Work Permit Expiry Date:</label>
                                    <div class="col-sm-8">
                                      <input type="date" name="permit_date" class="form-control datepickers" value="<?php echo isset($data['emp_permitexpiry']) ? $data['emp_permitexpiry'] : ''?>" />
                                    </div>
                                  </div>
                                
                            </div>
                          </div>
                          <hr>
                        <?php if(empty($id)) : ?>
                          <div class="row no-gutter">
                            <div class="col-md-4">
                                <div class="form-group">
                                <label for="inputPassword3" class="control-label">Check if login required</label>
                                
                                  <input type="checkbox" name="login" class="form-control" checked="" value="1" />
                                
                              </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label for="inputPassword3" class="control-label">User Level:</label>
                                <div class="">
                                  <select name="user_level" class="form-control">
                                    <option value="4">Employee</option>
                                    <option value="3">Line Manager</option>
                                    <option value="0">System Admin</option>
                                    <option value="2">General Manager</option>
                                    <option value="1">Director</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <label for="inputPassword3" class="control-label">Check if induction was conducted</label>
                                
                                  <input type="checkbox" class="form-control" name="inducted" value="1" />
                                
                              </div>
                            </div>
                          </div>
                        <?php endif; ?>
                         <?php if (isset($data['emp_id'])) {
                           echo "<input type='hidden' name='employee_id' value='$id'/>";
                         } ?>
                        <hr />
                          <div class="form-group">
                            <div class="">
                                <div id="my-alert" class="pull-left"></div>
                              <button type="submit" class="btn btn-primary btn-lg pull-right" style="width: 40%;" id="btn-emp">Save Employee</button>
                            </div>
                          </div>
                        </form>  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


<!-- Modal -->
<div class="modal fade" id="dept-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Add/Modify Department</h4>
      </div>
      <form role="form" method="post" class="ajax-form" action="<?php echo site_url('system/act/hr_model/create_dept')?>">
      <div class="modal-body">
        
          <div class="form-group">
            <label for="exampleInputEmail1">Department Name</label>
            <input type="text" name="dept" required="" class="form-control" id="dept_name"/>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Department HOD</label>
            <select name="hod" class="form-control">
                <option value="0">--Select HOD--</option>
                <?php foreach(get_data('tbl_employees', 'WHERE emp_active = 1') as $e) { ?>
                <option value="<?php echo $e['emp_id']?>"><?php echo $e['emp_fullname']?></option>
                <?php } ?>
            </select>
          </div>
          <input type="hidden" name="dept_id" id="dept_id"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" id="btn-main" class="btn btn-theme">Save Department</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php
$this->load->view('layout/footer', ['sel' => 1]);
?>