<?php 
$dets['page'] = 7; $dets['title'] = 'New Purchase Order';
$this->load->view('layout/header', $dets); 
$id = $params;
// $user = get_row_data('tbl_users', 'user_id', $this->session->userdata('user_id'));
// $emp = get_row_data('tbl_employees', 'emp_id', $user['user_empid']);
$url = site_url('system/act/vendors_model/create_po');
if (!empty($id)) {
  $url = site_url('system/act/vendors_model/update_po');
  $data = get_row_data('tbl_po', 'po_id', $id);
}
?>
 <div class="app-content my-3 my-md-5">
          <div class="side-app">
            <div class="page-header">
              <h4 class="page-title"><?php echo $dets['title']; ?></h4>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Welcome</a></li>
                <li class="breadcrumb-item active" aria-current="page">View <?php echo $dets['title']; ?></li>
              </ol>
            </div>
            <div class="row">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <form method="post" action="<?php echo $url?>" class="ajax-form">
                <div class="card-header">
                  <div class="card-title">

                    <label>Select Supplier/Vendor</label>
                    <select class="form-control" id="supplier" name="supplier">
                      <?php foreach (get_data('tbl_suppliers', "WHERE supplier_active = 1 ORDER BY supplier_name ASC", 'supplier_id, supplier_name') as $c) : ?>
                        <?php if(isset($data['po_supplierid'])) : ?>
                          <option value="<?php echo $data['po_supplierid'];?>"><?php echo get_that_data('tbl_suppliers', 'supplier_name', 'supplier_id', $data['po_supplierid'])?></option>
                        <?php endif; ?>
                        <option value="<?php echo $c['supplier_id'];?>"><?php echo $c['supplier_name'];?></option>
                      <?php endforeach; ?>
                    </select>
                    
                  </div>
                  <ul class="card-options panel_toolbox">
                    <li><a href="#" data-toggle="modal" data-target="#supplier-modal" class="btn btn-default" id="add-new"><i class="fa fa-plus"></i> Add New Supplier</a>
                    </li>

                    <li>
                      <a href="#" class="btn btn-default" data-toggle="modal" data-target="#wh_items-modal"><i class="fa fa-plus"></i> Add New Item</a>
                    </li>
                    
                  </ul>
                </div>
                <div class="card-body">
                                    <div class="modal-body">
                                      <div class="form-row">
                                        <div class="col">
                                          <div class="form-group">
                                            <label>Contact Person Name</label>
                                            <input type="text" name="cp_name" class="form-control" value="<?php echo isset($data['po_cpname']) ? $data['po_cpname'] : ''?>" placeholder="Seperate with commas" required>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <label>Contact Phone</label>
                                            <input type="text" name="cp_phone" class="form-control" value="<?php echo isset($data['po_cpphone']) ? $data['po_cpphone'] : ''?>" placeholder="Seperate with commas" required>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <label>Contact Email</label>
                                            <input type="email" name="cp_email" class="form-control" value="<?php echo isset($data['po_cpemail']) ? $data['po_cpemail'] : ''?>" placeholder="Seperate with commas">
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <div class="form-row">
                                        <div class="col">
                                          <div class="form-group">
                                            <label>Quotation/Invoice #</label>
                                            <input type="text" name="ref" class="form-control" value="<?php echo isset($data['po_ref']) ? $data['po_ref'] : ''?>">
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <label>Currency</label>
                                            <select name="currency" class="form-control">
                                              <?php if(isset($data['po_currency'])) : ?>
                                                <option><?php echo $data['po_currency']?></option>
                                              <?php endif; ?>
                                              <option>SSP</option>
                                              <option>USD</option>
                                            </select>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <hr>

                                      <div class="form-group">
                                          <table id="items" class="table table-bordered">
                                            <thead>
                                              <th>Item Description</th>
                                              <th>Qty</th>
                                              <th>Price</th>
                                              <th></th>
                                            </thead>
                                            <tbody>
                                              
                                            </tbody>
                                          </table>
                                          <button class="btn btn-default" type="button" data-toggle="modal" data-target="
                                          #wh_req-modal">Add Items</button>
                                      </div>
                                      <input type="hidden" name="mats" required="" id="mats">
                                      <input type="hidden" value="1" name="type">
                                    </div>

                                     <div class="form-group">
                  <label>Terms & conditions:</label>
                  <textarea class="form-control" name="terms" placeholder="Specific terms" rows="10" cols="4">
1. Please send two copies of invoice.
2. Enter this order in accordance with price, terms, delviery period and specifications.
3. Please notify us immediately if you cannot deliver as specified.
                  </textarea>
                </div>
                                    <div class="modal-footer">
                                      <button type="button" onclick="location.reload()" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                      <button type="submit" class="btn btn-primary">Save PO</button>
                                    </div>
                                    </form> 
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>
<!-- Modal -->
<div class="modal fade" id="wh_req-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add Items</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div>
      <div class="modal-body">
                
        <form id="itm-frm">
        <div class="form-group">
            <label>Select Materials</label>
            <select name="material" class="form-control" id="mat">
                <?php foreach(get_data('tbl_wh_items', "WHERE item_active = 1 ORDER BY item_name ASC") as $w) { ?>
                <option value="<?php echo $w['item_id']?>"><?php echo $w['item_name'] .' ('.$w['item_qty'].')'?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea name="desc" id="desc" class="form-control" placeholder="Any other item Description"></textarea>
        </div>
        <div class="form-group">
            <label>Quantity</label>
            <input type="number" class="form-control" required="" name="qty" id="qty" />
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Unit Price</label>
            <input type="number" name="price" step="any" required="" class="form-control" id="price" />
          </div>


      </div>
        </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn-main" class="btn btn-theme">Add</button>
      </div>
      </div> 
    </div>
  </div>
</div>

<div class="modal fade" id="wh_items-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add/Modify Items</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/stores_model/create_item')?>" id="frm-main">
      <div class="modal-body">
        
          <div class="form-group">
            <label for="exampleInputEmail1">BIN NO</label>
            <input type="text" name="bin_no" required="" class="form-control" id="bin_no" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Item Name</label>
            <input type="text" name="item_name" required="" class="form-control" id="item_name" />
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Item Description</label>
            <input type="text" name="desc" required="" class="form-control" id="desc" />
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Inventory Type</label>
            <select name="type" class="form-control">
              <option>Raw Material</option>
              <option>Office Stationary</option>
              <option>Asset</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Main Category</label>
            <select name="cat" class="form-control">
              <?php foreach(get_data('tbl_wh_cats', 'WHERE cat_active = 1 AND cat_parentid = 0') as $c) : ?>
                <option value="<?php echo $c['cat_id'];?>"><?php echo $c['cat_name'];?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Sub Category</label>
            <select name="subcat" class="form-control">
              <?php foreach(get_data('tbl_wh_cats', 'WHERE cat_active = 1 AND cat_parentid > 0') as $c) : ?>
                <option value="<?php echo $c['cat_id'];?>"><?php echo $c['cat_name'];?></option>
              <?php endforeach; ?>
            </select>
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1">Item Quantity</label>
            <input type="number" name="qty" required="" class="form-control" id="qty" />
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Item Reorderlevel</label>
            <input type="number" name="reorder" required="" class="form-control" id="reorder" />
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Item Size</label>
            <input type="text" name="size" class="form-control" placeholder="e.g. A1, L, XXL etc">
          </div>
          <!--<div class="form-group">
            <label for="exampleInputEmail1">Item Bundlesize</label>
            <input type="number" name="bundle" required="" class="form-control" id="bundle" />
          </div>-->
          <div class="form-group">
            <label for="exampleInputEmail1">Units/Serial Number: <small>e.g. Kgs, Litres, Reams etc</small></label>
            <input type="text" name="units" required="" class="form-control" id="units" />
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Price per unit</label>
            <input type="number" step="any" name="price" class="form-control" id="price" />
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Currency</label>
            <select name="currency" class="form-control" id="currency">
              <option>SSP</option>
              <option>USD</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">GMS</label>
            <input type="number" name="gsm" class="form-control" id="gsm" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Used At</label>
            <input type="text" name="used_at" class="form-control" id="used_at" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Used For</label>
            <input type="text" name="used_for" class="form-control" id="used_for" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Expiry Date</label>
            <input type="text" name="expiry" class="form-control datepickers" id="expiry" value="" />
          </div>
          <!--
          <div class="form-group" style="max-height:320px;border:1px solid #ccc;font:16px/26px Georgia, Garamond, Serif;overflow:auto;">
            <label for="inputEmail3">Job Types:</label>
            <br />
                <?php //foreach(get_data('tbl_jobtypes', 'WHERE jobtype_active = 1') as $e) { ?>
                <input type="checkbox" name="jobtype_name[]" value="<?php //echo $e['jobtype_id']?>"/>  &nbsp;<?php //echo $e['jobtype_name']?> &nbsp;
                <?php //} ?> 
                
          </div> -->
          <div class="form-group">
            <label>Job Types <small>You may select more than one item</small></label>
            <select class="form-control multiple" name="jobtype_name[]" multiple="">
                <?php foreach(get_data('tbl_jobtypes', 'WHERE jobtype_active = 1') as $e) { ?>
                    <option value="<?php echo $e['jobtype_id']?>"> <?php echo $e['jobtype_name']?></option>
                <?php } ?> 
                </select>
          </div>
            <input type="hidden" name="item_id" id="item_id"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" id="btn-main" class="btn btn-theme">Save Item</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#btn-main').click(function() {
    //console.log('clicked');
    var id = $('#mat :selected').val();
    var name = $('#mat :selected').text();
    var qty = $('#qty').val();
    let price = $('#price').val();
    let desc = $('#desc').val();
    
    $("#items").find('tbody').append('<tr><td><div>'+name+'</div><span class="text-muted">'+desc+'</span></td><td>'+qty+'</td><td>'+price+'</td><td><a href="#" class="rm-item" data-id="'+id+'"><i class="fa fa-trash-o"></i></a></td></tr>');
    //$('#itm-frm').clear();
    $('#wh_req-modal').modal('hide');

    let mats = $('#mats').val();
    if (mats.length == 0) $('#mats').val(id+'#'+qty+'#'+price+'#'+desc);
    else $('#mats').val(mats+','+id+'#'+qty+'#'+price+'#'+desc);

  });

  $('.rm-item').click(function() {
    $(this).parent().parent().remove();
  });
</script>
      <!--main content end-->
<?php $this->load->view('layout/footer', array('sel' => 1, 'dtp' => 1)); ?>