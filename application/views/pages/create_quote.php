<?php
$h= ['title' => 'Quotations', 'page' => 1];
$this->load->view('layout/header', $h);
$uid = $this->session->userdata('user_id');

$Jobs = get_data('tbl_quotations', "JOIN tbl_clients ON client_id = quotation_clientid WHERE quotation_active = 1 ", 'quotation_id, quotation_creator, quotation_createdate, quotation_clientid, quotation_status, quotation_currency, quotation_items, client_name, client_marketerid, client_phone');
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $h['title']?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="<?php echo site_url('welcome/view/create_quote');?>" class="btn btn-primary" ><i class="fa fa-plus"></i> Add New</a>
											</li>
										</ul>
									</div>
									<div class="card-body">
									<div class="table-responsive">
										<form method="post" action="<?php echo site_url('system/act/clients_model/create_quote')?>" class="ajax-form">
										<div class="form-row">
											<div class="col">
												<div class="form-group">
													<label>Select Client</label>
								                    <select class="form-control" id="client" name="client">
								                      <?php foreach (get_data('tbl_clients', "WHERE client_active = 1 ORDER BY client_name ASC", 'client_id, client_name') as $c) : ?>
								                        <option value="<?php echo $c['client_id'];?>"><?php echo $c['client_name'];?></option>
								                      <?php endforeach; ?>
								                    </select>
												</div>
											</div>
						               		<div class="col">
						               			<div class="form-group">
							                		<label>RFQ/Ref #</label>
							                		<input type="text" name="ref" class="form-control">
							                	</div>
						               		</div>
						               		<div class="col">
						               			<div class="form-group">
							                		<label>Currency</label>
							                		<select name="currency" class="form-control">
							                			<option>SSP</option>
							                			<option>USD</option>
							                		</select>
							                	</div>
						               		</div>
						               	</div>
						                <div class="table-responsive push">
						                  <table class="table" id="tbl">
						                    <thead>
						                      <tr class=" ">
						                      <th>Description</th>
						                      <th class="text-center" style="width: 20%">Qty</th>
						                      <th class="text-right" style="width: 20%">Unit</th>
						                    </tr>
						                    </thead>
						                   
						                   	<tbody>
						                      <tr>
						                      <td style="width: 10%" class="w-2">
						                        <textarea class="form-control" name="desc[]" required="" placeholder="Description of items" rows="2"></textarea>
						                      </td>
						                      <td class="text-center">
						                      	<input type="number" name="qty[]" class="form-control" required="" placeholder="Quantity" step="any">
						                      </td>
						                      <td class="text-right">
						                      	<input type="number" name="amt[]" class="form-control" required="" placeholder="Price" step="any">
						                      </td>
						                    </tr>
						                    
						                   </tbody>

						                   <tfoot>
						                   	<tr>
						                   		<td colspan="3" class="text-right">
						                   			<button class="btn btn-primary" type="button" id="add-more"><i class="fa fa-plus"></i> Add More</button>
						                   		</td>
						                   	</tr>
						                   </tfoot>
						                   
						                  </table>
						                </div>
						                <div class="form-group">
						                	<label>Terms & conditions:</label>
						                	<textarea class="form-control" name="terms" placeholder="Specific terms" rows="10" cols="4">

						                	</textarea>
						                </div>
						                   			<button class="btn btn-primary btn-lg pull-right" type="submit" id="save"><i class="fa fa-save"></i> Save</button>

						             </form>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



<script type="text/javascript">
	$('#add-more').click(function() {
		$('#tbl').find('tbody').append(
			'<tr>'+
					'<td style="width: 10%" class="w-2">'+
                        '<textarea class="form-control" name="desc[]" required="" placeholder="Description of items" rows="2"></textarea>'+
                      '</td>'+
                      '<td class="text-center">'+
                      	'<input type="number" name="qty[]" class="form-control" required="" placeholder="Quantity" step="any">'+
                      '</td>'+
                      '<td class="text-right">'+
                      	'<input type="number" name="amt[]" class="form-control" required="" placeholder="Price" step="any">'+
                      '</td>'+
                    '</tr>');
	})
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>