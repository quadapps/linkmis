<?php 
$dets['page'] = 7; $dets['title'] = 'Purchase/Service Order';
$this->load->view('layout/header', $dets); 
$id = $params;

$url = site_url('system/act/vendors_model/create_po');
if (!empty($id)) {
  $url = site_url('system/act/vendors_model/update_po');
  $data = get_row_data('tbl_po', 'po_id', $id);
}
?>
     
     <div class="app-content my-3 my-md-5">
          <div class="container">
            
            <div class="row">
              <div class="col-md-12 col-lg-12">
              <div class="card">
              	<form method="post" action="<?php echo $url?>" class="ajax-form">
                <div class="card-header">
                  <div class="card-title">
                    <label>Select supplier</label>
                    <select class="form-control" id="supplier" name="supplier">
                      <?php foreach (get_data('tbl_suppliers', "WHERE supplier_active = 1 ORDER BY supplier_name ASC", 'supplier_id, supplier_name') as $c) : ?>
                        <?php if(isset($data['po_supplierid'])) : ?>
                          <option value="<?php echo $data['po_supplierid'];?>"><?php echo get_that_data('tbl_suppliers', 'supplier_name', 'supplier_id', $data['po_supplierid'])?></option>
                        <?php endif; ?>
                        <option value="<?php echo $c['supplier_id'];?>"><?php echo $c['supplier_name'];?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <ul class="card-options panel_toolbox">
                    <li><a href="#" data-toggle="modal" data-target="#supplier-modal" class="btn btn-secondary" id="add-new"><i class="fa fa-plus"></i> Add New supplier</a>
                    </li>
                    
                  </ul>
                </div>
                <div class="card-body">
                  
                
               	<div class="form-row">
               		<div class="col">
               			<div class="form-group">
	                		<label>Invoice/Quotation #</label>
	                		<input type="text" name="ref" class="form-control" value="<?php echo isset($data['po_ref']) ? $data['po_ref'] : ''?>">
	                	</div>
               		</div>
               		<div class="col">
               			<div class="form-group">
	                		<label>Currency</label>
	                		<select name="currency" class="form-control">
                        <?php if(isset($data['po_currency'])) : ?>
                          <option><?php echo $data['po_currency']?></option>
                        <?php endif; ?>
	                			<option>SSP</option>
	                			<option>USD</option>
	                		</select>
	                	</div>
               		</div>
               	</div>
                <div class="table-responsive push">
                  <table class="table" id="tbl">
                    <thead>
                      <tr class=" ">
                      <th>Description</th>
                      <th class="text-center" style="width: 20%">Qty</th>
                      <th class="text-right" style="width: 20%">Unit</th>
                      <th width="2%"></th>
                    </tr>
                    </thead>
                   
                   	<tbody>
                      <?php if (isset($data['po_items'])) { $items = json_decode($data['po_items'], true); 
                      for ($i=0; $i < count($items['desc']); $i++) { ?> 
                          <tr>
                            <td style="width: 10%" class="w-2">
                              <textarea class="form-control" name="desc[]" required="" placeholder="Description of items" rows="1"><?php echo $items['desc'][$i];?></textarea>
                            </td>
                            <td class="text-center">
                              <input type="number" name="qty[]" class="form-control" required="" placeholder="Quantity" step="any" value="<?php echo $items['qty'][$i];?>">
                            </td>
                            <td class="text-right">
                              <input type="number" name="amt[]" class="form-control" required="" placeholder="Price" step="any" value="<?php echo $items['price'][$i];?>">
                            </td>
                            <td width="2%" class="text-center"><a href="javascript: void(0)" class="remove-item"><i class="text-danger fa fa-trash-o"></i></a></td>
                          </tr>
                      <?php  }  ?>
                      
                      <?php } else { ?>
                      <tr>
                        <td style="width: 10%" class="w-2">
                          <textarea class="form-control" name="desc[]" required="" placeholder="Description of items" rows="1"></textarea>
                        </td>
                        <td class="text-center">
                          <input type="number" name="qty[]" class="form-control" required="" placeholder="Quantity" step="any">
                        </td>
                        <td class="text-right">
                          <input type="number" name="amt[]" class="form-control" required="" placeholder="Price" step="any">
                        </td>
                        <td></td>
                      </tr>
                      <?php } ?>
                    
                   </tbody>

                   <tfoot>
                   	<tr>
                   		<td colspan="3" class="text-right">
                   			<button class="btn btn-primary" type="button" id="add-more"><i class="fa fa-plus"></i> Add More</button>
                   		</td>
                   	</tr>
                   </tfoot>
                   
                  </table>
                </div>

                <input type="hidden" name="po_id" value="<?php echo $id?>">
                <input type="hidden" name="type" value="2">

                <div class="form-group">
                	<label>Terms & conditions:</label>
                	<textarea class="form-control" name="terms" placeholder="Specific terms" rows="10" cols="4">
1. Please send two copies of invoice.
2. Enter this order in accordance with price, terms, delviery period and specifications.
3. Please notify us immediately if you cannot deliver as specified
                	</textarea>
                </div>
                   			<button class="btn btn-primary btn-lg pull-right" type="submit" id="save"><i class="fa fa-save"></i> Save</button> <br>
                </form>
                <p class="text-muted text-center">Thank you very much for doing business with us. We look forward to working with you again!</p>
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>

      <!--main content end-->

<div class="modal fade" id="supplier-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add/Modify Supplier</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" method="post" action="<?php echo
site_url('system/act/vendors_model/create') ?>">
      <div class="modal-body">
        
          <div class="form-group">
            <label for="client_name">Supplier Name</label>
            <input type="text" name="name" class="form-control" id="supplier_name" placeholder="Supplier Name" required="">
          </div>
          <div class="form-row">
            <div class="col">
              <label for="address">Address</label>
              <input type="text" name="address" class="form-control" id="address1" placeholder="Physical Address" />
            </div>
            <div class="col">
              <label for="address">Country</label>
              <select id="country" name="country" class="form-control">
               <option value="South Sudan">South Sudan</option>
              <option value="Uganda">Uganda</option>
               <option value="Kenya">Kenya</option>
               <option value="Afganistan">Afghanistan</option>
               <option value="Albania">Albania</option>
               <option value="Algeria">Algeria</option>
               <option value="American Samoa">American Samoa</option>
               <option value="Andorra">Andorra</option>
               <option value="Angola">Angola</option>
               <option value="Anguilla">Anguilla</option>
               <option value="Antigua & Barbuda">Antigua & Barbuda</option>
               <option value="Argentina">Argentina</option>
               <option value="Armenia">Armenia</option>
               <option value="Aruba">Aruba</option>
               <option value="Australia">Australia</option>
               <option value="Austria">Austria</option>
               <option value="Azerbaijan">Azerbaijan</option>
               <option value="Bahamas">Bahamas</option>
               <option value="Bahrain">Bahrain</option>
               <option value="Bangladesh">Bangladesh</option>
               <option value="Barbados">Barbados</option>
               <option value="Belarus">Belarus</option>
               <option value="Belgium">Belgium</option>
               <option value="Belize">Belize</option>
               <option value="Benin">Benin</option>
               <option value="Bermuda">Bermuda</option>
               <option value="Bhutan">Bhutan</option>
               <option value="Bolivia">Bolivia</option>
               <option value="Bonaire">Bonaire</option>
               <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
               <option value="Botswana">Botswana</option>
               <option value="Brazil">Brazil</option>
               <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
               <option value="Brunei">Brunei</option>
               <option value="Bulgaria">Bulgaria</option>
               <option value="Burkina Faso">Burkina Faso</option>
               <option value="Burundi">Burundi</option>
               <option value="Cambodia">Cambodia</option>
               <option value="Cameroon">Cameroon</option>
               <option value="Canada">Canada</option>
               <option value="Canary Islands">Canary Islands</option>
               <option value="Cape Verde">Cape Verde</option>
               <option value="Cayman Islands">Cayman Islands</option>
               <option value="Central African Republic">Central African Republic</option>
               <option value="Chad">Chad</option>
               <option value="Channel Islands">Channel Islands</option>
               <option value="Chile">Chile</option>
               <option value="China">China</option>
               <option value="Christmas Island">Christmas Island</option>
               <option value="Cocos Island">Cocos Island</option>
               <option value="Colombia">Colombia</option>
               <option value="Comoros">Comoros</option>
               <option value="Congo">Congo</option>
               <option value="Cook Islands">Cook Islands</option>
               <option value="Costa Rica">Costa Rica</option>
               <option value="Cote DIvoire">Cote DIvoire</option>
               <option value="Croatia">Croatia</option>
               <option value="Cuba">Cuba</option>
               <option value="Curaco">Curacao</option>
               <option value="Cyprus">Cyprus</option>
               <option value="Czech Republic">Czech Republic</option>
               <option value="Denmark">Denmark</option>
               <option value="Djibouti">Djibouti</option>
               <option value="Dominica">Dominica</option>
               <option value="Dominican Republic">Dominican Republic</option>
               <option value="East Timor">East Timor</option>
               <option value="Ecuador">Ecuador</option>
               <option value="Egypt">Egypt</option>
               <option value="El Salvador">El Salvador</option>
               <option value="Equatorial Guinea">Equatorial Guinea</option>
               <option value="Eritrea">Eritrea</option>
               <option value="Estonia">Estonia</option>
               <option value="Ethiopia">Ethiopia</option>
               <option value="Falkland Islands">Falkland Islands</option>
               <option value="Faroe Islands">Faroe Islands</option>
               <option value="Fiji">Fiji</option>
               <option value="Finland">Finland</option>
               <option value="France">France</option>
               <option value="French Guiana">French Guiana</option>
               <option value="French Polynesia">French Polynesia</option>
               <option value="French Southern Ter">French Southern Ter</option>
               <option value="Gabon">Gabon</option>
               <option value="Gambia">Gambia</option>
               <option value="Georgia">Georgia</option>
               <option value="Germany">Germany</option>
               <option value="Ghana">Ghana</option>
               <option value="Gibraltar">Gibraltar</option>
               <option value="Great Britain">Great Britain</option>
               <option value="Greece">Greece</option>
               <option value="Greenland">Greenland</option>
               <option value="Grenada">Grenada</option>
               <option value="Guadeloupe">Guadeloupe</option>
               <option value="Guam">Guam</option>
               <option value="Guatemala">Guatemala</option>
               <option value="Guinea">Guinea</option>
               <option value="Guyana">Guyana</option>
               <option value="Haiti">Haiti</option>
               <option value="Hawaii">Hawaii</option>
               <option value="Honduras">Honduras</option>
               <option value="Hong Kong">Hong Kong</option>
               <option value="Hungary">Hungary</option>
               <option value="Iceland">Iceland</option>
               <option value="Indonesia">Indonesia</option>
               <option value="India">India</option>
               <option value="Iran">Iran</option>
               <option value="Iraq">Iraq</option>
               <option value="Ireland">Ireland</option>
               <option value="Isle of Man">Isle of Man</option>
               <option value="Israel">Israel</option>
               <option value="Italy">Italy</option>
               <option value="Jamaica">Jamaica</option>
               <option value="Japan">Japan</option>
               <option value="Jordan">Jordan</option>
               <option value="Kazakhstan">Kazakhstan</option>
               <option value="Kiribati">Kiribati</option>
               <option value="Korea North">Korea North</option>
               <option value="Korea Sout">Korea South</option>
               <option value="Kuwait">Kuwait</option>
               <option value="Kyrgyzstan">Kyrgyzstan</option>
               <option value="Laos">Laos</option>
               <option value="Latvia">Latvia</option>
               <option value="Lebanon">Lebanon</option>
               <option value="Lesotho">Lesotho</option>
               <option value="Liberia">Liberia</option>
               <option value="Libya">Libya</option>
               <option value="Liechtenstein">Liechtenstein</option>
               <option value="Lithuania">Lithuania</option>
               <option value="Luxembourg">Luxembourg</option>
               <option value="Macau">Macau</option>
               <option value="Macedonia">Macedonia</option>
               <option value="Madagascar">Madagascar</option>
               <option value="Malaysia">Malaysia</option>
               <option value="Malawi">Malawi</option>
               <option value="Maldives">Maldives</option>
               <option value="Mali">Mali</option>
               <option value="Malta">Malta</option>
               <option value="Marshall Islands">Marshall Islands</option>
               <option value="Martinique">Martinique</option>
               <option value="Mauritania">Mauritania</option>
               <option value="Mauritius">Mauritius</option>
               <option value="Mayotte">Mayotte</option>
               <option value="Mexico">Mexico</option>
               <option value="Midway Islands">Midway Islands</option>
               <option value="Moldova">Moldova</option>
               <option value="Monaco">Monaco</option>
               <option value="Mongolia">Mongolia</option>
               <option value="Montserrat">Montserrat</option>
               <option value="Morocco">Morocco</option>
               <option value="Mozambique">Mozambique</option>
               <option value="Myanmar">Myanmar</option>
               <option value="Nambia">Nambia</option>
               <option value="Nauru">Nauru</option>
               <option value="Nepal">Nepal</option>
               <option value="Netherland Antilles">Netherland Antilles</option>
               <option value="Netherlands">Netherlands (Holland, Europe)</option>
               <option value="Nevis">Nevis</option>
               <option value="New Caledonia">New Caledonia</option>
               <option value="New Zealand">New Zealand</option>
               <option value="Nicaragua">Nicaragua</option>
               <option value="Niger">Niger</option>
               <option value="Nigeria">Nigeria</option>
               <option value="Niue">Niue</option>
               <option value="Norfolk Island">Norfolk Island</option>
               <option value="Norway">Norway</option>
               <option value="Oman">Oman</option>
               <option value="Pakistan">Pakistan</option>
               <option value="Palau Island">Palau Island</option>
               <option value="Palestine">Palestine</option>
               <option value="Panama">Panama</option>
               <option value="Papua New Guinea">Papua New Guinea</option>
               <option value="Paraguay">Paraguay</option>
               <option value="Peru">Peru</option>
               <option value="Phillipines">Philippines</option>
               <option value="Pitcairn Island">Pitcairn Island</option>
               <option value="Poland">Poland</option>
               <option value="Portugal">Portugal</option>
               <option value="Puerto Rico">Puerto Rico</option>
               <option value="Qatar">Qatar</option>
               <option value="Republic of Montenegro">Republic of Montenegro</option>
               <option value="Republic of Serbia">Republic of Serbia</option>
               <option value="Reunion">Reunion</option>
               <option value="Romania">Romania</option>
               <option value="Russia">Russia</option>
               <option value="Rwanda">Rwanda</option>
               <option value="St Barthelemy">St Barthelemy</option>
               <option value="St Eustatius">St Eustatius</option>
               <option value="St Helena">St Helena</option>
               <option value="St Kitts-Nevis">St Kitts-Nevis</option>
               <option value="St Lucia">St Lucia</option>
               <option value="St Maarten">St Maarten</option>
               <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
               <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
               <option value="Saipan">Saipan</option>
               <option value="Samoa">Samoa</option>
               <option value="Samoa American">Samoa American</option>
               <option value="San Marino">San Marino</option>
               <option value="Sao Tome & Principe">Sao Tome & Principe</option>
               <option value="Saudi Arabia">Saudi Arabia</option>
               <option value="Senegal">Senegal</option>
               <option value="Seychelles">Seychelles</option>
               <option value="Sierra Leone">Sierra Leone</option>
               <option value="Singapore">Singapore</option>
               <option value="Slovakia">Slovakia</option>
               <option value="Slovenia">Slovenia</option>
               <option value="Solomon Islands">Solomon Islands</option>
               <option value="Somalia">Somalia</option>
               <option value="South Africa">South Africa</option>
               <option value="Spain">Spain</option>
               <option value="Sri Lanka">Sri Lanka</option>
               <option value="Sudan">Sudan</option>
               <option value="Suriname">Suriname</option>
               <option value="Swaziland">Swaziland</option>
               <option value="Sweden">Sweden</option>
               <option value="Switzerland">Switzerland</option>
               <option value="Syria">Syria</option>
               <option value="Tahiti">Tahiti</option>
               <option value="Taiwan">Taiwan</option>
               <option value="Tajikistan">Tajikistan</option>
               <option value="Tanzania">Tanzania</option>
               <option value="Thailand">Thailand</option>
               <option value="Togo">Togo</option>
               <option value="Tokelau">Tokelau</option>
               <option value="Tonga">Tonga</option>
               <option value="Trinidad & Tobago">Trinidad & Tobago</option>
               <option value="Tunisia">Tunisia</option>
               <option value="Turkey">Turkey</option>
               <option value="Turkmenistan">Turkmenistan</option>
               <option value="Turks & Caicos Is">Turks & Caicos Is</option>
               <option value="Tuvalu">Tuvalu</option>
               
               <option value="United Kingdom">United Kingdom</option>
               <option value="Ukraine">Ukraine</option>
               <option value="United Arab Erimates">United Arab Emirates</option>
               <option value="United States of America">United States of America</option>
               <option value="Uraguay">Uruguay</option>
               <option value="Uzbekistan">Uzbekistan</option>
               <option value="Vanuatu">Vanuatu</option>
               <option value="Vatican City State">Vatican City State</option>
               <option value="Venezuela">Venezuela</option>
               <option value="Vietnam">Vietnam</option>
               <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
               <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
               <option value="Wake Island">Wake Island</option>
               <option value="Wallis & Futana Is">Wallis & Futana Is</option>
               <option value="Yemen">Yemen</option>
               <option value="Zaire">Zaire</option>
               <option value="Zambia">Zambia</option>
               <option value="Zimbabwe">Zimbabwe</option>
            </select>
            </div>
          </div>
          <div class="row no-gutter">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="client_email">Supplier Email</label>
                    <input type="email" name="email" class="form-control" id="cp_email" placeholder=" Email" />
                  </div>
            </div>
            <div class="col-md-6">
                  <div class="form-group">
                    <label for="client_phone">Supplier Phone</label>
                    <input type="text" name="phone" class="form-control" placeholder=" Phone" id="supplier_phone" required="" />
                  </div>
            </div>
          </div>
          <div class="form-group">
            <label for="contact_person">Contact Person Name</label>
            <input type="text" name="cp" class="form-control" placeholder="Contact Person" id="cp" required="" />
          </div>
          <div class="row no-gutter">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="contact_email">Contact Person Email</label>
                    <input type="email" name="cp_email" class="form-control" id="cp_email" placeholder="Contact Email" />
                  </div>
            </div>
            <div class="col-md-6">
                  <div class="form-group">
                    <label for="contact_phone">Contact Person Phone</label>
                    <input type="text" name="cp_phone" class="form-control" placeholder="Contact Phone" id="cp_phone" required="" />
                  </div>
            </div>
          </div>
          
        <input type="hidden" name="supplier_id" id="supplier_id"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" id="btn-supplier" class="btn btn-primary">Save Supplier</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('#add-more').click(function() {
		$('#tbl').find('tbody').append(
			'<tr>'+
					'<td style="width: 10%" class="w-2">'+
                        '<textarea class="form-control" name="desc[]" placeholder="Description of items" rows="1"></textarea>'+
                      '</td>'+
                      '<td class="text-center">'+
                      	'<input type="number" name="qty[]" class="form-control" placeholder="Quantity" step="any">'+
                      '</td>'+
                      '<td class="text-right">'+
                      	'<input type="number" name="amt[]" class="form-control" placeholder="Price" step="any">'+
                      '</td>'+
                      '<td><a href="javascript: void(0)" class="remove-item"><i class="text-danger fa fa-trash-o"></i></a></td>' +
                    '</tr>');
	});

  $('.remove-item').click(function() {
    $(this).parent('td').parent('tr').remove();
  });
</script>
<?php $this->load->view('layout/footer', array('sel' => 1)); ?>