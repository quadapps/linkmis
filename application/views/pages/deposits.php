<?php 
$dets['page'] = 3; $dets['title'] = 'Banking Details';
$this->load->view('layout/header', $dets); 
$user = get_row_data('tbl_users', 'user_id', $this->session->userdata('user_id'));

$start = date('Y-01-01');
$end = date('Y-m-d');

if (!empty($params)) {
  $arr = explode('_', $params);
  $start = reset($arr); $end = end($arr);
}

$filter = $filter1 = $filter2 = $bank = '';
if(isset($_GET['f']) && !empty($_GET['f'])) {
  $f = $_GET['f'];
  $filter = " AND deposit_bank = '$f' ";
  $filter1 = " AND withdrawal_bank = '$f' ";
  $filter2 = " AND bc_bankid = '$f' ";
  $bank = get_row_data('tbl_banks', 'bank_id', $f);
}

$data = get_data('tbl_deposits', " JOIN tbl_banks ON bank_id = deposit_bank WHERE deposit_active = 1 $filter AND DATE(deposit_date) BETWEEN '$start' AND '$end'", 'tbl_deposits.*, CONCAT(bank_name, bank_acctno, bank_currency) AS bank');
$data1 = get_data('tbl_withdrawals', "JOIN tbl_banks ON bank_id = withdrawal_bank WHERE withdrawal_active = 1 $filter1 AND DATE(withdrawal_date) BETWEEN '$start' AND '$end'", 'tbl_withdrawals.*, CONCAT(bank_name, bank_acctno, bank_currency) AS bank');
$data2 = get_data('tbl_bankingcharges', "JOIN tbl_banks ON bc_bankid = bank_id WHERE bc_active = 1 $filter2 AND DATE(bc_createdate) BETWEEN '$start' AND '$end'", 'tbl_bankingcharges.*, CONCAT(bank_name, bank_acctno, bank_currency) AS bank');
$total = $amt = 0;

// foreach(get_data('tbl_payments') as $p) {
//     $total += $p['payment_amt'];
// }
// foreach(get_data('tbl_deposits') as $d) {
//     $amt += $d['deposit_amt'];
// }
// $q = get_data('tbl_deposits', 'ORDER BY deposit_id DESC LIMIT 1');
// $last = 'Never';
// foreach($q as $v) {
//     $last = $v['deposit_amt'].' on <small class="text-muted">'.$v['deposit_createdate'].'</small>';
// }


// $total1 = $amt1 = 0;
// foreach(get_data('tbl_payments') as $p) {
//     $total1 += $p['payment_amt'];
// }
// foreach(get_data('tbl_withdrawals') as $d) {
//     $amt1 += $d['withdrawal_amt'];
// }

$q = get_data('tbl_deposits', "WHERE deposit_active = 1 $filter ORDER BY deposit_date DESC LIMIT 1", 'deposit_date, deposit_amt, deposit_slipurl, deposit_slipname', true);
$last = empty($q) ? 'Never' : $q['deposit_amt'].' on <small class="text-muted">'.$q['deposit_date'].'</small>';

$q1 = get_data('tbl_withdrawals', "WHERE withdrawal_active = 1 $filter1 ORDER BY withdrawal_date DESC LIMIT 1", 'withdrawal_date, withdrawal_amt', true);

$last1 = empty($q1) ? 'Never' : $q1['withdrawal_amt'].' on <small class="text-muted">'.$q1['withdrawal_date'].'</small>';

$withdrawals = $this->db->query("SELECT SUM(withdrawal_amt) AS total, withdrawal_currency FROM tbl_withdrawals WHERE withdrawal_active = 1 $filter1 AND DATE(withdrawal_date) BETWEEN '$start' AND '$end' GROUP BY withdrawal_currency")->result_array();

$deps = $this->db->query("SELECT SUM(deposit_amt) AS total, deposit_currency FROM tbl_deposits WHERE deposit_active = 1 $filter AND DATE(deposit_date) BETWEEN '$start' AND '$end' GROUP BY deposit_currency")->result_array();

$charges = $this->db->query("SELECT SUM(bc_amt) AS total, bc_currency FROM tbl_bankingcharges WHERE bc_active = 1 $filter2 AND DATE(bc_createdate) BETWEEN '$start' AND '$end' GROUP BY bc_currency")->result_array();
?>
     <div class="app-content my-3 my-md-5" style="min-height: 500px;">
          <div class="container">
            

            <div class="row row-cards row-decks">
              <div class="col-lg-4 col-md-12 m-b-3">
                <div class="widget-info card" style="min-height: 200px">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12 col-12 ">
                        <p>Last Deposit: <?php echo $last?></p>
                                        <?php foreach ($deps as $key => $value) {
                                          echo "<h4 class='font-weight-bold'>Total Deposits: ".$value['deposit_currency'].number_format($value['total'], 2)."</h4>";
                                        } ?>
                        <!-- <a href="#" class="text-white">View Statement</a> -->
                      </div>
                      <!-- <div class="col-sm-6 col-6 m-t-2 text-right"> <i class="fa fa-usd fa-3x text-white"></i> </div> -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-12 m-b-3">
                <div class="widget-info card " style="min-height: 200px">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 ">
                        <p>Last Withdrawal: <?php echo $last1?></p>
                                        <?php foreach ($withdrawals as $key => $value) {
                                          echo "<h4 class='font-weight-bold'>Total Withdrawals: ".$value['withdrawal_currency'].number_format($value['total'], 2)."</h4>";
                                        } ?>
                        <!-- <a href="#" class="text-white">View Statement</a> -->
                      </div>
                      <!-- <div class="col-6 m-t-2 text-right">  <i class="fa fa-globe fa-3x text-white"></i>  </div> -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-12 m-b-3">
                <div class="widget-info card" style="min-height: 200px">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12">
                        <p>Banking Charges</p>
                                        <?php foreach ($charges as $key => $value) {
                                          echo "<h4 class='font-weight-bold'>Total : ".$value['bc_currency'].number_format($value['total'], 2)."</h4>";
                                        } ?>
                                    </div>
                        <!-- <a href="#" class="text-white">View Statement</a> -->
                      </div>
                      <!-- <div class="col-6 m-t-2 text-right"><i class="fa fa-pie-chart fa-3x text-white" ></i></div> -->
                    </div>
                  </div>
                </div>
              </div>

            <div class="row">
              <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title'].' :: '.$bank['bank_name'].'-'.$bank['bank_currency']; ?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">

                    
                    <li class="dropdown">
                    <a aria-expanded="false" aria-haspopup="true" href="javascript: void()" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-plus"></i> Add New</a>
                              <div class="dropdown-menu">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal1">Deposit</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal">Withdrawal</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal2">Banking Charges</a>
                              </div>
                    </li>

                    <li>
                      <a class="btn btn-secondary" id="reportrange" href="javascript: void()" data-url="<?php echo site_url('welcome/view/deposits')?>"><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end)) ;?></a>
                    </li>
                    
                  </ul>
                </div>
                <div class="card-body">
                  
                    <div class="panel panel-primary">
                      <div class=" tab-menu-heading">
                        <div class="tabs-menu1 ">
                          <!-- Tabs -->
                          <ul class="nav panel-tabs">
                            <li class=""><a href="#tab5" class="active" data-toggle="tab">Deposits</a></li>
                            <li><a href="#tab6" data-toggle="tab">Withdrawals</a></li>
                            <li><a href="#tab7" data-toggle="tab">Bank Charges</a></li>
                        </div>
                      </div>
                      <div class="panel-body tabs-menu-body">
                        <div class="tab-content">
                          <div class="tab-pane active " id="tab5">
                            <div class="table-responsive">
                    <table id="d-table" class="table table-hover table-bordered data-table-buttons">
                            <thead>
                                <th>Entry Date</th>
                                <th>TXN Date</th>
                                <th>Currency</th>
                                <th>Amount</th>
                                <th>Mode</th>
                                <th>Bank</th>
                                <th>Entered By</th>
                                <th>Remarks</th>
                                <th>Slip</th>
                                <th></th>
                            </thead>
                            <tbody>
                            <?php foreach($data as $j) { ?>
                                <tr>
                                    <td><?php echo $j['deposit_createdate']?></td>
                                    <td><?php echo $j['deposit_date']?></td>
                                    <td><?php echo $j['deposit_currency'];?></td>
                                    <td><?php echo $j['deposit_amt']?></td>
                                    <td><?php echo $j['deposit_mode']?></td>
                                    <td><?php echo $j['bank']?></td>
                                    <td><?php echo get_fullname($j['deposit_creator'])?></td>
                                    <td><?php echo $j['deposit_remarks']?></td>
                                    <td><a href="<?php echo $j['deposit_slipurl']?>" title="Download"><?php echo $j['deposit_slipname']?></a></td>
                                    <td><button class="btn btn-default text-danger" onclick="remove_things('tbl_deposits', <?php echo $j['deposit_id'];?>, true)"><i class="fa fa-trash-o"></i></button> </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                           </table>
                  </div>
                          </div>
                          <div class="tab-pane " id="tab6">
                            <div class="clearfix"></div>
                            <table id="d-table" class="table table-hover table-bordered data-table-buttons">
                            <thead>
                                <th>Date</th>
                                <th>Currency</th>
                                <th>Amount</th>
                                <th>Mode</th>
                                <th>Bank</th>
                                <th>Entered By</th>
                                <th>Remarks</th>
                                <th>Slip</th>
                                <th></th>
                            </thead>
                            <tbody>
                            <?php foreach($data1 as $j) { ?>
                                <tr>
                                    <td><?php echo $j['withdrawal_date']?></td>
                                    <td><?php echo $j['withdrawal_currency'];?></td>
                                    <td><?php echo $j['withdrawal_amt']?></td>
                                    <td><?php echo $j['withdrawal_mode']?></td>
                                    <td><?php echo $j['bank']?></td>
                                    <td><?php echo get_fullname($j['withdrawal_creator'])?></td>
                                    <td><?php echo $j['withdrawal_remarks']?></td>
                                    <td><a href="<?php echo $j['withdrawal_slipurl']?>" title="Download"><?php echo $j['withdrawal_slipname']?></a></td>
                                    <td><button class="btn btn-default text-danger" onclick="remove_things('tbl_withdrawals', <?php echo $j['withdrawal_id'];?>, true)"><i class="fa fa-trash-o"></i></button> </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                           </table>
                          </div>

                          <div class="tab-pane " id="tab7">
                            <div class="clearfix"></div>
                            <table id="d-table" class="table table-hover table-bordered data-table-buttons">
                            <thead>
                                <th>Date</th>
                                <th>Currency</th>
                                <th>Amount</th>
                                <th>Bank</th>
                                <th>Entered By</th>
                                <th></th>
                            </thead>
                            <tbody>
                            <?php foreach($data2 as $j) { ?>
                                <tr>
                                    <td><?php echo $j['bc_createdate']?></td>
                                    <td><?php echo $j['bc_currency'];?></td>
                                    <td><?php echo $j['bc_amt']?></td>
                                    <td><?php echo $j['bank']?></td>
                                    <td><?php echo get_fullname($j['bc_creator'])?></td>
                                    <td><button class="btn btn-default text-danger" onclick="remove_things('tbl_bankingcharges', <?php echo $j['bc_id'];?>, true)"><i class="fa fa-trash-o"></i></button> </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                           </table>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
              
            </div>
          </div>
<?php $banks = get_data('tbl_banks', "WHERE bank_active = 1"); ?>
<!-- Modal -->
<div class="modal fade" id="enq-modal1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Make Deposit</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" enctype="multipart/form-data" method="post" action="<?php echo site_url('system/act/clients_model/make_deposit')?>">
      <div class="modal-body">
          
          <div class="form-group">
            <label>Amount Deposited</label>
            <input type="number" class="form-control" name="amt" required=""/>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Currency:</label>
            <select name="currency" class="form-control">
                <option>SSP</option>
                <option>USD</option>
            </select>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Deposit mode:</label>
            <select name="mode" class="form-control">
                <option>Cash</option>
                <option>Cheque</option>
                <option>BankTransfer</option>
            </select>
          </div>
          
          <div class="form-group">
            <label>Date Deposited</label>
            <input type="text" class="form-control datepickers" name="date" required=""/>
          </div>

          <div class="form-group">
            <label>Bank Deposited</label>
            <select name="bank" class="form-control">
              <?php foreach($banks as $b) { ?>
                <option><?php echo $b['bank_name'].' - '.$b['bank_acctno'].' - '.$b['bank_currency'];?></option>
              <?php } ?>
            </select>
          </div>

          <!-- <div class="form-group">
            <label>Account Deposited</label>
            <input type="text" class="form-control " name="acct" required=""/>
          </div> -->
          
          <div class="form-group">
            <label for="inputPassword3">Deposit Slip:</label>
            <input type="file" name="userfile" />
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Remarks:</label>
            <textarea class="form-control" name="remarks" placeholder=""></textarea>
          </div>
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Save Deposit</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

<div class="modal fade" id="enq-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Make Withdrawal</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" enctype="multipart/form-data" method="post" action="<?php echo site_url('system/act/clients_model/make_withdrawal')?>">
      <div class="modal-body">
          
          <div class="form-group">
            <label>Amount Withdrawn</label>
            <input type="number" class="form-control" name="amt" required=""/>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Currency:</label>
            <select name="currency" class="form-control">
                <option>SSP</option>
                <option>USD</option>
            </select>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Mode:</label>
            <select name="mode" class="form-control">
                <option>Cash</option>
                <option>Cheque</option>
                <option>BankTransfer</option>
            </select>
          </div>
          
          <div class="form-group">
            <label>Date Withdrawn</label>
            <input type="text" class="form-control datepickers" name="date" required=""/>
          </div>

          <div class="form-group">
            <label>Bank Withdrawn</label>
            <select name="bank" class="form-control">
              <?php foreach($banks as $b) { ?>
                <option><?php echo $b['bank_name'].' - '.$b['bank_acctno'].' - '.$b['bank_currency'];?></option>
              <?php } ?>
            </select>
          </div>

          <!-- <div class="form-group">
            <label>Account Withdrawn</label>
            <input type="text" class="form-control " name="acct" required=""/>
          </div> -->
          
          <div class="form-group">
            <label for="inputPassword3">Bank Slip:</label>
            <input type="file" name="userfile" />
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Remarks:</label>
            <textarea class="form-control" name="remarks" placeholder=""></textarea>
          </div>
          </div>    
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Save Withdrawal</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

  <div class="modal fade" id="enq-modal2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">New Bank Charges</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" method="post" action="<?php echo site_url('system/act/clients_model/create_bankcharges')?>">
      <div class="modal-body">
          
          <div class="form-group">
            <label>Amount Charged</label>
            <input type="number" class="form-control" name="amt" required=""/>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Currency:</label>
            <select name="currency" class="form-control">
                <option>SSP</option>
                <option>USD</option>
            </select>
          </div>
          
          <div class="form-group">
            <label>Date </label>
            <input type="text" class="form-control datepickers" name="date" required=""/>
          </div>

          <div class="form-group">
            <label>Bank Account</label>
            <select name="bank" class="form-control">
              <?php foreach($banks as $b) { ?>
                <option><?php echo $b['bank_name'].' - '.$b['bank_acctno'].' - '.$b['bank_currency'];?></option>
              <?php } ?>
            </select>
          </div>

          <!-- <div class="form-group">
            <label>Account Deposited</label>
            <input type="text" class="form-control " name="acct" required=""/>
          </div> -->
          
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Save </button>
          </div>
      </form> 
    </div>
  </div>
  </div>

<?php $this->load->view('layout/footer', array('dtt' => 1, 'sel' => 1, 'dtp' => 1)); ?>