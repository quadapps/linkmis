<?php
$dets = ['title' => 'Departments', 'page' => 5];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');
$user = get_data('tbl_users', "JOIN tbl_employees ON emp_id = user_empid WHERE user_id = $uid", 'user_id, user_levelid, user_empid, emp_deptid', true);
// $emp = get_data('tbl_employees', "WHERE emp_id = ".$user['user_empid'], 'emp_id, emp_designation', true);
// $des = strtolower($emp['emp_designation']);
// if (strpos($des, 'project') !== false || $des == 'production manager') {
// 	$this->load->view('dashboards/projects');
// } else {
// 	$this->load->view('dashboards/main');
// }
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#deptModal" ><i class="fa fa-plus"></i> Add New</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter">
											<thead>
												<tr>
													<th>Name</th>
													<th>Hod</th>
													<th>No of Employees</th>
													<th>Created By</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach(get_data('tbl_departments', "LEFT JOIN tbl_users ON user_id = dept_creator WHERE dept_active =1 ", 'tbl_departments.*, user_fullname') as $d) : ?>
													<tr>
														<td><?php echo $d['dept_name'];?></td>
														<td><?php echo get_that_data('tbl_employees', 'emp_fullname', 'emp_id', $d['dept_empid']);?></td>
														<td><?php echo count(get_data('tbl_employees', "WHERE emp_active = 1 AND emp_deptid = ".$d['dept_id'], 'emp_id'));?></td>
														<td><?php echo $d['user_fullname'];?></td>
														<td class="btn-group">
															<?php if($user['emp_deptid'] == 2 || $user['user_levelid'] < 3) : ?>
															<button class="btn btn-secondary btn-sm edit-dept" data-id="<?php echo $d['dept_id'];?>"><i class="fa fa-pencil"></i></button>
															<button class="btn btn-danger btn-sm delete-things" data-tbl="departments" data-id="<?php echo $d['dept_id'];?>"><i class="fa fa-trash-o"></i></button>
															<?php endif; ?>
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add/Modify Department</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/hr_model/create_dept');?>" class="ajax-form" id="dept-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Deparment Name</label>
	        	<input type="text" name="dept" class="form-control" required="" id="dept-name">
	        </div>
	        <div class="form-group">
	        	<label>HOD</label>
	        	<select name="hod" class="form-control">
	        		<?php foreach(get_Data('tbl_employees', "WHERE emp_active = 1") as $e) { ?>
	        			<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php } ?>
	        	</select>
	        </div>

	        <input type="hidden" name="dept_id" id="dept-id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('.edit-dept').click(function() {
		let id = $(this).data('id');
		let name = $(this).closest('tr').find("td:nth-child(1)").text();
		$('#dept-form').attr('action', '<?php echo site_url('system/act/hr_model/update_dept');?>');
		$('#dept-id').val(id);
		$('#dept-name').val(name);
		$('#deptModal').modal('show');
	})
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>