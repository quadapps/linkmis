<?php 
$dets['page'] = 5;  $dets['title'] = "Employee Details";
$this->load->view('layout/header', $dets);
$id = $params;
$arr =  explode('-', $id);
$emp_id = end($arr); 
$_eid = $this->session->flashdata('emp_id');
$emp_id = ($emp_id != 'undefined') ? $emp_id : $_eid;
$mode = $arr[0];
$data = get_row_data('tbl_employees', 'emp_id', $emp_id);

if (is_int(intval($emp_id)) && intval($emp_id) > 0) {
@$documents = get_data('tbl_employeedocs', "where empid = $emp_id");
}
// filter menu display (filter, export)
$user = get_row_data('tbl_users','user_id', $this->session->userdata('user_id'));
$emp = get_row_data('tbl_employees', 'emp_id', $user['user_empid']);
if (strtoupper($emp['emp_designation']) == 'HR OFFICER' || strtoupper($emp['emp_designation']) == 'IT & SYSTEMS ADMIN' ||
        strtoupper($emp['emp_designation']) == 'MANAGING DIRECTOR' || strtoupper($emp['emp_designation']) == 'GENERAL MANAGER') {
  $_display = true;
} else {
  $_display = false;
}
?>
      
     <div class=" my-3 my-md-5">
          <div class="container">
            <!-- <div class="page-header">
              <h4 class="page-title"><?php echo $dets['title']; ?></h4>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Welcome</a></li>
                <li class="breadcrumb-item active" aria-current="page">View <?php echo $dets['title']; ?></li>
              </ol>
            </div> -->
            <div class="row  row-cards row-deck">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">
                    <!--<li><a href="#" data-toggle="modal" data-target="#dept-modal" class="btn btn-default" id="add-new"><i class="fa fa-plus"></i> Add New</a>
                    </li>-->
                    
                  </ul>
                </div>
                <div class="card-body">
                    <div role="tabpanel"> 
                     <!-- Nav tabs -->
                          <div class=" tab-menu-heading">
                        <div class="tabs-menu1 ">
                          <!-- Tabs -->
                          <ul class="nav panel-tabs">
                            <li class=""><a href="#home" class="active" data-toggle="tab">Personal Info</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Work Experience</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Educational Details</a></li>
                            <li role="presentation"><a href="#bank" aria-controls="bank" role="tab" data-toggle="tab">Bank Details</a></li>
                            <?php if ($_display) { ?>
                              <li role="presentation"><a href="#docs" aria-controls="bank" role="tab" data-toggle="tab">Documents</a></li>
                            <?php } ?>
                          </ul>
                        </div>
                      </div>
                          
                          <!-- Tab panes -->
                          <div class="tab-content">
                             <div role="tabpanel" class="tab-pane active" id="home">
                                <?php if(is_null($data['emp_photourl'])) { ?>
                                <form id="frm-upload" action="<?php echo site_url('system/act/hr_model/upload_empphoto')?>" method="post"  enctype="multipart/form-data">
                                        <label>Upload Recent Photograph</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="file" name="userfile" required=""/>
                                    
                                        </div>
                                        <input type="hidden" name="emp_id" value="<?php echo $emp_id?>"/>
                                        <div class="col-sm-6">
                                            <button class="btn btn-default btn-sm" type="submit" id="btn-upload">Upload</button>
                                        </div>
                                    </div>
                                </form>
                                <?php } else { ?>
                                <div id="img">
                                <img  class="img-responsive" src="<?php echo $data['emp_photourl']?>"/>
                                </div>
                                <div id="change-pic" style="visibility: hidden;">
                                <form id="frm-upload" action="<?php echo site_url('system/act/hr_model/upload_empphoto')?>" method="post"  enctype="multipart/form-data" class="ajax-form">
                                        <label>Upload Recent Photograph</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="file" name="userfile" required=""/>
                                    
                                        </div>
                                        <input type="hidden" name="emp_id" value="<?php echo $emp_id?>"/>
                                        <div class="col-sm-6">
                                            <button class="btn btn-default btn-sm" type="submit" id="btn-upload">Upload</button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                                <button class="btn btn-default" onclick="change_pic()" id="btn-pic">Change</button>
                                <?php } ?> <hr />
                                 <form class="form-horizontal ajax-form-2" role="form" method="post" action="<?php echo site_url('system/act/hr_model/update_employees')?>" id="frm-emp">
                                 <h3>Personal Details</h3>
                                  <div class="row no-gutter">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Fullnames:</label>
                                        <div class="col-sm-9">
                                          <input type="text" name="full_name" class="form-control" required="" value="<?php echo $data['emp_fullname']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Permanent Residence:</label>
                                        <div class="col-sm-9">
                                          <textarea class="form-control" name="address" required="" rows="5" <?php echo $mode == 'view' ? 'disabled' : ''?>><?php echo $data['emp_residence']?></textarea>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Home <i class="fa fa-phone-square"></i>:</label>
                                        <div class="col-sm-9">
                                          <input type="text" name="home_phone" class="form-control" required="" value="<?php echo $data['emp_homephone']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Work <i class="fa fa-phone-square"></i>:</label>
                                        <div class="col-sm-9">
                                          <input type="text" name="work_phone" class="form-control" required="" value="<?php echo $data['emp_workphone']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Cell <i class="fa fa-mobile"></i>:</label>
                                        <div class="col-sm-9">
                                          <input type="text" name="cell_phone" class="form-control" required="" value="<?php echo $data['emp_cellphone']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Email <i class="fa fa-envelope"></i>:</label>
                                        <div class="col-sm-9">
                                          <input type="text" name="email" class="form-control" required="" value="<?php echo $data['emp_email']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row no-gutter">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Date of Birth:</label>
                                        <div class="col-sm-8">
                                          <input type="text" name="birth_date" class="form-control datepicker" value="<?php echo $data['emp_birthdate']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Citizenship:</label>
                                        <div class="col-sm-8">
                                          <input type="text" name="nationality" class="form-control" required="" value="<?php echo $data['emp_nationality']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row no-gutter">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Passport/ID NO:</label>
                                        <div class="col-sm-8">
                                          <input type="text" name="ppid" class="form-control" value="<?php echo $data['emp_ppno']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Place of Issue:</label>
                                        <div class="col-sm-8">
                                          <input type="text" name="place" class="form-control" value="<?php echo $data['emp_issueplace'] ?>" required=""/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row no-gutter">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Gender:</label>
                                        <div class="col-sm-8">
                                          <select name="gender" class="form-control" <?php echo $mode == 'view' ? 'disabled' : ''?> >
                                            <option <?php echo (strtolower($data['emp_gender']) == 'male') ? 'selected' : '' ?> >Male</option>
                                            <option <?php echo (strtolower($data['emp_gender']) == 'female') ? 'selected' : '' ?> >Female</option>
                                            <option>Other</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Marital Status:</label>
                                        <div class="col-sm-8">
                                          <input type="text" name="marital_status" class="form-control" value="<?php echo $data['emp_maritalstatus']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <h3>Employment Details</h3>
                                  <div class="row no-gutter">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Duties/Responsibilities</label>
                                            <div class="col-sm-9">
                                            <textarea class="form-control" name="duties" required="" rows="6" <?php echo $mode == 'view' ? 'disabled' : ''?>><?php echo $data['emp_duties']?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                  
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Employment Date:</label>
                                        <div class="col-sm-8">
                                          <input type="text" name="emp_date" class="form-control datepicker" value="<?php echo $data['emp_empdate']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div>
                                    
                                      <!--  <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Basic Salary:</label>
                                        <div class="col-sm-8">
                                          <input type="number" class="form-control" name="salary" value="<?php //echo $data['emp_salary']?>" <?php //echo $mode == 'view' ? 'disabled' : ''?>/>
                                        </div>
                                      </div> -->
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Designation:</label>
                                        <div class="col-sm-8">
                                          <?php if($mode == 'view') { ?>
                                          <input type="text" name="designation" class="form-control" value="<?php echo $data['emp_designation']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                          <?php } else { ?>
                                          <select name="designation" class="form-control">
                                            <option value="0">--Select Designation--</option>
                                    <option>MANAGING DIRECTOR</option>

                                    <option>SECRETARY</option>
                                    <option>IT CHIEF SECURITY OFFICER</option>
                                    <option>INTERNAL AUDITOR</option>
                                    <option>LEGAL ADVISOR</option>

                                    
                                    <option>FINANCE, ADMIN &amp; HR MANAGER</option>
                                    <option>SALES, MARKETING &amp; BIZ DEV MANAGER</option>
                                    <option>PRODUCTION MANAGER</option>
                                    <option>CREATIVE QC/QA MANAGER</option>
                                    <option>PROCUREMENT &amp; LOGISTICS MANAGER</option>
                                    <option>SUB EDITOR-CITY REVIEW NEWSLETTER</option>
                                    
                                    <option>IT MANAGER</option>
                                     <option>IT &amp; SYSTEMS ADMIN</option>
                                    <option>ASST IT &amp; SYSTEMS ADMIN</option>
                                   

                                    <option>ADMIN OFFICER</option>
                                    <option>HR OFFICER</option>

                                    
                                    <option>SENIOR GRAPHIC DESIGNER</option>
                                    <option>GRAPHIC DESIGNER</option>
                                    <option>JUNIOR GRAPHIC DESIGNER</option>
                                    <option>WEB DESIGNER</option>
                                    <option>3D &amp; ANIMATION DESIGNER</option>

                                    <option>SALES &amp; MARKETING ASST. MANAGER</option>
                                    <option>SALES &amp; MARKETING OFFICER</option>
                                    <option>SALES TRAINEE</option>
                                    <option>PROCUREMENT OFFICER</option>
                                    <option>LOGISTIC OFFICER</option>
                                    <option>ACCOUNTANT</option>
                                    <option>SENIOR ACCOUNTANT</option>
                                    <option>CASHIER</option>
                                    <option>DEBTS COLLECTOR</option>

                                    <option>REPORTER</option>
                                     <option>TRANSLATOR</option>
                                      <option>ASST SUB EDITOR</option>
                                    
                                    <option>CUSTOMER CARE EXECUTIVE</option>
                                    <option>MACHINE OPERATOR</option>
                                    <option>SCREEN PRINTING OPERATOR</option>
                                    <option>STORE KEEPER</option>
                                    <option>DRIVER</option>
                                    <option>BINDING &amp; FINISHING SUPERVISOR</option>
                                  
                                    <option>SECURITY PERSONNEL</option>                                 
                                    <option>OFFICE CLEANER</option>
                                    <option>CHEF</option>   
                                    <option>GENERAL STAFF</option>
                                          </select>
                                          <?php } ?>
                                        </div>
                                      </div>
                                    
                                        <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Department:</label>
                                        <div class="col-sm-8">
                                          <select class="form-control" name="dept" <?php echo $mode == 'view' ? 'disabled' : ''?>>
                                            <option value="<?php echo $data['emp_deptid']?>"><?php echo get_that_data('tbl_departments', 'dept_name', 'dept_id', $data['emp_deptid'])?></option>
                                            <?php foreach(get_data('tbl_departments', 'WHERE dept_active = 1') as $d) { ?>
                                            <option value="<?php echo $d['dept_id']?>"><?php echo $d['dept_name']?></option>
                                            <?php } ?>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <h3>Visa/Work Permit Details</h3>
                          <div class="row no-gutter">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Visa Number</label>
                                    <div class="col-sm-8">
                                    <input type="text" name="visano" class="form-control" value="<?php echo $data['emp_visano']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Work Permit Number</label>
                                    <div class="col-sm-8">
                                    <input type="text" name="permit" class="form-control" value="<?php echo $data['emp_permitno']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 control-label">Visa Expiry Date:</label>
                                    <div class="col-sm-8">
                                      <div class="col-lg-10">
                                        <input type="text" name="visa_date" class="form-control datepicker" value="<?php echo $data['emp_visaexpiry']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                      </div>
                                      <div class="col-lg-2">
                                        <a class="btn btn-sm btn-theme" title="Renew" onclick="renew_permit('visa',<?php echo $emp_id?>)"><i class="fa fa-refresh"></i></a>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 control-label">Work Permit Expiry Date:</label>
                                    <div class="col-sm-8">
                                      <div class="col-lg-10">
                                        <input type="text" name="permit_date" class="form-control datepicker" value="<?php echo $data['emp_permitexpiry']?>" <?php echo $mode == 'view' ? 'disabled' : ''?>/>
                                      </div>
                                      <div class="col-lg-2">
                                        <a class="btn btn-sm btn-theme" title="Renew" onclick="renew_permit('work',<?php echo $emp_id?>)"><i class="fa fa-refresh"></i></a>
                                      </div>
                                    </div>
                                  </div>
                                
                            </div>
                          </div>
                                 
                                 <input type="hidden" name="employee_id" value="<?php echo $emp_id?>"/>
                                <hr />
                                  <div class="form-group">
                                    <div class="">
                                        <div id="my-alert" class="pull-left"></div>
                                      <?php if($_display) { ?>
                                      <a href="<?php echo site_url('welcome/page/create_emp/'.$emp_id)?>" class="btn btn-danger btn-lg pull-right" style="width: 40%;" id="btn-edit">Edit Details</a>
                                      <?php } ?>
                                    </div>
                                  </div>
                                </form> 
                              </div>
                              
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#hist-modal">Add</button></h3> <hr />
                                        <?php foreach(get_data('tbl_emphistory', "WHERE history_empid = $emp_id") as $eh) { ?>
                                        <div class="row mt">
                                        <div class="col-sm-12">
                                        <div class="white-panel pn">
                                            <div class="panel-heading">
                                                <h3>Employer: <?php echo $eh['history_employer']?> <p class="pull-right">Duration: <?php echo $eh['history_from'].' - '.$eh['history_to']?></p></h3>
                                            </div>
                                            <div class="panel-body">
                                                <p><strong>Position: </strong> <?php echo $eh['history_position']?></p>
                                                <p><strong>Duties: </strong> <?php echo $eh['history_duties']?></p>
                                                <p><strong>Reason for leaving: </strong> <?php echo $eh['history_reason']?></p>
                                                <br />
                                                <div class="btn-group pull-right">
                                                    <button class="btn btn-theme03" onclick="edit_exp(<?php echo $eh['history_id']?>)">Edit</button>
                                                    <button class="btn btn-danger" onclick="remove_item('tbl_emphistory',<?php echo $eh['history_id']?>)">Remove</button>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="messages">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#edu-modal">Add</button></h3> <hr />
                                        <?php foreach(get_data('tbl_educationalbackground', "WHERE edu_empid = $emp_id") as $eh) { ?>
                                        <div class="row mt">
                                        <div class="col-sm-12">
                                        <div class="white-panel pn">
                                            <div class="panel-heading">
                                                <h3>Institution: <?php echo $eh['edu_institution']?> </h3>
                                            </div>
                                            <div class="panel-body">
                                                <p><strong>Award: </strong> <?php echo $eh['edu_degree']?></p>
                                                <p><strong>Majors: </strong> <?php echo $eh['edu_majors']?></p>
                                                <p><strong>Duration: </strong> <?php echo $eh['edu_from'].' - '.$eh['edu_to']?></p>
                                                <br />
                                                <div class="btn-group pull-right">
                                                    <button class="btn btn-theme03" onclick="edit_edu(<?php echo $eh['edu_id']?>)">Edit</button>
                                                    <button class="btn btn-danger" onclick="remove_item('tbl_educationalbackground',<?php echo $eh['edu_id']?>)">Remove</button>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="bank">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#bank-modal">Add</button></h3> <hr />
                                        <?php foreach(get_data('tbl_empbanks', "WHERE bank_empid = $emp_id") as $b) { ?>
                                        <div class="row mt">
                                        <div class="col-sm-12">
                                        <div class="white-panel pn">
                                            <div class="panel-heading">
                                                <h3>Bank: <?php echo $b['bank_name']?> <p class="pull-right">Branch: <?php echo $b['bank_branch']?></p></h3>
                                            </div>
                                            <div class="panel-body">
                                                <p><strong>Account Name: </strong><?php echo $b['bank_acctname']?></p>
                                                <p><strong>Account Number: </strong><?php echo $b['bank_acctno']?></p>
                                                <br />
                                                <div class="btn-group pull-right">
                                                    <button class="btn btn-theme03" onclick="edit_bank(<?php echo $b['bank_id']?>)">Edit</button>
                                                    <button class="btn btn-danger" onclick="remove_item('tbl_empbanks',<?php echo $b['bank_id']?>)">Remove</button>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="docs">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#document-modal">Add</button>
                                        </h3>
                                        <hr>
                                        <table class="table table-striped datatable">
                                            <thead>
                                                <tr>
                                                    <th>Document Title</th>
                                                    <th width="20%">Date Uploaded</th>
                                                    <th width="10%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($documents as $document) { ?>
                                                <tr>
                                                    <td><?php echo $document['title'] ?></td>
                                                    <td><?php echo date('jS M, Y', strtotime($document['date'])) ?></td>
                                                    <td>
                                                        <a href="<?php echo base_url('uploads/'.trim($document['filename']))  ?>" target="_blank" class="btn btn-xs btn-primary" title="Download Document"><i class="fa fa-download"></i></a>
                                                        <a href="#" class="btn btn-xs btn-danger delete_doc" title="Delete Document" data-docid="<?php echo $document['id'] ?>"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table> 
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div> 
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Change Password ?</h4>
		                      </div>
                              <form method="post" action="<?php echo site_url('system/change_pass')?>" id="frm-main" class="ajax-form">
		                      <div class="modal-body">
		                          <p>New Password</p>
		                          <input type="password" name="pass" required=""autocomplete="off" class="form-control placeholder-no-fix">
		
                                    <p>Confirm Password</p>
		                          <input type="password" name="pass1" required="" autocomplete="off" class="form-control placeholder-no-fix">
		                      </div>
                              <input type="hidden" name="emp" value="<?php echo $emp_id?>"/>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		                          <button class="btn btn-theme" type="submit" id="btn-main">Submit</button>
		                      </div>
                              </form>
		                  </div>
		              </div>
		          </div>
                  
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bank-modal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Add/Modify Bank Details</h4>
		                      </div>
                              <form method="post" action="<?php echo site_url('system/act/hr_model/add_bankdetails')?>" id="frm-main1" class="ajax-form">
		                      <div class="modal-body">
		                          <div class="form-group">
                                    <label>Bank Name</label>
                                    <input type="text" name="bank_name" class="form-control" required="" id="bank_name"/>
                                  </div>
                                  <div class="form-group">
                                    <label>Bank Branch</label>
                                    <input type="text" name="branch" class="form-control" required="" id="branch"/>
                                  </div>
                                  <div class="form-group">
                                    <label>Account Name</label>
                                    <input type="text" name="acct_name" class="form-control" required="" id="acct_name"/>
                                  </div>
                                  <div class="form-group">
                                    <label>Account Number</label>
                                    <input type="text" name="acct_no" class="form-control" required="" id="acct_no"/>
                                  </div>
		                      </div>
                              <input type="hidden" name="emp" value="<?php echo $emp_id?>"/>
                              <input type="hidden" name="bank_id" id="bank_id"/>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		                          <button class="btn btn-theme" type="submit" id="btn-main1">Save changes</button>
		                      </div>
                              </form>
		                  </div>
		              </div>
		          </div>
                  
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="hist-modal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Add/Modify Employment History</h4>
		                      </div>
                              <form method="post" action="<?php echo site_url('system/act/hr_model/create_emphistory')?>" id="frm-main2" class="ajax-form">
		                      <div class="modal-body">
		                          <div class="form-group">
                                    <label>Employer Name</label>
                                    <input type="text" name="employer" class="form-control" required="" id="employer"/>
                                  </div>
                                  <div class="form-group">
                                    <label>Position</label>
                                    <input type="text" name="position" class="form-control" required="" id="position"/>
                                  </div>
                                  <div class="form-group">
                                    <label>Duties/Responsibilities</label>
                                    <textarea name="duties" class="form-control" required="" rows="6" id="duties"></textarea>
                                  </div>
                                  <div class="row no-gutter">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From (Date)</label>
                                            <input type="date" name="from" class="form-control datepicker" required="" id="from"/>
                                          </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To (Date)</label>
                                            <input type="date" name="to" class="form-control datepicker" required="" id="to"/>
                                          </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Reason for leaving</label>
                                    <textarea name="reason" class="form-control" required="" rows="2" id="reason"></textarea>
                                  </div>
		                      </div>
                              <input type="hidden" name="emp" value="<?php echo $emp_id?>"/>
                              <input type="hidden" name="history_id" id="history_id"/>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		                          <button class="btn btn-theme" type="submit" id="btn-main2">Save changes</button>
		                      </div>
                              </form>
		                  </div>
		              </div>
		          </div>
                  
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edu-modal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Add/Modify Educational History</h4>
		                      </div>
                              <form method="post" action="<?php echo site_url('system/act/hr_model/create_educationalbackground')?>" id="frm-main3" class="ajax-form">
		                      <div class="modal-body">
		                          <div class="form-group">
                                    <label>Institution Name</label>
                                    <input type="text" name="institution" class="form-control" required="" id="institution"/>
                                  </div>
                                  <div class="form-group">
                                    <label>Award</label>
                                    <input type="text" name="degree" class="form-control" required="" placeholder="e.g. Ceritificate/Diploma/Bachelors Degree in Applied Mathematics" id="degree"/>
                                  </div>
                                  <div class="form-group">
                                    <label>Majors/Subjects</label>
                                    <textarea name="majors" class="form-control" required="" rows="2" id="majors"></textarea>
                                  </div>
                                  <div class="row no-gutter">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>From (Date)</label>
                                            <input type="date" name="from" class="form-control datepicker" required="" id="edu_from"/>
                                          </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>To (Date)</label>
                                            <input type="date" name="to" class="form-control datepicker" required="" id="edu_to"/>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <input type="hidden" name="emp" value="<?php echo $emp_id; ?>">
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                    <button class="btn btn-theme" type="submit" id="btn-main2">Save changes</button>
                                  </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>

                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="document-modal" class="modal fade">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Add/Modify Employee Document</h4>
                          </div>
                          <form method="post" action="<?php echo site_url('system/act/hr_model/upload_empdoc') ?>" id='frm-main3' enctype="multipart/form-data" class="ajax-form">
                            <div class="modal-body">
                              <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control" required="" placeholder="document title">
                                  </div>
                                  <div class="form-group">
                                    <label>File</label>
                                    <input type="file" name="userfile" class="form-control" required="">
                                  </div>
                                  <div class="form-group">
                                    <input type="hidden" name="empid" value="<?php echo $emp_id; ?>">
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                    <button class="btn btn-theme" type="submit" id="btn-main2">Save changes</button>
                                  </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>

<!-- <script>submit_ajax_form2();</script> -->
<?php $this->load->view('layout/footer', array('dtp' => 1, 'sel' => 1));?>