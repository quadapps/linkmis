<?php
$dets = ['title' => 'Employees', 'page' => 5];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');
$user = get_data('tbl_users', "WHERE user_id = $uid", 'user_id, user_levelid, user_empid', true);
$emp = get_data('tbl_employees', "WHERE emp_id = ".$user['user_empid'], 'emp_id, emp_designation', true);

$id = $params;
if(!empty($id)) {
    $data = get_data('tbl_employees', "WHERE emp_active = 1 AND emp_deptid = $id");
} else {
    if($user['user_levelid'] == 4) 
    $data = get_data('tbl_employees', 'where emp_id = '.$user['user_empid']);
    
    elseif($user['user_levelid'] == 3) 
    $data = get_data('tbl_employees', 'WHERE emp_active = 1 AND emp_deptid = '.get_that_data('tbl_employees', 'emp_deptid', 'emp_id', $user['user_empid']));
    
    else
    $data = get_data('tbl_employees', 'where emp_active = 1');
    
}
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="<?php echo site_url('welcome/view/create_emp')?>" class="btn btn-secondary" ><i class="fa fa-plus"></i> Add New</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter">
											<thead>
												<tr>
													<th>NO</th>
													<th>Name</th>
													<th>Designation</th>
													<th>Department</th>
													<th>Joining Date</th>
													<th>Work Phone</th>
													<th>Email</th>
													<th>Reg Date</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach(get_data('tbl_employees', "LEFT JOIN tbl_users ON user_id = emp_creator LEFT JOIN tbl_departments ON emp_deptid = dept_id WHERE emp_active =1 ", 'tbl_employees.*, user_fullname, dept_name') as $d) : ?>
													<tr>
														<td><a href="<?php echo site_url('welcome/view/emp_details/'.$d['emp_id']);?>">LPL<?php echo $d['emp_id'];?></a></td>
														<td><?php echo $d['emp_fullname'];?></td>
														<td><?php echo $d['emp_designation'];?></td>
														<td><?php echo $d['dept_name'];?></td>
														<td><?php echo $d['emp_empdate'];?></td>
														<td><?php echo $d['emp_workphone'];?></td>
														<td><?php echo $d['emp_email'];?></td>
														<td><?php echo $d['emp_createdate'];?></td>
														<td><?php if($user['user_levelid'] == 0 || $emp['emp_designation'] == 'HR OFFICER' || $emp['emp_designation'] == 'HR MANAGER') { ?>
						                                      <div class="dropdown">
						                                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-cogs"></i>
						                                          Manage
						                                        </button>
						                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						                                          <a class="dropdown-item" href="<?php echo site_url('welcome/view/emp_details/'.$d['emp_id']);?>">View</a>
						                                          <a class="dropdown-item" href="<?php echo site_url('welcome/view/create_emp/'.$d['emp_id']);?>">Edit</a>
						                                          <a class="dropdown-item transfer" data-id="<?php echo $d['emp_id']?>" href="#">Transfer</a>
						                                          <a class="dropdown-item update" data-id="<?php echo $d['emp_id']?>" href="#">Change Status</a>
						                                          <a class="dropdown-item terminate" data-id="<?php echo $d['emp_id']?>" href="#">Terminate</a>
						                                        </div>
						                                      </div>
						                                    <?php } ?></td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



<div class="modal fade" id="term-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">End Employment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/hr_model/terminate')?>" class="ajax-form">
      <div class="modal-body">
        <div class="form-group">
          <label>Form of Separation</label>
          <select name="status" class="form-control">
            <option>Resignation</option>
            <option>Termination of Contract</option>
            <option>Unsuccessful Probation</option>
            <option>End of Contract</option>
            <option>Death</option>
          </select>
        </div>
        <div class="form-group">
          <label>Date</label>
          <input type="date" name="date" class="form-control datepickers" required="">
        </div>
        <div class="form-group">
          <label>Remarks</label>
          <textarea name="remarks" class="form-control"></textarea>
        </div>
        <div class="form-group">
          <label class="custom-switch">
                            <input type="checkbox" name="cleared" class="custom-switch-input" value="1">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Employee has been cleared</span>
                          </label>
        </div>
        <input type="hidden" name="emp_id" id="emp-id">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="t-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Transfer Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/hr_model/transfer')?>" class="ajax-form">
      <div class="modal-body">
        <div class="form-group">
          <label>Select Branch</label>
          <select name="branch" class="form-control">
            <?php foreach(get_data('tbl_branches', "WHERE branch_active = 1") as $b) : ?>
              <option value="<?php echo $b['branch_id'];?>"><?php echo $b['branch_name'];?></option>
            <?php endforeach; ?>
          </select>
        </div>
        
        <input type="hidden" name="emp_id" id="empl-id">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="change-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Transfer Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/hr_model/change_status')?>" class="ajax-form">
      <div class="modal-body">
        <div class="form-group">
          <label>Select Status</label>
          <select name="status" class="form-control">
            <option>Probation</option>
            <option>Confirmed</option>
            <option>ContractExtended</option>
          </select>
        </div>
        
        <input type="hidden" name="emp_id" id="emp-id1">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

      <!--main content end-->

<script type="text/javascript">
  $('.terminate').click(function() {
    var id = $(this).data('id');
    $('#emp-id').val(id);
    $('#term-modal').modal('show');
  });

  $('.transfer').click(function() {
    var id = $(this).data('id');
    $('#empl-id').val(id);
    $('#t-modal').modal('show');
  });

  $('.update').click(function() {
    var id = $(this).data('id');
    $('#emp-id1').val(id);
    $('#change-modal').modal('show');
  });
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>