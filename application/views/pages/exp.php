<?php 
$data = get_data('tbl_expenses', "JOIN tbl_departments ON dept_id = exp_deptid JOIN tbl_users ON user_id = exp_creator WHERE exp_id = '$params'", 'tbl_expenses.*, user_fullname, dept_name', true);

if(empty($data)) die("Error occured <a href='javascript: window.history.back()'>Go back</a>");
$dets['page'] = 7; $dets['title'] = 'Purchase/Request Voucher';
$this->load->view('layout/header', $dets); 
//$id = $params;

// $supplier = get_data('tbl_suppliers', 'supplier_id', $data['po_supplierid']);

$Total = 0;
$des = get_that_data('tbl_employees', 'emp_designation', 'emp_id', $this->session->userdata('emp_id'));
?>

<style type="text/css">
.hide-on-screen {display:none;}
/*.yesPrint, .noPrint {display:block;}*/

/*td, th {
  font-size: 16px !important;
  font-weight: bold;
}

table {
  border-style: solid;
  border-width:3px !important;
}*/

@media print {
   /*.noPrint {display:none;}*/
   .hide-on-screen {display:block;}
   td, th {
      font-size: 16px !important;
      font-weight: bold;
    }

    table {
      border-style: solid;
      border-width:3px !important;
    }
    p, address, div, strong, .h3 {
      font-size: 16px !important;
      font-weight: bold;
    }
}
</style>
     
     <div class="app-content my-3 my-md-5">
          <div class="container">
            
            <div class="row">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                  </div>
                  <ul class="card-options panel_toolbox">
                    
                    <li>
                      <a href="#" class="btn btn-default" onclick="javascript:window.print();"><i class="si si-printer"></i> Print</a>
                    </li>
                    <!--<li>
                      <a href="#" class="btn btn-default" onclick="javascript:window.print();"><i class="si si-save"></i> Save & Close</a>
                    </li>-->
                    
                  </ul>
                </div>
                <div class="card-body">
                  <p class="text-center"><img class="hide-on-screen " src="<?php echo base_url('res/assets/images/lp_logo.png');?>"> </p> <div class="clearfix"></div> <br><br>
                    <div class="row "> <br>
                  <div class="col-sm-6 ">
                    <p class="h3">Purchase/Request Voucher</p>
                    
                  </div>
                  <div class="col-sm-6 ">
                    <p class="h3">NO: <?php echo date('d/m', strtotime($data['exp_createdate'])).'/'.$data['exp_id'];?></p>
                    <!-- <address class="rounded border"><strong>SHIP TO:</strong><br><br>
                      LINKS PAY LTD<br>
                      HAI MALAKAL, JUBA, SOUTH SUDAN<br>
                      Mobile No : <strong>+211929321060</strong><br>
                      Email ID: <strong>info@links-pay.com</strong>
                    </address> -->
                  </div>
                  <div class="col-sm-12">
                    <address class="rounded border"> <strong>REQUESTED BY:<br></strong>
                      Name: <strong><?php echo $data['user_fullname'];?></strong> <br>
                      Department: <?php echo $data['dept_name'];?><br>
                      Date: <?php echo date('d/m/Y');?><br>
                      Signature:<br>
                      <br>
                    </address>
                  </div>
                </div>

                <div class="table-responsive push">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr class=" ">
                      <th class="text-center " style="width: 1%">S/NO</th>
                      <th>Item Description</th>
                      <th class="text-center" style="width: 1%">Qty</th>
                      <th class="text-right" style="width: 5%">Unit  (<?php echo $data['exp_currency'];?>)</th>
                      <th class="text-right" style="width: 5%">Amount  (<?php echo $data['exp_currency'];?>)</th>
                    </tr>
                    </thead>
                   <tbody>
                    <?php if($data['exp_poid'] == 0) {  
                    $j = 1; ?>
                    <tr>
                      <td class="text-center"><?php echo $j;?></td>
                      <td>
                        <div class=""><?php echo $data['exp_details'];?></div>
                        <span class="text-muted"></span>
                      </td>
                      <td class="text-center"><STRONG></STRONG></td>
                      <td class="text-right"><strong></strong></td>
                      <td class="text-right"><strong><?php echo $Total = $data['exp_amt'];?></strong></td>
                    </tr>
                    
                    
                    <tr>
                      <td colspan="4" class="font-weight-bold text-uppercase text-right"><strong>Total</strong> </td>
                      <td class="font-weight-bold text-right"><strong><?php echo number_format($Total, 2)?></strong></td>
                    </tr>
                    <?php } else { $arr = get_row_data('tbl_po', 'po_id', $data['exp_poid']);
                    $items = json_decode($arr['po_items'], true); 
                     $j = 1; for($i=0; $i<count($items['desc']); $i++) : if(isset($items['desc'][$i])) : ?>
                    <tr>
                      <td class="text-center"><?php echo $j;?></td>
                      <td>
                        <strong class=""><?php echo $items['desc'][$i]?></strong>
                      </td>
                      <td class="text-center"><STRONG><?php echo (int)$items['qty'][$i]?></STRONG></td>
                      <td class="text-right"><strong><?php echo number_format((float)$items['price'][$i], 2);?></strong></td>
                      <td class="text-right"><strong><?php $amt = (int)$items['qty'][$i]*(float)$items['price'][$i]; echo number_format($amt, 2)?></strong></td>
                    </tr>
                    <?php $Total += $amt; $j++; endif; endfor; ?>
                    
                    <tr>
                      <td colspan="4" class="font-weight-bold text-uppercase text-right"><strong>Total</strong> </td>
                      <td class="font-weight-bold text-right"><strong><?php echo number_format($Total, 2)?></strong></td>
                    </tr>
                  <?php } ?>
                    <!--<tr>
                      <td colspan="5" class="text-right">
                        
                      </td>
                    </tr>-->
                   </tbody>
                  </table>
                </div>
                <h4>Amount in words: <u><?php echo to_words($Total).' '.$data['exp_currency'];?> Only</u></h4>
                <p><?php //echo preg_replace("/\r\n|\r|\n/",'<br/>', $data['po_terms']);?></p> <hr>
                <div class="row">
                  
                  <div class="col-sm-3">
                    Prepared By: <strong><?php echo get_fullname();?></strong><br><br>
                    Checked & Approved by: <br><br>
                    Authorized by : <br><br>
                    Audit Check : 
                  </div>
                  <div class="col-sm-3">
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                    ________________________________ 
                  </div>
                  <div class="col-sm-2 text-right">
                    Date: <br><br>
                    Date: <br><br>
                    Date: <br><br>
                    Date: 
                  </div>
                  <div class="col-sm-4">
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                    ________________________________ 
                  </div>

                </div><br><br>
                <h5 class="text-center border">Receiver's Details</h5> <br>
                <div class="row">
                  <div class="col-sm-3">
                    Name : <br><br>
                    Signature & Mobile No: <br><br>
                  </div>
                  <div class="col-sm-3">
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                  </div>
                  <div class="col-sm-2 text-right">
                    Company's Name & Staff ID: <br><br>
                    Date: <br><br> 
                  </div>
                  <div class="col-sm-4">
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                </div>
                
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>

      <!--main content end-->
<?php $this->load->view('layout/footer'); ?>