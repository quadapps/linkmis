<?php
$dets = ['title' => 'Bills/Expenses', 'page' => 3];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');
// $user = get_data('tbl_users', "WHERE user_id = $uid", 'user_id, user_levelid, user_empid', true);
// $emp = get_data('tbl_employees', "WHERE emp_id = ".$user['user_empid'], 'emp_id, emp_designation', true);
// $des = strtolower($emp['emp_designation']);
// if (strpos($des, 'project') !== false || $des == 'production manager') {
// 	$this->load->view('dashboards/projects');
// } else {
// 	$this->load->view('dashboards/main');
// }

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-d', strtotime('-1 month'));
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d');

$data = get_data('tbl_expenses', "LEFT JOIN tbl_users ON user_id = exp_creator JOIN tbl_departments ON dept_id = exp_deptid WHERE exp_active =1 AND DATE(exp_createdate) BETWEEN '$start' AND '$end' ", 'tbl_expenses.*, user_fullname, dept_name');

$bills = get_data('tbl_expenses', " WHERE exp_active =1 AND exp_status >= 0 AND DATE(exp_createdate) BETWEEN '$start' AND '$end' GROUP BY exp_currency", 'SUM(exp_amt) AS total, exp_currency');

$payments = get_data('tbl_payments', "WHERE payment_active = 1 AND DATE(payment_createdate) BETWEEN '$start' AND '$end' GROUP BY payment_currency", 'SUM(payment_amt) AS total, payment_currency');

$ssp_bill = isset($bills[0]['total']) ? $bills[0]['total'] : 0;
$usd_bill = isset($bills[1]['total']) ? $bills[1]['total'] : 0;
$ssp_paid = isset($payments[0]['total']) ? $payments[0]['total'] : 0;
$usd_paid = isset($payments[1]['total']) ? $payments[1]['total'] : 0;
?>

<div class="my-3 my-md-5">
					<div class="container-fluid">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="card-body row">
									<div class="col-6">
										<h5><strong>Total Pending Bills</strong> </h5>
										<p><strong><?php echo isset($bills[0]['exp_currency']) ? $bills[0]['exp_currency'] : 'SSP'?>: </strong> <?php echo number_format($ssp_bill - $ssp_paid, 2);?></p>
										<p><strong><?php echo isset($bills[1]['exp_currency']) ? $bills[1]['exp_currency'] : 'USD'?>: </strong> <?php echo number_format($usd_bill - $usd_paid, 2);?></p>
									</div>
									<div class="col-6">
										<h5><strong>Total Paid Bills</strong> </h5>
										<p><strong><?php echo isset($payments[0]['exp_currency']) ? $payments[0]['exp_currency'] : 'SSP';?>: </strong> <?php echo number_format($ssp_paid, 2);?></p>
										<p><strong><?php echo isset($payments[1]['exp_currency']) ? $payments[1]['exp_currency'] : 'USD';?>: </strong> <?php echo number_format($usd_paid, 2);?></p>
									</div>
								</div>
							</div>
						</div>
																		
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#deptModal" ><i class="fa fa-plus"></i> Add New</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table table-sm card-table table-vcenter">
											<thead>
												<tr>
													<th>Date</th>
													<th>Vendor/Supplier</th>
													<th>Details</th>
													<th>Dept</th>
													<th>Currency</th>
													<th>Amount</th>
													<th>Posted By</th>
													<th title="Estimated Payment Date">EPD</th>
													<th>mGurush Type</th>
													<th>Number</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : ?>
													<tr>
														<td><?php echo $d['exp_billdate'];?></td>
														<td><?php echo $d['exp_name'];?></td>
														<td><?php echo $d['exp_details'];?></td>
														<td><?php echo $d['dept_name'];?></td>
														<td><?php echo $d['exp_currency'];?></td>
														<td><?php echo $d['exp_amt'];?></td>
														<td><?php echo $d['user_fullname'];?></td>
														<td><?php echo $d['exp_epd'];?></td>
														<td><?php echo $d['exp_type'];?></td>
														<td><?php echo $d['exp_acctnumber'];?></td>
														<td><?php echo $d['exp_status'] < 0 ? "<label class='alert alert-danger small'>Rejected<br>".$d['exp_rejectremarks']."</label>" : get_req_status($d['exp_id']) ;?></td>
														<td class="dropdown">
															<button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															   <i class="fa fa-cogs"></i> Manage
															  </button>
															  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
															    <?php if($d['exp_status'] >= 0) : ?>
															    	<button class="dropdown-item reject" data-toggle="modal" data-target="#act-modal" data-id="<?php echo ($d['exp_id']);?>" title="Reject Expense"><i class="fa fa-exclamation"></i> Reject</button>
																	<button class="dropdown-item approve" data-id="<?php echo ($d['exp_id']);?>"><i class="fa fa-thumbs-up" title="Approve"></i> Approve</button>
																	<a class="dropdown-item" href="<?php echo site_url('welcome/view/exp/'.$d['exp_id']);?>"><i class="si si-printer" title="Print"></i> Print</a>
																	<button class="dropdown-item pay-exp" data-id="<?php echo ($d['exp_id']);?>" title="Pay"><i class="fa fa-money"></i> Pay</button>
																<?php endif; ?>
																
															
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="payModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pay Bill</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/finance_model/pay_bill');?>" class="ajax-form">
      	<div class="modal-body">
	        
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>Currency</label>
		        	<input type="text" class="form-control" name="currency" id="currency" required="">
		        </div>
		        <div class="form-group">
		        	<label>Amount Due</label>
		        	<input type="number" step="any" name="amt_due" class="form-control" required="" id="amt-due" readonly="">
		        </div>
	        </div>
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>Payment Mode</label>
		        	<select name="mode" class="form-control">
		        		<option>mGurush</option>
		        		<option>Cash</option>
		        	</select>
		        </div>
		        <div class="form-group">
		        	<label>Transaction No</label>
		        	<input type="number" name="txn" class="form-control" id="mgurush">
		        </div>
	        </div>
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>Amount Paid </label>
		        	<input type="number" name="amt" id="amt-paid" class="form-control">
		        </div>
		        <div class="form-group">
		        	<label>Remarks</label>
		        	<textarea name="remarks" class="form-control"></textarea>
		        </div>
	        </div>

	        <input type="text" name="exp" id="exp-id">

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Expense</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/finance_model/create_exp');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Vendor/Supplier Name</label>
	        	<input type="text" name="name" class="form-control" required="">
	        </div>
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>Expense/Ledger Head</label>
		        	<select name="cat" class="form-control">
		        		<?php foreach(get_data('tbl_expensecats', "WHERE active = 1") as $e) { ?>
		        			<option value="<?php echo $e['id'];?>"><?php echo $e['name'];?></option>
		        		<?php } ?>
		        	</select>
		        </div>
		        <div class="form-group col">
		        	<label>Department</label>
		        	<select name="dept" class="form-control">
		        		<?php foreach(get_data('tbl_departments', "WHERE dept_active = 1") as $e) { ?>
		        			<option value="<?php echo $e['dept_id'];?>"><?php echo $e['dept_name'];?></option>
		        		<?php } ?>
		        	</select>
		        </div>
	        </div>
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>Bill/Invoice/Quote No</label>
		        	<input type="text" name="bill_no" class="form-control" required="">
		        </div>
		        <div class="form-group col">
		        	<label>Bill Date</label>
		        	<input type="date" name="bill_date" class="form-control" required="">
		        </div>
	        </div>
	        <div class="form-group">
	        	<label>Bill Details</label>
	        	<textarea name="details" class="form-control" required=""></textarea>
	        </div>
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>Currency</label>
		        	<select name="currency" class="form-control">
		        		<option>SSP</option>
		        		<option>USD</option>
		        	</select>
		        </div>
		        <div class="form-group col">
		        	<label>Amount Due</label>
		        	<input type="number" step="any" name="amt" class="form-control" required="">
		        </div>
	        </div>
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>mGurush Type</label>
		        	<select name="type" class="form-control">
		        		<option>Merchant</option>
		        		<option>Customer</option>
		        	</select>
		        </div>
		        <div class="form-group col">
		        	<label>mGurush No</label>
		        	<input type="number" name="acct" class="form-control" required="">
		        </div>
	        </div>
	        <div class="form-row">
	        	<div class="form-group col-4">
		        	<label>EPD <i class="fa fa-questionmark" title="Estimated Payment Date"></i></label>
		        	<input type="date" name="epd" class="form-control">
		        </div>
		        <div class="form-group col-8">
		        	<label>Remarks</label>
		        	<textarea name="remarks" class="form-control"></textarea>
		        </div>
	        </div>


	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="act-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/finance_model/act_expense')?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Why?</label>
	        	<textarea class="form-control" name="remarks" required=""></textarea>
	        </div>

	        <input type="hidden" name="act_id" id="act-id" >

	        <input type="hidden" name="type" id="type">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('.pay-exp').click(function() {
		// let txt = [];
		// let i = 0;
		//  $(this).closest('tr').find('td').each(function() {
		//         var textval = $(this).text(); // this will be the text of each <td>
		//         //console.log(textval);
		//         txt[i] = textval;
		//         i++;
		//    });
		// console.log(txt);
		// $('#vendor').val($(this).closest('tr').find("td:nth-child(2)").text());
		$('#amt-due').val($(this).closest('tr').find("td:nth-child(6)").text());
		$('#currency').val($(this).closest('tr').find("td:nth-child(5)").text());
		// $('#bill-date').val($(this).closest('tr').find("td:nth-child(8)").text());
		// $('#bill-details').val($(this).closest('tr').find("td:nth-child(3)").text());
		// $('#mgurush').val($(this).closest('tr').find("td:nth-child(10)").text());

		$('#exp-id').val($(this).data('id'));

		$('#payModal').modal('show');
	});

	$('.approve').click(function() {
		//let id = $(this).data(id);
		$('#act-id').val($(this).attr('data-id'));
		$('#type').val(1);
		$('#act-modal').modal('show');
	});
	$('.reject').click(function() {
		//let id = $(this).data(id);
		$('#act-id').val($(this).attr('data-id'));
		$('#type').val(-1);
		// $('#act-modal').modal('show');
	})
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>