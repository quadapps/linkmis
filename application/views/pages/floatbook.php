<?php 
$and = '';
$title = 'Floatbook';
if(isset($_GET['f'])) {
	$and .= " AND tbl_recons.created_by = '".$_GET['f']."'";
	$title .= ' : '.$_REQUEST['f'];
	#even more
}

$dets['page'] = 3; $dets['title'] = $title;
$this->load->view('layout/header', $dets); 

$start = empty($_GET['s']) ? date('Y-m-d', strtotime('-1 month')) : $_GET['s']; 
$end = empty($_GET['e']) ? date('Y-m-d') : $_GET['e'];

// $data = get_data('tbl_recons', "JOIN tbl_reconbranches ON recon_ec5_uuid = ec5_uuid WHERE DATE(tbl_recons.created_at) BETWEEN '$start' AND '$end' $and");

// $data = get_data('tbl_employees', "JOIN tbl_users ON user_empid = emp_id WHERE emp_active = 1 AND emp_designation LIKE 'CASH%'", 'emp_id, emp_fullname, emp_gmail, user_id');

$range = dateRanges($start, $end);

?>
     <div class="my-3 my-md-5">
          <div class="container-fluid">
            
            <div class="row row-cards row-deck">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">
                    <li><a href="#" class="btn btn-secondary" id="sync"><i class="fa fa-refresh"></i> Sync</a>
                    </li>
                    <li>
                      <a class="btn btn-secondary" id="reportrange" href="javascript: void()" data-url="<?php echo site_url('welcome/view/recons')?>"><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end)) ;?></a>
                    </li>
                    
                  </ul>
                </div>
                <div class="card-body">
                	<div class="panel panel-primary">
											<div class=" tab-menu-heading">
												<div class="tabs-menu1 ">
													<!-- Tabs -->
													<ul class="nav panel-tabs">
														<li class=""><a href="#tab5" class="active" data-toggle="tab">SSP</a></li>
														<li><a href="#tab6" data-toggle="tab">USD</a></li>
													</ul>
												</div>
											</div>
											<div class="panel-body tabs-menu-body">
												<div class="tab-content">
													<div class="tab-pane active " id="tab5">
														<div class="table-responsive">
											                  <table id="d-table12" class="table table-hover data-table-buttons">
											                            <thead>
											                            	<th>No</th>
											                            	<th>Date</th>
											                                <th> Name</th>
											                                <th>Float In</th>
											                                <th>Float Out</th>
											                                <th>Float Balance</th>
											                                
											                            </thead>
											                            <tbody class="tableData">
											                            <?php foreach($range as $j) {
											                            	$data = get_data('tbl_cashbook', "WHERE DATE(cb_createdate) = '$j' AND cb_currency = 'SSP' AND cb_float = 1");
											                            	foreach($data as $r) : 
											                             ?>
											                                <tr>
											                                	<td><?php echo $r['cb_id'];?></td>
											                                	<td><?php echo $j;?></td>
											                                    <td><a href="#"><?php echo get_fullname($r['cb_creator']);?></a></td>
											                                    
											                                    <td><?php echo $r['cb_type'] == 1 ? $r['cb_amt'] : 0;?></td>
											                                    <td><?php echo $r['cb_type'] == 2 ? $r['cb_amt'] : 0;; ?></td>
											                                    <td><?php echo $r['cb_linebal'];?></td>
											                                    
											                                </tr>
											                            <?php endforeach; } ?>
											                            </tbody>

											                            <tfoot>
											                            	
											                            </tfoot>
											                           </table>
											                </div>
													</div>
													<div class="tab-pane " id="tab6">
														<div class="table-responsive">
										                  <table id="d-table12" class="table table-hover data-table-buttons">
											                            <thead>
											                            	<th>Date</th>
											                                <th> Name</th>
											                                <th>Float In</th>
											                                <th>Float Out</th>
											                                <th>Float Balance</th>
											                                
											                            </thead>
											                            <tbody class="tableData">
											                            <?php foreach($range as $j) {
											                            	$data = get_data('tbl_cashbook', "WHERE DATE(cb_createdate) = '$j' AND cb_currency = 'usd' AND cb_float = 1");
											                            	foreach($data as $r) : 
											                             ?>
											                                <tr>
											                                	<td><?php echo $j;?></td>
											                                    <td><a href="#"><?php echo get_fullname($r['cb_creator']);?></a></td>
											                                    
											                                    <td><?php echo $r['cb_type'] == 1 ? $r['cb_amt'] : 0;?></td>
											                                    <td><?php echo $r['cb_type'] == 2 ? $r['cb_amt'] : 0;; ?></td>
											                                    <td><?php echo '';?></td>
											                                    
											                                </tr>
											                            <?php endforeach; } ?>
											                            </tbody>

											                            <tfoot>
											                            	
											                            </tfoot>
											                           </table>
										                </div>
													</div>
													
												</div>
											</div>
										</div>
                                  
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>
<!-- Modal -->
<div class="modal fade" id="pay-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add/Modify Targets</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/marketing_model/create_target')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      <div class="form-group">
        <label for="inputEmail3">Employee:</label>
            <select name="emp_id" class="form-control">
                <option value="0">--Select Employee--</option>
                    <?php foreach(get_data('tbl_employees', 'WHERE emp_active = 1 ORDER BY emp_fullname ASC') as $e) { ?>
                    <option value="<?php echo $e['emp_id']?>"><?php echo $e['emp_fullname']?></option>
                    <?php } ?>
            </select>
      </div>
      
        <div class="form-group">
            <label for="exampleInputEmail1">Month</label>
            <select name="month" class="form-control">
                <option value="0">--Select Month--</option>
                <?php for($i=1;$i<13;$i++) { ?>
                <option value="<?php echo date('F', mktime(0,0,0,$i)) ?>"><?php echo date('F', mktime(0,0,0,$i)) ?></option>
                <?php } ?>
            </select>
          </div>
         <div class="form-group">
            <label for="exampleInputEmail1">No of Transactions</label>
            <input type="number" name="qty" required="" class="form-control" id="" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">USD Amount</label>
            <input type="number" name="usd" required="" class="form-control" id="" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">SSP Amount</label>
            <input type="number" name="ssp" required="" class="form-control" id="" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save Target</button>
          </div>
      </form> 
    </div>
  </div>
</div>
</div>
      <!--main content end-->

<div class="modal fade" id="recon-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Reconcile</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/finance_model/reconcile')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      

          <div class="form-group">
            <label for="exampleInputEmail1">Comments</label>
            <textarea name="comments" class="form-control" required="" id="comments"></textarea>
          </div>

          <div class="form-group">
            <input type="hidden" name="tdr_id" required="" class="form-control" id="recon-id" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
</div>

<script type="text/javascript">
	// calc_bal();
	$('#sync').click(function(){
		calc_bal();
	});

	$('.reconcile').click(function() {
		let id = $(this).data('id');
		let comm = $(this).data('comm');
		$('#recon-id').val(id);
		$('#comments').text(comm);
		$('#recon-modal').modal('show');
	});

	function get_collections()
	{
		$.get('<?php echo site_url('epicollect/sync_collections')?>', function(r) {
			// if(r > 0)
				location.reload();
		}).fail(function() {
			swal("No new records found!");
		    location.reload();
		});
	}

	function calc_bal() {
		$('.tableData tr').each(function () {
		    var row = $(this);
		    var rowTotal = 0;
		    let ci = row.find("td:nth-child(3)").text();
		    console.log(ci);
		    let co = row.find("td:nth-child(4)").text();
		    rowTotal = ci-co;
		    row.find('td:last').text(rowTotal);
		});
	}
</script>

<?php $this->load->view('layout/footer', array('dtt' => 1, 'dtp' => 1)); ?>