<?php
$dets = ['title' => 'Fueling', 'page' => 7];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');
// $user = get_data('tbl_users', "WHERE user_id = $uid", 'user_id, user_levelid, user_empid', true);
// $emp = get_data('tbl_employees', "WHERE emp_id = ".$user['user_empid'], 'emp_id, emp_designation', true);
// $des = strtolower($emp['emp_designation']);
// if (strpos($des, 'project') !== false || $des == 'production manager') {
// 	$this->load->view('dashboards/projects');
// } else {
// 	$this->load->view('dashboards/main');
// }\\

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-01');
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d');

$fuel = get_data('tbl_fueling', "WHERE fuel_active =1 AND DATE(fuel_date) BETWEEN '$start' AND '$end'", 'SUM(fuel_cost*fuel_lts) AS val, SUM(fuel_lts) AS lts', true);

$data = get_data('tbl_vehicles', "WHERE car_active = 1");

function get_fueling($car, $start, $end)
{
	$data = get_data('tbl_fueling', " WHERE fuel_active =1 AND fuel_carid = $car AND DATE(fuel_date) BETWEEN '$start' AND '$end' ORDER BY fuel_date DESC LIMIT 2");
	return $data;
}
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="card-body">
									<h5><strong>Total Fueling</strong> </h5>
									<p><strong>Litres: </strong> <?php echo number_format($fuel['lts'], 2);?> lts</p>
									<p><strong>Value: </strong>SSP <?php echo number_format($fuel['val'], 2);?></p>
								</div>
							</div>
						</div>
																		
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="javascript:void()" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/view/fueling');?>" ><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end));?></a>
											</li>
											<li>
												<a href="javascript:void()" class="btn btn-secondary" id="" data-toggle="modal" data-target="#deptModal" ><i class="fa fa-plus"></i> Log Fueling</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter">
											<thead>
												<tr>
													<th>No</th>
													<th>Vehicle No</th>
													<th>Type</th>
													<th>Last Fueling</th>
													<th>Lts Fueled</th>
													<th>Last Odo Reading</th>
													<th>Current Odo Reading</th>
													<th>KMs Run</th>
													<th>KMs per Lt</th>
													<th>Lts Fueled</th>
													<th>Cost per Lt</th>
													<th>Total Cost</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : $f = get_fueling($d['car_id'], $start, $end); ?>
													<tr>
														<td><?php echo $d['car_id'];?></td>
														<td><?php echo $d['car_number'];?></td>
														<td><?php echo $d['car_type'];?></td>
														<td><?php echo isset($f[1]['fuel_date']) ? $f[1]['fuel_date'] : '';?></td>
														<td><?php echo $lts = isset($f[1]['fuel_lts']) ? $f[1]['fuel_lts'] : 0;?></td>
														<td><?php echo $prev = isset($f[1]['fuel_reading']) ? $f[1]['fuel_reading'] : 0;?></td>
														<td><?php echo $cur = isset($f[0]['fuel_reading']) ? $f[0]['fuel_reading'] : 0;?></td>
														<td><?php echo $run = $cur - $prev;?></td>
														<td><?php echo $lts > 0 ? $run/$lts : 0;?></td>
														<td><?php echo $cur_lts = isset($f[0]['fuel_lts']) ? $f[0]['fuel_lts'] : 0;?></td>
														<td><?php echo $cost = isset($f[0]['fuel_cost']) ? $f[0]['fuel_cost'] : 0;?></td>
														<td><?php echo $cur_lts * $cost;?></td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Vehicle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/admin_model/create_fuel');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Select Vehicle</label>
	        	<select name="car" class="form-control">
	        		<?php foreach($data as $c) : ?>
	        			<option value="<?php echo $c['car_id'];?>"><?php echo $c['car_number'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>Refuel Date</label>
	        	<input type="date" name="date" class="form-control" >
	        </div>
	        <div class="form-group">
	        	<label>Litres Fueled</label>
	        	<input type="number" name="lts" class="form-control"  value="0">
	        </div>
	        <div class="form-group">
	        	<label>Cost Per Ltd</label>
	        	<input type="number" name="cost" class="form-control"  value="0">
	        </div>

	        <div class="form-group">
	        	<label>Last Odometer Reading</label>
	        	<input type="number" name="reading" class="form-control" value="0">
	        </div>

	        
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="recon-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Confirm Collection</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/finance_model/confirm_collection')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      		<div class="form-group">
	        	<label>SSP Recieved</label>
	        	<input type="number" name="ssp_amt" id="ssp-amt" class="form-control" required="">
	        </div>
	        
	        <div class="form-group">
	        	<label>USD Recieved</label>
	        	<input type="number" name="usd_amt" id="usd-amt" class="form-control" required="">
	        </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Comments</label>
            <textarea name="comments" class="form-control" required="" id="comments"></textarea>
          </div>

          <div class="form-group">
            <input type="hidden" name="coll_id" required="" class="form-control" id="recon-id" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
</div>


<script type="text/javascript">
	$('#sync').click(function(){
		swal('submitted');
		$.get('<?php echo site_url('epicollect/sync_collections')?>', function(r) {
			//if(r > 0)
				location.reload();
		});
	});

	$('.reconcile').click(function() {
		let id = $(this).data('id');
		let comm = $(this).data('comm');
		let usd = $(this).closest('tr').find("td:nth-child(5)").text();
		let ssp = $(this).closest('tr').find("td:nth-child(6)").text();
		$('#recon-id').val(id);
		$('#comments').text(comm);
		$('#ssp-amt').val(ssp);
		$('#usd-amt').val(usd);
		$('#recon-modal').modal('show');
	})
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>