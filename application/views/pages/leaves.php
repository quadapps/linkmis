<?php
$dets = ['title' => 'Leave Management', 'page' => 5];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');
$user = get_data('tbl_users', "JOIN tbl_employees ON emp_id = user_empid WHERE user_id = ".$this->session->userdata('user_id'), 'user_empid, user_levelid, user_id, emp_deptid',  true);
$and = '';
if ($user['user_levelid'] == 4) {
  $and = " AND emp_id = ".$user['user_empid'];
} 
if ($user['user_levelid'] == 3) {
  $and = " AND emp_deptid = ".$user['emp_deptid'];
}

$emps = get_data('tbl_employees', "LEFT JOIN tbl_departments ON dept_id = emp_deptid WHERE emp_active = 1 $and ", 'emp_id, emp_fullname, emp_designation, dept_name');

$leaves = get_data('tbl_leaves', "LEFT JOIN tbl_employees ON emp_id = leave_empid LEFT JOIN tbl_leavetypes ON lt_id = leave_type $and ", 'tbl_leaves.*, emp_id, emp_fullname, emp_designation, lt_days, lt_name, TIMESTAMPDIFF(DAY,leave_from,leave_to) AS taken_days');

$lts = get_data('tbl_leavetypes');

function get_leave_balance($lts, $emp_id)
{

	$recs = get_data('tbl_leaves', "WHERE leave_empid = $emp_id AND leave_status = 2 AND leave_type = $lts", "SUM(TIMESTAMPDIFF(DAY,leave_from,leave_to)) AS taken_days", true);

	return isset($recs['taken_days']) ? $recs['taken_days']+1 : 0;
	
}
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#lModal" ><i class="fa fa-plus"></i> Apply Leave</a>
											</li>
											<li>
												<a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#hModal" ><i class="fa fa-plus"></i> Add Holiday</a>
											</li>
										</ul>
									</div>
									<div class="p-6 card-body">
										<div class="panel panel-primary">
											<div class=" tab-menu-heading">
												<div class="tabs-menu1 ">
													<!-- Tabs -->
													<ul class="nav panel-tabs">
														<li class=""><a href="#tab5" class="active" data-toggle="tab">Leave Balances</a></li>
														<li><a href="#tab6" data-toggle="tab">Leave Applications</a></li>
														<li><a href="#tab7" data-toggle="tab">Holidays</a></li>
														
													</ul>
												</div>
											</div>
											<div class="panel-body tabs-menu-body">
												<div class="tab-content">
													<div class="tab-pane active table-responsive" id="tab5">
														<table class="table table-bordered table-hover data-table-buttons" >
															<thead>
																<th>Emp No</th>
																<th>Full Name</th>
																<th>Designation</th>
																<?php foreach($lts as $l) : ?>
																	<th><?php echo $l['lt_name'];?></th>
																<?php endforeach; ?>
															</thead>
															<tbody>
																<?php foreach($emps as $e) : ?>
																	<tr>
																		<td><?php echo $e['emp_id'];?></td>
																		<td><?php echo $e['emp_fullname'];?></td>
																		<td><?php echo $e['emp_designation'];?></td>
																		<?php foreach($lts as $l) : ?>
																			<td><?php echo $l['lt_days'] - get_leave_balance($l['lt_id'], $e['emp_id']);?></td>
																		<?php endforeach; ?>
																	</tr>
																<?php endforeach; ?>
															</tbody>
														</table>
													</div>
													<div class="tab-pane table-responsive" id="tab6">
														<table class="table table-bordered table-hover data-table-buttons" >
															<thead>
																<th>Emp No</th>
																<th>Full Name</th>
																<th>Designation</th>
																<th>From</th>
																<th>To</th>
																<th>Resuming On</th>
																<th>Leave Type</th>
																<th>Days Taken</th>
																<th>Balance</th>
																<th>Status</th>
																<th>Attachment</th>
																<th>Action</th>
															</thead>
															<tbody>
																<?php foreach($leaves as $e) : ?>
																	<tr>
																		<td><?php echo $e['emp_id'];?></td>
																		<td><?php echo $e['emp_fullname'];?></td>
																		<td><?php echo $e['emp_designation'];?></td>
																		<td><?php echo $e['leave_from'];?></td>
																		<td><?php echo $e['leave_to'];?></td>
																		<td><?php echo $e['leave_resuming'];?></td>
																		<td><?php echo $e['lt_name'];?></td>
																		<td><?php echo $tak = $e['taken_days']+1;?></td>
																		<td><?php echo $e['lt_days']-$tak;?></td>
																		<td>
																			<?php 
																			if($e['leave_status'] == 0) echo 'Pending';
																			if($e['leave_status'] == 1) echo 'Approved by HOD';
																			if($e['leave_status'] == 2) echo 'Approved by HR';
																			if($e['leave_status'] == -1) echo 'Rejected<br>'.$e['leave_approveremarks'];
																			?>
																		</td>

																		<td><?php echo !empty($e['leave_attachment']) ? '<a href="'.$e['leave_attachment'].'">View</a> &nbsp; <a href="#">Remove</a>' : $e['leave_attachment'].'<a href="#">Attach</a>';?></td>

																		<td class="btn-group">
																			<button data-id="<?php echo $e['leave_id'];?>" data-status="approve" class="btn btn-sm btn-success leave-act" <?php echo $e['leave_status'] == 2 || $e['leave_status'] == -1 ? 'disabled' : '';?>>Approve</button>
																			<button data-id="<?php echo $e['leave_id'];?>" data-status="-1" class="btn btn-sm btn-danger leave-act" <?php echo $e['leave_status'] == 2 || $e['leave_status'] == -1 ? 'disabled' : '';?>>Reject</button>
																		</td>
																	</tr>
																<?php endforeach; ?>
															</tbody>
														</table>
													</div>
													<div class="tab-pane table-responsive " id="tab7">
														<table class="table table-bordered table-hover data-table-buttons" >
															<thead>
																<th>Name</th>
																<th>Date</th>
																<th>Action</th>
															</thead>
															<tbody>
																<?php foreach(get_data('tbl_holidays', "WHERE active = 1") as $e) : ?>
																	<tr>
																		<td><?php echo $e['name'];?></td>
																		<td><?php echo $e['date'];?></td>
																		<td class="btn-group">
																			<a href="#" class="btn btn-sm btn-primary">Edit</a>
																			<a href="#" class="btn btn-sm btn-danger remove-things">Delete</a>
																		</td>
																		
																	</tr>
																<?php endforeach; ?>
															</tbody>
														</table>
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

<!-- Modal -->
<div class="modal fade" id="hModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Holiday</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" class="ajax-form" action="<?php echo site_url('system/act/hr_model/add_holiday');?>">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Holiday Name</label>
	        	<input type="text" name="hname" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>Day/Date</label>
	        	<input type="date" name="hdate" class="form-control" required="">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="lModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apply Leave</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" class="ajax-form" action="<?php echo site_url('system/act/hr_model/create_leave');?>">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Employee Name</label>
	        	<select name="emp_id" class="form-control">
	        		<?php foreach($emps as $e) : ?>
		        		<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
		        	<?php endforeach; ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>Type</label>
	        	<select name="type" class="form-control">
	        		<?php foreach($lts as $e) : ?>
		        		<option value="<?php echo $e['lt_id'];?>"><?php echo $e['lt_name'];?></option>
		        	<?php endforeach; ?>
	        	</select>
	        </div>
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>From</label>
		        	<input type="date" name="leave_from" class="form-control" required="">
		        </div>
		        <div class="form-group col">
		        	<label>To</label>
		        	<input type="date" name="leave_to" class="form-control" required="">
		        </div>
	        </div>
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>Resuming On</label>
		        	<input type="date" name="resuming" class="form-control" required="">
		        </div>
		        <div class="form-group col">
		        	<label>Backstopper</label>
		        	<select name="backstopper" class="form-control">
		        		<?php foreach($emps as $e) : ?>
			        		<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
			        	<?php endforeach; ?>
		        	</select>
		        </div>
	        </div>
	        <div class="form-group">
	        	<label>Reason</label>
	        	<textarea name="leave_reason" class="form-control" required=""></textarea>
	        </div>
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="actModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Act on Leave</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" class="ajax-form" action="<?php echo site_url('system/act/hr_model/leave_action');?>">
      	<div class="modal-body">
	        
	        <div class="form-group">
	        	<label>Remarks</label>
	        	<textarea name="remarks" class="form-control" required=""></textarea>
	        </div>

	        <input type="hidden" name="leave_id" id="leave-id">
	        <input type="hidden" name="status" id="status">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('.leave-act').click(function() {
		let id = $(this).data('id');
		let status = $(this).data('status');

		$('#leave-id').val(id);
		$('#status').val(status);

		$('#actModal').modal('show');
	})
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>