<?php
$dets = ['title' => 'Salary Advance/Loans', 'page' => 5];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');

$user = get_data('tbl_users', "JOIN tbl_employees ON emp_id = user_empid WHERE user_id = ".$this->session->userdata('user_id'), 'user_empid, user_levelid, user_id, emp_deptid, emp_designation',  true);
$and = '';
if ($user['user_levelid'] > 0 || $user['emp_designation'] != 'HR MANAGER' || $user['emp_designation'] != 'FINANCE MANAGER') {
  $and = " AND emp_id = ".$user['user_empid'];
} 
// if ($user['user_levelid'] == 3) {
//   $and = " AND emp_deptid = ".$user['emp_deptid'];
// }

$data = get_data('tbl_loans', "LEFT JOIN tbl_employees ON loan_empid = emp_id WHERE loan_active =1 $and ", 'tbl_loans.*, emp_fullname, emp_deptid');

$emps = get_data('tbl_employees', "WHERE emp_active =  1 $and ", 'emp_id, emp_fullname');

function get_loan_status($data)
{
	$status = $data['loan_status'];
	switch ($status) {
            case '-1':
            	return '<span class="status-icon bg-danger"></span>Rejected<figure>
					  <blockquote class="blockquote">
					    <p>'.$data['loan_rejectremarks'].'.</p>
					  </blockquote>
					  <figcaption class="blockquote-footer">By
					    '.get_fullname($data['loan_rejectby']).' <cite title="Source Title"> on '.timeAgo($data['loan_rejectdate']).'</cite>
					  </figcaption>
					</figure>';
                break;

            case '0':
            	return '<span class="status-icon bg-primary"></span>Pending';
                break;

            case '1':
            	return '<span class="status-icon bg-info"></span>Forwarded<figure>
					  <blockquote class="blockquote">
					    <p>'.$data['loan_forwardremarks'].'.</p>
					  </blockquote>
					  <figcaption class="blockquote-footer">By 
					    '.get_fullname($data['loan_forwardedby']).' <cite title="Source Title"> on '.timeAgo($data['loan_forwarddate']).'</cite>
					  </figcaption>
					</figure>';
                break;

            case '2':
            	return '<span class="status-icon bg-success"></span>Approved<figure>
					  <blockquote class="blockquote">
					    <p>'.$data['loan_approveremarks'].'.</p>
					  </blockquote>
					  <figcaption class="blockquote-footer">By 
					    '.get_fullname($data['loan_approvedby']).' <cite title="Source Title"> on '.timeAgo($data['loan_approveddate']).'</cite>
					  </figcaption>
					</figure>';
                break;

            case '3':
            	return '<span class="status-icon bg-success"></span>Paid<figure>
					  <figcaption class="blockquote-footer">By 
					    '.get_fullname($data['loan_updator']).' <cite title="Source Title"> on '.timeAgo($data['loan_updatedate']).'</cite>
					  </figcaption>
					</figure>';
                break;
            
            default:
                return json_encode(['status' => 0, 'msg' => 'Unknown status!']);
                break;
        }
}
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#loan-modal" ><i class="fa fa-plus"></i> Add New</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter">
											<thead>
												<tr>
													<th>Date</th>
													<th>Name</th>
													<th>Amount</th>
													<th>Duration</th>
													<th>Type</th>
													<th>Created By</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : ?>
													<tr>
														<td><?php echo $d['loan_createdate'];?></td>
														<td><?php echo $d['emp_fullname'];?></td>
														<td><?php echo $d['loan_amt'];?></td>
														<td><?php echo $d['loan_duration'];?> month(s)</td>
														<td><?php echo $d['loan_type'];?></td>
														<td><?php echo get_that_data('tbl_users', 'user_fullname', 'user_id', $d['loan_creator']);?></td>
														<td><?php echo get_loan_status($d);?></td>
														<td>
															<?php if($user['user_levelid'] == 3 && $user['emp_deptid'] == $d['emp_deptid'] && $d['loan_status'] == 0) { ?>
																<button class="btn btn-primary btn-sm act-on" data-id="<?php echo $d['loan_id'];?>" data-status="1">Forward</button>
															<?php } ?>
															

															<?php if($user['emp_designation'] == 'HR MANAGER' && $d['loan_status'] == 1) : ?>
																<button class="btn btn-success btn-sm act-on" data-id="<?php echo $d['loan_id'];?>" data-status="2">Approve</button>
															<?php endif; ?>

															<?php if($d['loan_status'] == 2 && ($user['emp_designation'] == 'FINANCE MANAGER' || $user['emp_designation'] == 'CASHIER' || $user['emp_designation'] == 'ACCOUNTANT')) : ?>
																<button class="btn btn-success btn-sm act-on" data-id="<?php echo $d['loan_id'];?>" data-status="3">Pay</button>
															<?php endif; ?>

															<?php if($d['loan_status'] < 3) : ?>
																<button class="btn btn-danger btn-sm act-on" data-id="<?php echo $d['loan_id'];?>" data-status="-1">Reject</button>
															<?php endif; ?>
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



<!-- Modals -->
<div class="modal fade" id="loan-modal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Loan/Salary Advance Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/hr_model/create_loan');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Employee</label>
	        	<select name="empid" class="form-control">
	        		<?php foreach($emps as $e) { ?>
	        			<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php } ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>Type</label>
	        	<select name="type" class="form-control">
	        		<option value="Advance">Salary Advance</option>
	        		<option value="Loan">Loan</option>
	        	</select>
	        </div>
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>Amount</label>
		        	<input type="number" step="any" name="amt" class="form-control" required="">
		        </div>
		        <div class="form-group col">
		        	<label>Currency</label>
		        	<select name="currency" class="form-control">
		        		<option value="SSP">SSP</option>
		        		<option value="USD">USD</option>
		        	</select>
		        </div>
	        </div>
	        <div class="form-group">
	        	<label>Duration</label>
	        	<input type="number" name="duration" value="1" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>Reason</label>
	        	<textarea name="reason" class="form-control" required="" placeholder="Reason for request"></textarea>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="act-modal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Loan/Salary Advance Action</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/hr_model/toggle_loan');?>" class="ajax-form">
      	<div class="modal-body">
	        
	        <div class="form-group">
	        	<label>Remarks</label>
	        	<textarea name="remarks" class="form-control" required="" placeholder="Reason for action"></textarea>
	        </div>
	        <input type="hidden" name="status" id="status">
	        <input type="hidden" name="loan" id="loan-id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('.act-on').click(function() {
		let id = $(this).data('id');
		let status = $(this).data('status');
		$('#loan-id').val(id);
		$('#status').val(status);
		$('#act-modal').modal('show');
	})
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>