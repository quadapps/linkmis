<?php 
$dets['page'] = 7; $dets['title'] = 'Purchase Order';
$this->load->view('layout/header', $dets); 
//$id = $params;
$data = get_row_data('tbl_po', "po_id", $params);
$supplier = get_row_data('tbl_suppliers', 'supplier_id', $data['po_supplierid']);

$Total = 0;
$des = get_that_data('tbl_employees', 'emp_designation', 'emp_id', $this->session->userdata('emp_id'));
?>

<style type="text/css">
.hide-on-screen {display:none;}
/*.yesPrint, .noPrint {display:block;}*/

/*td, th {
  font-size: 16px !important;
  font-weight: bold;
}

table {
  border-style: solid;
  border-width:3px !important;
}*/

@media print {
   /*.noPrint {display:none;}*/
   .hide-on-screen {display:block;}
   td, th {
      font-size: 16px !important;
      font-weight: bold;
    }

    table {
      border-style: solid;
      border-width:3px !important;
    }
    p, address, div, strong, .h3 {
      font-size: 16px !important;
      font-weight: bold;
    }
}
</style>
     
     <div class="app-content my-3 my-md-5">
          <div class="container">
            
            <div class="row">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                  </div>
                  <ul class="card-options panel_toolbox">
                    <li><a href="<?php echo site_url('welcome/view/create_po')?>" class="btn btn-default" id="add-new"><i class="fa fa-plus"></i> Add New</a>
                    </li>
                    <?php if ($data['po_status'] == 0 && ($data['po_creator'] == $this->session->userdata('user_id')) || $des == 'GENERAL MANAGER') : ?>
                    <li><a href="<?php echo site_url('welcome/view/create_so/'.$data['po_id'])?>" class="btn btn-default"><i class="fa fa-pencil"></i> Edit</a>
                    </li>
                    <?php endif; ?>
                    <li>
                      <a href="#" class="btn btn-default" onclick="javascript:window.print();"><i class="si si-printer"></i> Print</a>
                    </li>
                    <!--<li>
                      <a href="#" class="btn btn-default" onclick="javascript:window.print();"><i class="si si-save"></i> Save & Close</a>
                    </li>-->
                    
                  </ul>
                </div>
                <div class="card-body">
                  <img class="hide-on-screen" src="<?php echo base_url('res/assets/images/lp_logo.png');?>"> <div class="clearfix"></div> <br><br>
                    <div class="row "> <br>
                  <div class="col-sm-6 ">
                    <p class="h3">Purchase Order</p>
                    <address class="rounded border"> <strong>TO:<br></strong>
                      Name: <strong><?php echo $supplier['supplier_name'];?></strong> <br>
                      Address: <?php echo $supplier['supplier_address'];?><br>
                      RFQ: <?php echo $data['po_ref'];?><br>
                      Contact Person: <?php echo $supplier['supplier_contactperson'];?><br>
                      <?php echo $supplier['supplier_email'];?>
                    </address>
                  </div>
                  <div class="col-sm-6 ">
                    <p class="h3">PO NO: <?php echo date('d/m', strtotime($data['po_createdate'])).'/'.$data['po_id'];?></p>
                    <address class="rounded border"><strong>SHIP TO:</strong><br><br>
                      LINKS PAY LTD<br>
                      HAI MALAKAL, JUBA, SOUTH SUDAN<br>
                      Mobile No : <strong>+211929321060</strong><br>
                      Email ID: <strong>info@links-pay.com</strong>
                    </address>
                  </div>
                </div>

                <!--<div class=" text-dark">
                  <p class="mb-1 mt-5"><span class="font-weight-semibold"> Date :</span> <?php //echo date('d F, Y', strtotime($data['po_createdate']))?></p>
                  <p><span class="font-weight-semibold">No :</span> <?php// echo date('d/m', strtotime($data['po_createdate'])).'/'.$data['po_id'];?></p>
                </div>-->
                <div class="table-responsive push">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr class=" ">
                      <th class="text-center " style="width: 1%">S/NO</th>
                      <th>Item Description</th>
                      <th class="text-center" style="width: 1%">Qty</th>
                      <th class="text-right" style="width: 5%">Unit  (<?php echo $data['po_currency'];?>)</th>
                      <th class="text-right" style="width: 5%">Amount  (<?php echo $data['po_currency'];?>)</th>
                    </tr>
                    </thead>
                   <tbody>
                    <?php if($data['po_type'] == 1) { $arr = explode(',', $data['po_items']);  
                    $j = 1; foreach($arr as $a) : $items = explode('#', $a); ?>
                    <tr>
                      <td class="text-center"><?php echo $j;?></td>
                      <td>
                        <div class=""><?php echo $name = get_that_data('tbl_wh_items', 'item_name', 'item_id', $items[0])?></div>
                        <span class="text-muted"><?php echo isset($items[3]) ? $items[3] : $name;?></span>
                      </td>
                      <td class="text-center"><STRONG><?php echo $items[1]?></STRONG></td>
                      <td class="text-right"><strong><?php echo number_format((float)$items[2], 2);?></strong></td>
                      <td class="text-right"><strong><?php $amt = (float)$items[1]*(float)$items[2]; echo number_format($amt, 2)?></strong></td>
                    </tr>
                    <?php $Total += $amt; $j++; endforeach; ?>
                    <tr>
                      <td colspan="4" class="font-w600 text-right"><strong>Subtotal</strong></td>
                      <td class="text-right"><strong><?php echo number_format($Total, 2)?></strong></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="font-w600 text-right"><strong>Vat</strong></td>
                      <td class="text-right"><strong>00.00</strong></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="font-weight-bold text-uppercase text-right"><strong>Total</strong> </td>
                      <td class="font-weight-bold text-right"><strong><?php echo number_format($Total, 2)?></strong></td>
                    </tr>
                    <?php } else { $items = json_decode($data['po_items'], true);
                     $j = 1; for($i=0; $i<count($items['desc']); $i++) : if(isset($items['desc'][$i])) : ?>
                    <tr>
                      <td class="text-center"><?php echo $j;?></td>
                      <td>
                        <strong class=""><?php echo $items['desc'][$i]?></strong>
                      </td>
                      <td class="text-center"><STRONG><?php echo (int)$items['qty'][$i]?></STRONG></td>
                      <td class="text-right"><strong><?php echo number_format((float)$items['price'][$i], 2);?></strong></td>
                      <td class="text-right"><strong><?php $amt = (int)$items['qty'][$i]*(float)$items['price'][$i]; echo number_format($amt, 2)?></strong></td>
                    </tr>
                    <?php $Total += $amt; $j++; endif; endfor; ?>
                    <tr>
                      <td colspan="4" class="font-w600 text-right"><strong>Subtotal</strong></td>
                      <td class="text-right"><strong><?php echo number_format($Total, 2)?></strong></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="font-w600 text-right"><strong>Vat</strong></td>
                      <td class="text-right"><strong>00.00</strong></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="font-weight-bold text-uppercase text-right"><strong>Total</strong> </td>
                      <td class="font-weight-bold text-right"><strong><?php echo number_format($Total, 2)?></strong></td>
                    </tr>
                  <?php } ?>
                    <!--<tr>
                      <td colspan="5" class="text-right">
                        
                      </td>
                    </tr>-->
                   </tbody>
                  </table>
                </div>
                <h5>Terms and Conditions:</h5>
                <p><?php echo preg_replace("/\r\n|\r|\n/",'<br/>', $data['po_terms']);?></p> <hr>
                <div class="row">
                  <!-- <div class="col-sm-8">
                    Thanks with Regards, <br><br><br>

                    <?php echo get_fullname()?><br>
                    <?php echo strtolower($des) == 'general manager' ? $des : ''?>
                  </div>
                  <div class="col-sm-4 rounded border text-right">
                    <strong><u>AUTHORIZATION</u></strong>:<br><br>
                     Prepared By: <strong><?php echo get_fullname();?></strong> ________________________________ <br><br>
                     Approved by Finance Manager: ________________________________ <br><br>
                     Approved by Admin & Finance: ________________________________ <br><br>
                     Approved by General Manager: ________________________________ <br><br>
                     General Manager,Signature&Date <br>
                  </div> -->
                  <div class="col-sm-3">
                    Prepared By: <strong><?php echo get_fullname();?></strong><br><br>
                    Checked by Procurement: <br><br>
                    Approved by Finance: <br><br>
                    Approved by Admin: <br><br>
                    Approved by GM: 
                  </div>
                  <div class="col-sm-3">
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                    ________________________________
                  </div>
                  <div class="col-sm-2 text-right">
                    Date: <br><br>
                    Date: <br><br>
                    Date: <br><br>
                    Date: <br><br>
                    Date: 
                  </div>
                  <div class="col-sm-4">
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                    ________________________________ <br><br>
                    ________________________________
                  </div>

                </div><br><br>
                <p class="text-muted text-center">Thank you very much for doing business with us. We look forward to working with you!</p>
                
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>

      <!--main content end-->
<?php $this->load->view('layout/footer'); ?>