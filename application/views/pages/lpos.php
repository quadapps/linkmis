<?php 
$dets['page'] = 7; $dets['title'] = 'Purchase Orders';
$this->load->view('layout/header', $dets); 

$start = date('Y-m-d', strtotime('-30 days')); $end = date('Y-m-d');

if (!empty($params)) {
  $arr = explode('_', $params);
  $start = reset($arr); $end = end($arr);
}
$filter = '';
if(isset($_GET['f']) && !empty($_GET['f']))
  $filter = ' AND po_supplierid = '.$_GET['f'];

$Jobs = get_data('tbl_po', "JOIN tbl_suppliers ON supplier_id = po_supplierid WHERE po_active = 1 $filter AND DATE(po_createdate) BETWEEN '$start' AND '$end' ", 'po_id, po_creator, po_createdate, po_supplierid, po_status, po_currency, po_items, po_type, po_cancelremarks, supplier_name, CONCAT(supplier_phone, ",",supplier_email) AS cc');

function get_po_value($type, $items)
{
  $amt = $i =0;
  if($type == 1) {
    $arr = explode(',', $items);
    
    foreach($arr as $a) : 
      $items = explode('#', $a);
      $amt += $items[1]*$items[2];
      $i++;
    endforeach;
  }
  else {
    $items = json_decode($items, true);
    for($i=0; $i<count($items['desc']); $i++) : 
      if(isset($items['desc'][$i]))
        $amt += (int)$items['qty'][$i]*(float)$items['price'][$i];
    endfor;
  }

  return [$i, $amt];
}

?>


     <div class="app-content my-3 my-md-5">
          <div class="container-fluid">
            <!--  -->
            <div class="row">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title'];?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">
                    <li class="dropdown">
                      <a href="<?php echo site_url('welcome/view/create_so?t=2');?>" class="btn btn-secondary" ><i class="fa fa-plus"></i> Add New</a>
                    </li>
                    <li><a href="javascript: void()" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/view/lpos/')?>"><i class="fa fa-calendar"></i> <span><?php echo date('F j, Y', strtotime($start)). ' - '.date('F j, Y', strtotime($end));?></span> <b class="caret"></b></a>
                    </li>
                  </ul>
                </div>
                <div class="card-body">
                                  <div class="table-responsive">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                          <th>Date</th>
                        <th class="w-1">No</th>
                          <th>Supplier Name</th>
                          <th>Contacts</th>
                          <th>Status</th>
                          <th>Items</th>
                          <th>Value</th>
                          <th>Creator</th>
                          <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($Jobs as $d) { $value = get_po_value($d['po_type'], $d['po_items']); ?>
                        <tr <?php if($d['po_status'] == 0 && get_working_days($d['po_createdate'], date('Y-m-d H:i:s')) > 5) echo 'class="table-warning"';
                        if($d['po_status'] == -1 ) echo 'class="table-danger"';?>>
                          <td><?php echo $d['po_createdate'];?></td>
                          <td><span class="text-muted"><a href="<?php echo site_url('welcome/view/lpo/'.$d['po_id']);?>"><?php echo $d['po_id'];?></a></span></td>
                          <td><a href="<?php echo site_url('welcome/view/supplier_details/'.$d['po_supplierid']);?>" class="text-inherit"><?php echo $d['supplier_name'];?></a></td>
                          <td><?php echo $d['cc'];?></td>
                          <td><?php if($d['po_status'] == 0) echo '<span class="status-icon bg-warning"></span> Awaiting approval'; if($d['po_status'] == 1) echo  '<span class="status-icon bg-info"></span> Approved'; if($d['po_status'] == 2) echo  '<span class="status-icon bg-success"></span> Received'; if($d['po_status'] == -1) echo '<span class="status-icon bg-danger"></span> Cancelled<br>'.$d['po_cancelremarks']; ?></td>

                          <td><a href="#" data-toggle="modal" data-target="#ajaxModal" data-url="<?php echo site_url('welcome/modal/po_items/'.$d['po_id']);?>" data-title="PO Items"><?php echo $value[0];?></a></td>

                          <td><a href="#" data-toggle="modal" data-target="#ajaxModal" data-url="<?php echo site_url('welcome/modal/po_items/'.$d['po_id']);?>" data-title="PO Items"><?php echo $d['po_currency']. ' ' .number_format($value[1]);?></a></td>

                          <td><?php echo get_fullname($d['po_creator'])?></td>
                          <td class="ticket-actions">
                            
                            <div class="btn btn-group dropdown">
                              <button aria-expanded="false" aria-haspopup="true" class="btn btn-oblong btn-default btn-sm dropdown-toggle btn btn-sm" data-toggle="dropdown" type="button"><i class="fa fa-cogs"></i> Manage</button>
                              <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?php echo site_url('welcome/view/lpo/'.$d['po_id']);?>"><i class="fa fa-eye fa-fw"></i>Details</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#ajaxModal" data-url="<?php echo site_url('welcome/modal/po_items/'.$d['po_id']);?>" data-title="PO Items"><i class="fa fa-eye fa-fw"></i>View</a>

                                <div class="dropdown-divider"></div>
                                <?php if($d['po_status'] != 2) : ?>
                                  <a class="dropdown-item cancel" href="#" data-id="<?php echo $d['po_id']?>"><i class="fa fa-times text-danger fa-fw"></i>Cancel</a>
                                <?php endif; ?>

                                <?php if($d['po_status'] == 0) : ?>
                                  <a class="dropdown-item confirm" href="#" data-id="<?php echo $d['po_id']?>"><i class="fa fa-check text-success fa-fw"></i>Confirm</a>
                                <?php endif; ?>

                              </div>
                            </div>
                        
                          </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>

<!-- Modal -->
<div class="modal fade" id="conf-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Approve PO </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" enctype="multipart/form-data" action="<?php echo site_url('system/act/vendors_model/confirm_po')?>" class="ajax-form">
        <div class="modal-body">
        <div class="form-group">
          <label>Enter Supplier's Quotation Number/Invoice #</label>
          <input type="text" name="ref" required="" class="form-control">

          <input type="hidden" name="quote" id="quote">
        </div>

        <div class="form-group">
          <label>Estimated Delivery Time in Days</label>
          <input type="number" name="duration" required="" value="2" class="form-control">
        </div>

        <div class="form-row">
          <div class="col">
            <label>Mode of Delivery:</label><br>
            <input type="radio" name="collection_mode" value="Own"> Pickup
            <input type="radio" name="collection_mode" checked="" value="Delivery"> Delivery
          </div>
          <div class="col">
            <label>Attach Supplier Quotation/Invoice:</label><br>
            <input type="file" name="userfile" required="" /> 
          </div>
        </div>

        <div class="form-row">
            <div class="form-group col">
              <label>Expense/Ledger Head</label>
              <select name="cat" class="form-control">
                <?php foreach(get_data('tbl_expensecats', "WHERE active = 1") as $e) { ?>
                  <option value="<?php echo $e['id'];?>"><?php echo $e['name'];?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col">
              <label>Department</label>
              <select name="dept" class="form-control">
                <?php foreach(get_data('tbl_departments', "WHERE dept_active = 1") as $e) { ?>
                  <option value="<?php echo $e['dept_id'];?>"><?php echo $e['dept_name'];?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col">
              <label>mGurush Type</label>
              <select name="type" class="form-control">
                <option>Merchant</option>
                <option>Customer</option>
              </select>
            </div>
            <div class="form-group col">
              <label>mGurush No</label>
              <input type="number" name="acct" class="form-control" required="">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-4">
              <label>EPD <i class="fa fa-questionmark" title="Estimated Payment Date"></i></label>
              <input type="date" name="epd" class="form-control">
            </div>
            <div class="form-group col-8">
              <label>Remarks</label>
              <textarea name="remarks" class="form-control"></textarea>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="cancel-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cancel PO </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/vendors_model/cancel_po')?>" class="ajax-form">
        <div class="modal-body">
        <div class="form-group">
          <label>Why?</label>
          <textarea name="remarks" class="form-control" required=""></textarea>

          <input type="hidden" name="po" id="po">
        </div>

        

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.confirm').click(function() {
    var id = $(this).data('id');
    $('#quote').val(id);
    $('#conf-modal').modal('show');
  });


  $('.cancel').click(function() {
    var id = $(this).data('id');
    $('#po').val(id);
    $('#cancel-modal').modal('show');
  });

  function confirmQuote(id) {
    alert(id);
  }
</script>
<?php $this->load->view('layout/footer', array('dtt' => 1, 'dtp' => 1)); ?>