<?php 
$dets['page'] = 5; $dets['title'] = 'Payroll';
$this->load->view('layout/header', $dets); 
$user = get_row_data('tbl_users', 'user_id', $this->session->userdata('user_id'));

$start = date('Y-01-01');
$end = date('Y-m-d');

if (!empty($params)) {
  $arr = explode('_', $params);
  $start = reset($arr); $end = end($arr);
}

$salos = get_data('tbl_employees', "WHERE emp_active = 1 GROUP BY emp_currency", 'SUM(emp_salary+emp_incentive) AS total, emp_currency');
$pit = get_data('tbl_payslips', "WHERE ps_active = 1", 'SUM(ps_pit) AS total', true)['total'];

$ssp_pr = get_data('tbl_payroll', "LEFT OUTER JOIN tbl_payslips ON pr_id = ps_prid LEFT JOIN tbl_employees ON emp_id = ps_empid WHERE pr_active = 1 AND ps_active = 1 AND emp_currency = 'SSP' GROUP BY pr_id ", 'pr_month, pr_id, SUM(ps_due) AS payable, SUM(ps_pit) AS tax, SUM(ps_nssf) AS nssf, SUM(ps_allowances) AS allowances, SUM(ps_loans) AS loans');

$usd_pr = get_data('tbl_payroll', "LEFT OUTER JOIN tbl_payslips ON pr_id = ps_prid LEFT JOIN tbl_employees ON emp_id = ps_empid WHERE pr_active = 1 AND ps_active = 1 AND emp_currency = 'USD' GROUP BY pr_id ", 'pr_month, pr_id, SUM(ps_due) AS payable, SUM(ps_pit) AS tax, SUM(ps_nssf) AS nssf, SUM(ps_allowances) AS allowances, SUM(ps_loans) AS loans');

function get_pr_details($id, $currency)
{
  $data = get_data('tbl_payslips', "LEFT JOIN tbl_employees ON emp_id = ps_empid WHERE pr_active = 1 AND ps_active = 1 AND emp_currency = '$currency' AND ps_prid = $id ", 'SUM(ps_due) AS payable, SUM(ps_pit) AS tax, SUM(ps_nssf) AS nssf, SUM(ps_allowances) AS allowances, SUM(ps_loans) AS loans');

  return $data;
}

?>
     <div class="app-content my-3 my-md-5">
          <div class="container">
            
            <div class="row row-cards row-deck">
              <div class=" col-lg-12">
                <div class="row">
                  <div class="col-sm-12 col-lg-3">
                    <div class="card">
                      <div class="card-body">
                        <div class="card-value float-right text-purple"><i class="fe fe-user-plus"></i></div>
                        <h3 class="mb-1"><?php echo count(get_data('tbl_employees', "WHERE emp_active = 1", 'emp_id'));?></h3>
                        <div class="text-muted">Employees</div>
                      </div>
                      <div class="card-chart-bg">
                        <div id="chart-bg-users-1"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-3">
                    <div class="card">
                      <div class="card-body">
                        <div class="card-value float-right text-lightpink-red"><i class="fe fe-dollar-sign"></i></div>
                        <h3 class="mb-1"><?php echo number_format($salos[0]['total'], 2);?></h3>
                        <div class="text-muted">Total <?php echo $salos[0]['emp_currency'];?> Salaries</div>
                      </div>
                      <div class="card-chart-bg">
                        <div id="chart-bg-users-2" style="height: 100%"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-3">
                    <div class="card">
                      <div class="card-body">
                        <div class="card-value float-right text-orange"><i class="fe fe-dollar-sign"></i></div>
                        <h3 class="mb-1"><?php echo number_format($salos[1]['total'], 2);?></h3>
                        <div class="text-muted">Total <?php echo $salos[1]['emp_currency'];?> Salaries</div>
                      </div>
                      <div class="card-chart-bg">
                        <div id="chart-bg-users-3" style="height: 100%"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-3">
                    <div class="card">
                      <div class="card-body">
                        <div class="card-value float-right text-green"><i class="fa fa-diamond"></i></div>
                        <h3 class="mb-1"><?php echo number_format($pit, 2);?></h3>
                        <div class="text-muted">Total Income Tax</div>
                      </div>
                      <div class="card-chart-bg">
                        <div id="chart-bg-users-4" style="height: 100%"></div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">

                    <li>
                      <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#bank-modal"><i class="fa fa-plus"></i> Create Payroll</a>
                    </li>

                    <li>
                      <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#file-modal"><i class="fa fa-file"></i> Create Allowances</a>
                    </li>

                    <li>
                      <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#salos-modal"><i class="fa fa-file"></i> Update Salaries</a>
                    </li>
                    
                    <!-- <li class="dropdown">
                    <a aria-expanded="false" aria-haspopup="true" href="javascript: void()" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-plus"></i> New Transaction</a>
                              <div class="dropdown-menu">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal1">Deposit</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal">Withdrawal</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#enq-modal2">Banking Charges</a>
                              </div>
                    </li> -->

                    <!-- <li>
                      <a class="btn btn-secondary" id="reportrange" href="javascript: void()" data-url="<?php echo site_url('welcome/view/banking')?>"><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end)) ;?></a>
                    </li> -->
                    
                  </ul>
                </div>
                <div class="card-body p-6">
                    <div class="panel panel-primary">
                      <div class=" tab-menu-heading">
                        <div class="tabs-menu1 ">
                          <!-- Tabs -->
                          <ul class="nav panel-tabs">
                            <li class=""><a href="#tab5" class="active" data-toggle="tab">SSP Payroll</a></li>
                            <li><a href="#tab6" data-toggle="tab">USD Payroll</a></li>
                            <li><a href="#tab7" data-toggle="tab">Allowances</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="panel-body tabs-menu-body">
                        <div class="tab-content">
                          <div class="tab-pane active " id="tab5">
                            <div class="table-responsive">
                              <table class="table table-hover table-outline table-vcenter text-nowrap card-table data-table-buttons">
                                <thead>
                                  <tr>
                                    
                                    <th>NO</th>
                                    <th>Month</th>
                                    <th>Total Payable</th>
                                    <th>Total Allowances</th>
                                    <th>Total Taxes</th>
                                    <th>Total NSSF</th>
                                    <th>Total Loans</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  
                                  <?php foreach($ssp_pr as $b) : ?>
                                    <tr>
                                    
                                    <td >
                                      <div><a href="<?php echo site_url('welcome/view/payslips/?c=SSP&f='.$b['pr_id'].'&m='.$b['pr_month']);?>">LPL/HR/SSP-PR/<?php echo $b['pr_id'];?></a></div>
                                    </td>
                                    <td >
                                      <div> <?php echo $b['pr_month'];?> </div>
                                    </td>

                                    <td >
                                      <div><?php echo number_format($b['payable'], 2);?></div>
                                    </td>
                                    <td >
                                      <div><?php echo number_format($b['allowances'], 2);?></div>
                                    </td>
                                    <td >
                                      <div><?php echo number_format($b['tax'], 2);?></div>
                                    </td>
                                    <td>
                                      <!-- <div class="small text-muted">Last login</div> -->
                                      <div><?php echo number_format($b['nssf'], 2);?></div>
                                    </td>
                                    <td class="">
                                      <div><?php echo number_format($b['loans'], 2);?></div>
                                    </td>
                                    <td class="">
                                      <button class="btn btn-danger btn-sm delete-payroll" data-id="<?php echo $b['pr_id'];?>"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                  </tr>
                                  <?php endforeach; ?>
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="tab-pane " id="tab6">
                            <div class="table-responsive">
                              <table class="table table-hover table-outline table-vcenter text-nowrap card-table data-table-buttons">
                                <thead>
                                  <tr>
                                    
                                    <th>NO</th>
                                    <th>Month</th>
                                    <th>Total Payable</th>
                                    <th>Total Allowances</th>
                                    <th>Total Taxes</th>
                                    <th>Total NSSF</th>
                                    <th>Total Loans</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  
                                  <?php foreach($usd_pr as $b) : ?>
                                    <tr>
                                    
                                    <td >
                                      <div><a href="<?php echo site_url('welcome/view/payslips/?c=USD&f='.$b['pr_id'].'&m='.$b['pr_month']);?>">LPL/HR/USD-PR/<?php echo $b['pr_id'];?></a></div>
                                    </td>
                                    <td >
                                      <div> <?php echo $b['pr_month'];?> </div>
                                    </td>

                                    <td >
                                      <div><?php echo number_format($b['payable'], 2);?></div>
                                    </td>

                                    <td >
                                      <div><?php echo number_format($b['allowances'], 2);?></div>
                                    </td>
                                    <td >
                                      <div><?php echo number_format($b['tax'], 2);?></div>
                                    </td>
                                    <td>
                                      <!-- <div class="small text-muted">Last login</div> -->
                                      <div><?php echo number_format($b['nssf'], 2);?></div>
                                    </td>
                                    <td class="">
                                      <div><?php echo number_format($b['loans'], 2);?></div>
                                    </td>
                                    <td class="">
                                      <button class="btn btn-danger btn-sm delete-payroll" data-id="<?php echo $b['pr_id'];?>"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                  </tr>
                                  <?php endforeach; ?>
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>

                          <div class="tab-pane " id="tab7">
                            <div class="table-responsive">
                              <table class="table table-vcenter table-hover card-table data-table-buttons">
                                <thead>
                                  <tr>
                                    <th>NO</th>
                                    <th>Employee</th>
                                    <th>Name</th>
                                    <th>Amount</th>
                                    <th>Created On</th>
                                    <th>Created By</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach(get_data('tbl_allowances', "JOIN tbl_employees ON emp_id = allowance_empid WHERE allowance_active = 1") as $a) : ?>
                                    <tr>
                                      <td><?php echo $a['emp_id'];?></td>
                                      <td><?php echo $a['emp_fullname'];?></td>
                                      <td><?php echo $a['allowance_name'];?></td>
                                      <td><?php echo $a['emp_currency'].' '.$a['allowance_amt'];?></td>
                                      <td><?php echo $a['allowance_createdate'];?></td>
                                      <td><?php echo get_fullname($a['allowance_creator']);?></td>
                                      <td>
                                        <button class="btn btn-danger btn-sm" onclick="remove_things('allowances', <?php echo $a['allowance_id'];?>)"><i class="fa fa-trash-o"></i></button>
                                      </td>
                                    </tr>
                                  <?php endforeach; ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
              
            </div>
          </div>

<?php //$banks = get_data('tbl_banks', "WHERE bank_active = 1"); ?>
<!-- Modal -->

  <div class="modal fade" id="bank-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">New Payroll</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" enctype="multipart/form-data" method="post" action="<?php echo site_url('system/act/payroll_model/create')?>">
      <div class="modal-body">
          
          <div class="form-group">
            <label>Year</label>
            <input type="text" class="form-control" name="year" value="<?php echo date('Y');?>" required=""/>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Month</label>
            <select name="month" class="form-control">
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
          </div>
          
          <div class="form-group">
            <label>USD Exchange Rate</label>
            <input type="number" class="form-control " name="rate" placeholder="605" required=""/>
          </div>
          
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

  <div class="modal fade" id="file-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Create Allowance</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-form" method="post" action="<?php echo site_url('system/act/payroll_model/create_allowance')?>">
      <div class="modal-body">


          <div class="form-group">
            <label>Select Employee</label>
            <select name="emp" class="form-control">
              <?php foreach(get_data('tbl_employees', "WHERE emp_active = 1") as $b) { ?>
                <option value="<?php echo $b['emp_id'];?>"><?php echo $b['emp_fullname'];?></option>
              <?php } ?>
            </select>
          </div>
          
          <div class="form-group">
            <label for="inputPassword3">Allowance Name:</label>
            <input type="text" class="form-control" name="name" required="" />
          </div>

          <div class="form-group">
            <label for="inputPassword3">Allowance Amount:</label>
            <input type="number" step="any" class="form-control" name="amt" required="" />
          </div>
          
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Create</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

  <div class="modal fade" id="salos-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Update Salaries</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" class="ajax-formZ" method="post" action="<?php echo site_url('system/act/payroll_model/upload_salos')?>" enctype="multipart/form-data">
      <div class="modal-body">


          <div class="form-group">
            <label>Select File</label>
            <input type="file" name="import">
          </div>
          
          <a class="btn btn-link" href="<?php echo base_url('sample_salaries.csv');?>">Download Sample</a>
          
          </div>      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Create</button>
          </div>
      </form> 
    </div>
  </div>
  </div>

<script type="text/javascript">
  $('.delete-payroll').click(function() {
    let id = $(this).data('id');
    if(confirm('Are you sure?')) {
      $.post('<?php echo site_url('system/act/payroll_model/delete_payroll');?>', {pr_id : id}).done(function(r) {
        swal('Deleted');
        location.reload();
      })
    }
  })
</script>

<?php $this->load->view('layout/footer', array('dtt' => 1, 'sel' => 1, 'dtp' => 1)); ?>