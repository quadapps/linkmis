<?php 
$dets['page'] = 7; $dets['title'] = 'Payslip';
$this->load->view('layout/header', $dets); 
//$id = $params;
$data = get_data('tbl_payslips', "JOIN tbl_employees ON emp_id = ps_empid JOIN tbl_payroll ON pr_id = ps_prid WHERE ps_id = $params", 'tbl_payslips.*, pr_month, emp_fullname, emp_designation', true);

$des = get_that_data('tbl_employees', 'emp_designation', 'emp_id', $this->session->userdata('emp_id'));
?>

<style type="text/css">
.hide-on-screen {display:none;}
/*.yesPrint, .noPrint {display:block;}*/

/*td, th {
  font-size: 16px !important;
  font-weight: bold;
}

table {
  border-style: solid;
  border-width:3px !important;
}*/

@media print {
   /*.noPrint {display:none;}*/
   .hide-on-screen {display:block;}
   td, th {
      font-size: 16px !important;
      font-weight: bold;
    }

    table {
      border-style: solid;
      border-width:3px !important;
    }
    p, address, div, strong, .h3 {
      font-size: 16px !important;
      font-weight: bold;
    }
}
</style>
     
     <div class="app-content my-3 my-md-5">
          <div class="container">
            
            <div class="row">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                  </div>
                  <ul class="card-options panel_toolbox">
                                       
                    <li>
                      <a href="#" class="btn btn-default" onclick="javascript:window.print();"><i class="si si-printer"></i> Print</a>
                    </li>
                    <!--<li>
                      <a href="#" class="btn btn-default" onclick="javascript:window.print();"><i class="si si-save"></i> Save & Close</a>
                    </li>-->
                    
                  </ul>
                </div>
                <div class="card-body">
                  <div class="clearfix"></div> <br><br>
                    <div class="row "> <br>
                  <div class="col-sm-6 ">
                    <img class="hide-on-screen" src="<?php echo base_url('res/assets/images/lp_logo.png');?>"> 
                  </div>
                  <div class="col-sm-6 ">
                    
                    <address class="rounded border"><strong>LINKS PAY LTD</strong><br><br>
                     
                      HAI MALAKAL, JUBA, SOUTH SUDAN<br>
                      Mobile No : <strong>+211929321060</strong><br>
                      Email ID: <strong>info@links-pay.com</strong>
                    </address>
                  </div>
                </div>

                <div class=" text-dark">
                  <p class="mb-1 mt-5"><span class="font-weight-semibold"> Payslip for the period :</span> <?php echo date('F, Y', strtotime($data['pr_month']))?></p>
                  
                </div>
                <div class="table-responsive push row">
                  <div class="col-sm-12">
                  	<table class="table table-bordered">
	                  	<tr>
	                  		<th>Employee NO</th>
	                  		<td>LPL<?php echo $data['ps_empid'];?></td>
	                  		<th>Employee Name</th>
	                  		<td><?php echo $data['emp_fullname'];?></td>
	                  	</tr>
	                  	<tr>
	                  		<th>Days Worked</th>
	                  		<td><?php echo get_worked_days($data['ps_empid'], $data['pr_month']);?></td>
	                  		<th>Designation</th>
	                  		<td><?php echo $data['emp_designation'];?></td>
	                  	</tr>
	                  </table>
                  </div>
                </div>

                <div class="table-responsive push row">
                  <div class="col-sm-12">
                  	<table class="table table-bordered">
	                  	<thead>
	                  		<th>Description</th>
	                  		<th class="text-right">Earnings</th>
	                  		<th class="text-right">Deductions</th>
	                  	</thead>
	                  	<tbody>
	                  		<tr>
	                  			<td>Basic Salary</td>
	                  			<td class="text-right"><?php echo $data['ps_basic'];?></td>
	                  			<td></td>
	                  		</tr>
	                  		<tr>
	                  			<td>Incentive</td>
	                  			<td class="text-right"><?php echo $data['ps_incentive'];?></td>
	                  			<td></td>
	                  		</tr>
	                  		<tr>
	                  			<td>Allowances</td>
	                  			<td class="text-right"><?php echo $data['ps_allowances'];?></td>
	                  			<td></td>
	                  		</tr>
	                  		<tr>
	                  			<td>PIT (15%)</td>
	                  			<td></td>
	                  			<td class="text-right"><?php echo $data['ps_pit'];?></td>
	                  		</tr>
	                  		<tr>
	                  			<td>Social Security (8%)</td>
	                  			<td></td>
	                  			<td class="text-right"><?php echo $data['ps_nssf'];?></td>
	                  		</tr>
	                  		<tr>
	                  			<td>Loans/Advances</td>
	                  			<td></td>
	                  			<td class="text-right"><?php echo $data['ps_loans'];?></td>
	                  		</tr>
	                  		<tr>
	                  			<td>Total</td>
	                  			<td class="text-right"><?php echo number_format(((float)$data['ps_allowances']+(float)$data['ps_incentive']+(float)$data['ps_basic']), 2);?></td>
	                  			<td class="text-right"><?php echo number_format(((float)$data['ps_pit']+(float)$data['ps_nssf']+(float)$data['ps_loans']), 2);?></td>
	                  		</tr>
	                  		<tr>
	                  			<th></th>
	                  			<td>Net Pay</td>
	                  			<td class="text-right"><?php echo number_format(((float)$data['ps_due']+(float)$data['ps_allowances']), 2);?></td>
	                  		</tr>
	                  	</tbody>
	                  </table>
                  </div>
                </div>
                
                

                </div><br><br>
                <!-- <p class="text-muted text-center">Thank you very much for doing business with us. We look forward to working with you!</p> -->
                
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>

      <!--main content end-->
<?php $this->load->view('layout/footer'); ?>