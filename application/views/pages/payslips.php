<?php
$currency = isset($_GET['c']) ? $_GET['c'] : 'SSP';
$dets = ['title' => $currency.' Pay Roll : '.$_GET['m'], 'page' => 5];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');

$user = get_data('tbl_users', "JOIN tbl_employees ON emp_id = user_empid WHERE user_id = $uid", 'emp_designation, user_levelid, user_empid', true);

$and = " AND emp_id = ".$user['user_empid'];
if($user['user_levelid'] < 3 || ($user['emp_designation'] == 'HR MANAGER' || $user['emp_designation'] == 'FINANCE MANAGER'))
$and = '';
if($user['emp_designation'] == 'FINANCE MANAGER')
$and = ' AND ps_status > 0';

$pr_id = $_GET['f'];
$data = get_data('tbl_payslips', "JOIN tbl_employees ON emp_id = ps_empid JOIN tbl_payroll ON pr_id = ps_prid WHERE ps_prid = $pr_id AND ps_active = 1 AND emp_currency = '$currency' $and ", 'tbl_payslips.*, pr_month, emp_fullname, emp_designation');


?>

<div class="my-3 my-md-5">
					<div class="container">
						
																		
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<?php if($user['emp_designation'] == 'HR MANAGER') : ?>
												<li>
													<a href="javascript:void()" class="btn btn-secondary" data-status="1" id="process-slips" ><i class="fa fa-thumbs-up"></i> Process</a>
												</li>
											<?php endif; ?>
											<?php if($user['emp_designation'] == 'FINANCE MANAGER') : ?>
												<li>
													<a href="javascript:void()" class="btn btn-secondary" data-status="2" id="pay-slips" ><i class="fa fa-money"></i> Pay</a>
												</li>
											<?php endif; ?>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter">
											<thead>
												<tr>
													<th><input type="checkbox" name="" id="checkAll" > Emp ID</th>
													<th>Full Name</th>
													<th>Designation</th>
													<th>Basic Pay</th>
													<th>Incentive</th>
													<th>Worked Days</th>
													<th>Taxable Income</th>
													<th>NSSF</th>
													<th>Taxable Amount</th>
													<th>PIT</th>
													<th>Loans/Advances</th>
													<th>Total Deductions</th>
													<th>Allowances</th>
													<th>Total Pay</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody id="payslips">
												
												<?php foreach($data as $d) : ?>
													<tr>
														
														<td><input type="checkbox" name="" class="chk-process" value="<?php echo $d['ps_id'];?>"> <?php echo $d['ps_empid'];?></td>
														<td><?php echo $d['emp_fullname'];?></td>
														<td><?php echo $d['emp_designation'];?></td>
														<td><?php echo $d['ps_basic'];?></td>
														<td><?php echo $d['ps_incentive'];?></td>
														<td><?php echo get_worked_days($d['ps_empid'], $d['pr_month']);?></td>
														<td><?php echo $d['ps_taxableincome'];?></td>
														<td><?php echo $d['ps_nssf'];?></td>
														<td><?php echo $d['ps_taxableamt'];?></td>
														<td><?php echo $d['ps_pit'];?></td>
														<td><?php echo $d['ps_loans'];?></td>
														<td><?php echo $d['ps_nssf']+$d['ps_pit']+$d['ps_loans'];?></td>
														<td><?php echo $d['ps_allowances'];?></td>
														<td><?php echo (float)$d['ps_due']+(float)$d['ps_allowances'];?></td>
														<td>
															<?php 
															if($d['ps_status'] == 0) echo '<span class="status-icon bg-danger"></span>Pending';
															if($d['ps_status'] == 1) echo '<span class="status-icon bg-primary"></span>Processed'; 
															if($d['ps_status'] == 2) echo '<span class="status-icon bg-success"></span>Paid';
															?>
																
														</td>
														<td class="btn-group">
															<?php if($d['ps_status'] == 1) : ?>
																<a class="btn btn-secondary btn-sm " href="<?php echo site_url('welcome/view/payslip/'.$d['ps_id']);?>" title="Print"><i class="fa fa-print"></i></a>

																<?php if($user['emp_designation'] == 'FINANCE MANAGER') : ?>
																	<button class="btn btn-primary btn-sm pay-slip" data-id="<?php echo $d['ps_id'];?>" data-status="2" title="Process"><i class="fa fa-thumbs-up"></i></button>
																<?php endif; ?>
															<?php endif; ?>

															<?php if($user['emp_designation'] == 'HR MANAGER') : ?>
																<button class="btn btn-primary btn-sm process" data-id="<?php echo $d['ps_id'];?>" data-status="1" title="Process"><i class="fa fa-thumbs-up"></i></button>
															<?php endif; ?>
															
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<script type="text/javascript">
	$('#process-slips, #pay-slips').click(function(e) {
		let status = $(this).data('status');
		let selecteditems = [];

		$("#payslips").find("input:checked").each(function (i, ob) { 
		    selecteditems.push($(ob).val());
		});
		if(selecteditems.length == 0) {
			swal('Please select at least one item');
		}
		else {
			let str_items = selecteditems.join(',');
			if(confirm("You are about to process "+selecteditems.length+" payslips. Continue?")) {
				process_slips(str_items, status);
			}
		}
	});

	$('.process, .pay-slip').click(function() {
		let str_items = $(this).data('id');
		let status = $(this).data('status');
		if(confirm("You are about to process 1 payslip. Continue?")) {
			process_slips(str_items, status);
		}
	});

	function process_slips(str_items, status) {
		$.post('<?php echo site_url('system/act/payroll_model/process')?>', {slips:str_items, status: status}).done(function(r) {
					swal(r+" Paylips processed");
					location.reload();
				}).fail(function(e,r) {
					swal("error", "Error: "+e.responseText);
				});
	}

	$("#checkAll").click(function(){
	    $("#payslips").find('input:checkbox').not(this).prop('checked', this.checked);
	});
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>