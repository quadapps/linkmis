<?php
$h= ['title' => 'Profile', 'page' => 5];
$this->load->view('layout/header', $h);
$uid = $this->session->userdata('user_id');

$Jobs = get_data('tbl_quotations', "JOIN tbl_clients ON client_id = quotation_clientid WHERE quotation_active = 1 ", 'quotation_id, quotation_creator, quotation_createdate, quotation_clientid, quotation_status, quotation_currency, quotation_items, client_name, client_marketerid, client_phone');
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Profile</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Pages</a></li>
								<li class="breadcrumb-item active" aria-current="page">Profile</li>
							</ol>
						</div> -->
						<div class="row">
							<div class="col-md-12">
								<div class="card card-profile "  style="background-image: url(assets/images/photos/1.jpg); background-size:cover;">
									<div class="card-body text-center">
										<img class="card-profile-img" src="assets/images/faces/male/16.jpg" alt="img">
										<h3 class="mb-3 text-white">George Mestayer</h3>
										<p class="mb-4 text-white">Vobilet Administrator</p>
										<button class="btn btn-primary btn-sm">
											<span class="fa fa-twitter"></span> Follow
										</button>
										<a href="./editprofile.html" class="btn btn-success btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> Edit profile</a>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card p-5 ">
									<div class="card-title">
										Contact &amp; Personal Info
									</div>
									<div class="media-list">
										<div class="media mt-1 pb-2">
											<div class="mediaicon">
												<i class="fa fa-link" aria-hidden="true"></i>
											</div>
											<div class="media-body ml-5 mt-1">
												<h6 class="mediafont text-dark">Websites</h6><a class="d-block" href="">http://vobilet.com</a> <a class="d-block" href="">http://vobilet.net</a>
											</div>
											<!-- media-body -->
										</div>
										<!-- media -->
										
										<!-- media -->
										<div class="media mt-1 pb-2">
											<div class="mediaicon">
												<i class="fa fa-envelope-o" aria-hidden="true"></i>
											</div>
											<div class="media-body ml-5 mt-1">
												<h6 class="mediafont text-dark">Email Address</h6><span class="d-block">georgemestayer@vobilet.com</span>
											</div>
											<!-- media-body -->
										</div>
										<!-- media -->
										<div class="media mt-1 pb-2">
											<div class="mediaicon">
												<i class="fa fa-twitter" aria-hidden="true"></i>
											</div>
											<div class="media-body ml-5 mt-1">
												<h6 class="mediafont text-dark">Twitter</h6><a class="d-block" href="#">@vobilet</a>
											</div>
											<!-- media-body -->
										</div>
										<!-- media -->
									</div>
									<!-- media-list -->
								</div>
								
								<div class="card">
									<div class="card-header pt-5 pb-5">
										<div class="input-group">
											<input type="text" class="form-control" placeholder="Message">
											<div class="input-group-append">
												<button type="button" class="btn btn-primary">
													<i class="fa fa-search" aria-hidden="true"></i>
												</button>
											</div>
										</div>
									</div>
									<ul class="list-group card-list-group">
										<li class="list-group-item py-5">
											<div class="media m-0">
												<div class="media-object avatar brround avatar-md mr-4" style="background-image: url(assets/images/faces/male/16.jpg)"></div>
												<div class="media-body">
													<div class="media-heading">
														<small class="float-right text-muted">4 min</small>
														<h5><strong>George Mestayer</strong></h5>
													</div>
													<div>
														Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet
													</div>
													<ul class="media-list">
														<li class="media mt-4">
															<div class="media-object brround avatar mr-4" style="background-image: url(assets/images/faces/female/17.jpg)"></div>
															<div class="media-body">
																<strong>Holley Corby: </strong>
																I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you 
															</div>
														</li>
														
													</ul>
												</div>
											</div>
										</li>
										<li class="list-group-item py-5">
											<div class="media m-0">
												<div class="media-object avatar brround avatar-md mr-4" style="background-image: url(assets/images/faces/male/16.jpg)"></div>
												<div class="media-body">
													<div class="media-heading">
														<small class="float-right text-muted">34 min</small>
														<h5><strong>George Mestayer</strong></h5>
													</div>
													<div>
														 Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus</div>
													<ul class="media-list">
														<li class="media mt-4">
															<div class="media-object brround avatar mr-4" style="background-image: url(assets/images/faces/male/32.jpg)"></div>
															<div class="media-body">
																<strong>Art Langner: </strong>
																master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure
															</div>
														</li>
													</ul>
												</div>
											</div>
										</li>
									</ul>
								</div>
								
								<div class="card">
									<div class="card-body">
										<div class="card-box tilebox-one">
											<i class="icon-layers float-right text-muted"><i class="fa fa-cubes text-success" aria-hidden="true"></i></i>
											<h6 class="text-drak text-uppercase mt-0">Projects</h6>
											<h2 class="m-b-20">678</h2>
											<span class="badge badge-success"> +78% </span> <span class="text-muted">From previous period</span>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-body">
										<div class="card-box tilebox-one">
											<i class="icon-layers float-right text-muted"><i class="fa fa-bar-chart text-secondary" aria-hidden="true"></i></i>
											<h6 class="text-drak text-uppercase mt-0">Profits</h6>
											<h2 class="m-b-20">7,908</h2>
											<span class="badge badge-secondary"> +66% </span> <span class="text-muted">Last year</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-8">
								<div class="card">
									<div class="card-body">
										<div class=" " id="profile-log-switch">
											<div class="fade show active " >
												<div class="table-responsive border ">
													<table class="table row table-borderless w-100 m-0 ">
														<tbody class="col-lg-6 p-0">
															<tr>
																<td><strong>Full Name :</strong> George Mestayer</td>
															</tr>
															<tr>
																<td><strong>Location :</strong> USA</td>
															</tr>
															<tr>
																<td><strong>Languages :</strong> English, German, Spanish.</td>
															</tr>
														</tbody>
														<tbody class="col-lg-6 p-0">
															<tr>
																<td><strong>Website :</strong> vobilet.com</td>
															</tr>
															<tr>
																<td><strong>Email :</strong> georgemestayer@vobilet.com</td>
															</tr>
															<tr>
																<td><strong>Phone :</strong> +125 254 3562 </td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="row mt-5 profie-img">
													<div class="col-md-12">
														<div class="media-heading">
														<h5><strong>Biography</strong></h5>
													</div>
													<p>
														 Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus</p>
													<p >because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure.</p>
													</div>
													<img class="img-fluid rounded w-25 h-25 m-2" src="assets/images/photos/8.jpg " alt="banner image">
													<img class="img-fluid rounded w-25 h-25 m-2" src="assets/images/photos/10.jpg" alt="banner image ">
													<img class="img-fluid rounded w-25 h-25 m-2" src="assets/images/photos/11.jpg" alt="banner image ">
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Recent Projects</h3>
									</div>
									<div class="card-body">
										<div class="table-responsive">
										<table class="table card-table table-vcenter border text-nowrap">
											<thead>
												<tr>
													<th>Project Name</th>
													<th>Date</th>
													<th>Status</th>
													<th>Price</th>
												</tr>
											</thead>
											<tbody>
												
												<tr>
													<td><a href="store.html" class="text-inherit">Untrammelled prevents </a></td>
													<td>28 May 2018</td>
													<td><span class="status-icon bg-success"></span> Completed</td>
													<td>$56,908</td>
													
												</tr>
												<tr>
													<td><a href="store.html" class="text-inherit">Untrammelled prevents</a></td>
													<td>12 June 2018</td>
													<td><span class="status-icon bg-danger"></span> On going</td>
													<td>$45,087</td>
													
												</tr>
												<tr>
													<td><a href="store.html" class="text-inherit">Untrammelled prevents</a></td>
													<td>12 July 2018</td>
													<td><span class="status-icon bg-warning"></span> Pending</td>
													<td>$60,123</td>
													
												</tr>
												<tr>
													<td><a href="store.html" class="text-inherit">Untrammelled prevents</a></td>
													<td>14 June 2018</td>
													<td><span class="status-icon bg-warning"></span> Pending</td>
													<td>$70,435</td>
													
												</tr>
												<tr>
													<td><a href="store.html" class="text-inherit">Untrammelled prevents</a></td>
													<td>25 June 2018</td>
													<td><span class="status-icon bg-success"></span> Completed</td>
													<td>$15,987</td>
													
												</tr>
											</tbody>
										</table>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>