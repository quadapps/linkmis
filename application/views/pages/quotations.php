<?php
$h= ['title' => 'Quotations', 'page' => 2];
$this->load->view('layout/header', $h);
$uid = $this->session->userdata('user_id');

$Jobs = get_data('tbl_quotations', "JOIN tbl_clients ON client_id = quotation_clientid WHERE quotation_active = 1 ", 'quotation_id, quotation_creator, quotation_createdate, quotation_clientid, quotation_status, quotation_currency, quotation_items, client_name, client_marketerid, client_phone');
?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $h['title']?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="<?php echo site_url('welcome/view/create_quote');?>" class="btn btn-primary" ><i class="fa fa-plus"></i> Add New</a>
											</li>
										</ul>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table id="example" class="table card-table table-vcenter">
												<thead>
							                      <tr>
							                        <th class="w-1">No</th>
							                          <th>Client Name</th>
							                          <th>Phone</th>
							                          <th>Marketer</th>
							                          <th>Date</th>
							                          <th>Status</th>
							                          <th>Items</th>
							                          <th></th>
							                      </tr>
							                    </thead>
							                    <tbody>
							                      <?php foreach ($Jobs as $d) { ?>
							                        <tr <?php if($d['quotation_status'] == 0 && get_working_days($d['quotation_createdate'], date('Y-m-d H:i:s')) > 5) echo 'class="table-warning"';
							                        if($d['quotation_status'] == -1 ) echo 'class="table-danger"';?>>
							                          <td><span class="text-muted"><a href="<?php echo site_url('welcome/view/quotation/'.$d['quotation_id']);?>"><?php echo $d['quotation_id'];?></a></span></td>
							                          <td><a href="<?php echo site_url('welcome/view/client_details/'.$d['quotation_clientid']);?>" class="text-inherit"><?php echo $d['client_name'];?></a></td>
							                          <td><?php echo $d['client_phone'];?></td>
							                          <td><?php echo get_that_data('tbl_employees', 'emp_fullname', 'emp_id', 'client_marketerid');?></td>
							                          <td><?php echo $d['quotation_createdate'];?></td>
							                          <td><?php if($d['quotation_status'] == 0) echo '<span class="status-icon bg-warning"></span> Awaiting confirmation'; if($d['quotation_status'] == 1) echo  '<span class="status-icon bg-success"></span> Confirmed'; if($d['quotation_status'] == -1) echo '<span class="status-icon bg-danger"></span> Not Confirmed'; ?></td>
							                          <td><?php echo count(json_decode($d['quotation_items'], true)['desc'])?></td>
							                          <td class="ticket-actions">
							                            
							                            <div class="btn btn-group dropdown">
							                              <button aria-expanded="false" aria-haspopup="true" class="btn  btn-secondary btn-sm dropdown-toggle btn btn-sm" data-toggle="dropdown" type="button"><i class="fa fa-cogs"></i> Action</button>
							                              <div class="dropdown-menu">
							                                <a class="dropdown-item" href="<?php echo site_url('welcome/view/quotation/'.$d['quotation_id']);?>"><i class="fa fa-eye fa-fw"></i>Details</a>
							                                <a class="dropdown-item cancel" href="#" data-id="<?php echo $d['quotation_id']?>"><i class="fa fa-times text-danger fa-fw"></i>Cancel</a>
							                                <div class="dropdown-divider"></div>
							                                <a class="dropdown-item" href="#"><i class="fa fa-flag fa-fw"></i>Flag</a>
							                                <?php if($d['quotation_status'] == 0) : ?>
							                                <a class="dropdown-item confirm" href="#" data-id="<?php echo $d['quotation_id']?>"><i class="fa fa-check text-success fa-fw"></i>Confirm</a>
							                                <?php endif; ?>
							                              </div>
							                            </div>
							                        
							                          </td>
							                      </tr>
							                      <?php } ?>
							                    </tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

<!-- Modal -->
<div class="modal fade" id="conf-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm Quotation #<?php echo $params?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/finance_model/confirm_quote')?>" class="ajax-form">
        <div class="modal-body">
        <div class="form-group">
          <label>Enter PO Number/Email ID used to confirm/RFQ Number as per Quotation</label>
          <input type="text" name="po" required="" class="form-control">

          <input type="hidden" name="quote" id="quote">
        </div>

        <div class="form-group">
          <label>Remarks</label>
          <textarea class="form-control" name="remarks"></textarea>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="cancel-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cancel Quotation #<?php echo $params?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/finance_model/cancel_quote')?>" class="ajax-form">
        <div class="modal-body">
        <div class="form-group">
          <label>Why?</label>
          <textarea name="remarks" class="form-control" required=""></textarea>

          <input type="hidden" name="quote" id="quotation">
        </div>

        

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.confirm').click(function() {
    var id = $(this).data('id');
    $('#quote').val(id);
    $('#conf-modal').modal('show');
  });


  $('.cancel').click(function() {
    var id = $(this).data('id');
    $('#quotation').val(id);
    $('#cancel-modal').modal('show');
  });

  function confirmQuote(id) {
    alert(id);
  }
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>