<?php 
$deta['page'] = 7; $deta['title'] = 'Requisition Details: '.$params;
$this->load->view('layout/header', $deta);

$req = get_data('tbl_reqs', "JOIN tbl_departments ON dept_id = req_deptid WHERE req_id = $params", 'dept_name, tbl_reqs.*', true);

$user = get_data('tbl_users', "WHERE user_id = ".$this->session->userdata('user_id'), 'user_levelid, user_empid', true);

$emp = get_data('tbl_employees', "WHERE emp_id = ".$this->session->userdata('emp_id'), 'emp_id, emp_designation', true);

function get_requestfile($which, $req_id)

{

    $data = get_data('tbl_requestfiles', "WHERE requestfiles_type LIKE '$which' AND requestfiles_requesttype = 1 AND requestfiles_requestid = $req_id");

    //if(count($data) == 0) return false;

    $ret = array();

    foreach($data as $d) {

        $ret = $d;

    }

    

    return $ret;

}
?>

<style type="text/css">
.hide-on-screen {display:none;}
/*.yesPrint, .noPrint {display:block;}*/

td, th {
  font-size: 16px !important;
  font-weight: bold;
}

table, td, th, tr {
  border-style: solid;
  border-width:1px !important;
}

@media print {
   .hide-on-screen {display:block;}
   .no-print, .no-print *
    {
        display: none !important;
    }
    td, th {
      font-size: 16px !important;
      font-weight: bold;
    }

    table {
      border-style: solid;
      border-width:3px !important;
    }
   p, address, div, strong, .h3 {
      font-size: 16px !important;
      font-weight: bold;
    }
}
</style>
      
      <div class="app-content my-3 my-md-5">
          <div class="container">
            <div class="page-header">
              <div class="row">
                  <div class="col-md-12">
                      <img class="hide-on-screen" src="<?php echo base_url('res/assets/images/lp_logo.png');?>"> <div class="clearfix"></div>
                  </div>
                  <div class="col-md-12">
                      <h4 class="page-title"><?php echo $deta['title'];?></h4>
                  </div>
              </div>

            </div>
            <div class="row">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="white-panel pn">                                        
                                            
                                        

                                    <ul class="card-options panel_toolbox no-print">
                    
                    
                    <li>
                      <a href="javascript: window.print()" class="btn btn-default" ><i class="fa fa-print"></i> Print</a>
                    </li>
                    
                    <li>
                        <a href="#" class="btn btn-default" data-toggle="modal" data-target="#file-modal"><i class="fa fa-file"></i> Attach File</a>
                    </li>
                    
                  </ul>
                                    </div>
                                    <div class="card-body">
                                        <h3>Employee: <?php echo get_fullname($req['req_creator']);?> </h3>
                                        <p><strong>Department: </strong> <?php echo $req['dept_name'];?></p>
                                        <div><strong>Items Requested: </strong> 
                                                <?php if($req['req_type'] == 1) : ?>
                                                <table class="table table-hover">
                                                    <thead>
                                                        <th>Qty</th>
                                                        <th>Description</th>
                                                    </thead>
                                                    <tbody>
                                                        <?php $data = json_decode($req['req_items'], true); 
                                                        for($i=0; $i<count($data['desc'] ); $i++) : ?>
                                                        <tr>
                                                            <td><?php echo $data['qty'][$i];?></td>
                                                            <td><?php echo $data['desc'][$i];?></td>
                                                        </tr>
                                                        <?php endfor; ?>
                                                    </tbody>
                                                </table>
                                                <?php endif; ?>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-3">
                                                Prepared By: <strong><?php echo get_fullname();?></strong><br><br>
                                                Checked by (HOD): <br><br>
                                                Authorized by (Head of LPL): <br><br>
                                                Approved by (Finance) : 
                                              </div>
                                              <div class="col-sm-3">
                                                ________________________________ <br><br>
                                                ________________________________ <br><br>
                                                ________________________________ <br><br>
                                                ________________________________ 
                                              </div>
                                              <div class="col-sm-2 text-right">
                                                Date: <br><br>
                                                Date: <br><br>
                                                Date: <br><br>
                                                Date: 
                                              </div>
                                              <div class="col-sm-4">
                                                ________________________________ <br><br>
                                                ________________________________ <br><br>
                                                ________________________________ <br><br>
                                                ________________________________ 
                                              </div>

                                        </div>
                                        <div class="no-print">
                                            <hr />
                                        <h5>First Quotation: <?php $f1 = get_requestfile('Quotation1', $req['req_id']); echo count($f1) == 0 ? '<a href="#" data-toggle="modal" data-target="#file-modal">Attach Quote 1</a>' : '<a href="'.$f1['requestfiles_url'].'">'.$f1['requestfiles_name'].'</a>';?></h5>
                                        <h5>Second Quotation: <?php $f1 = get_requestfile('Quotation2', $req['req_id']); echo count($f1) == 0 ? '<a href="#" data-toggle="modal" data-target="#file-modal">Attach Quote 2</a>' : '<a href="'.$f1['requestfiles_url'].'">'.$f1['requestfiles_name'].'</a>';?></h5>
                                        <h5>Third Quotation: <?php $f1 = get_requestfile('Quotation3', $req['req_id']); echo count($f1) == 0 ? '<a href="#" data-toggle="modal" data-target="#file-modal">Attach Quote 3</a>' : '<a href="'.$f1['requestfiles_url'].'">'.$f1['requestfiles_name'].'</a>';?></h5>
                                        <h5>Bid Analysis Report: <?php $f1 = get_requestfile('BidReport', $req['req_id']); echo count($f1) == 0 ? '<a href="#" data-toggle="modal" data-target="#file-modal">Attach Bid Report</a>' : '<a href="'.$f1['requestfiles_url'].'">'.$f1['requestfiles_name'].'</a>';?></h5>
                                        <h3>Attached files </h3>
                                        <?php $files = get_data('tbl_requestfiles', "WHERE requestfiles_requesttype = 1 AND requestfiles_requestid = ".$req['req_id']); if(count($files) != 0) { ?>
                                            <table class="table table-hover">
                                                <thead>
                                                    <th>Filename</th> <th>Type</th> <th>Uploaded By</th> <th>Uploaded On</th>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($files as $f) { ?>
                                                    <tr>
                                                        <td><a href="<?php echo $f['requestfiles_url']?>" target="_blank"><?php echo $f['requestfiles_name']?></a></td>
                                                        <td><?php echo $f['requestfiles_type']?></td>
                                                        <td><?php echo get_fullname($f['requestfiles_creator'])?></td>
                                                        <td><?php echo timeAgo($f['requestfiles_createdate'])?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        <?php } ?>
                                        </div>
                                    </div>
                                    
                                </div>
                                </div>
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>

<div class="modal fade" id="file-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload File</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo site_url('system/act/procurement_model/upload_file')?>" enctype="multipart/form-data" id="frm-upload">
            <label>Select Type of file</label>
            <select name="type" class="form-control">
                <option value="Quotation1">Quotation 1</option>
                <option value="Quotation2">Quotation 2</option>
                <option value="Quotation3">Quotation 3</option>
                <option value="BidReport">Bid Analysis Report</option>
                <option value="Invoice">Invoice</option>
                <option value="LPO">LPO</option>
                <option value="Receipt">Receipt</option>
                <option value="Salary Advance Form">Salary Advance Form</option>
            </select>
            <br />
            <label>Remarks/Comments</label>
            <textarea name="remarks" class="form-control" required="" placeholder=""></textarea>
            <br />
            <input type="file" name="userfile" required=""/>
            <input type="hidden" name="req_id" value="<?php echo $req_id?>"/>
            <input type="hidden" name="req_type" value="<?php echo $type=='item' ? 1 : 2;?>"/>
      </div>
      <div class="modal-footer">
        <div id="progress" class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            <span class="sr-only">60% Complete</span>
          </div>
        </div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Hide</button>
        <button type="submit" class="btn btn-theme" id="btn-upload"><i class="fa fa-upload"></i> Upload</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="used-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Returned amount after purchase</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo site_url('system/act/procurement_model/refund')?>" id="frm-main" enctype="multipart/form-data">
            <label>Enter Amount Used</label>
            <input type="number" class="form-control" name="amt_used" placeholder="Amount used" required=""/>
            <br />
            <label>Enter Amount Refunded</label>
            <input type="number" class="form-control" name="amt_refunded" placeholder="Amount returned" required=""/>
            <br />
            <label>Attach Receipt</label>
            <input type="file" name="userfile" required=""/>
            <br />
            <label>Remarks/Comments</label>
            <textarea name="remarks" class="form-control" required="" placeholder=""></textarea>
            <br />
            <input type="hidden" name="req_id" value="<?php echo $req_id?>"/>
            <input type="hidden" name="req_type" value="<?php echo $type?>"/>
            <input type="hidden" name="pmt_id" id="pmt-id"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Hide</button>
        <button type="submit" class="btn btn-theme" id="btn-main"><i class="fa fa-save"></i> Save</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="rModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Approve Requisition</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="ajax-form" method="post" action="<?php echo site_url('system/act/procurement_model/approve_request')?>">
          <div class="modal-body">
            <div class="form-group">
                <label>Remarks</label>
                <textarea class="form-control" name="remarks" required=""></textarea>
            </div>
            <input type="hidden" name="typeid" value="<?php echo $id?>">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
      </form>
    </div>
  </div>
</div>

<script>
    function refund(pmt_id)
    {
        $('#pmt-id').val(pmt_id);
        $('#used-modal').modal('show');
    }
</script>
<?php $this->load->view('layout/footer'); ?>