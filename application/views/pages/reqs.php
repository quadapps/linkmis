<?php
$dets = ['title' => 'Requisitions', 'page' => 3];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');
$user = get_data('tbl_users', "JOIN tbl_employees ON emp_id = user_empid WHERE user_id = $uid", 'user_id, user_levelid, user_empid, emp_id, emp_designation, emp_deptid', true);
//$emp = get_data('tbl_employees', "WHERE emp_id = ".$user['user_empid'], 'emp_id, emp_designation', true);
$des = strtolower($user['emp_designation']);
// if (strpos($des, 'project') !== false || $des == 'production manager') {
// 	$this->load->view('dashboards/projects');
// } else {
// 	$this->load->view('dashboards/main');
// }

$heads = get_data('tbl_expensecats', " WHERE active = 1");

$and = '';
if($user['user_levelid'] == 4 && $user['emp_designation'] != 'ADMIN OFFICER') 
	$and = ' AND req_creator = '.$user['user_id'];
if($user['user_levelid'] == 3 && $user['emp_designation'] != 'ADMIN OFFICER')
	$and = ' AND req_deptid = '.$user['emp_deptid'];

$reqs = get_data('tbl_reqs', "LEFT JOIN tbl_users ON user_id = req_creator LEFT JOIN tbl_expensecats ON req_exphead = tbl_expensecats.id WHERE req_active = 1 $and ", 'tbl_reqs.*, name, user_fullname');

$depts = get_data('tbl_departments', " WHERE dept_active = 1", 'dept_id, dept_name');

function get_approvals($req_id, $user)
{
	$recs = get_data('tbl_reqapprovals', "WHERE ra_reqid = $req_id ", "COUNT(ra_id) AS tot", true);
	if(!isset($recs['tot']) || $recs['tot'] == 0)
		return '<span class="alert alert-warning" role="alert">Pending</span>';
	if($recs['tot'] == 1 || $recs['tot'] == 2) {
		$des = get_data('tbl_employees', "JOIN tbl_users ON user_empid = emp_id WHERE user_id = ".$user, 'emp_designation', true)['emp_designation'];
		return '<label class="alert alert-primary" role="alert">Aprroved by '.$des.'</label>';
	}

	if($recs['tot'] > 2) {
		$des = get_data('tbl_employees', "JOIN tbl_users ON user_empid = emp_id WHERE user_id = ".$user, 'emp_designation', true)['emp_designation'];
		$html = '<label class="alert alert-success" role="alert">Aprroved by '.$des.'</label>';
		if($des == 'CASHIER' || $des == 'ACCOUNTANT') $html .= '<button class="btn btn-success"><i class="fa fa-money"></i></button>';
		return $html;
	}
}

function can_approve($req_id, $uid)
{
	$approvals = get_data('tbl_reqapprovals', "WHERE ra_creator = $uid AND ra_reqid = $req_id", "COUNT(ra_id) AS tots", true)['tots'];
	if($approvals > 0) return false;


	return true;
}
?>

<div class="my-3 my-md-5">
					<div class="container-fluid">
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li class="dropdown">
												<a href="#" class="btn btn-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus"></i> Add New</a>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
												    <a class="dropdown-item" data-toggle="modal" data-target="#cash-Modal" href="#">Cash Requisition</a>
												    <a class="dropdown-item" data-toggle="modal" data-target="#pur-Modal" href="#">Purchase Requisition</a>
												    <!-- <a class="dropdown-item" data-toggle="modal" data-target="#fuel-Modal" href="#">Fuel Requisition</a> -->
												  </div>
											</li>
										</ul>
									</div>
									<div class="p-6 card-body">
										<div class="panel panel-primary">
											<div class=" tab-menu-heading">
												<div class="tabs-menu1 ">
													<!-- Tabs -->
													<ul class="nav panel-tabs">
														<li class=""><a href="#tab5" class="active" data-toggle="tab">Requisitions</a></li>
														<li><a href="#tab6" data-toggle="tab">Expense Heads/Ledgers</a></li>
														
													</ul>
												</div>
											</div>
											<div class="panel-body tabs-menu-body">
												<div class="tab-content">
													<div class="tab-pane active " id="tab5">
														<table id="example" class="table table-bordered table-hover">
															<thead>
																<tr>
																	<th>NO</th>
																	<th>Date</th>
																	<th>Requisitioner</th>
																	<th>Type</th>
																	<th>Details</th>
																	<th>Amount</th>
																	<th>Currency</th>
																	<th>Head/Ledger</th>
																	<th>Status</th>
																	<th>Action</th>
																</tr>
															</thead>
															<tbody>
																<?php foreach($reqs as $r) :
																	$type = $name = false;
																if($r['req_type'] == 1) {
																	$type = 'Purchase';
																	$arr = json_decode($r['req_items'], true);
																	$name = '';
																	for($i=0; $i<count($arr['desc']); $i++)
																		$name .= $arr['qty'][$i].' - '.$arr['desc'][$i].', ';
																}
																if($r['req_type'] == 2) $type = 'Cash';
																if($r['req_type'] == 3) $type = 'Fuel'; ?>
																	<tr>
																		<td><?php echo $r['req_id'];?></td>
																		<td><?php echo $r['req_createdate'];?></td>
																		<td><?php echo $r['user_fullname'];?></td>
																		<td><?php echo $type;?></td>
																		<td><?php echo $name ? rtrim($name, ', ') : $r['req_items'];?></td>
																		<td><?php echo $r['req_amt'];?></td>
																		<td><?php echo $r['req_currency'];?></td>
																		<td><?php echo $r['name'];?></td>
																		<td><?php echo $r['req_status'] == -1 ? '<span class="alert alert-danger" role="alert">Rejected</span>' : get_approvals($r['req_id'], $r['req_creator']);?></td>
																		<td class="btn-group">
																			<?php if((can_approve($r['req_id'], $uid) && $user['user_levelid'] < 3) || $r['req_status'] > -1) { ?>
																				<a class="btn btn-success btn-sm approve" data-toggle="modal" data-target="#appModal" data-id="<?php echo $r['req_id'];?>" data-status="approve">Approve</a>
																			<?php } ?>
																			<?php if(can_approve($r['req_id'], $uid) || $user['user_levelid'] < 3 || $r['req_status'] == 0) { ?>
																				<a class="btn btn-danger btn-sm"  data-toggle="modal" data-target="#appModal" data-id="<?php echo $r['req_id'];?>" data-status="-1">Reject</a>
																			<?php } ?>
																			<a class="btn btn-secondary btn-sm " href="<?php echo site_url('welcome/view/req_details/'.$r['req_id']);?>" >View</a>
																		</td>
																	</tr>
																<?php endforeach; ?>
															</tbody>
														</table>
													</div>
													<div class="tab-pane " id="tab6">
														<table class="table table-bordered table-hover data-table">
															<thead>
																<tr>
																	<th>S/No</th>
																	<th>Name</th>
																	<th>Description</th>
																	<th>Action</th>
																</tr>
															</thead>
															<tbody>
																<?php foreach($heads as $r) : ?>
																	<tr>
																		<td><?php echo $r['sno'];?></td>
																		<td><?php echo $r['name'];?></td>
																		<td><?php echo $r['cat_desc'];?></td>
																		<td></td>
																	</tr>
																<?php endforeach; ?>
															</tbody>
														</table>
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

<!-- Modal -->
<div class="modal fade" id="cash-Modal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cash Requisition</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/procurement_model/create_cashrequest');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Select Department</label>
	        	<select class="form-control" name="deptid">
	        		<?php foreach($depts as $in) : ?>
	        			<option value="<?php echo $in['dept_id'];?>"><?php echo $in['dept_name'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>

	        <div class="form-row">
	        <div class="form-group col">
	          <label>Currency</label>
	          <select class="form-control" name="currency">
	          	<option>SSP</option>
	          	<option>USD</option>
	          </select>
	        </div>
	        <div class="form-group col">
	          <label>Amount Requested</label>
	          <input type="number" step="any" name="amt" required="" class="form-control">
	        </div>
	        </div>

	        <div class="form-row">
	        <div class="form-group col">
	          <label> Mode</label>
	          <select name="mode" class="form-control">
	          	<option>Cash</option>
	          	<option>Cheque</option>
	          	<option>BankTransfer</option>
	          	<option>mGurush</option>
	          </select>
	        </div>
	        <div class="form-group col">
	          <label>Expense Head/Ledger</label>
	          <select class="form-control" name="head">
	        		<?php foreach($heads as $in) : ?>
	        			<option value="<?php echo $in['id'];?>"><?php echo $in['sno'].' - '.$in['name'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>

	        </div>


	        <div class="form-group">
	          <label>Purpose</label>
	          <textarea class="form-control" name="purpose"></textarea>
	        </div>
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="pur-Modal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Purchase Requisition</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/procurement_model/create_request');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-row">
	        	<div class="form-group col">
	        	<label>Select Department</label>
	        	<select class="form-control" name="deptid">
	        		<?php foreach($depts as $in) : ?>
	        			<option value="<?php echo $in['dept_id'];?>"><?php echo $in['dept_name'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>

	        <div class="form-group col">
	          <label>Currency</label>
	          <select class="form-control" name="currency">
	          	<option>SSP</option>
	          	<option>USD</option>
	          </select>
	        </div>

	        <div class="form-group col">
	          <label>Expense Head/Ledger</label>
	          <select class="form-control" name="head">
	        		<?php foreach($heads as $in) : ?>
	        			<option value="<?php echo $in['id'];?>"><?php echo $in['sno'].' - '.$in['name'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>
	        </div>

	        <div class="form-group">
	          <label>Items</label>
	          <table class="table" id="kpi-table">
	          	<thead>
	          		<th width="15%">Qty</th>
	          		<th>Description</th>
	          		<th width="20%">Unit Price</th>
	          	</thead>
	          	<tbody>
	          		<tr>
	          			<td><input type="number" step="any" name="qty[]" class="form-control"></td>
	          			<td><input type="text" name="desc[]" class="form-control"></td>
	          			<td><input type="number" step="any" name="price[]" class="form-control"></td>
	          		</tr>
	          	</tbody>
	          </table>
	          <div class="btn-group float-right">
	          	<button type="button" class="btn btn-sm btn-secondary" id="add-kpis">Add More</button>
	          	<button type="button" class="btn btn-sm btn-danger" id="remove-kpis">Remove Last</button>
	          </div>
	        </div>
	        <hr>
	      </div>
	      <div class="modal-footer float-right">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="pur-Modal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Purchase Requisition</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/procurement_model/create_request');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-row">
	        	<div class="form-group col">
	        	<label>Select Department</label>
	        	<select class="form-control" name="deptid">
	        		<?php foreach($depts as $in) : ?>
	        			<option value="<?php echo $in['dept_id'];?>"><?php echo $in['dept_name'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>

	        <div class="form-group col">
	          <label>Currency</label>
	          <select class="form-control" name="currency">
	          	<option>SSP</option>
	          	<option>USD</option>
	          </select>
	        </div>

	        <div class="form-group col">
	          <label>Expense Head/Ledger</label>
	          <select class="form-control" name="head">
	        		<?php foreach($heads as $in) : ?>
	        			<option value="<?php echo $in['id'];?>"><?php echo $in['sno'].' - '.$in['name'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>
	        </div>

	        <div class="form-group">
	          <label>Items</label>
	          <table class="table" id="kpi-table">
	          	<thead>
	          		<th width="15%">Qty</th>
	          		<th>Description</th>
	          		<th width="20%">Unit Price</th>
	          	</thead>
	          	<tbody>
	          		<tr>
	          			<td><input type="number" step="any" name="qty[]" class="form-control"></td>
	          			<td><input type="text" name="desc[]" class="form-control"></td>
	          			<td><input type="number" step="any" name="price[]" class="form-control"></td>
	          		</tr>
	          	</tbody>
	          </table>
	          <div class="btn-group float-right">
	          	<button type="button" class="btn btn-sm btn-secondary" id="add-kpis">Add More</button>
	          	<button type="button" class="btn btn-sm btn-danger" id="remove-kpis">Remove Last</button>
	          </div>
	        </div>
	        <hr>
	      </div>
	      <div class="modal-footer float-right">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div> -->

<div class="modal fade" id="appModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Act on Requisition</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/procurement_model/create_request');?>" class="ajax-form" id="app-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Remarks</label>
	        	<textarea class="form-control" name="remarks" required=""></textarea>
	        </div>
	        <input type="hidden" name="status" id="act-status">
	        <input type="hidden" name="req_id" id="req-id">
	      </div>
	      <div class="modal-footer float-right">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
    $('#appModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var id = button.data('id') // Extract info from data-* attributes
      var status = button.data('status') // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this)
      //modal.find('.modal-title').text('New message to ' + recipient)
      modal.find('.modal-body #req-id').val(id);
      modal.find('.modal-body #act-status').val(status);

      if(status == -1) {
      	$('#app-form').attr('action', '<?php echo site_url('system/act/procurement_model/reject');?>')
      } else {
      	$('#app-form').attr('action', '<?php echo site_url('system/act/procurement_model/approve');?>')
      }
    });

    $('#add-kpis').click(function() {
        var $tableBody = $('#kpi-table').find("tbody");
        add_tr($tableBody);
    });
    $('#add-target').click(function() {
        var $tableBody = $('#target-table').find("tbody");
        add_tr($tableBody);
    });
    $('#remove-kpis').click(function(){
        //$('#kpi-table tr:last').remove();
        if($('#kpi-table tr').length > 2) $('#kpi-table tr:last').remove();
    });
    $('#remove-target').click(function(){
        //$('#target-table tr:last').remove();
        alert($('#target-table tr:last').index());
    });

    function add_tr($tableBody) {
        let $trLast = $tableBody.find("tr:last");
        let $trNew = $trLast.clone();
        $trNew.find('input, textarea').val('');
        $trLast.after($trNew);
    }
</script>
<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>