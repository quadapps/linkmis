<?php 
$dets['page'] = 4; $dets['title'] = 'Sales Targets';
$this->load->view('layout/header', $dets); 
?>
     <div class="my-3 my-md-5">
          <div class="container">
            
            <div class="row row-cards row-deck">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">
                    <li class="dropdown">
                      <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        New Target
                      </a>

                      <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agent-modal">Agent</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#pay-modal">BDR</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#m-modal">Marketer</a>
                      </div>
                    </li>
                    
                  </ul>
                </div>
                <div class="card-body">
                                  <div class="table-responsive">
                  <table id="d-table" class="table table-hover data-table">
                            <thead>
                                <th>Employee Name</th>
                                <th>Month</th>
                                <th>Year</th>
                                <th>No of Transactions</th>
                                <th>No of Customer Registrations</th>
                                <th>No of Agent Activations</th>
                                <th>No of Merchant Activations</th>
                                <th>No of Visits</th>
                                <th>USD Amount</th>
                                <th>SSP Amount</th>
                            </thead>
                            <tbody>
                            <?php foreach(get_data('tbl_targets') as $j) { ?>
                                <tr>
                                    <td><?php echo get_that_data('tbl_employees', 'emp_fullname', 'emp_id', $j['target_empid'])?></td>
                                    <td><?php echo $j['target_month']?></td>
                                    <td><?php echo $j['target_year']?></td>
                                    <td><?php echo $j['target_qty']?></td>
                                    <td><?php echo $j['target_regs']?></td>
                                    <td><?php echo $j['target_acts']?></td>
                                    <td><?php echo $j['target_merchants']?></td>
                                    <td><?php echo $j['target_visits']?></td>
                                    <td><?php echo $j['target_usdamt']?></td>
                                    <td><?php echo $j['target_sspamt']?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                           </table>
                </div>
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>
<!-- Modal -->
<div class="modal fade" id="pay-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add/Modify Targets</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/marketing_model/create_target')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      <div class="form-group">
        <label for="inputEmail3">Employee:</label>
            <select name="emp_id" class="form-control">
                <option value="0">--Select Employee--</option>
                    <?php foreach(get_data('tbl_employees', 'WHERE emp_active = 1 AND emp_designation LIKE "BDR" ORDER BY emp_fullname ASC') as $e) { ?>
                    <option value="<?php echo $e['emp_id']?>"><?php echo $e['emp_fullname']?></option>
                    <?php } ?>
            </select>
      </div>
      
        <div class="form-group">
            <label for="exampleInputEmail1">Month</label>
            <select name="month" class="form-control">
                <option value="0">--Select Month--</option>
                <?php for($i=1;$i<13;$i++) { ?>
                <option value="<?php echo date('F', mktime(0,0,0,$i)) ?>"><?php echo date('F', mktime(0,0,0,$i)) ?></option>
                <?php } ?>
            </select>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">No of Customer Registrations</label>
            <input type="number" name="regs" required="" class="form-control" id="" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">No of Agent Activations</label>
            <input type="number" name="acts" required="" class="form-control" id="" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">No of Merchant Activations</label>
            <input type="number" name="merchants" required="" class="form-control" id="" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">No of Visits</label>
            <input type="number" name="visits" required="" class="form-control" id="" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save Target</button>
          </div>
      </form> 
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="agent-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add/Modify Targets</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/marketing_model/create_target')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      <div class="form-group">
        <label for="inputEmail3">Employee:</label>
            <select name="emp_id" class="form-control">
                <option value="0">--Select Employee--</option>
                    <?php foreach(get_data('tbl_employees', 'WHERE emp_active = 1 AND emp_designation LIKE "agent" ORDER BY emp_fullname ASC') as $e) { ?>
                    <option value="<?php echo $e['emp_id']?>"><?php echo $e['emp_fullname']?></option>
                    <?php } ?>
            </select>
      </div>
      
        <div class="form-group">
            <label for="exampleInputEmail1">Month</label>
            <select name="month" class="form-control">
                <option value="0">--Select Month--</option>
                <?php for($i=1;$i<13;$i++) { ?>
                <option value="<?php echo date('F', mktime(0,0,0,$i)) ?>"><?php echo date('F', mktime(0,0,0,$i)) ?></option>
                <?php } ?>
            </select>
          </div>

         <div class="form-group">
            <label for="exampleInputEmail1">No of Transactions</label>
            <input type="number" name="qty" required="" class="form-control" id="" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">USD Amount</label>
            <input type="number" name="usd" required="" class="form-control" id="" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">SSP Amount</label>
            <input type="number" name="ssp" required="" class="form-control" id="" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save Target</button>
          </div>
      </form> 
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="m-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add/Modify Targets</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/marketing_model/create_target')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      <div class="form-group">
        <label for="inputEmail3">Employee:</label>
            <select name="emp_id" class="form-control">
                <option value="0">--Select Employee--</option>
                    <?php foreach(get_data('tbl_employees', 'WHERE emp_active = 1 AND emp_designation LIKE "marketing" ORDER BY emp_fullname ASC') as $e) { ?>
                    <option value="<?php echo $e['emp_id']?>"><?php echo $e['emp_fullname']?></option>
                    <?php } ?>
            </select>
      </div>
      
        <div class="form-group">
            <label for="exampleInputEmail1">Month</label>
            <select name="month" class="form-control">
                <option value="0">--Select Month--</option>
                <?php for($i=1;$i<13;$i++) { ?>
                <option value="<?php echo date('F', mktime(0,0,0,$i)) ?>"><?php echo date('F', mktime(0,0,0,$i)) ?></option>
                <?php } ?>
            </select>
          </div>

         <div class="form-group">
            <label for="exampleInputEmail1">No of Visits</label>
            <input type="number" name="visits" required="" class="form-control" id="" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save Target</button>
          </div>
      </form> 
    </div>
  </div>
</div>
</div>
      <!--main content end-->
<?php $this->load->view('layout/footer', array('dtt' => 1, 'sel' => 1)); ?>