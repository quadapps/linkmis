<?php
$dets = ['title' => 'Task Management', 'page' => 5];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');
$user = get_data('tbl_users', "JOIN tbl_employees ON emp_id = user_empid WHERE user_id = $uid", 'user_id, user_levelid, user_empid, emp_deptid', true);

$where = '';
$emps = get_data('tbl_employees', "WHERE emp_active = 1");
if($user['user_levelid'] == 4) {
	$where .= " OR task_empid = ".$user['user_empid'];
	$emps = get_data('tbl_employees', "WHERE emp_active = 1 AND emp_id = ".$user['user_empid']);
}
if($user['user_levelid'] == 3) {
	$where .= " OR emp_deptid = ".$user['emp_deptid'];
	$emps = get_data('tbl_employees', "WHERE emp_active = 1 AND emp_deptid = ".$user['emp_deptid']);
}

$data = get_data('tbl_tasks', "JOIN tbl_employees ON emp_id = task_empid JOIN tbl_users ON user_id = task_creator WHERE task_creator = ".$user['user_id']." $where ", 'tbl_tasks.*, emp_fullname, user_fullname');

?>

<div class="my-3 my-md-5">
					<div class="container">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#tModal" ><i class="fa fa-plus"></i> Add New</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter">
											<thead>
												<tr>
													<th>Date</th>
													<th>Title</th>
													<th>Description</th>
													<th>Assigned to</th>
													<th>Start</th>
													<th>End</th>
													<th>Status</th>
													<th>Complexity</th>
													<th>Created By</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : ?>
													<tr>
														<td><?php echo $d['task_createdate'];?></td>
														<td><a href="#"><?php echo $d['task_title'];?></a></td>
														<td><?php echo $d['task_description'];?></td>
														<td><?php echo $d['emp_fullname'];?></td>
														<td><?php echo $d['task_start'];?></td>
														<td><?php echo $d['task_end'];?></td>
														<td><?php echo $d['task_status'];?></td>
														<td><?php echo $d['task_complexity'];?></td>
														<td><?php echo $d['user_fullname'];?></td>
														<td></td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="tModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" class="ajax-form" action="<?php echo site_url('system/act/hr_model/create_task');?>">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Assign To</label>
	        	<select name="empid" class="form-control">
	        		<?php foreach($emps as $e) : ?>
		        		<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
		        	<?php endforeach; ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>Title</label>
	        	<input type="text" name="title" class="form-control" required="">
	        </div>

	        <div class="form-group">
	        	<label>Description</label>
	        	<textarea name="desc" class="form-control" required=""></textarea>
	        </div>

	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>Start</label>
		        	<input type="time" name="start" class="form-control" required="">
		        </div>
		        <div class="form-group col">
		        	<label>End</label>
		        	<input type="time" name="end" class="form-control" required="">
		        </div>
	        </div>
	        <div class="form-row">
	        	<div class="form-group col">
		        	<label>Complexity</label>
		        	<select name="complexity" class="form-control">
		        		<option>Easy</option>
		        		<option>Moderate</option>
		        		<option>Complex</option>
		        	</select>
		        </div>
		        <div class="form-group col">
		        	<label>Status</label>
		        	<select name="backstopper" class="form-control">
		        		<option>Pending</option>
		        		<option>Started</option>
		        		<option>OnGoing</option>
		        		<option>Completed</option>
		        	</select>
		        </div>
	        </div>
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1]);
?>