<?php 
$dets['page'] = 8; $dets['title'] = 'User Activity';
$this->load->view('layout/header', $dets); 
$user = get_row_data('tbl_users', 'user_id', $this->session->userdata('user_id'));
/*if($user['user_levelid'] != 0) {
    die('Error 500 - Access Denied');
}*/
$this->load->helper('date');

function get_logindetails($emp)

{

    $A =& get_instance();

    $uid = get_that_data('tbl_users', 'user_id', 'user_empid', $emp);

    $query = $A->db->query("SELECT * FROM tbl_loginactivity WHERE login_userid = $uid ORDER BY login_id DESC LIMIT 1");

    if($query->num_rows() == 0) return array();

    

    return $query->row_array();

}
?>
      
      <div class=" my-3 my-md-5">
          <div class="container">
            
            <div class="row row-cards row-deck">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">
                    <li><a href="<?php echo base_url('UserManual.pdf')?>" class="btn btn-default btn-flat pull-right" target="_blank"><i class="fa fa-file-pdf"></i> User Manual</a>
                    </li>
                    
                  </ul>
                </div>
                <div class="card-body">
                                  <div class="table-responsive">
                  <table id="d-table" class="table table-hover data-table">
                            <thead>
                                <th> Name</th>
                                <th>Email</th>
                                <th>Last Login Date</th>
                                <th>Duration</th>
                                <th>Last Login IP</th>
                                <th>Location</th>
                                <th>Status</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php foreach(get_data('tbl_users', "WHERE user_active = 1") as $j) { 
                                $details = get_logindetails($j['user_empid']);
                                //if(!empty($details)) { 
                                $status = $j['user_active']; ?>
                                <tr>
                                    <td><?php echo $j['user_fullname']?></td>
                                    <td><?php echo $j['user_email']?></td>
                                    <td><?php echo isset($details['login_time']) ? timeAgo($details['login_time']) : 'Never';?></td>
                                    <td><?php if(isset($details['logout_time'])) echo strtotime($details['logout_time']) < 0 ? timespan(strtotime($details['login_time']), time()) : timespan(strtotime($details['login_time']), strtotime($details['logout_time'])); else echo 'Never';?></td>
                                    <td><?php echo isset($details['login_ip']) ? $details['login_ip'] : 'Never';?></td>
                                    <td><?php //echo get_geolocation($details['login_ip'])?></td>
                                    <td><?php if(isset($details['logout_time'])) echo strtotime($details['logout_time']) < 0 ? '<label class="label label-success">Online</label>' : '<label class="label label-default">Offline</label>'; else echo 'Never'; ?></td>
                                    <td>
                                      <div class="item-action dropdown">
                                        <a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-more-vertical"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                          <a href="javascript: void()" data-toggle="modal" data-target="#ajaxModal" data-url="<?php echo site_url('welcome/modal/user/'.$j['user_id'])?>" data-title="Update User Login Details" class="dropdown-item"><i class="dropdown-icon fe fe-edit-2"></i> Edit </a>
                                          <a href="javascript: void()" class="dropdown-item reset-pass" data-id="<?php echo $j['user_id']?>"><i class="dropdown-icon fa fa-refresh"></i> Resend Login </a>
                                          <?php if($status == 1) { ?>
                                          <a href="javascript: void()" class="dropdown-item" onclick="remove_things('tbl_users', <?php echo $j['user_id']?>, true)"><i class="dropdown-icon fa fa-trash-o"></i> Deactivate</a>
                                          <?php } else { ?>
                                          <a href="javascript: void()" class="dropdown-item" onclick="toggle_user(<?php echo $j['user_id']?>, 1)"><i class="dropdown-icon fa fa-check"></i> Activate</a>
                                          <?php } ?>
                                        </div>
                                      </div>
                                    
                                    </td>
                                </tr>
                            <?php } //} ?>
                            </tbody>
                           </table>
                </div>
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>
<!-- Modal -->
<div class="modal fade" id="user-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">User Action</h4>
      </div>
      <form role="form" id="frm-main" method="post" action="<?php echo site_url('system/act/users_model/toggle_user')?>">
      <div class="modal-body">
    
         <div class="form-group">
            <div class="panel panel-danger">
                <div class="panel-heading">Confirm</div>
                <div class="panel-body">
                Are you sure you want to deactivate/activate this user?
                </div>
            </div>
          </div>
          
          <div class="form-group">
            <input type="hidden" name="status" required="" class="form-control" id="user-status" />
          </div>
          
          <div class="form-group">
            <input type="hidden" name="uid" id="user-id"/>
          </div>
      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-theme">Proceed</button>
          </div>
      </form> 
    </div>
  </div>
  </div>
</div>
      <!--main content end-->
      <script type="text/javascript">
        get_geoloc(ip) {
          $.get("https://ipinfo.io/"+ip+"/geo?token=b106a6d48a8660", function(response) {
            console.log(response.city, response.country);
          }, "jsonp");
          
        }
      </script>
      <script type="text/javascript">
        $(document).ready(function() {
          $('.reset-pass').click(function() {
            var id = $(this).data('id');
            $("body").css("opacity",0.5);
            $.post('<?php echo site_url('system/act/users_model/reset_passkey')?>', {uid: id}).done(function(d) {
              var response = JSON.parse(d);
              if (response.status == 1) {
                swal('SUCCESS', response.msg, 'success');
              } else {
                swal('Error!', response.msg, 'danger');
              }
              $("body").css("opacity",1);
            });
          });
        })
      </script>
<?php $this->load->view('layout/footer', array('dtt' => 1)); ?>