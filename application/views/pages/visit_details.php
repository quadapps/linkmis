<?php 
$id = $params;
$arr = explode('n', $id);
$emp_id = $arr[0];
$start = $arr[1];
$end = $arr[2];
?>

<?php $data = get_data('tbl_visits', "WHERE visit_empid = $emp_id AND DATE(visit_createdate) BETWEEN '$start' AND '$end'"); ?>
<div class="modal-body">
    <table class="table table-hover table-striped" id="d-tables">
    <thead>
        <th>Date/Time</th>
        <th>Client</th>
        <th>Reason</th>
        <th>Remarks</th>
    </thead>
    <tbody>
        <?php foreach($data as $v) { ?>
        <tr>
            <td><?php echo date('d/m/Y g:i a', strtotime($v['visit_createdate']))?></td>
            <td><?php echo get_that_data('tbl_clients', 'client_name', 'client_id', $v['visit_clientid'])?></td>
            <td><?php echo $v['visit_reason']?></td>
            <td><?php echo $v['visit_remarks']?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<br />

<fieldset>
    <legend>Summary</legend>
    <p><strong>Total No of visits: </strong><?php echo count($data)?></p>
    
</fieldset>
</div>
<br />
<script>
    $(document).ready(function() {
        $('#d-tables').dataTable( {

            "bPaginate": true,

            "bLengthChange": true,

            "bFilter": true,

            "bSort": true,

            "bInfo": false,

            "bAutoWidth": true,
            buttons: {
                buttons: [
                    { extend: 'copy', className: 'btn-primary' },
                    { extend: 'excel', className: 'btn-primary' },
                    { extend: 'pdf', className: 'btn-primary' }
                ]
              }

        } );
    })
</script>