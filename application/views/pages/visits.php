<?php
$dets = ['title' => 'Visits', 'page' => 4];
$this->load->view('layout/header', $dets);
$user = get_row_data('tbl_users', 'user_id', $this->session->userdata('user_id'));
$id = $params;
$emp = get_row_data('tbl_employees', 'emp_id', $user['user_empid']);

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-1'); 
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d');

if($user['user_levelid'] == 4) {
    $query = $this->db->query("SELECT COUNT(visit_clientid) AS clients, visit_empid FROM tbl_visits WHERE DATE(visit_createdate) BETWEEN '$start' AND '$end' AND visit_empid = ".$emp['emp_id']." GROUP BY visit_empid");
    $data = $query->result_array();
} else {
    $query = $this->db->query("SELECT COUNT(visit_clientid) AS clients, visit_empid FROM tbl_visits WHERE DATE(visit_createdate) BETWEEN '$start' AND '$end' GROUP BY visit_empid");
    $data = $query->result_array();
}

?>

<div class="my-3 my-md-5">
          <div class="container">
            <!-- <div class="page-header">
              <h4 class="page-title">Dashboard</h4>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
              </ol>
            </div> -->
                                    
            <div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title"><?php echo $dets['title'];?></h3>
                    <ul class="card-options panel-toolbox">
                      <li>
                        <a href="#" class="btn btn-secondary" data-toggle="modal" data-target="#deptModal" ><i class="fa fa-plus"></i> Add New</a>
                      </li>
                      <li><a href="javascript: void()" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/view/visits')?>"><i class="fa fa-calendar"></i> <span><?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end))?></span> <b class="caret"></b></a>
                    </li>
                    </ul>
                  </div>
                  <div class="table-responsive card-body">
                    <table id="example" class="table table-hover">
                            <thead>
                                <th>No</th>
                                <th>Employee Name</th>
                                <th>Clients Visited</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach($data as $j) { ?>
                                <tr>
                                    <td><?php echo $i?></td>
                                    <td><?php echo $emp_name = get_that_data('tbl_employees', 'emp_fullname', 'emp_id', $j['visit_empid'])?></td>
                                    <td><?php echo $j['clients']?></td>
                                    
                                    <td><a href="#" data-toggle="modal" data-target="#ajaxModal" data-url="<?php echo site_url('welcome/page/visit_details/'.$j['visit_empid'].'n'.$start.'n'.$end)?>" data-title="Visit Details for <?php echo $emp_name; ?>">Details</a></td>
                                </tr>
                            <?php $i++; } ?>
                            </tbody>
                           </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Visit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/marketing_model/create_visit')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      <div class="form-group">
            <label for="inputEmail3">Select Client:</label>
            <div class="row no-gutter">
            <div class="col-sm-10">
                
                <select name="client" class="form-control">
                    <option value="0">--Select Client Name--</option>
                        <?php foreach(get_data('tbl_clients', "WHERE client_active = 1 ORDER BY client_name ASC") as $e) { ?>
                        <option value="<?php echo $e['client_id']?>"><?php echo $e['client_name']?></option>
                        <?php } ?>
                </select>
            </div>
            <div class="col-sm-2">
                <button class="btn btn-outline-default btn-sm" type="button" data-toggle="modal" data-target="#client-modal"><i class="fa fa-plus"></i></button>
            </div>
            </div>
      </div>
      
         <div class="form-group">
            <label for="exampleInputEmail1">Reason for visit</label>
            <textarea name="reason" class="form-control" required="" placeholder="e.g Sales, introducing company, collection, follow up, delivery etc"></textarea>
         </div>
         
         <div class="form-group">
            <label for="exampleInputEmail1">Remarks</label>
            <textarea name="remarks" class="form-control" required="" placeholder="e.g Response from client"></textarea>
         </div>
      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save Visit</button>
          </div>
      </form> 
    </div>
  </div>
</div>

<div class="modal fade" id="achModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Achieved Target</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/marketing_model/post_target');?>" class="ajax-form">
        <div class="modal-body">
          <input type="hidden" name="target_id" id="target-id">
          <div class="form-group">
            <label>No of Transactions</label>
            <input type="number" name="qty" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>USD Value</label>
            <input type="number" name="usd" class="form-control" required="">
          </div>
          <div class="form-group">
            <label>SSP Value</label>
            <input type="number" name="ssp" class="form-control" required="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.achieved').click(function() {
    let id = $(this).data('id');
    $('#target-id').val(id);
    $('#achModal').modal('show');
  })
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>