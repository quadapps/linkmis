<?php
$dets = ['title' => 'Agents Daily Report', 'page' => 3];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-01');
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d');

$emps = get_data('tbl_agents', "WHERE agent_active = 1");

$range = dateRanges($start, $end); 
?>

<div class="my-3 my-md-5">
					<div class="container-fluid">
						
																		
						<div class="row row-cards row-deck">
							<div class="card col-12">
								<div class="">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="javascript:void()" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/report/agents');?>" ><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end));?></a>
											</li>
											<!-- <li>
												<a href="javascript:void()" class="btn btn-secondary" id="" data-toggle="modal" data-target="#deptModal" ><i class="fa fa-plus"></i> New CashOut</a>
											</li> -->
										</ul>
									</div>
									<div class="table-responsive card-body">
										<div class="panel panel-primary">
											<div class="tab-menu-heading">
												<div class="tabs-menu ">
													<!-- Tabs -->
													<ul class="nav panel-tabs">
														<li class=""><a href="#tab1" class="active" data-toggle="tab">SSP</a></li>
														<li><a href="#tab2" data-toggle="tab">USD</a></li>
													</ul>
												</div>
											</div>
											<div class="panel-body tabs-menu-body">
												<div class="tab-content">
													<div class="tab-pane active " id="tab1">
														<div class="table-responsive">
															<table class="table card-table table-vcenter text-nowrap data-table-buttons table-bordered">
																<thead>
																	<tr>
																		<th class="w-1">Date</th>
																		<th>Agent Name</th>
																		<th>Opening Balance</th>
																		<th>Float In</th>
																		<th>Deposits</th>
																		<th>Withdrawals</th>
																		<th>Float Balance</th>
																		<th>Cash In</th>
																		<th>Cash Out</th>
																		<th>Cash Balance</th>
																		<!-- <th rowspan="2">Status</th> -->
																	</tr>
																</thead>
																<tbody id="recons" class="tableData">
																
																<?php foreach($emps as $e) : foreach($range as $r) : 
																
																$cb = $op = get_bal($r, $e['agent_empid'], 'SSP');
																$bal = get_data('tbl_agentrecons', "WHERE ar_date = '$r' AND ar_currency = 'SSP' AND ar_empid = ".$e['agent_empid'], '*', true);
																$ob = get_data('tbl_agentrecons', "WHERE ar_date < '$r' AND ar_currency = 'SSP' AND ar_empid = ".$e['agent_empid']." ORDER BY ar_date DESC LIMIT 1", '*', true); ?>
																	<tr>
																		<td><?php echo $r;?></td>
																		<td><?php echo $e['agent_name'];?></td>
																		<td><?php echo isset($ob['ar_cb']) ? $ob['ar_cb'] : 0;?></td>
																		<td><?php echo $f = $cb['float']-$cb['fo'];?></td>
																		<td><?php echo $cb['deposits'];?></td>
																		<td><?php echo $cb['withdrawals'];?></td>
																		<td><?php echo isset($bal['ar_fb']) ? $bal['ar_fb'] : 0;?></td>
																		<td><?php echo $cb['co'];?></td>
																		<td><?php echo $cb['collections'];?></td>
																		<td><?php echo isset($bal['ar_cb']) ? $bal['ar_cb'] : 0;;?></td>
																	</tr>



																<?php $bal = []; endforeach; endforeach; ?>
																	
																</tbody>
															</table>
														</div>
													</div>
													<div class="tab-pane  " id="tab2">
														<div class="table-responsive">
														<table class="table card-table table-vcenter text-nowrap data-table-buttons table-bordered">
																<thead>
																	<tr>
																		<th class="w-1">Date</th>
																		<th>Agent Name</th>
																		<th>Opening Balance</th>
																		<th>Float In</th>
																		<th>Deposits</th>
																		<th>Withdrawals</th>
																		<th>Float Balance</th>
																		<th>Cash In</th>
																		<th>Cash Out</th>
																		<th>Cash Balance</th>
																		<!-- <th rowspan="2">Status</th> -->
																	</tr>
																</thead>
																<tbody>
																
																<?php foreach($emps as $e) : foreach($range as $r) : 
																$cb = $op = get_bal($r, $e['agent_empid'], 'USD');
																$bal = get_data('tbl_agentrecons', "WHERE ar_date = '$r' AND ar_currency = 'USD' AND ar_empid = ".$e['agent_empid'], '*', true);
																$ob = get_data('tbl_agentrecons', "WHERE ar_date < '$r' AND ar_currency = 'USD' AND ar_empid = ".$e['agent_empid']." ORDER BY ar_date DESC LIMIT 1", '*', true); ?>
																	<tr>
																		<td><?php echo $r;?></td>
																		<td><?php echo $e['agent_name'];?></td>
																		<td><?php echo isset($ob['ar_cb']) ? $ob['ar_cb'] : 0;?></td>
																		<td><?php echo $f = $cb['float']-$cb['fo'];?></td>
																		<td><?php echo $cb['deposits'];?></td>
																		<td><?php echo $cb['withdrawals'];?></td>
																		<td><?php echo isset($bal['ar_fb']) ? $bal['ar_fb'] : 0;?></td>
																		<td><?php echo $cb['co'];?></td>
																		<td><?php echo $cb['collections'];?></td>
																		<td><?php echo isset($bal['ar_cb']) ? $bal['ar_cb'] : 0;;?></td>
																	</tr>

																<?php endforeach; endforeach; ?>
																	
																</tbody>
															</table>
															</div>
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Issue Cash/Float</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/finance_model/create_cashout');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Agent Name</label>
	        	<select name="name" class="form-control">
	        		<?php foreach(get_data('tbl_agents', "JOIN tbl_employees ON emp_id = agent_empid WHERE agent_active = 1", 'agent_empid, emp_fullname') as $e) : ?>
	        			<option value="<?php echo $e['agent_empid'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>
	        
	        <div class="form-group">
	        	<label>Type</label>
	        	<select name="type" class="form-control">
	        		<option value="0">Cash</option>
	        		<option value="1">Float</option>
	        	</select>
	        </div>

	        <div class="form-group">
	        	<label>Currency</label>
	        	<input type="radio" name="currency" class="form-control" required="" value="SSP" checked=""> SSP<br>
	        	<input type="radio" name="currency" class="form-control" required="" value="USD"> USD<br>
	        </div>

	        <div class="form-group">
	        	<label>Amount Issued</label>
	        	<input type="number" step="any" name="amt" class="form-control" required="" value="0">
	        </div>

	        <div class="form-group">
	        	<label>Collector</label>
	        	<select name="collector" class="form-control">
	        		<?php foreach(get_data('tbl_employees', "WHERE emp_active = 1 AND emp_designation LIKE 'CASH%'") as $e) : ?>
	        			<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php endforeach; ?>
	        	</select>
	        </div>
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="recon-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Confirm Collection</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/finance_model/confirm_collection')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      		<div class="form-group">
	        	<label>SSP Recieved</label>
	        	<input type="number" name="ssp_amt" id="ssp-amt" class="form-control" required="">
	        </div>
	        
	        <div class="form-group">
	        	<label>USD Recieved</label>
	        	<input type="number" name="usd_amt" id="usd-amt" class="form-control" required="">
	        </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Comments</label>
            <textarea name="comments" class="form-control" required="" id="comments"></textarea>
          </div>

          <div class="form-group">
            <input type="hidden" name="coll_id" required="" class="form-control" id="recon-id" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save changes</button>
          </div>
      </form> 
    </div>
  </div>
</div>


<script type="text/javascript">
	calc_bal();
	$('#sync').click(function(){
		swal('submitted');
		$.get('<?php echo site_url('epicollect/sync_collections')?>', function(r) {
			//if(r > 0)
				location.reload();
		});
	});

	$('.reconcile').click(function() {
		let id = $(this).data('id');
		let comm = $(this).data('comm');
		let usd = $(this).closest('tr').find("td:nth-child(5)").text();
		let ssp = $(this).closest('tr').find("td:nth-child(6)").text();
		$('#recon-id').val(id);
		$('#comments').text(comm);
		$('#ssp-amt').val(ssp);
		$('#usd-amt').val(usd);
		$('#recon-modal').modal('show');
	});

	function calc_bal() {
		$('.tableData tr').each(function () {
		    var row = $(this);
		    var rowTotal = 0;
		    let p_ob = row.prev().find('td:last').text();
		    let p_fb = row.prev().find("td:nth-child(7)").text();
		    // console.log(ob);
		    let float = row.find("td:nth-child(4)").text();
		    let debit = row.find("td:nth-child(5)").text();
		    let credit = row.find("td:nth-child(6)").text();
		    let fb = Number(float)+Number(credit)-Number(debit)+Number(p_fb);
		    // rowTotal = ci-co;
		    row.find("td:nth-child(3)").text(p_ob);
		    row.find("td:nth-child(7)").text(fb);
		});
	}
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>