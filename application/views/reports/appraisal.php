<?php
$dets = array('page' => 3, 'title' => 'Employee Appraisal');
$this->load->view('layout/header', $dets);
//$client = get_row_data('tbl_clients', 'client_id', $params);
$start = date('Y-m-d', strtotime('-30 days')); $end = date('Y-m-d');
if(!empty($params)) {
  $arr = explode('_', $params);
  $start = reset($arr); $end = end($arr);
}

$user = get_data('tbl_employees', 'JOIN tbl_users ON user_empid = emp_id WHERE emp_id = '.$this->session->userdata('emp_id'), 'emp_deptid, emp_designation, emp_id, user_levelid', true );
$and = '';
if($user['user_levelid'] > 2 && $user['emp_designation'] != 'HR OFFICER') $and  = ' AND dept_id = '.$user['emp_deptid'];
$depts = get_data('tbl_departments', "WHERE dept_active = 1 $and ");
?>

<!-- <style type="text/css">
  table {
    max-width: 100%;
    overflow-x: hidden;
    table-layout:fixed;
    word-wrap: break-word;
}
</style> -->

<div class="app-content my-3 my-md-5" style="min-height: 500px;">
          <div class="side-app">
            <div class="page-header">
              <h4 class="page-title"><?php echo $dets['title']; ?></h4>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Clients</a></li>
                <li class="breadcrumb-item active" aria-current="page">View <?php echo $dets['title']; ?></li>
              </ol>
            </div>
            <div class="row">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                    <div id="dept-name"></div>
                  </div>
                  <ul class="card-options panel_toolbox">

                    <li class="dropdown">
                      <a href="#" class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-list"></i> Select Department <i class="fa fa-fw"></i>
                      </a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <?php foreach($depts as $d) :?>
                          <a class="dropdown-item change-dept" data-id="<?php echo $d['dept_id'];?>" href="#"><?php echo $d['dept_name'];?></a>
                        <?php endforeach; ?>
                      </div>
                    </li>
                    
                    <li class="dropdown">
                    <a class="btn btn-default" href="javascript: void()" id="reportrange" type="button" data-url="<?php echo site_url('welcome/report/appraisal')?>"><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end))?></a>
                    </li>
                    
                  </ul>
                </div>
                <div class="card-body">
                  <div class="table-responsive" style="max-width: 100%" id="ajax-content">
                    
                  </div>
                    
                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>
<!--Modal-->
<div class="modal fade" id="log-modal" tabindex="-1" role="dialog" aria-labelledby="log-modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="log-modalLabel">Start JOB</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="ajax-form" method="post" action="<?php echo site_url('system/act/jobs_model/start_schedule');?>">
        <div class="modal-body">
          
          <div class="alert alert-warning">
            Are you sure you want to start schedule? Current time will be logged.
          </div>

        </div>
        <input type="hidden" name="js_id" id="item-id" >
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success">Yes</button>
        </div>
      </form>
    </div>
  </div>
</div>   

<div class="modal fade" id="s-modal" tabindex="-1" role="dialog" aria-labelledby="log-modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="log-modalLabel">Complete Job</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="ajax-form" method="post" action="<?php echo site_url('system/act/jobs_model/complete_schedule');?>">
        <div class="modal-body">

          
          <div class="form-group">
            <label>Comments/Remarks</label>
            <textarea name="comments" class="form-control" placeholder="Anything worth noting?"></textarea>
          </div>

        </div>
        <input type="hidden" name="js_id" id="job-item" >
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>    

<script type="text/javascript">
  $(function() {

    $('.change-dept').click(function() {
      var id = $(this).data('id');
      $('#dept-name').html($(this).html());
      $.get('<?php echo site_url('welcome/modal/appraisal')?>/'+id+'_<?php echo $start.'_'.$end;?>', function(r) {
        $('#ajax-content').html(r);
      });
    });

    $('.accept-item').click(function() {
      var id = $(this).data('id');
      //$('#item-id').val(id);
      if(confirm("Are you sure?")) {
        swal('Thanks for acknowleding the job schedule. Please mark "Start" as soon as you begin!');
      }
    });

    $('.schedule-item').click(function() {
      var id = $(this).data('id');
      $('#job-item').val(id);
      $('#s-modal').modal('show');
    });
  });
</script> 
<?php $this->load->view('layout/footer', array('tabs' => 1 ,'dtp' => 1, 'dtt' => 1)); ?>