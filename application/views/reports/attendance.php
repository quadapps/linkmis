<?php 
$dets['page'] = 5; $dets['title'] = 'Attendance Report';
$this->load->view('layout/header', $dets); 

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-01'); 
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d'); 

$user = get_data('tbl_users', "WHERE user_id = ".$this->session->userdata('user_id'), 'user_empid, user_levelid, user_id',  true);
$and = '';
if ($user['user_levelid'] == 4) {
  $and = " AND emp_id = ".$user['user_empid'];
} 
if ($user['user_levelid'] == 3) {
  $and = " AND emp_deptid = ".get_that_data('tbl_employees', 'emp_deptid', 'emp_id', $user['user_empid']);
}

// $data = get_data('tbl_empcheckin', "JOIN tbl_employees ON emp_id = empcheckin_empid WHERE DATE(empcheckin_date) BETWEEN '$start' AND '$end' $and", 'tbl_empcheckin.*, emp_fullname, emp_deptid, emp_id');
$data = get_data('tbl_employees', "LEFT JOIN tbl_departments ON emp_deptid = dept_id WHERE emp_active = 1", 'emp_id, emp_fullname, emp_deptid, dept_name');

function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {
    $dates = [];
    $current = strtotime( $first );
    $last = strtotime( $last );

    while( $current <= $last ) {

        $dates[] = date( $format, $current );
        $current = strtotime( $step, $current );
    }

    return $dates;
}

$range = dateRange($start, $end);
//print_r($range); exit;
?>

     <div class="my-3 my-md-5">
          <div class="container">
            
            <div class="row row-cards row-deck">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title'];?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">
                    <!--<li><a href="#" class="btn btn-default" data-toggle="modal" data-target="#resModal" id="add-new"><i class="fa fa-plus"></i> Add New</a>
                    </li>-->
                    <li><a href="javascript: void()" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/report/attendance')?>"><i class="fa fa-calendar"></i> <span><?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end))?></span> <b class="caret"></b></a>
                    </li>
                  </ul>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                  <tr>
                                      <th>Date</th>
                                      <th>Name</th>
                                      <th>Department</th>
                                      <th>Time In</th>
                                      <th>Time Out</th>
                                      <th>Total Hrs</th>
                                      <th>Overtime In</th>
                                      <th>Overtime Out</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($data as $d) { 
                                    // $e = get_data('tbl_employees', "JOIN tbl_departments ON dept_id = emp_deptid WHERE emp_id = ".$d['empcheckin_empid'], 'emp_id, emp_fullname, dept_name', true);
                                    //$a = get_data('tbl_empcheckin', "WHERE empcheckin_empid = ".$d['emp_id']." AND DATE(empcheckin_date) BETWEEN '$start' AND '$end'");
                                    foreach($range as $r) :
                                      $e = get_data('tbl_empcheckin', "WHERE empcheckin_empid = ".$d['emp_id']." AND DATE(empcheckin_date) = '$r'", 'tbl_empcheckin.*', true);
                                   ?>
                                    <tr>
                                      <td><a href="#" ><?php echo date('Y-m-d, l', strtotime($r));?></td>
                                      <td><?php echo $d['emp_fullname'];?></td>
                                      <td><?php echo $d['dept_name'];?></td>
                                      <td><?php echo isset($e['empcheckin_checkintime']) ? date('g:i a', strtotime($e['empcheckin_checkintime'])) : 'Not checked in';?></td>
                                      <td><?php echo isset($e['empcheckin_checkouttime']) && $e['empcheckin_checkouttime'] !== '00:00:00' ? date('g:i a', strtotime($e['empcheckin_checkouttime'])) : 'Not checked out';?></td>
                                      <td><?php echo isset($e['empcheckin_checkintime']) ? time_dif($e['empcheckin_checkintime'], $e['empcheckin_checkouttime']) : 0;?></td>
                                      <td><?php echo isset($e['empcheckin_otin']) && $e['empcheckin_otin'] !== '00:00:00' ? date('g:i a', strtotime($e['empcheckin_otin'])) : 'None';?></td>
                                      <td><?php echo isset($e['empcheckin_otout']) && $e['empcheckin_otout'] !== '00:00:00' ? date('g:i a', strtotime($e['empcheckin_otout'])) : 'None';?></td>
                                      
                                  </tr>
                                  <?php endforeach; } ?>
                                </tbody>
                              </table>
                            </div>
                                  
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>

<?php $this->load->view('layout/footer', array('dtt' => 1, 'dtp' => 1, 'sel' => 1)); ?>