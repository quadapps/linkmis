<?php 
$dets['page'] = 5; $dets['title'] = 'Attendance Sheet';
$this->load->view('layout/header', $dets); 

$start = isset($_GET['s']) ? $_GET['s'] : date('Y-m-01'); 
$end = isset($_GET['e']) ? $_GET['e'] : date('Y-m-d');


$user = get_data('tbl_users', "WHERE user_id = ".$this->session->userdata('user_id'), 'user_empid, user_levelid, user_id',  true);
$and = '';
if ($user['user_levelid'] == 4) {
  $and = " AND emp_id = ".$user['user_empid'];
} 
if ($user['user_levelid'] == 3) {
  $and = " AND emp_deptid = ".get_that_data('tbl_employees', 'emp_deptid', 'emp_id', $user['user_empid']);
}

$data = get_data('tbl_employees', "LEFT JOIN tbl_departments ON dept_id = emp_deptid WHERE emp_active = 1 $and", 'emp_fullname, emp_deptid, emp_id, emp_designation, dept_name');

function get_hrs_worked($start, $end, $emp)
{
  $data = get_data('tbl_empcheckin', "WHERE DATE(empcheckin_date) BETWEEN '$start' AND '$end' AND empcheckin_empid = $emp", 'empcheckin_checkintime, empcheckin_checkouttime');
  $amt = 0;
  foreach($data as $d) 
    $amt += time_dif($d['empcheckin_checkintime'], $d['empcheckin_checkouttime']);

  return $amt;
}

function get_checkin_status($emp_id, $start, $end)
{
  $early = get_data('tbl_empcheckin', "WHERE DATE(empcheckin_date) BETWEEN '$start' AND '$end' AND (IF (DAYOFWEEK(empcheckin_date) = 7, empcheckin_checkintime > '09:05:00', empcheckin_checkintime > '08:05:00')) AND empcheckin_empid = $emp_id", 'COUNT(empcheckin_id) AS early', true);
  $late = get_data('tbl_empcheckin', "WHERE DATE(empcheckin_date) BETWEEN '$start' AND '$end' AND (IF (DAYOFWEEK(empcheckin_date) = 7, empcheckin_checkintime < '09:05:00', empcheckin_checkintime < '08:05:00')) AND empcheckin_empid = $emp_id", 'COUNT(empcheckin_id) AS late', true);

  return [$early['early'], $late['late']];
}

?>

     <div class="my-3 my-md-5">
          <div class="container">
            
            <div class="row row-cards row-deck">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title'];?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">
                    <!--<li><a href="#" class="btn btn-default" data-toggle="modal" data-target="#resModal" id="add-new"><i class="fa fa-plus"></i> Add New</a>
                    </li>-->
                    <li><a href="javascript: void()" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/report/attendance_sheet')?>"><i class="fa fa-calendar"></i> <span><?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end))?></span> <b class="caret"></b></a>
                    </li>
                  </ul>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                              <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                  <tr>
                                      <th>NO</th>
                                      <th>Name</th>
                                      <th>Department</th>
                                      <th>Designation</th>
                                      <th>Days Late</th>
                                      <th>Days Early</th>
                                      <th>Total Days Worked</th>
                                      <th>Total Hrs Worked</th>
                                      <th>Total OT </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($data as $d) { $state = get_checkin_status($d['emp_id'], $start, $end);
                                   ?>
                                    <tr>
                                      <td><a href="<?php echo site_url('welcome/view/attendance?f='.$d['emp_id']);?>" >LPL<?php echo $d['emp_id'];?></td>
                                      <td><?php echo $d['emp_fullname'];?></td>
                                      <td><?php echo $d['dept_name'];?></td>
                                      <td><?php echo $d['emp_designation'];?></td>
                                      <td><?php echo $state[0];?></td>
                                      <td><?php echo $state[1];?></td>
                                      <td><?php echo count(get_data('tbl_empcheckin', "WHERE DATE(empcheckin_date) BETWEEN '$start' AND '$end' AND empcheckin_empid = ".$d['emp_id'], 'empcheckin_id'));;?></td>
                                      <td><?php echo get_hrs_worked($start, $end, $d['emp_id']);?></td>
                                      <td><?php echo count(get_data('tbl_empcheckin', "WHERE DATE(empcheckin_date) BETWEEN '$start' AND '$end' AND empcheckin_otin != '00:00:00' AND empcheckin_empid = ".$d['emp_id'], 'empcheckin_id'));?></td>
                                      
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                            </div>
                                  
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>

<?php $this->load->view('layout/footer', array('dtt' => 1, 'dtp' => 1, 'sel' => 1)); ?>