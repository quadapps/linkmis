<?php 
$dets['page'] = 5; $dets['title'] = 'Employees';
$this->load->view('layout/header', $dets); 
$user = get_row_data('tbl_users', 'user_id', $this->session->userdata('user_id'));
$emp = get_row_data('tbl_employees', 'emp_id', $user['user_empid']);
$id = $params;
if(!empty($id)) {
    $data = get_data('tbl_employees', "WHERE emp_active = 0 AND emp_deptid = $id");
} else {
    if($user['user_levelid'] == 4) 
    $data = get_data('tbl_employees', 'where emp_active = 0 AND emp_id = '.$user['user_empid']);
    
    elseif($user['user_levelid'] == 3) 
    $data = get_data('tbl_employees', 'WHERE emp_active = 0 AND emp_deptid = '.get_that_data('tbl_employees', 'emp_deptid', 'emp_id', $user['user_empid']));
    
    else
    $data = get_data('tbl_employees', 'where emp_active = 0');
    
}
?>


     <div class="app-content my-3 my-md-5">
          <div class="side-app">
            <div class="page-header">
              <h4 class="page-title">Employees</h4>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Employees</a></li>
                <li class="breadcrumb-item active" aria-current="page">View List</li>
              </ol>
            </div>
            <div class="row">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title">Seperated Employees
                    
                  </div>
                  <ul class="card-options panel_toolbox">
                    
                    
                  </ul>
                </div>
                <div class="card-body">
                                  <div class="table-responsive">
                 <table id="example" class="table table-hover">
                            <thead>
                                <th>Emp No</th>
                                <th>Full Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Designation</th>
                                <th>Department</th>
                                <th>Date</th>
                                <th>Reason</th>
                                <th>Remarks</th>
                            </thead>
                            <tbody>
                            <?php foreach($data as $e) { ?>
                                <tr>
                                    <td><a href="<?php echo site_url('welcome/view/emp_details/'.$e['emp_id'])?>"><?php echo $e['emp_id']?></a></td>
                                    <td><?php echo $e['emp_fullname']?></td>
                                    <td><?php echo $e['emp_cellphone']?></td>
                                    <td><?php echo $e['emp_email']?></td>
                                    <td><?php echo $e['emp_designation']?></td>
                                    <td><?php echo get_that_data('tbl_departments', 'dept_name', 'dept_id', $e['emp_deptid'])?></td>
                                    <td><?php echo $e['emp_terminatedon'] ?></td>
                                    <td><?php echo $e['emp_terminationreason'] ?></td>
                                    <td><?php echo $e['emp_terminationremarks'] ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                           </table>
                </div>
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>


<!-- Modal -->
<div class="modal fade" id="dept-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Add/Modify Department</h4>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/hr_model/create_dept')?>">
      <div class="modal-body">
        
          <div class="form-group">
            <label for="exampleInputEmail1">Department Name</label>
            <input type="text" name="dept" required="" class="form-control" id="exampleInputEmail1" />
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Department Head</label>
            <select name="hod" class="form-control">
                <option value="0">--Select HOD--</option>
                <?php foreach(get_data('tbl_employees', 'WHERE emp_active =1') as $e) { ?>
                <option value="<?php echo $e['emp_id']?>"><?php echo $e['emp_fullname']?></option>
                <?php } ?>
            </select>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" id="btn-dept" class="btn btn-theme">Save Department</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="term-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">End Employement</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/hr_model/terminate')?>" class="ajax-form">
      <div class="modal-body">
        <div class="form-group">
          <label>Form of Separation</label>
          <select name="status" class="form-control">
            <option>Resignation</option>
            <option>Termination of Contract</option>
            <option>Unsuccessful Probation</option>
            <option>End of Contract</option>
            <option>Death</option>
          </select>
        </div>
        <div class="form-group">
          <label>Date</label>
          <input type="text" name="date" class="form-control datepickers" required="">
        </div>
        <div class="form-group">
          <label>Remarks</label>
          <textarea name="remarks" class="form-control"></textarea>
        </div>
        <div class="form-group">
          <label class="custom-switch">
                            <input type="checkbox" name="cleared" class="custom-switch-input" value="1">
                            <span class="custom-switch-indicator"></span>
                            <span class="custom-switch-description">Employee has been cleared</span>
                          </label>
        </div>
        <input type="hidden" name="emp_id" id="emp-id">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
      <!--main content end-->

<script type="text/javascript">
  $('.terminate').click(function() {
    var id = $(this).data('id');
    $('#emp-id').val(id);
    $('#term-modal').modal('show');
  });
</script>
<?php $this->load->view('layout/footer', array('dtt' => 1, 'dtp' => 1)); ?>