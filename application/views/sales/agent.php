<?php 
$and = '';
$title = 'Field Sales - Agents';


$start = empty($_GET['s']) ? date('Y-m-d', strtotime('-1 month')) : $_GET['s']; 
$end = empty($_GET['e']) ? date('Y-m-d') : $_GET['e'];

if(isset($_GET['f'])) {
	$and .= " AND tbl_recons.created_by = '".$_GET['f']."'";
	$title = "Field Sales : $start - $end";
	#even more
}

$dets['page'] = 2; $dets['title'] = $title;
$this->load->view('layout/header', $dets); 
// if(!empty($_GET['s'])) {
// 	$arr = explode('_', $params);
// 	$start = reset($arr); $end = end($arr);
// }


$data = get_data('tbl_recons', "JOIN tbl_reconbranches ON recon_ec5_uuid = ec5_uuid WHERE DATE(tbl_recons.created_at) BETWEEN '$start' AND '$end' $and");

?>
     <div class="my-3 my-md-5">
          <div class="container-fluid">
            
            <div class="row row-cards row-deck">
              <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title"><?php echo $dets['title']; ?>
                    
                  </div>
                  <ul class="card-options panel_toolbox">
                    <li>
                      <a class="btn btn-secondary" id="reportrange" href="javascript: void()" data-url="<?php echo site_url('welcome/sales/agent')?>"><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end)) ;?></a>
                    </li>
                    <li>
                    	<button id="sync" class="btn btn-secondary"><i class="fa fa-refresh"></i> Sync</button>
                    </li>
                    <li>
                    	<button id="excel-export" class="btn btn-secondary" data-name="Agent_Daily_Report"><i class="fa fa-file-excel-o"></i> Excel</button>
                    </li>
                    <li>
                    	<button id="pdf-export" class="btn btn-secondary" data-name="Agent_Daily_Report"><i class="fa fa-file-pdf-o"></i> PDF</button>
                    </li>
                    
                  </ul>
                </div>
                <div class="card-body">
                	<div class="table-responsive">
						<table id="table" class="table card-table table-vcenter text-nowrap data-table-buttons table-bordered bst-buttons" data-name="Agent_Daily_Report" data-toggle="table"
  data-search="true"
  data-show-columns="true"
  data-sortable="true"
  data-show-export="true"
  data-export-data-type="all"
  data-click-to-select="true"
			 data-toolbar="#toolbar">
											<thead>
												<tr>
													<th class="w-1" rowspan="2">Date</th>
													<th rowspan="2">Agent Name</th>
													<th rowspan="2">Agent Code</th>
													<th colspan="2">Opening Cash</th>
													<th colspan="2">Opening Float</th>
													<th colspan="2">Closing Cash</th>
													<th colspan="2">Closing Float</th>
													<th rowspan="2">No of Transactions</th>
													<th rowspan="2">Start Time</th>
													<th rowspan="2">Close Time</th>
													<th rowspan="2">Hours Worked</th>
												</tr>
												<tr>
													<th>SSP</th>
													<th>USD</th>
													<th>SSP</th>
													<th>USD</th>
													<th>SSP</th>
													<th>USD</th>
													<th>SSP</th>
													<th>USD</th>
												</tr>
											</thead>
											<tbody>
											
											<?php foreach(get_data('tbl_agentreports', "ORDER BY created_at DESC") as $d) : ?>
												<tr>
													<td><?php echo date('Y-m-d', strtotime($d['created_at']));?></td>
													<td><?php echo get_that_data('tbl_employees', 'emp_fullname', 'emp_gmail', $d['created_by']);?></td>
													<td><?php echo $d['Agent_Code'];?></td>
													<td><?php echo $d['Opening_Cash__SSP'];?></td>
													<td><?php echo $d['Opening_Cash__USD'];?></td>
													<td><?php echo $d['Opening_Float__SSP'];?></td>
													<td><?php echo $d['Opening_Float__USD'];?></td>
													<td><?php echo $d['Closing_Cash_SSP'];?></td>
													<td><?php echo $d['Closing_Cash_USD'];?></td>
													<td><?php echo $d['Closing_Float_SSP'];?></td>
													<td><?php echo $d['Closing_Float_USD'];?></td>
													<td><?php echo $d['No_of_transaction'];?></td>
													<td><?php echo $d['created_at'];?></td>
													<td><?php echo $d['COD_Time'];?></td>
													<td><?php echo time_dif($d['created_at'], $d['COD_Time']);?></td>
												</tr>

											<?php endforeach; ?>
												
											</tbody>
										</table>
										</div>
                                  
                                </div>
                <!-- table-wrapper -->
              </div>
              <!-- section-wrapper -->

              </div>
            </div>
          </div>
<!-- Modal -->
<div class="modal fade" id="pay-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add/Modify Targets</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <form role="form" method="post" action="<?php echo site_url('system/act/marketing_model/create_target')?>" id="frm-main" class="ajax-form">
      <div class="modal-body">
      
      <div class="form-group">
        <label for="inputEmail3">Employee:</label>
            <select name="emp_id" class="form-control">
                <option value="0">--Select Employee--</option>
                    <?php foreach(get_data('tbl_employees', 'WHERE emp_active = 1 ORDER BY emp_fullname ASC') as $e) { ?>
                    <option value="<?php echo $e['emp_id']?>"><?php echo $e['emp_fullname']?></option>
                    <?php } ?>
            </select>
      </div>
      
        <div class="form-group">
            <label for="exampleInputEmail1">Month</label>
            <select name="month" class="form-control">
                <option value="0">--Select Month--</option>
                <?php for($i=1;$i<13;$i++) { ?>
                <option value="<?php echo date('F', mktime(0,0,0,$i)) ?>"><?php echo date('F', mktime(0,0,0,$i)) ?></option>
                <?php } ?>
            </select>
          </div>
         <div class="form-group">
            <label for="exampleInputEmail1">No of Transactions</label>
            <input type="number" name="qty" required="" class="form-control" id="" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">USD Amount</label>
            <input type="number" name="usd" required="" class="form-control" id="" />
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">SSP Amount</label>
            <input type="number" name="ssp" required="" class="form-control" id="" />
          </div>


      
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" id="btn-main" class="btn btn-primary">Save Target</button>
          </div>
      </form> 
    </div>
  </div>
</div>
</div>
      <!--main content end-->

<script type="text/javascript">
	$('#sync').click(function(){
		swal('Submitted');
		$.get('<?php echo site_url('epicollect/sync_agent_report')?>', function(r) {
			// if(r > 0)
				location.reload();
		}).fail(function() {
			swal("No new records found!");
		    location.reload();
		});
	})
</script>

<?php $this->load->view('layout/footer', array('bst' => 1, 'dtp' => 1)); ?>