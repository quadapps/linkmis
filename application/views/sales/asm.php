<?php
$dets = ['title' => 'Field Sales - RMs', 'page' => 2];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');

// $start = date('Y-m-d', strtotime('start of month'));
// $end = date('Y-m-d');

$start = !empty($_GET['s']) ? $_GET['s'] : date('Y-m-d', strtotime('start of month'));
$end = !empty($_GET['e']) ? $_GET['e'] : date('Y-m-d');

if(empty($_GET['f']))
	$data = get_data('tbl_rmactivities', "JOIN tbl_employees ON emp_gmail = rm_email WHERE DATE(rm_createdate) BETWEEN '$start' AND '$end' GROUP BY emp_id", 'COUNT(rm_id) AS visits, emp_fullname, emp_id');
else
	$data = get_data('tbl_rmactivities', "JOIN tbl_employees ON emp_gmail = rm_email WHERE DATE(rm_createdate) BETWEEN '$start' AND '$end' AND emp_id = ".$_GET['f'], 'tbl_rmactivities.*, emp_fullname');

?>

<div class="my-3 my-md-5">
					<div class="container-fluid">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="#" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/sales/asm');?>" ><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end));?></a>
											</li>
											<li>
												<a href="#" class="btn btn-secondary"  id="sync-tdr" ><i class="fa fa-refresh"></i> Sync</a>
											</li>
										</ul>
									</div>
									<div class="table-responsivenes card-body">
										<table id="example" class="table card-table  table-responsive">
											<?php if(!empty($_GET['f'])) { ?>
											<thead>
												<tr>
													<th>Date</th>
													<th>Employee</th>
													<th>Merchant Name</th>
													<th>Merchant Code</th>
													<th>Contact Person</th>
													<th>Current Challenges</th>
													<th>Possible Sollutions</th>
													<th>Next Follow Up</th>
													<th>Follow Up Comments</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : ?>
													<tr>
														<td><?php echo $d['rm_createdate'];?></td>
														<td><?php echo $d['emp_fullname'];?></td>
														<td><?php echo $d['rm_name'];?></td>
														<td><?php echo $d['rm_code'];?></td>
														<td><?php echo $d['rm_contact'];?></td>
														<td><?php echo $d['rm_challenges'];?></td>
														<td><?php echo $d['rm_strategy'];?></td>
														<td><?php echo $d['rm_followup'];?></td>
														<td><?php echo $d['rm_comments'];?></td>
														<td class="btn-group">
															
															
															<button data-id="<?php echo $d['rm_id'];?>" class="btn btn-primary btn-sm follow-up" title="Follow Up"><i class="fa fa-plus"></i></button>
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
											<?php } else { ?>
											<thead>
												<tr>
													<th>Employee</th>
													<th>No of Visits</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : ?>
													<tr>
														<td><a href="<?php echo site_url('welcome/sales/asm/?f='.$d['emp_id'])?>"><?php echo $d['emp_fullname'];?></a></td>
														<td><?php echo $d['visits'];?></td>
														<td class="btn-group">
															
															<a class="btn btn-primary btn-sm " href="<?php echo site_url('welcome/sales/asm/?f='.$d['emp_id'])?>" title="Refresh"><i class="fa fa-eye"></i></a>
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										<?php } ?>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Target</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/marketing_model/create_target');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Month</label>
	        	<input type="text" name="month" class="form-control" required="" value="<?php echo date('Y-m');?>">
	        </div>
	        <div class="form-group">
	        	<label>Employee</label>
	        	<select name="emp_id" class="form-control">
	        		<?php foreach(get_Data('tbl_employees', "WHERE emp_active = 1") as $e) { ?>
	        			<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php } ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>No of Transactions</label>
	        	<input type="number" name="qty" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>USD Value</label>
	        	<input type="number" name="usd" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>SSP Value</label>
	        	<input type="number" name="ssp" class="form-control" required="">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="achModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Follow Up</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/marketing_model/follow_up');?>" class="ajax-form">
      	<div class="modal-body">
	        <input type="hidden" name="rm_id" id="target-id">
	        
	        <div class="form-group">
	        	<label>Input follow up remarks</label>
	        	<textarea class="form-control" name="remarks" required=""></textarea>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('.follow-up').click(function() {
		let id = $(this).data('id');
		$('#target-id').val(id);
		$('#achModal').modal('show');
	});

	$('#sync-tdr').click(function() {
		swal('Submitted');
		$.get('<?php echo site_url('epicollect/sync_rm');?>', function(r) {
			swal(r);
			location.reload();
		});
	});
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>