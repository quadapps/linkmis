<?php
$dets = ['title' => 'Field Sales - Cluster Managers', 'page' => 2];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');

// $start = date('Y-m-d', strtotime('start of month'));
// $end = date('Y-m-d');

$start = !empty($_GET['s']) ? $_GET['s'] : date('Y-m-d', strtotime('-1 month'));
$end = !empty($_GET['e']) ? $_GET['e'] : date('Y-m-d');

if(isset($_GET['f']) && !empty($_GET['f'])) {
	$data = get_data('tbl_tdractivities', "JOIN tbl_employees ON emp_gmail = tdr_email WHERE DATE(tdr_sodtime) BETWEEN '$start' AND '$end' AND emp_id = ".$_GET['f'], 'tbl_tdractivities.*, emp_fullname, emp_id');
	$dets['title'] .= ' : '. $data[0]['emp_fullname'];
} else {
	$data = get_data('tbl_tdractivities', "JOIN tbl_employees ON emp_gmail = tdr_email WHERE DATE(tdr_sodtime) BETWEEN '$start' AND '$end' GROUP BY tdr_email", 'SUM(tdr_recs+tdr_acts+tdr_regs) AS target, SUM(tdr_recs) AS recs, SUM(tdr_regs) AS regs, SUM(tdr_acts) AS acts, SUM(tdr_recsach+tdr_actsach+tdr_regsach) AS achieved, SUM(tdr_recsach) AS recsach, SUM(tdr_actsach) AS actsach, SUM(tdr_regsach) AS regsach, COUNT(tdr_id) AS total, emp_fullname, emp_id');
}

$data1 = get_data('tbl_agentvisits', "JOIN tbl_employees ON emp_gmail = av_createdby WHERE DATE(av_createdat) BETWEEN '$start' AND '$end' GROUP BY av_createdby", 'COUNT(av_id) AS visits, emp_fullname, emp_id, av_createdby');

function distance($gmail, $start, $end) {
  $data = get_data('tbl_agentvisits', "WHERE av_createdby = '$gmail' AND DATE(av_createdat) BETWEEN '$start' AND '$end' ", 'av_location');
  
  $distance = 0;  
  foreach($data as $d) {
    if(next($data) !== false){
        $current = current($data);
        $next = next($data);
        
        $arr = explode(',', $current['av_location']);
        $lat1 = reset($arr); $lon1 = end($arr);
        $arr = explode(',', $next['av_location']);
        $lat2 = reset($arr); $lon2 = end($arr);
        $distance += calculate_distance((float)$lat1, (float)$lon1, (float)$lat2, (float)$lon2);
    }
  }
  
  return number_format($distance, 1);
}

function calculate_distance($lat1, $lon1, $lat2, $lon2) {
    $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  //$unit = strtoupper($unit);

  return ($miles * 1.609344); // KMs
    
}
?>

<div class="my-3 my-md-5">
					<div class="container-fluid">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="#" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/sales/cluster');?>" ><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end));?></a>
											</li>
											<li>
												<a href="<?php echo site_url('epicollect/sync_tdr');?>" class="btn btn-secondary"  id="sync-tdrx" ><i class="fa fa-refresh"></i> Sync TDR</a>
											</li>
											<li>
												<a href="<?php echo site_url('epicollect/sync_cm');?>" class="btn btn-secondary"  id="sync-tdry" ><i class="fa fa-refresh"></i> Sync CM</a>
											</li>
											<li>
						                    	<button id="excel-export" class="btn btn-secondary" data-name="CM_Daily_Commitment"><i class="fa fa-file-excel-o"></i> Excel</button>
						                    </li>
						                    <li>
						                    	<button id="pdf-export" class="btn btn-secondary" data-name="CM_Daily_Commitment"><i class="fa fa-file-pdf-o"></i> PDf</button>
						                    </li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<div class="panel panel-primary">
											<div class=" tab-menu-heading">
												<div class="tabs-menu1 ">
													<!-- Tabs -->
													<ul class="nav panel-tabs">
														<li class=""><a href="#tab5" class="active" data-toggle="tab">Daily Commitment</a></li>
														<li><a href="#tab6" data-toggle="tab">Agent Visits</a></li>
													</ul>
												</div>
											</div>
											<div class="panel-body tabs-menu-body">
												<div class="tab-content">
													<div class="tab-pane active " id="tab5">
														<div class="table-responsive">
															<?php if(!isset($_GET['f']) || empty($_GET['f'])) { ?>
											                  <table id="table" class="table card-table table-vcenter table-responsive table-bordered" data-name="CM_Daily_Commitment" data-toggle="table"
  data-search="true"
  data-show-columns="true"
  data-sortable="true"
  data-show-export="true"
  data-export-data-type="all"
  data-click-to-select="true"
			 data-toolbar="#toolbar">
																
																	<thead>
																		<tr>
																			<th rowspan="2"><STRONG>Employee</STRONG></th>
																			<th rowspan="2"><strong>Days Worked</strong></th>
																			<th colspan="3" class="text-center"><strong>Customer Registrations</strong></th>
																			<th colspan="3" class="text-center"><strong>Agent Activations</strong></th>
																			<th colspan="3" class="text-center"><strong>Agent Recruitments</strong></th>
																			<th rowspan="2"><strong>Overall Performance</strong></th>
																			<th rowspan="2"></th>
																		</tr>
																		<tr>
																			<th>Target</th>
																			<th>Achieved</th>
																			<th>Performance</th>
																			<th>Target</th>
																			<th>Achieved</th>
																			<th>Performance</th>
																			<th>Target</th>
																			<th>Achieved</th>
																			<th>Performance</th>
																		</tr>
																	</thead>
																	<tbody>
																		<?php foreach($data as $d) : ?>
																			<tr>
																				<td><?php echo $d['emp_fullname'];?></td>
																				<td><?php echo $d['total'];?></td>

																				<td><?php echo $d['regs'];?></td>
																				<td><?php echo $d['regsach'];?></td>
																				<td><?php echo number_format(($d['regsach']/$d['regs'])*100);?>%</td>

																				<td><?php echo $d['acts'];?></td>
																				<td><?php echo $d['actsach'];?></td>
																				<td><?php echo $d['acts'] == 0 ? $d['acts'] : number_format(($d['actsach']/$d['acts'])*100);?>%</td>

																				<td><?php echo $d['recs'];?></td>
																				<td><?php echo $d['recsach'];?></td>
																				<td><?php echo $d['recs']==0 ? $d['recs'] : number_format(($d['recsach']/$d['recs'])*100);?>%</td>
																				
																				<td><?php echo $d['target']==0 ? $d['target'] : number_format(($d['achieved']/$d['target'])*100);?>%</td>
																				<td><a href="<?php echo site_url('welcome/sales/cluster/?f='.$d['emp_id'])?>">View</a></td>
																			</tr>
																		<?php endforeach; ?>
																	</tbody>
																</table>
																	<?php } else { ?>
																		<table class="table table-bordered table-hover" id="example">
																	<thead>
																	<tr>
																		<th>Start Time</th>
																		<th>BAs Available</th>
																		<th>Recruitment Target</th>
																		<th>Activations Target</th>
																		<th>Registrations Target</th>
																		<th>End Time</th>
																		<th>BAs Active</th>
																		<th>Recruitment Achieved</th>
																		<th>Registrations Achieved</th>
																		<th>Activations Achieved</th>
																		<th>Agent Codes</th>
																		<th>Worked Hrs</th>
																		<th>Performance</th>
																		<th>Action</th>
																	</tr>
																</thead>
																<tbody>
																	
																	<?php foreach($data as $d) : ?>
																		<tr>
																			
																			<td><?php echo $d['tdr_sodtime'];?></td>
																			<td><?php echo $d['tdr_bas'];?></td>
																			<td><?php echo $d['tdr_recs'];?></td>
																			<td><?php echo $d['tdr_acts'];?></td>
																			<td><?php echo $d['tdr_regs'];?></td>
																			<td><?php echo $d['tdr_codtime'];?></td>
																			<td><?php echo $d['tdr_basach'];?></td>
																			<td><?php echo $d['tdr_recsach'];?></td>
																			<td><?php echo $d['tdr_regsach'];?></td>
																			<td><?php echo $d['tdr_actsach'];?></td>
																			<td><?php echo $d['tdr_activations'];?></td>
																			<td><?php echo time_dif($d['tdr_sodtime'], $d['tdr_codtime']);?></td>
																			<th><?php echo ($d['tdr_recsach']+$d['tdr_regsach']+$d['tdr_actsach'])/($d['tdr_recs']+$d['tdr_acts']+$d['tdr_regs'])*100;?>%</th>
																			<td class="btn-group">
																				
																				<button class="btn btn-primary btn-sm achieved" data-id="<?php echo $d['tdr_epi'];?>" title="Refresh"><i class="fa fa-refresh"></i></button>
																				<button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash-o"></i></button>
																			</td>
																		</tr>
																	<?php endforeach; ?>
																	
																</tbody>
															<?php } ?>
															</table>
											                </div>
													</div>
													<div class="tab-pane " id="tab6">
														<div class="table-responsive">
										                  <table id="d-tables" class="table table-hover data-table-buttons">
										                            <thead>
										                            	
										                                <th>Employee Name</th>
										                                <th>No of Visits</th>
                                                                        <th>Distance Covered</th>
										                                <th>Action</th>
										                            </thead>
										                            <tbody>
										                            	<?php foreach($data1 as $d) : ?>
										                            		<tr>
										                            			<td><?php echo $d['emp_fullname'];?></td>
										                            			<td><?php echo $d['visits'];?></td>
                                                                                <td><?php echo distance($d['av_createdby'], $start, $end);?> KMs</td>
										                            			<td><a href="<?php echo site_url('welcome/sales/agent_visits?f='.$d['emp_id'])?>">View</a></td>
										                            		</tr>
										                            	<?php endforeach; ?>
										                            </tbody>
										                           </table>
										                </div>
													</div>
													
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Target</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/marketing_model/create_target');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Month</label>
	        	<input type="text" name="month" class="form-control" required="" value="<?php echo date('Y-m');?>">
	        </div>
	        <div class="form-group">
	        	<label>Employee</label>
	        	<select name="emp_id" class="form-control">
	        		<?php foreach(get_Data('tbl_employees', "WHERE emp_active = 1") as $e) { ?>
	        			<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php } ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>No of Transactions</label>
	        	<input type="number" name="qty" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>USD Value</label>
	        	<input type="number" name="usd" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>SSP Value</label>
	        	<input type="number" name="ssp" class="form-control" required="">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="achModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Achieved Target</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/marketing_model/post_target');?>" class="ajax-form">
      	<div class="modal-body">
	        <input type="hidden" name="target_id" id="target-id">
	        <div class="form-group">
	        	<label>No of Transactions</label>
	        	<input type="number" name="qty" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>USD Value</label>
	        	<input type="number" name="usd" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>SSP Value</label>
	        	<input type="number" name="ssp" class="form-control" required="">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('.achieved').click(function() {
		let id = $(this).data('id');
		$('#target-id').val(id);
		$('#achModal').modal('show');
	});

	$('#sync-tdr').click(function() {
		swal('Submitted');
		$.get('<?php echo site_url('epicollect/sync_tdr');?>', function(r) {
			sync_visits();
		});
	});

	function sync_visits()
	{
		$.get('<?php echo site_url('epicollect/sync_agent_visits');?>', function(r) {
			sync_cm(r)
		});
	}

	function sync_cm(tdr) {
		$.get('<?php echo site_url('epicollect/sync_cm');?>', function(r) {
			var i = parseInt(tdr)+parseInt(r);
			swal(i+ ' records received');
			location.reload();
		});
	}
</script>

<?php
$this->load->view('layout/footer', ['bst' => 1, 'dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>