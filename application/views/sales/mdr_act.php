<?php
$dets = ['title' => 'Field Sales - MDRs', 'page' => 2];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');

// $start = date('Y-m-d', strtotime('start of month'));
// $end = date('Y-m-d');

$start = !empty($_GET['s']) ? $_GET['s'] : date('Y-m-d', strtotime('-1 month'));
$end = !empty($_GET['e']) ? $_GET['e'] : date('Y-m-d');

if(isset($_GET['f'])) {
	$data = get_data('tbl_mdrreport', "JOIN tbl_employees ON emp_gmail = mr_email WHERE DATE(mr_createdat) BETWEEN '$start' AND '$end' AND emp_id = ".$_GET['f'], 'tbl_mdrreport.*, emp_fullname, emp_id');
} else {
	$data = get_data('tbl_mdrreport', "JOIN tbl_employees ON emp_gmail = mr_email WHERE DATE(mr_createdat) BETWEEN '$start' AND '$end' GROUP BY mr_email", 'SUM(No_of_Recruitments+No_of_Activations) AS target, SUM(No_of_Recruitments) AS recs, SUM(No_of_Activations) AS acts, SUM(Achieved_Recruitment+Achieved_Activations) AS achieved, SUM(Achieved_Recruitment) AS recsach, SUM(Achieved_Activations) AS actsach, COUNT(mr_id) AS total, emp_fullname, emp_id');
}

$data1 = get_data('tbl_mdractivities', "JOIN tbl_employees ON emp_gmail = mdr_email  WHERE DATE(mdr_createdate) BETWEEN '$start' AND '$end' GROUP BY mdr_email", 'COUNT(mdr_id) AS visits, emp_fullname, emp_id');


?>

<div class="my-3 my-md-5">
					<div class="container-fluid">
																								
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="#" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/sales/cluster');?>" ><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end));?></a>
											</li>
											<li>
												<a href="#" class="btn btn-secondary"  id="sync-tdr" ><i class="fa fa-refresh"></i> Sync</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<div class="panel panel-primary">
											<div class=" tab-menu-heading">
												<div class="tabs-menu1 ">
													<!-- Tabs -->
													<ul class="nav panel-tabs">
														<li class=""><a href="#tab5" class="active" data-toggle="tab">Daily Commitment</a></li>
														<li><a href="#tab6" data-toggle="tab">Merchant Visits</a></li>
													</ul>
												</div>
											</div>
											<div class="panel-body tabs-menu-body">
												<div class="tab-content">
													<div class="tab-pane active " id="tab5">
														<div class="table-responsive">
											                  <table id="example" class="table card-table table-vcenter table-responsive table-bordered">
																<?php if(!isset($_GET['f'])) { ?>
																	<thead>
																		<tr>
																			<th rowspan="2"><STRONG>MDR Name</STRONG></th>
																			<th rowspan="2"><strong>Days Worked</strong></th>

																			<th colspan="3" class="text-center"><strong>Merchant Recruitments</strong></th>
																			
																			<th colspan="3" class="text-center"><strong>Merchant Activations</strong></th>
																			
																			<th rowspan="2"><strong>Overall Performance</strong></th>
																			<th rowspan="2"></th>
																		</tr>
																		<tr>
																			<th>Target</th>
																			<th>Achieved</th>
																			<th>Performance</th>
																			<th>Target</th>
																			<th>Achieved</th>
																			<th>Performance</th>
																		</tr>
																	</thead>
																	<tbody>
																		<?php foreach($data as $d) : ?>
																			<tr>
																				<td><?php echo $d['emp_fullname'];?></td>
																				<td><?php echo $d['total'];?></td>

																				<td><?php echo $d['recs'];?></td>
																				<td><?php echo $d['recsach'];?></td>
																				<td><?php echo $d['recs'] == 0 ? 0 : number_format(($d['recsach']/$d['recs'])*100);?>%</td>

																				<td><?php echo $d['acts'];?></td>
																				<td><?php echo $d['actsach'];?></td>
																				<td><?php echo $d['acts'] == 0 ? 0 : number_format(($d['actsach']/$d['acts'])*100);?>%</td>

																				<td><?php echo number_format(($d['achieved']/$d['target'])*100);?>%</td>
																				<td><a href="<?php echo site_url('welcome/sales/mdr_act/?f='.$d['emp_id'])?>">View</a></td>
																			</tr>
																		<?php endforeach; ?>
																	</tbody>
																	<?php } else { ?>
																	<thead>
																	<tr>
																		
																		<th>MDR Name</th>
																		<th>Start Time</th>
																		
																		<th>Recruitment Target</th>
																		<th>Activations Target</th>
																		
																		<th>End Time</th>
																		
																		<th>Recruitment Achieved</th>
																		
																		<th>Activations Achieved</th>
																		<th>Merchant Codes</th>

																		<th>Worked Hrs</th>
																		<th>Performance</th>
																		<th>Action</th>
																	</tr>
																</thead>
																<tbody>
																	
																	<?php foreach($data as $d) : ?>
																		<tr>
																			
																			<td><?php echo $d['emp_fullname'];?></td>
																			<td><?php echo $d['mr_createdat'];?></td>
																			
																			<td><?php echo $d['No_of_Recruitments'];?></td>
																			<td><?php echo $d['No_of_Activations'];?></td>
																			
																			<td><?php echo $d['COD_Time'];?></td>
																			
																			<td><?php echo $d['Achieved_Recruitment'];?></td>
																			<td><?php echo $d['Achieved_Activations'];?></td>

																			<td><?php echo $d['Merchant_Codes'];?></td>
																			
																			<td><?php echo empty($d['COD_Time']) ? 0 : time_dif($d['mr_createdat'], $d['COD_Time']);?></td>
																			<th><?php echo ($d['Achieved_Recruitment']+$d['Achieved_Activations'])/($d['No_of_Recruitments']+$d['No_of_Activations'])*100;?>%</th>
																			<td class="btn-group">
																				
																				
																			</td>
																		</tr>
																	<?php endforeach; ?>
																	
																</tbody>
															<?php } ?>
															</table>
											                </div>
													</div>
													<div class="tab-pane " id="tab6">
														<div class="table-responsive">
										                  <table id="d-tables" class="table table-hover data-table-buttons">
										                            <thead>
										                            	
										                                <th>Employee Name</th>
										                                <th>No of Visits</th>
										                                <th>Action</th>
										                            </thead>
										                            <tbody>
										                            	<?php foreach($data1 as $d) : ?>
										                            		<tr>
										                            			<td><?php echo $d['emp_fullname'];?></td>
										                            			<td><?php echo $d['visits'];?></td>
										                            			<td><a href="<?php echo site_url('welcome/sales/mdr?f='.$d['emp_id'])?>">View</a></td>
										                            		</tr>
										                            	<?php endforeach; ?>
										                            </tbody>
										                           </table>
										                </div>
													</div>
													
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Target</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/marketing_model/create_target');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Month</label>
	        	<input type="text" name="month" class="form-control" required="" value="<?php echo date('Y-m');?>">
	        </div>
	        <div class="form-group">
	        	<label>Employee</label>
	        	<select name="emp_id" class="form-control">
	        		<?php foreach(get_Data('tbl_employees', "WHERE emp_active = 1") as $e) { ?>
	        			<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php } ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>No of Transactions</label>
	        	<input type="number" name="qty" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>USD Value</label>
	        	<input type="number" name="usd" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>SSP Value</label>
	        	<input type="number" name="ssp" class="form-control" required="">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="achModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Achieved Target</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/marketing_model/post_target');?>" class="ajax-form">
      	<div class="modal-body">
	        <input type="hidden" name="target_id" id="target-id">
	        <div class="form-group">
	        	<label>No of Transactions</label>
	        	<input type="number" name="qty" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>USD Value</label>
	        	<input type="number" name="usd" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>SSP Value</label>
	        	<input type="number" name="ssp" class="form-control" required="">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('.achieved').click(function() {
		let id = $(this).data('id');
		$('#target-id').val(id);
		$('#achModal').modal('show');
	});

	$('#sync-tdr').click(function() {
		swal('Submitted');
		$.get('<?php echo site_url('epicollect/sync_mdr_report');?>', function(r) {
			sync_visits(r);
		});
	});

	function sync_visits()
	{
		$.get('<?php echo site_url('epicollect/sync_mdr');?>', function(r) {
			let total = parseInt(r) + parseInt(i);
			swal(total+ ' records received');
			location.reload();
		});
	}
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>