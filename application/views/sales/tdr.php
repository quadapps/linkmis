<?php
$dets = ['title' => 'Field Sales - TDRs', 'page' => 2];
$this->load->view('layout/header', $dets);
$uid = $this->session->userdata('user_id');

// $start = date('Y-m-d', strtotime('start of month'));
// $end = date('Y-m-d');

$start = !empty($_GET['s']) ? $_GET['s'] : date('Y-m-d', strtotime('start of month'));
$end = !empty($_GET['e']) ? $_GET['e'] : date('Y-m-d');

$data = get_data('tbl_tdractivities', "JOIN tbl_employees ON emp_gmail = tdr_email WHERE DATE(tdr_codtime) BETWEEN '$start' AND '$end'", 'tbl_tdractivities.*, emp_fullname');

?>

<div class="my-3 my-md-5">
					<div class="container-fluid">
						<!-- <div class="page-header">
							<h4 class="page-title">Dashboard</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
							</ol>
						</div> -->
																		
						<div class="row row-cards row-deck">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title"><?php echo $dets['title'];?></h3>
										<ul class="card-options panel-toolbox">
											<li>
												<a href="#" class="btn btn-secondary" id="reportrange" data-url="<?php echo site_url('welcome/sales/cluster');?>" ><i class="fa fa-calendar"></i> <?php echo date('F j, Y', strtotime($start)).' - '.date('F j, Y', strtotime($end));?></a>
											</li>
											<li>
												<a href="#" class="btn btn-secondary"  id="sync-tdr" ><i class="fa fa-refresh"></i> Sync</a>
											</li>
											<li>
												<a href="<?php echo site_url('welcome/sales/agent_visits');?>" class="btn btn-secondary"  ><i class="fa fa-refresh"></i> View Agent Visits</a>
											</li>
										</ul>
									</div>
									<div class="table-responsive card-body">
										<table id="example" class="table card-table table-vcenter table-responsive">
											<thead>
												<tr>
													<th>Date</th>
													<th>Employee</th>
													<th>Start Time</th>
													<th>BAs Available</th>
													<th>Recruitment Target</th>
													<th>Activations Target</th>
													<th>Registrations Target</th>
													<th>End Time</th>
													<th>BAs Active</th>
													<th>Recruitment Achieved</th>
													<th>Registrations Achieved</th>
													<th>Activations Achieved</th>
													<th>Agent Codes</th>
													<th>Worked Hrs</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
												<?php foreach($data as $d) : ?>
													<tr>
														<td><?php echo $d['tdr_updatedate'];?></td>
														<td><?php echo $d['emp_fullname'];?></td>
														<td><?php echo $d['tdr_sodtime'];?></td>
														<td><?php echo $d['tdr_bas'];?></td>
														<td><?php echo $d['tdr_recs'];?></td>
														<td><?php echo $d['tdr_acts'];?></td>
														<td><?php echo $d['tdr_regs'];?></td>
														<td><?php echo $d['tdr_codtime'];?></td>
														<td><?php echo $d['tdr_basach'];?></td>
														<td><?php echo $d['tdr_recsach'];?></td>
														<td><?php echo $d['tdr_regsach'];?></td>
														<td><?php echo $d['tdr_actsach'];?></td>
														<td><?php echo $d['tdr_activations'];?></td>
														<td><?php echo time_dif($d['tdr_sodtime'], $d['tdr_codtime']);?></td>
														<td class="btn-group">
															
															<button class="btn btn-primary btn-sm achieved" data-id="<?php echo $d['tdr_epi'];?>" title="Refresh"><i class="fa fa-refresh"></i></button>
															<button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash-o"></i></button>
														</td>
													</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="deptModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Target</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/marketing_model/create_target');?>" class="ajax-form">
      	<div class="modal-body">
	        <div class="form-group">
	        	<label>Month</label>
	        	<input type="text" name="month" class="form-control" required="" value="<?php echo date('Y-m');?>">
	        </div>
	        <div class="form-group">
	        	<label>Employee</label>
	        	<select name="emp_id" class="form-control">
	        		<?php foreach(get_Data('tbl_employees', "WHERE emp_active = 1") as $e) { ?>
	        			<option value="<?php echo $e['emp_id'];?>"><?php echo $e['emp_fullname'];?></option>
	        		<?php } ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<label>No of Transactions</label>
	        	<input type="number" name="qty" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>USD Value</label>
	        	<input type="number" name="usd" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>SSP Value</label>
	        	<input type="number" name="ssp" class="form-control" required="">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="achModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Achieved Target</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('system/act/marketing_model/post_target');?>" class="ajax-form">
      	<div class="modal-body">
	        <input type="hidden" name="target_id" id="target-id">
	        <div class="form-group">
	        	<label>No of Transactions</label>
	        	<input type="number" name="qty" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>USD Value</label>
	        	<input type="number" name="usd" class="form-control" required="">
	        </div>
	        <div class="form-group">
	        	<label>SSP Value</label>
	        	<input type="number" name="ssp" class="form-control" required="">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('.achieved').click(function() {
		let id = $(this).data('id');
		$('#target-id').val(id);
		$('#achModal').modal('show');
	});

	$('#sync-tdr').click(function() {
		swal('Submitted');
		$.get('<?php echo site_url('epicollect/sync_tdr');?>', function(r) {
			location.reload();
		});
	});
</script>

<?php
$this->load->view('layout/footer', ['dtt' => 1, 'sel' => 1, 'dtp' => 1]);
?>